# Mobilibus - Flutter App


## Project structure


### android and ios folders should not be deleted, they are base folder structure to install app on both platforms.


<details>
    <summary>Assets</summary>

    All assets related to all current projects

</details>


# Inside lib/ folder

<details>
    <summary>Accessibility</summary>

    All accessibility views and features

    Current working modules
        - Stops/Departures
        - Timetable
        - Tripplanner

</details>

<details>
    <summary>Addons</summary>

    Addons for Mobilibus

    - Recarga Agora (Manaus) - Complete Working (Dataprom + Gimave)
    - Transdata - Icomplete, missing AXIS module for payments
    - Twitter

</details>

<details>
    <summary>app</summary>

    All main Mobilibus features, on Urban and Charter projects

    urban/
        all views and features related to Urban projects

    charter/
        all views and features related to Charter projects


    select_city_*.dart (City/Project chooser)

</details>

<details>
    <summary>base</summary>

    All model base classes from converting json objects to Dart/Flutter objects
    For all Mobilibus, and Addons features

</details>

<details>
    <summary>beacon</summary>
    
    Beacon module for detecting beacons around. (No filtering beacons as it does not exist an API for accepted beacons)

</details>

<details>
    <summary>dev</summary>

    Developer views and features for testing purposes and checking API raw datas

</details>

<details>
    <summary>intro</summary>

    App Introduction views for Urban and Charter

</details>

<details>
    <summary>minigames</summary>

    Hobby features made from Mobilibus ecosystem and people

</details>

<details>
    <summary>traccar</summary>

    Traccar module for tracking Mobilibus devices in-app

</details>

<details>
    <summary>utils</summary>

    Utils features and API utils for interacting on whole Mobilibus projects.

    widgets/
        custom views for client/project especific projects

        Ex: Blumenau, Sao Jose, Sao Paulo
</details>


## Key changer file

    lib/utils/MOBApiUtils.dart

        Contains all API requests for main Mobilibus modules, and payload headers for analytics analysis

    lib/utils/TripplannerUtils.dart

        Converts most of the Trip raw data from API to valid Google Maps shapes, distances, markers and Mobilibus structure

    lib/utils/Utils.dart

        Contains all app, project by project and behaviour configurations.

## Mobilibus behaviour

    In lib/utils/Utils.dart you will find this properties

    This properties can be changed on the fly

        static User firebaseUser; //for firebase login and save data
        static BeaconService beaconService; //beacon singleton object for reading nearby devices
        static TraccarService traccarService; //traccar service for reading nearby traccar devices to match with beacon devices
        static final GoogleSignIn googleSignIn = GoogleSignIn(); //google signin feature
        static final FirebaseAuth auth = FirebaseAuth.instance; //firebase auth singletong to request firebaseUser
        static LocationData locationData; //periodically changes user location data streaming
        static List<MOBLine> lines = []; // global selected project holding on memory lines
        static MOBProject project; // global selected project
        static const String twitterUser = 'bus2app'; // twitter @/ID for loading twitter feed
        static bool isMobilibus = true; // if true shows IDS, otherwise don't
        static bool fullTimetable = false; // Depracated on Urban, used only on Charter
        static bool hasTwitter = true; // if app must show twitter option on Alerts and News
        static bool isCharter = false; // if true app is not an Urban project
        static CHARTER_TYPE charterType = CHARTER_TYPE.LOGIN; // charter login type
        static const bool isMultipleCities = true; // if app has more than you project to be working on
        static const bool isBus2 = true; // if app is working as Bus2 app
        static bool showAds = false; // show ads
        static bool requestChangeProject = true; // request dialog to change project on main map
        static int updateDelay = 20; // default update departure
        static const Duration timeoutDuration = Duration(seconds: 30); //global timeout duration for http requests
        static bool requestReview = false; // request app review
        static String iosAppStoreId = '1487690526'; //1487690526 : bus2




