import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/app/charter/builder/CharterRouteBuilder.dart';
import 'package:mobilibus/app/urban/more/information/full_information.dart';
import 'package:mobilibus/base/MOBCharterCompany.dart';
import 'package:mobilibus/base/MOBInformation.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class InformationCentral extends StatefulWidget {
  InformationCentral(this.routeId, this.tripIds);

  final int routeId;
  final List<int> tripIds;
  _InformationCentral createState() => _InformationCentral(
        routeId,
        tripIds,
      );
}

class _InformationCentral extends State<InformationCentral> {
  _InformationCentral(this.routeId, this.tripIds) : super();
  final List<int> tripIds;
  final int routeId;
  int mRouteId;

  Map<int, String> selectedTrip = {};
  List<Map<int, String>> trips = [];
  bool isLoading = true;
  var loadedNews = false;
  var loadedAlerts = false;
  List<MOBInformation> informations = [];
  List<Widget> widgets = [];
  TextEditingController tecFilterTrip = TextEditingController();
  int results = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) => AlertDialog(
                backgroundColor: Theme.of(context).primaryColor,
                title: Text('Notícias e Alertas',
                    style: TextStyle(
                        color: Utils.getColorByLuminanceTheme(context))),
                content: Text('Carregando, aguarde...',
                    style: TextStyle(
                        color: Utils.getColorByLuminanceTheme(context))),
              ));

      await getTrips();
      await getNews();
      await getAlerts();
    });
  }

  Future<void> getTrips() async {
    try {
      http.Response response =
          await MOBApiUtils.getTimetableFuture(Utils.project.projectId);

      if (response != null) {
        String json = convert.utf8.decode(response.bodyBytes);
        Iterable timetable = convert.json.decode(json);
        timetable.forEach((route) {
          int mmRouteId = route['routeId'];

          ///routeId nullability when project has no login,
          ///it means that it has single routeId

          if (routeId == null)
            mRouteId = mmRouteId;
          else
            mRouteId = routeId;
          if (mRouteId == mmRouteId) {
            List<dynamic> tripsIterable = route['timetable']['trips'].toList();
            if (tripIds.isNotEmpty)
              tripsIterable.removeWhere((trip) {
                int tripId = trip['tripId'];
                return !tripIds.contains(tripId);
              });
            tripsIterable.forEach((trip) {
              int tripId = trip['tripId'];
              String tripDesc = trip['tripDesc'];
              Map<int, String> map = {};
              map[tripId] = tripDesc;

              trips.add(map);
            });
            trips.sort((a, b) {
              String x = a.values.first;
              String y = b.values.first;
              return x.compareTo(y);
            });
            selectedTrip = trips.first;
          }
        });
      }
    } catch (e) {}
    isLoading = false;
    setState(() {});
  }

  Future<void> getNews() async {
    http.Response response =
        await MOBApiUtils.getNewsFuture(Utils.project.projectId);
    if (response != null) {
      String body = convert.utf8.decode(response.bodyBytes);
      Iterable list = convert.json.decode(body);
      List<MOBNews> news =
          list.map((model) => MOBNews.fromJson(model)).toList();
      informations.addAll(news);
      loadedNews = true;
      updateList();
    }
  }

  Future<void> getAlerts() async {
    http.Response response =
        await MOBApiUtils.getAlertsFuture(Utils.project.projectId);
    if (response != null) {
      String body = convert.utf8.decode(response.bodyBytes);
      Iterable list = convert.json.decode(body);
      List<MOBAlert> alerts =
          list.map((model) => MOBAlert.fromJson(model)).toList();
      informations.addAll(alerts);
      loadedAlerts = true;
      updateList();
    }
  }

  void updateList() {
    if (loadedNews && loadedAlerts) {
      informations.sort((a, b) => a.activeTo.compareTo(b.activeTo));
      Navigator.of(context).pop();
      widgets = [
        informations.length > 0
            ? ListView.builder(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemCount: informations.length,
                itemBuilder: (context, index) {
                  MOBInformation information = informations[index];

                  String title;
                  String content;
                  String effect;

                  if (information is MOBAlert) {
                    MOBAlert alert = information;

                    MOBAlertContent alertContent = alert.alertContent;
                    MOBAlertContentLanguage alertContentLanguage = alertContent
                        .alertContentLanguages
                        .firstWhere((a) => a.language == 'pt');

                    title = alertContentLanguage.title;
                    content = alertContentLanguage.content;
                  } else if (information is MOBNews) {
                    MOBNews news = information;

                    title = news.title;
                    content = news.content;
                  }

                  return ListTile(
                    contentPadding: EdgeInsets.all(16.0),
                    dense: true,
                    title: Container(
                      padding: EdgeInsets.only(bottom: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.only(right: 20),
                            child: information is MOBAlert
                                ? Text(
                                    '⚠️',
                                    style: TextStyle(fontSize: 40),
                                  )
                                : Icon(
                                    Icons.info_outline,
                                    color: Theme.of(context).primaryColor,
                                    size: 40,
                                  ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 132,
                            child: Text(title,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                    ),
                    subtitle: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        if (effect != null)
                          Container(
                            padding: EdgeInsets.only(bottom: 20),
                            child: Text(effect,
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor)),
                          ),
                        Text(
                          content,
                          style: TextStyle(color: Colors.black),
                          maxLines: 4,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.justify,
                        ),
                      ],
                    ),
                    onTap: () {
                      Widget w = FullInformation(information);
                      Navigator.push(
                          context, MaterialPageRoute(builder: (context) => w));
                    },
                  );
                },
              )
            : Column(
                children: <Widget>[
                  Container(
                    child: Card(
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Text(
                          'Nenhuma notícia ou alerta foi registrado.\nTente novamente mais tarde',
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                  Image.asset(
                    'assets/mobilibus/adrianavatar.png',
                    height: MediaQuery.of(context).size.height / 2,
                  ),
                ],
              )
      ];
      setState(() {});
    }
  }

  void goRoute() => CharterRouteBuilder(
        null,
        null,
        mRouteId,
        selectedTrip.keys.first,
        Theme.of(context).primaryColor,
        context,
        true,
      ).init();

  Widget notValidRouteIdWidget() => Container(
        padding: EdgeInsets.all(5),
        child: Card(
          child: Container(
            padding: EdgeInsets.all(10),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Fretamento'),
                        Text('routeId=$routeId'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Editor'),
                        Text('routeId=$mRouteId'),
                      ],
                    ),
                  ],
                ),
                Icon(
                  routeId == mRouteId ? Icons.done : Icons.clear,
                  color: routeId == mRouteId ? Colors.green : Colors.red,
                ),
              ],
            ),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(title: Text('Central do Cliente')),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                  strokeWidth: 3,
                  valueColor: AlwaysStoppedAnimation<Color>(
                      Utils.isLightTheme(context)
                          ? Utils.getColorFromPrimary(context)
                          : Colors.black)),
            )
          : Scrollbar(
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            if (Utils.isMobilibus &&
                                Utils.charterType != CHARTER_TYPE.NO_LOGIN &&
                                mRouteId != routeId)
                              notValidRouteIdWidget(),
                            Text('Rotas disponíveis',
                                style: TextStyle(fontSize: 20)),
                            TypeAheadField(
                              noItemsFoundBuilder: (context) => null,
                              errorBuilder: (context, object) => null,
                              textFieldConfiguration: TextFieldConfiguration(
                                controller: tecFilterTrip,
                                autofocus: false,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(
                                    left: 10.0,
                                    top: 5.0,
                                    bottom: 5.0,
                                  ),
                                  hintText:
                                      'Digite o código ou parte do nome da rota',
                                  hintStyle: TextStyle(fontSize: 12),
                                  fillColor: Theme.of(context).brightness ==
                                          Brightness.dark
                                      ? Colors.black
                                      : Colors.white,
                                  filled: true,
                                  enabled: true,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                ),
                              ),
                              onSuggestionSelected: (trip) {
                                selectedTrip = trip;
                                goRoute();
                              },
                              suggestionsCallback: (filter) async {
                                String f = filter.toLowerCase();
                                List<Map<int, String>> filterTrips = [];
                                if (f.isNotEmpty)
                                  for (final trip in trips) {
                                    int key = trip.keys.first;
                                    String keyStr =
                                        key.toString().toLowerCase();
                                    String desc =
                                        trip[key].toString().toLowerCase();
                                    if (desc.contains(f) || keyStr.contains(f))
                                      filterTrips.add(trip);
                                  }
                                else
                                  filterTrips = trips;

                                return Future<Iterable>.value(
                                    Iterable.castFrom(filterTrips));
                              },
                              itemBuilder: (context, trip) {
                                String desc = trip.values.first;
                                return Container(
                                  padding: EdgeInsets.all(10),
                                  child: Container(
                                    padding: EdgeInsets.all(10),
                                    child: Text(desc),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                      Divider(height: 3, thickness: 1),
                      if (informations.length > 0)
                        Text('Fique atento!', style: TextStyle(fontSize: 20)),
                      ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: widgets,
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
