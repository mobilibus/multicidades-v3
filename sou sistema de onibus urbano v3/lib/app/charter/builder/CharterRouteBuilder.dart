import 'dart:convert' as convert;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_utils/poly_utils.dart';
import 'package:google_maps_utils/spherical_utils.dart' as su;
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:mobilibus/app/charter/charter_route_viewer.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/base/MOBTripDetails.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/OSRM.dart';
import 'package:mobilibus/utils/Utils.dart';

class CharterRouteBuilder {
  CharterRouteBuilder(this.stop, this.myLocation, this.routeId, this.tripId,
      this.lineColor, this.context, this.showAllStops)
      : super();

  final MOBStop stop;
  final Point<double> myLocation;
  final int routeId;
  final int tripId;
  final Color lineColor;
  final BuildContext context;
  final bool showAllStops;

  Future<void> init() async {
    try {
      http.Response response = await MOBApiUtils.getTripDetailsFuture(tripId);
      if (response != null) {
        String body = convert.utf8.decode(response.bodyBytes);
        Map<String, dynamic> object = convert.json.decode(body);
        MOBTripDetails tripDetails = MOBTripDetails.fromJson(object);

        String shape = tripDetails.shape;
        List<PointLatLng> shapeDecoded = PolylinePoints().decodePolyline(shape);
        List<PointLatLng> shapeDecodedRounded = [];
        List<Marker> directionsMarker = [];
        List<Marker> directionsMarkerFar = [];

        for (final shape in shapeDecoded) {
          String strLat = shape.latitude.toStringAsFixed(6);
          String strLng = shape.longitude.toStringAsFixed(6);

          double lat = double.parse(strLat);
          double lng = double.parse(strLng);
          PointLatLng pointLatLng = PointLatLng(lat, lng);
          shapeDecodedRounded.add(pointLatLng);
        }
        shapeDecoded.clear();
        shapeDecoded = shapeDecodedRounded;

        int pointAreference = 0;
        int pointBreference = 10;

        String strAssetDirection = 'assets/images/direction_map.png';
        String strAssetDirectionInverted =
            'assets/images/direction_map_inverted.png';

        BitmapDescriptor iconDirection = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(size: Size(1, 1)), strAssetDirection);
        BitmapDescriptor iconDirectionInverted =
            await BitmapDescriptor.fromAssetImage(
                ImageConfiguration(size: Size(1, 1)),
                strAssetDirectionInverted);
        while (pointAreference < shapeDecodedRounded.length - 1 &&
            pointBreference < shapeDecodedRounded.length - 1) {
          bool inverted = false;
          PointLatLng first = shapeDecodedRounded[pointAreference];
          PointLatLng seconds = shapeDecodedRounded[pointBreference];

          Point firstLatLng = Point(first.latitude, first.longitude);
          Point secondLatLng = Point(seconds.latitude, seconds.longitude);

          double distance = su.SphericalUtils.computeDistanceBetween(
              firstLatLng, secondLatLng);
          if (distance >= 400) {
            MarkerId markerId =
                MarkerId('${firstLatLng.hashCode - secondLatLng.hashCode}');
            LatLng latLng = LatLng(first.latitude, first.longitude);
            double bearing =
                su.SphericalUtils.computeHeading(firstLatLng, secondLatLng);
            Marker marker = Marker(
              markerId: markerId,
              flat: true,
              draggable: false,
              consumeTapEvents: false,
              position: latLng,
              icon: inverted ? iconDirectionInverted : iconDirection,
              rotation: bearing,
            );
            directionsMarker.add(marker);

            if (distance >= 1000) directionsMarkerFar.add(marker);

            inverted = !inverted;
          }

          pointAreference += 10;
          pointBreference += 10;
        }

        List<MOBTripDetailsStop> stops = tripDetails.stops;
        List<MOBTripDetailsVehicle> vehicles = tripDetails.vehicles;

        //PointLatLng -> LatLng
        List<LatLng> latLngs = [];
        for (final shapeObject in shapeDecoded) {
          double lat = shapeObject.latitude;
          double lng = shapeObject.longitude;
          LatLng latLng = LatLng(lat, lng);
          latLngs.add(latLng);
        }

        //Create Polyline
        Polyline polyline = Polyline(
          polylineId: PolylineId(latLngs.hashCode.toString()),
          points: latLngs,
          width: 5,
          color: lineColor,
        );

        //Calculate bounds
        double x0, x1, y0, y1;
        for (final LatLng latLng in latLngs) {
          if (x0 == null) {
            x0 = x1 = latLng.latitude;
            y0 = y1 = latLng.longitude;
          } else {
            if (latLng.latitude > x1) x1 = latLng.latitude;
            if (latLng.latitude < x0) x0 = latLng.latitude;
            if (latLng.longitude > y1) y1 = latLng.longitude;
            if (latLng.longitude < y0) y0 = latLng.longitude;
          }
        }

        LatLng ne = LatLng(x1, y1);
        LatLng sw = LatLng(x0, y0);

        LatLngBounds bounds = LatLngBounds(northeast: ne, southwest: sw);

        //Calculate center from bounds
        double centerLatBound = (ne.latitude + sw.latitude) / 2;
        double centerLngBound = (ne.longitude + sw.longitude) / 2;

        LatLng center = LatLng(centerLatBound, centerLngBound);

        Polyline polylineOSRM;

        if (myLocation != null && stop != null) {
          Point<double> origin = myLocation ?? Point<double>(0.0, 0.0);
          if (Utils.locationData != null) {
            LocationData locationData = Utils.locationData;
            origin =
                Point<double>(locationData.latitude, locationData.longitude);
          }

          Point<double> destiny =
              Point<double>(stop?.latitude ?? 0.0, stop?.longitude ?? 0.0);

          List<LatLng> latLngsOSRM = [];

          http.Response response = await OSRMUtils.getOSRM(origin, destiny);
          if (response != null) {
            String json = convert.utf8.decode(response.bodyBytes);
            Map<String, dynamic> map = convert.json.decode(json);

            if (map.containsKey('code') && map['code'] == 'Ok') {
              OSRM osrm = OSRM.fromJson(map);

              List<OSRMRoute> routes = osrm.routes ?? [];

              List<Point<num>> pointsOSRM = [];

              routes.forEach((route) {
                String geometry = route.geometry;
                List<Point<num>> points = PolyUtils.decode(geometry);
                pointsOSRM.addAll(points);
              });
              pointsOSRM.forEach(
                  (point) => latLngsOSRM.add(LatLng(point.x, point.y)));
            }
          }

          polylineOSRM = Polyline(
            polylineId: PolylineId(latLngsOSRM.hashCode.toString()),
            points: latLngsOSRM,
            width: 5,
            color: Colors.pink,
          );
        }

        //Navigate to map
        CharterRouteViewer r = CharterRouteViewer(
          stop,
          routeId,
          tripId,
          tripDetails,
          polyline,
          polylineOSRM,
          stops,
          vehicles,
          directionsMarker,
          directionsMarkerFar,
          center,
          bounds,
          showAllStops,
        );
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => r));
      }
    } catch (e) {
      Navigator.of(context).pop();
    }
  }
}
