import 'dart:async';
import 'dart:convert' as convert;

import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/base/MOBDeparture.dart';
import 'package:mobilibus/app/urban/route/departures/widgets/DepartureItemLineWidget.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class CharterRouteDepartures extends StatefulWidget {
  CharterRouteDepartures(this.routeId, this.stopId, this.tripId);

  final int routeId;
  final int stopId;
  final int tripId;
  _CharterRouteDepartures createState() =>
      _CharterRouteDepartures(routeId, stopId, tripId);
}

class _CharterRouteDepartures extends State<CharterRouteDepartures>
    with WidgetsBindingObserver {
  _CharterRouteDepartures(this.routeId, this.stopId, this.tripId) : super();

  final int routeId;
  final int stopId;
  final int tripId;

  MOBDepartures departureResult;
  Timer timerUpdate;
  AppLifecycleState state;

  AdmobBanner admob = AdmobBanner(
    adUnitId: Utils.getBannerAdUnitId(),
    adSize: AdmobBannerSize.BANNER,
  );

  MOBDepartureTrip departureLine;
  MOBDepartureTrip departureLineNextDay;
  String stop = '';
  String headsign = '';
  String routeShortName = '';
  String routeLongName = '';

  @override
  void initState() {
    timerUpdate = Timer.periodic(
      Duration(seconds: Utils.updateDelay),
      (t) async => getDepartures(),
    );
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    didChangeAppLifecycleState(AppLifecycleState.resumed);
    getDepartures();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    if (timerUpdate != null) timerUpdate.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) =>
      setState(() => this.state = state);

  void getDepartures() async {
    if (state == AppLifecycleState.resumed) {
      http.Response response = await MOBApiUtils.getDepartures(stopId);
      String body = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> object = convert.json.decode(body);
      departureResult = MOBDepartures.fromJson(object, routeId, tripId);

      stop = departureResult.stop;

      for (final departure in departureResult.departuresToday)
        if (departure.routeId == routeId) {
          departureLine = departure;
          break;
        }
      for (final departure in departureResult.departuresNextDay)
        if (departure.routeId == routeId) {
          departureLineNextDay = departure;
          break;
        }
      if (departureLine != null) {
        headsign = departureLine.headsign;
        routeShortName = departureLine.shortName;
        routeLongName = departureLine.longName;
      }

      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Partidas', style: textStyle),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: themeColor),
          onPressed: () {
            if (timerUpdate != null) timerUpdate.cancel();
            Navigator.of(context).pop();
          },
        ),
        actions: <Widget>[
          if (Utils.isMobilibus)
            IconButton(
              icon: Icon(
                Icons.refresh,
                size: 35,
              ),
              onPressed: () => getDepartures(),
            )
        ],
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              if (Utils.isMobilibus) Text('stopId=$stopId'),
              if (departureLine == null && departureLineNextDay == null)
                CircularProgressIndicator(
                    strokeWidth: 3,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Utils.isLightTheme(context)
                            ? Utils.getColorFromPrimary(context)
                            : Colors.black)),
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    SvgPicture.asset(
                      'assets/images/pin_bus_stop_gmaps.svg',
                      width: 30,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 50,
                      child: Text(
                        stop,
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          color: Utils.isLightTheme(context)
                              ? Colors.black
                              : Utils.getColorFromPrimary(context),
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              if (departureLine != null)
                Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        color: Theme.of(context).primaryColor,
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          padding: EdgeInsets.all(5),
                          child: Text(
                            '$routeShortName $routeLongName',
                            style: TextStyle(
                              color: Theme.of(context).canvasColor,
                              backgroundColor: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Text('Destino: $headsign'),
                      ),
                    ],
                  ),
                ),
              ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  if (departureLine != null)
                    DepartureItemLineWidget(departureLine.departures),
                  if (departureLineNextDay != null)
                    DepartureItemLineWidget(departureLineNextDay.departures,
                        nextDay: true),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
