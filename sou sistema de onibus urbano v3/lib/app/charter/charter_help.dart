import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

class CharterHelp extends StatefulWidget {
  _CharterHelp createState() => _CharterHelp();
}

class _CharterHelp extends State<CharterHelp> with TickerProviderStateMixin {
  TabController tabController;
  String appName = '';
  String version = '';
  List<Widget> views = [];

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      version = packageInfo.version;
      appName = packageInfo.appName;
      tabController = TabController(length: 8, vsync: this);
      tabController.addListener(() => setState(() {}));
      views = [
        helpView(
          'assets/charter/charter_help1.png',
          'Bem-vindo\nao $appName',
          'Aqui você conhecerá todos os recursos que estão à sua disposição em nosso aplicativo',
          'Onde clicar, como navegar, e tornar sua experiência mais agradável, tudo isso você confere nas próximas páginas!',
        ),
        helpView(
          'assets/charter/charter_help2.png',
          'Login',
          'Caso sua empresa exija identificação, nessa tela você irá inserir suas credenciais. Selecione a empresa em que você está vinculado e a sua identificação - geralmente é o número da matrícula.',
          'Caso você ainda não possua acesso, consulte o RH de sua empresa!',
        ),
        helpView(
          'assets/charter/charter_help3.png',
          'Tela inicial',
          'Nosso app irá exibir a previsão com base no ponto de embarque mais próximo de sua localização.',
          'Caso seu veículo já esteja em deslocamento, você verá seu prefixo, e pode clicar na tela para visualizar no mapa onde ele se encontra.',
        ),
        helpView(
          'assets/charter/charter_help4.png',
          'Mapa',
          'Nele você visualiza sua rota, os pontos de embarque ao longo dela, e caso o veículo esteja em deslocamento, acompanha a sua localização em tempo real.',
          'Você acessa o mapa da sua rota clicando na tela inicial, ou pela Central do Cliente, selecionando a rota que desejar.',
        ),
        helpView(
          'assets/charter/charter_help5.png',
          'Menu',
          'Pela página inicial você acessa os demais recursos que seu aplicativo oferece, acessando o menu pelo círculo azul.',
          'Quando há notificação de um novo alerta ou notícia na Central do Cliente, um pequeno círculo vermelho indicará que esta ainda não foi lida.',
        ),
        helpView(
          'assets/charter/charter_help6.png',
          'Central do Cliente',
          'Na Central do Cliente você pode consultar as rotas e pontos de embarque oferecidos, e recebe informações e alertas por parte da operadora do transporte ou por sua empresa.',
          '',
        ),
        helpView(
          'assets/charter/charter_help7.png',
          'Entre em Contato',
          'Pelo nosso formulário você envia sua reclamação, sugestão ou encaminha suas dúvidas sobre nossos serviços.',
          'Coloque o maior número de informações possível - prefixo, rota, local, hora da ocorrência, etc - assim, os dados poderão ser tratados por nossa equipe',
        ),
        helpView2(
          'assets/charter/charter_help8.png',
          'Aplicativo desenvolvido pela',
        ),
      ];
      setState(() {});
    });
  }

  Widget helpView(String assetsImage, String title, String subtitle1,
          String subtitle2) =>
      Container(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              assetsImage,
              alignment: Alignment.center,
              height: MediaQuery.of(context).size.height / 2 - 2,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    title,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  child: Scrollbar(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(10),
                            child: Text(subtitle1),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            child: Text(subtitle2),
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      );

  Widget helpView2(String assetsImage, String title) => Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              assetsImage,
              alignment: Alignment.center,
              height: MediaQuery.of(context).size.height / 2,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    title,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Image.asset(
                    'assets/header/mobilibus.png',
                    width: MediaQuery.of(context).size.width / 3,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10),
                  child: RichText(
                    text: TextSpan(
                      style: TextStyle(color: Colors.black),
                      children: [
                        TextSpan(text: 'Confira nossos '),
                        TextSpan(
                          text: 'Termos de Uso\n',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: GoogleFonts.catamaran().fontFamily,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => launch(
                                'http://app.mobilibus.com/termos-de-uso.html'),
                        ),
                        TextSpan(text: 'e nossa '),
                        TextSpan(
                          text: 'Política de Privacidade ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: GoogleFonts.catamaran().fontFamily,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => launch(
                                'http://app.mobilibus.com/politica-de-privacidade.html'),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text('Versão: $version'),
                ),
              ],
            ),
          ],
        ),
      );

  void back() {
    tabController.animateTo(tabController.index - 1);
    setState(() {});
  }

  void next() {
    if (tabController.index == 7)
      Navigator.of(context).pop();
    else {
      tabController.animateTo(tabController.index + 1);
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height / 7 * 6,
              child: TabBarView(
                controller: tabController,
                children: views,
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('${tabController.index + 1}/8'),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      if (tabController.index != 0)
                        Card(
                          color: Theme.of(context).primaryColorDark,
                          child: IconButton(
                            onPressed: back,
                            icon: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      Card(
                        color: Theme.of(context).primaryColor,
                        child: IconButton(
                          onPressed: next,
                          icon: Icon(
                            tabController.index == 7
                                ? Icons.done
                                : Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
