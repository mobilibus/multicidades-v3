import 'dart:convert' as convert;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mobilibus/app/urban/tripplaner/trip_planner.dart';
import 'package:mobilibus/base/otp/OTPTrip.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavoritesTrips extends StatelessWidget {
  FavoritesTrips(this.tripsFavorited, this.scaffoldKey, this.context);
  final List<OTPTrip> tripsFavorited;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final BuildContext context;

  void delete(OTPTrip trip) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int projectId = Utils.project.projectId;
    String favTripsParam = 'trips_favorite_$projectId';

    tripsFavorited.remove(trip);
    String tripsString =
        convert.json.encode(List.of(tripsFavorited.map((e) => e.toMap())));
    prefs.setString(favTripsParam, tripsString);
    scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text('Viagem removida dos favoritos')));
    (context as Element)?.markNeedsBuild();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: tripsFavorited.length,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        OTPTrip trip = tripsFavorited[index];
        Point latLngFrom = Point(trip.fromLat, trip.fromLng);
        Point latLngTo = Point(trip.toLat, trip.toLng);
        return Container(
          padding: EdgeInsets.all(10),
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => TripPlanner(
                          trip.from,
                          trip.to,
                          latLngFrom,
                          latLngTo,
                        )));
              },
              child: Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.my_location),
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              width: MediaQuery.of(context).size.width - 125,
                              child: Text(trip.from),
                            )
                          ],
                        ),
                        Divider(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.directions_walk),
                            Container(
                              padding: EdgeInsets.only(left: 10),
                              width: MediaQuery.of(context).size.width - 125,
                              child: Text(trip.to),
                            )
                          ],
                        ),
                      ],
                    ),
                    IconButton(
                      onPressed: () => delete(trip),
                      icon: Icon(Icons.delete),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
