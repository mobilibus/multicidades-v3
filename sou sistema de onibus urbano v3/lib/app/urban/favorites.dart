import 'dart:async';
import 'dart:convert' as convert;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:mobilibus/app/urban/favorites/FavoritesCustomMarkers.dart';
import 'package:mobilibus/app/urban/favorites/FavoritesStops.dart';
import 'package:mobilibus/app/urban/favorites/FavoritesTimetable.dart';
import 'package:mobilibus/app/urban/favorites/FavoritesTrips.dart';
import 'package:mobilibus/app/urban/tripplaner/trip_planner.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/base/MOBMarker.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/base/otp/OTPTrip.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

Point _tripFrom = Point(0.0, 0.0);
Point _tripTo = Point(0.0, 0.0);
String _origin;
String _destiny;
BuildContext _context;

class Favorites extends StatefulWidget {
  @override
  _Favorites createState() => _Favorites();
}

class _Favorites extends State<Favorites> with TickerProviderStateMixin {
  final List<MOBLine> linesFavorited = [];
  final List<MOBStop> stopsFavorited = [];
  final List<OTPTrip> tripsFavorited = [];
  final List<MOBMarker> customMarkers = [];

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  List<Widget> tabs = [];
  TabController tabController;
  SharedPreferences sharedPreferences;

  bool downloadingTimetable = false;
  bool loading = true;

  @override
  void initState() {
    tabs = Utils.project.otpUri != null
        ? [
            Tab(text: 'Horários'),
            Tab(text: 'Pontos'),
            Tab(text: 'Viagens'),
          ]
        : [
            Tab(text: 'Horários'),
            Tab(text: 'Pontos'),
          ];

    tabController = TabController(length: tabs.length, vsync: this);
    super.initState();

    Future.delayed(Duration(), () async {
      sharedPreferences = await SharedPreferences.getInstance();
      for (final favTimetable in linesFavorited)
        Utils.selectedLineBehaviour(favTimetable, false, context, false);
      setState(() {});
    });
  }

  void initTimetable() {
    downloadingTimetable = true;
    loading = true;
    setState(() {});
    Future.delayed(Duration(), () async {
      await Utils.downloadTimetableList(Utils.project);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int projectId = Utils.project.projectId;
      String json = prefs.getString('timetable_$projectId') ?? '[]';
      Iterable iterable = convert.json.decode(json);
      if (Utils.lines.isEmpty)
        Utils.lines = iterable.map((e) => MOBLine.fromJson(e)).toList();

      setState(() => loading = false);

      downloadingTimetable = false;
    });
  }

  Future<void> setUpFavorites() async {
    linesFavorited.clear();
    stopsFavorited.clear();
    customMarkers.clear();
    tripsFavorited.clear();

    timetablePrefs();
    stopsPrefs();
    tripsPrefs();
  }

  void timetablePrefs() async {
    int projectId = Utils.project.projectId;

    String jsonObjectTimetable =
        sharedPreferences.getString('timetable_favorites_$projectId');

    Iterable favoritesTimetableIterable;
    List<dynamic> favoritesTimetable = [];
    if (jsonObjectTimetable != null) {
      favoritesTimetableIterable = convert.json.decode(jsonObjectTimetable);
      favoritesTimetable = favoritesTimetableIterable.toList();
    }

    for (final line in Utils.lines)
      if (favoritesTimetable.contains(line.routeId)) linesFavorited.add(line);
  }

  void stopsPrefs() async {
    int projectId = Utils.project.projectId;

    String favStopsParam = 'stops_favorite_$projectId';

    String jsonString = sharedPreferences.getString(favStopsParam) ?? '[]';
    Iterable iterable = convert.json.decode(jsonString) ?? [];
    stopsFavorited.addAll(iterable.map((e) => MOBStop.fromJson(e)).toList());

    jsonString = sharedPreferences.getString('custom_stops') ?? '[]';
    iterable = convert.json.decode(jsonString) ?? [];
    customMarkers.addAll(iterable.map((e) => MOBMarker.fromJson(e)).toList());
  }

  void tripsPrefs() async {
    int projectId = Utils.project.projectId;

    String favTripsParam = 'trips_favorite_$projectId';

    String jsonString = sharedPreferences.getString(favTripsParam) ?? '[]';
    Iterable iterable = convert.json.decode(jsonString) ?? [];
    tripsFavorited.addAll(iterable.map((e) => OTPTrip.fromJson(e)).toList());

    // super.initState();
    setState(() {});
  }

  Widget timetableTab() => SingleChildScrollView(
        child: Column(
          children: [
            FavoritesTimetable(linesFavorited, scaffoldKey, context),
          ],
        ),
      );
  Widget stopsTab() => SingleChildScrollView(
        child: Column(
          children: [
            FavoritesCustomMarkers(customMarkers, scaffoldKey, context),
            if (customMarkers.isNotEmpty && stopsFavorited.isNotEmpty)
              Divider(),
            FavoritesStops(stopsFavorited, scaffoldKey, context),
          ],
        ),
      );
  Widget tripsTab() => SingleChildScrollView(
        child: Column(
          children: [
            FavoritesTrips(tripsFavorited, scaffoldKey, context),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    if (Utils.lines.isEmpty && !downloadingTimetable) initTimetable();
    if (sharedPreferences != null) setUpFavorites();

    _context = context;
    bool hasTimetable = linesFavorited.length > 0;
    bool hasStops = stopsFavorited.length > 0 || customMarkers.length > 0;
    bool hasTrips = tripsFavorited.length > 0;

    Color themeColor = Utils.getColorByLuminanceTheme(context);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        title: TabBar(
          controller: tabController,
          labelColor: themeColor,
          indicatorWeight: 5,
          indicatorColor: Utils.globalAccentColor,
          tabs: tabs,
        ),
      ),
      body: Utils.lines
              .isEmpty //(!hasTimetable && !hasStops && !hasTrips)//Utils.lines.isEmpty
          ? //(setStateBool) ?
          Center(
              child: CircularProgressIndicator(
                  strokeWidth: 3,
                  valueColor: AlwaysStoppedAnimation<Color>(
                      Utils.isLightTheme(context)
                          ? Utils.getColorFromPrimary(context)
                          : Colors.black)),
            )
          : DefaultTabController(
              length: tabs.length,
              child: TabBarView(
                controller: tabController,
                children: tabs.length == 2
                    ? [
                        widgetFavorite(hasTimetable),
                        stopFavorite(stopsFavorited.length > 0 ||
                            customMarkers.length > 0)
                      ]
                    :
                    // TABS == 3
                    [
                        widgetFavorite(hasTimetable),
                        stopFavorite(stopsFavorited.length > 0 ||
                            customMarkers.length > 0),
                        travelFavorite(hasTrips)
                      ],
              ),
            ),
    );
  }

  Widget widgetFavorite(hasTimetable) {
    return hasTimetable
        ? Scrollbar(child: timetableTab())
        : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(50),
                child: Text(
                  'Nenhuma linha foi favoritada, vá para horários e selecione suas linhas mais utilizadas.',
                  textAlign: TextAlign.center,
                ),
              ),
              Image.asset(
                'assets/mobilibus/woman.png',
                height: (MediaQuery.of(context).size.height / 3),
              ),
            ],
          );
  }

  Widget stopFavorite(hasStops) {
    return hasStops
        ? Scrollbar(child: stopsTab())
        : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(50),
                child: Text(
                  'Nenhuma parada foi favoritada, vá para o mapa e selecione um ponto de ônibus mais utilizado.',
                  textAlign: TextAlign.center,
                ),
              ),
              Image.asset(
                'assets/mobilibus/woman.png',
                height: (MediaQuery.of(context).size.height / 3),
              ),
            ],
          );
  }

  Widget travelFavorite(hasTrips) {
    return Utils.project.otpUri != null && hasTrips
        ? Scrollbar(child: tripsTab())
        : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(50),
                child: Text(
                  'Nenhuma viagem favoritada, vá para o mapa e crie uma viagem de origem e destino que ela será automaticamente salva.',
                  textAlign: TextAlign.center,
                ),
              ),
              Image.asset(
                'assets/mobilibus/woman.png',
                height: (MediaQuery.of(context).size.height / 3),
              ),
            ],
          );
  }
}

void checkTrip() {
  if (_tripFrom != Point(0.0, 0.0) && _tripTo != Point(0.0, 0.0)) {
    Navigator.of(_context)
        .push(MaterialPageRoute(
            builder: (context) => TripPlanner(
                  _origin,
                  _destiny,
                  Point(_tripFrom.x, _tripFrom.y),
                  Point(_tripTo.x, _tripTo.y),
                )))
        .then((onValue) {
      _tripFrom = Point(0.0, 0.0);
      _tripTo = Point(0.0, 0.0);
    });
  }
}

void updateTripFrom(Point origin, String description) {
  _origin = description;
  _tripFrom = origin;
  checkTrip();
}

void updateTripTo(Point destiny, String description) {
  _destiny = description;
  _tripTo = destiny;
  checkTrip();
}
