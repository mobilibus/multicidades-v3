import 'dart:async';
import 'dart:convert' as convert;
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:mobilibus/app/urban/tripplaner/viewer/trip_planner_viewer_card_builder.dart';
import 'package:mobilibus/base/MOBTripDetails.dart';
import 'package:mobilibus/base/otp/OTPPlan.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'trip_planner_viewer_card_builder.dart';

class TripPlannerViewer extends StatefulWidget {
  TripPlannerViewer(this.itinerary, this.widgetTripDetails, this.showMap, this.startDTP, this.endDTP, this.durationDTP,
      {this.destiny, Key key});

  final OTPItinerary itinerary;
  final List<Widget> widgetTripDetails;
  final bool showMap;
  final String destiny;
  final String startDTP;
  final String endDTP;
  final String durationDTP;

  _TripPlannerViewer createState() =>
      _TripPlannerViewer(itinerary, widgetTripDetails, showMap, startDTP, endDTP, durationDTP,
          destiny: destiny);
}

class _TripPlannerViewer extends State<TripPlannerViewer> {
  _TripPlannerViewer(this.itinerary, this.widgetTripDetails, this.showMap, this.startDTP, this.endDTP, this.durationDTP,
      {this.destiny})
      : super();

  final OTPItinerary itinerary;
  final List<Widget> widgetTripDetails;
  final bool showMap;
  final String destiny;
  final String startDTP;
  final String endDTP;
  final String durationDTP;

  BitmapDescriptor iconBusStop;
  BitmapDescriptor iconSubwayStop;
  BitmapDescriptor iconTrainStop;
  BitmapDescriptor iconStart;
  BitmapDescriptor iconEnd;

  BitmapDescriptor iconBus0;
  BitmapDescriptor iconBus45;
  BitmapDescriptor iconBus90;
  BitmapDescriptor iconBus135;
  BitmapDescriptor iconBus180;
  BitmapDescriptor iconBus225;
  BitmapDescriptor iconBus270;
  BitmapDescriptor iconBus315;

  GoogleMapController controller;
  TripPlannerViewerCardBuilder tpvcb;
  List<Marker> stops = [];
  Set<Marker> markers = Set<Marker>();
  Marker startMarker;
  Marker endMarker;

  Map<String, Marker> vehiclesMarkers = {};
  List<Map<OTP_MODE, List<LatLng>>> stepsShapesLatLngs = [];
  List<Polyline> polylinesTripplaner = [];
  List<Polyline> polylinesTripDetails = [];
  LatLngBounds bounds;
  LatLng center = LatLng(0.0, 0.0);
  Timer timer;
  List<String> vehiclesId = [];
  Map<String, String> tripRouteIds = {};
  bool canFitBounds = true;
  int indexGB = 0;
  bool enabledInfoRT = false;
  String lastComunication = '';
  String vehicleId = '';
  MOBTripDetailsVehicle mtdVehicle;
  int routeIdGB = 0;
  int tripIdGB = 0;

  final PageController carouselController = PageController(initialPage: 0, viewportFraction: 0.9); //

  FlutterTts flutterTts = FlutterTts();

  @override
  void dispose() {
    if (showMap) timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    if (showMap)
      timer = Timer.periodic(Duration(seconds: 10), (t) => updateTripDetails());

    Future.delayed(Duration(), () {
      if (!showMap) {
        flutterTts.setLanguage('pt-BR');
        flutterTts.speak('Inicie seu percurso');
        Future.delayed(Duration(seconds: 2), () {
          flutterTts.speak('Destino: $destiny');
        });
      }
    });

    Future.delayed(Duration(seconds: 1), () async {
      Size size2_1 = Size(65, 72);
      Size size1_2 = Size(72, 65);
      Size size1_1 = Size(72, 72);

      int width = 100;

      iconBus0 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_0.svg', size2_1, width));
      iconBus180 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_180.svg', size2_1, width));

      iconBus270 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_270.svg', size1_2, width));
      iconBus90 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_90.svg', size1_2, width));

      iconBus135 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_135.svg', size1_1, width));
      iconBus45 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_45.svg', size1_1, width));
      iconBus225 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_225.svg', size1_1, width));
      iconBus315 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_315.svg', size1_1, width));

      String strAssetBusStop = 'assets/images/pin_bus_stop_gmaps.svg';
      String strAssetSubway = 'assets/images/pin_subway_stop_gmaps.svg';
      String strAssetTrain = 'assets/images/pin_train_stop_gmaps.svg';
      String strAssetFrom = 'assets/images/pin_from_gmaps.svg';
      String strAssetEnd = 'assets/images/pin_end_gmaps.svg';

      iconBusStop = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetBusStop, Size(64, 64), 64));
      iconSubwayStop = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetSubway, Size(64, 64), 64));
      iconTrainStop = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetTrain, Size(64, 64), 64));
      iconStart = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetFrom, Size(64, 64), 64));
      iconEnd = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetEnd, Size(64, 64), 64));

      buildStepsShapes();
      buildCenterAndBounds();
      updateTripDetails();
      buildShapeMap();

      Future.delayed(Duration(seconds: 2), () async {
        setState(() {});
      });
    });
  }

  void updateTripDetails() async {
    if (!tripRouteIds.containsKey(''))
      for (final tripId in tripRouteIds.keys) {
        int routeId = int.parse(tripRouteIds[tripId]);
        http.Response response =
            await MOBApiUtils.getTripDetailsFuture(int.parse(tripId));
        if (response != null) {
          String body = convert.utf8.decode(response.bodyBytes);
          Map<String, dynamic> object = convert.json.decode(body);
          MOBTripDetails tripDetails = MOBTripDetails.fromJson(object);

          String shape = tripDetails.shape;
          List<PointLatLng> shapeDecoded =
              PolylinePoints().decodePolyline(shape);

          List<LatLng> tripDetailsLatLngs = [];
          for (final point in shapeDecoded)
            tripDetailsLatLngs.add(LatLng(point.latitude, point.longitude));

          polylinesTripDetails.add(Polyline(
            polylineId: PolylineId(tripDetailsLatLngs.hashCode.toString()),
            points: tripDetailsLatLngs,
            width: 1,
            color: Colors.green,
          ));

          List<MOBTripDetailsVehicle> vehicles = tripDetails.vehicles;
          for (final vehicle in vehicles)
            if (vehiclesId.contains(vehicle.vehicleId))
              createVehicleMarkerAndContainer(
                  vehicle, routeId, int.parse(tripId));
        }
      }
    if (canFitBounds) {
      canFitBounds = false;
      buildCenterAndBounds();
      if (showMap) fitBounds(bounds);
    }
    setState(() {});
  }

  void updateInfoRT(MOBTripDetailsVehicle v)
  {
    if(enabledInfoRT){
      try{
        String date = v.positionTime.toString();
        DateTime dateTime = DateTime.parse(date);
        DateFormat dateFormat = DateFormat('dd/MM/yyyy HH:mm');;//DateFormat('yyyy-MM-dd HH:mm:ss');
        String format = dateFormat.format(dateTime);

        lastComunication = 'Posição atualizada em $format';
        vehicleId = v.vehicleId;
      }
      catch(e){
        print('erro');
      }
    }
  }

  void createVehicleMarkerAndContainer(
      MOBTripDetailsVehicle v, int routeId, int tripId) {

    mtdVehicle = v;
    routeIdGB = routeId;
    tripIdGB = tripId;

    double lat = v.latitude;
    double lng = v.longitude;
    LatLng latLng = LatLng(lat, lng);

    BitmapDescriptor iconBus;
    int heading = v.heading;
    if (heading >= 23 && heading <= 67)
      iconBus = iconBus45;
    else if (heading >= 68 && heading <= 112)
      iconBus = iconBus90;
    else if (heading >= 113 && heading <= 157)
      iconBus = iconBus135;
    else if (heading >= 158 && heading <= 202)
      iconBus = iconBus180;
    else if (heading >= 203 && heading <= 247)
      iconBus = iconBus225;
    else if (heading >= 248 && heading <= 292)
      iconBus = iconBus270;
    else if (heading >= 293 && heading <= 337)
      iconBus = iconBus315;
    else //0-22, 338-359
      iconBus = iconBus0;

    updateInfoRT(v);

    Marker m = Marker(
        markerId: MarkerId(v.hashCode.toString()),
        icon: iconBus,
        position: latLng,
        onTap: () {

          enabledInfoRT = !enabledInfoRT;
          setState(() {
            updateInfoRT(v);
          });

          controller.animateCamera(CameraUpdate.newLatLngZoom(latLng, 15));
          /*
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  backgroundColor: Theme.of(context).primaryColor,
                  title: Row(
                    children: <Widget>[
                      Icon(
                        Icons.directions_bus,
                        color: Utils.getColorByLuminanceTheme(context),
                        size: 30,
                      ),
                      Text(v.vehicleId.toString(),
                          style: TextStyle(
                            color: Utils.getColorByLuminanceTheme(context),
                          )),
                    ],
                  ),
                  content: Wrap(
                    children: <Widget>[
                      Text(lastComunication,
                          style: TextStyle(
                            color: Utils.getColorByLuminanceTheme(context),
                          ))
                    ],
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text('Compartilhar',
                          style: TextStyle(
                            color: Utils.getColorByLuminanceTheme(context),
                          )),
                      onPressed: () async {
                        String vehicleId = v.vehicleId;
                        String webHash = Utils.project.hash;
                        String hashRouteId = routeId.toRadixString(32);
                        String url =
                            'https://editor.mobilibus.com/web/bus2you/$webHash#$hashRouteId;$tripId;$vehicleId';
                        Share.share(url, subject: 'Compartilhar viagem');
                      },
                    ),
                    FlatButton(
                      child: Text('Fechar',
                          style: TextStyle(
                              color: Utils.getColorByLuminanceTheme(context))),
                      onPressed: () => Navigator.of(context).pop(),
                    ),
                  ],
                );
              });
          */
        });

    vehiclesMarkers[v.vehicleId] = m;
  }

  void fitBounds(LatLngBounds bounds) => controller != null
      ? controller.animateCamera(CameraUpdate.newLatLngBounds(bounds, 70))
      : {};

  Future<void> onMapCreated(mapController) async {
    tpvcb = TripPlannerViewerCardBuilder(context, mapController: mapController);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isDark = prefs.getBool('isDark') ?? false;
    controller = mapController;
    Completer<GoogleMapController>().complete(controller);
    rootBundle
        .loadString(isDark
            ? 'assets/maps_style_dark.json'
            : 'assets/maps_style_default.json')
        .then((style) => controller.setMapStyle(style));

    Future.delayed(Duration(seconds: 2), () {
      if (showMap) fitBounds(bounds);
    });
    setState(() {});
  }

  void fitBoundsStep(Map<OTP_MODE, List<LatLng>> stepShape) {
    if (stepShape.isNotEmpty) {
      List<LatLng> latLngs = stepShape.values.first;
      if (latLngs.length > 0) {
        LatLng firstLatLng = latLngs[0];
        double x0, x1, y0, y1;
        x0 = x1 = firstLatLng.latitude;
        y0 = y1 = firstLatLng.longitude;
        for (final latLng in latLngs) {
          if (latLng.latitude > x1) x1 = latLng.latitude;
          if (latLng.latitude < x0) x0 = latLng.latitude;
          if (latLng.longitude > y1) y1 = latLng.longitude;
          if (latLng.longitude < y0) y0 = latLng.longitude;
        }

        LatLng ne = LatLng(x1, y1);
        LatLng sw = LatLng(x0, y0);

        LatLngBounds bounds = LatLngBounds(northeast: ne, southwest: sw);

        fitBounds(bounds);
      }
    }
  }

  void buildStepsShapes() {
    List<OTPLeg> legs = itinerary.legs;
    LatLng lastPosition;
    for (final leg in legs)
      if (leg.vehicleId != null) {
        tripRouteIds[leg.appTripId] = leg.appRouteId;
        vehiclesId.add(leg.vehicleId);
      }

    for (final leg in legs) {
      LegGeometry legGeometry = leg.legGeometry;

      List<PointLatLng> shapeDecoded =
          PolylinePoints().decodePolyline(legGeometry.points);

      OTP_MODE mode = leg.mode;

      if (mode != OTP_MODE.WALK) {
        LatLng start = LatLng(leg.from.latitude, leg.from.longitude);
        LatLng end = LatLng(leg.to.latitude, leg.to.longitude);
        BitmapDescriptor vehicleIcon = iconBusStop;

        if (mode == OTP_MODE.RAIL)
          vehicleIcon = iconTrainStop;
        else if (mode == OTP_MODE.SUBWAY) vehicleIcon = iconSubwayStop;

        stops.add(stopMarker(start, vehicleIcon));
        stops.add(stopMarker(end, vehicleIcon));
      }

      List<LatLng> latLngs = [];
      for (final shapePoint in shapeDecoded) {
        LatLng latLng = LatLng(shapePoint.latitude, shapePoint.longitude);
        lastPosition = latLng;
        latLngs.add(latLng);
      }

      stepsShapesLatLngs.add({leg.mode: latLngs});
    }
    startMarker = Marker(
      markerId: MarkerId('startMarker'),
      position: LatLng(legs[0].from.latitude, legs[0].from.longitude),
      onTap: () => controller
          .animateCamera(CameraUpdate.newLatLngZoom(lastPosition, 18)),
      icon: iconStart,
    );
    endMarker = Marker(
      markerId: MarkerId(lastPosition.hashCode.toString()),
      position: lastPosition,
      onTap: () => controller
          .animateCamera(CameraUpdate.newLatLngZoom(lastPosition, 18)),
      icon: iconEnd,
    );
  }

  Marker stopMarker(LatLng latLng, BitmapDescriptor vehicleIcon) => Marker(
        markerId: MarkerId(latLng.hashCode.toString()),
        position: latLng,
        onTap: () =>
            controller.animateCamera(CameraUpdate.newLatLngZoom(latLng, 18)),
        icon: vehicleIcon,
      );

  void buildShapeMap() {
    for (final map in stepsShapesLatLngs) {
      String hash = map.hashCode.toString();
      PolylineId id = PolylineId(hash);
      for (final latLngs in map.values) {
        if (map.containsKey(OTP_MODE.WALK)) {
          polylinesTripplaner.add(Polyline(
            polylineId: id,
            points: latLngs,
            color: Colors.blue,
            width: 4,
          ));
        } else {
          polylinesTripplaner.add(Polyline(
            polylineId: id,
            points: latLngs,
            color: Colors.red,
            width: 7,
          ));
        }
      }
    }
  }

  void buildCenterAndBounds() {
    double x0, x1, y0, y1;
    for (final map in stepsShapesLatLngs)
      for (final stepShapeLatLngs in map.values)
        for (final LatLng latLng in stepShapeLatLngs) {
          if (x0 == null) {
            x0 = x1 = latLng.latitude;
            y0 = y1 = latLng.longitude;
          } else {
            if (latLng.latitude > x1) x1 = latLng.latitude;
            if (latLng.latitude < x0) x0 = latLng.latitude;
            if (latLng.longitude > y1) y1 = latLng.longitude;
            if (latLng.longitude < y0) y0 = latLng.longitude;
          }
        }

    for (final vM in vehiclesMarkers.values) {
      LatLng latLng = vM.position;
      if (x0 == null) {
        x0 = x1 = latLng.latitude;
        y0 = y1 = latLng.longitude;
      } else {
        if (latLng.latitude > x1) x1 = latLng.latitude;
        if (latLng.latitude < x0) x0 = latLng.latitude;
        if (latLng.longitude > y1) y1 = latLng.longitude;
        if (latLng.longitude < y0) y0 = latLng.longitude;
      }
    }

    LatLng ne = LatLng(x1, y1);
    LatLng sw = LatLng(x0, y0);

    bounds = LatLngBounds(northeast: ne, southwest: sw);

    //Calculate center from bounds
    double centerLatBound = (ne.latitude + sw.latitude) / 2;
    double centerLngBound = (ne.longitude + sw.longitude) / 2;

    center = LatLng(centerLatBound, centerLngBound);
  }

  @override
  Widget build(BuildContext context) {
    DateTime startTime =
        DateTime.fromMillisecondsSinceEpoch(itinerary.legs.first.startTime);

    DateTime endTime =
        DateTime.fromMillisecondsSinceEpoch(itinerary.legs.last.endTime);

    DateFormat dateFormat = DateFormat('HH:mm');

    String strStartTime = dateFormat.format(startTime).replaceAll(':', 'h');
    String strEndTime = dateFormat.format(endTime).replaceAll(':', 'h');

    markers = Set<Marker>.of(stops);
    if (startMarker != null) markers.add(startMarker);
    if (endMarker != null) markers.add(endMarker);
    markers.addAll(vehiclesMarkers.values);

    //OTPLeg leg = itinerary.legs.first;

    //LatLng start = LatLng(leg.from.latitude, leg.from.longitude);
    polylinesTripplaner.addAll(polylinesTripDetails);

    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);

    List<Widget> widgets = [];
    if (tpvcb != null) {
      for (final leg in itinerary.legs)
        tpvcb.buildItineraryLegWidget(leg, widgets, !showMap);
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        title: Text('Planejador de Viagem', style: textStyle),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: themeColor),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      floatingActionButton: bounds == null
          ? null
          : Container(
          padding: EdgeInsets.only(left: 30, bottom: 150),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FloatingActionButton(
                  mini: true,
                  child: Icon(Icons.navigate_before),
                  onPressed: () {
                    carouselController.previousPage(curve: Curves.decelerate, duration: Duration(milliseconds: 300));
                  }
              ),
              FloatingActionButton(
                mini: true,
                child: Icon(Icons.fullscreen_exit_sharp),
                onPressed: () => fitBounds(bounds),
              ),
              FloatingActionButton(
                  mini: true,
                  child: Icon(Icons.navigate_next),
                  onPressed: (){
                    carouselController.nextPage(curve: Curves.decelerate, duration: Duration(milliseconds: 300));
                  }
                //fitBounds(bounds),
              ),
            ],
          )
      ),
      body: showMap
          ?
      Column(
          children: [
            Container(
              color: Colors.white,
              height: 60,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(startDTP, style: TextStyle(fontSize: 20)),
                  Icon(Icons.chevron_right),
                  Text(endDTP, style: TextStyle(fontSize: 20)),
                  Container(
                    width: 60,
                  ),
                  Text(durationDTP, style: TextStyle(fontSize: 20)),
                ],
              ),
              ),
            if(enabledInfoRT )
            Divider(height: 2, thickness: 2,),

            if(enabledInfoRT )
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: [

                      Container(
                        padding: EdgeInsets.all(5),
                        child: SvgPicture.asset('assets/route/arrow_0.svg', height: 40,),
                      ),

                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 1.4,
                                child: Text(
                                  vehicleId,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 1.3,
                            child: Text(
                              lastComunication,
                              textAlign: TextAlign.start,
                            ),
                          ),
                        ],
                      ),

                      InkWell(
                        onTap: () async {
                          String vehicleId = mtdVehicle.vehicleId;
                          String webHash = Utils.project.hash;
                          String hashRouteId = routeIdGB.toRadixString(32);
                          String url = 'https://editor.mobilibus.com/web/bus2you/$webHash#$hashRouteId;$tripIdGB;$vehicleId';
                          Share.share(url, subject: 'Compartilhar viagem');
                        },
                        child: Icon(
                          Icons.share,
                          size: 30,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),

            Expanded(child: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                GoogleMap(
                  rotateGesturesEnabled: false,
                  trafficEnabled: false,
                  myLocationEnabled: true,
                  mapToolbarEnabled: false,
                  zoomGesturesEnabled: true,
                  myLocationButtonEnabled: false,
                  zoomControlsEnabled: false,
                  markers: markers,
                  mapType: MapType.normal,
                  onMapCreated: onMapCreated,
                  padding: EdgeInsets.only(
                      top: 20, bottom: 5),
                  polylines: Set<Polyline>.of(polylinesTripplaner),
                  initialCameraPosition: CameraPosition(
                    zoom: 15,
                    target: LatLng(center.latitude, center.longitude),
                  ),
                ),

                Container(
                  height: MediaQuery.of(context).size.height / 4,
                  child: PageView.builder(
                    controller: carouselController,
                    onPageChanged: (index) => tpvcb.goTo(index),
                    itemCount: widgets.length,
                    itemBuilder: (context, index) => InkWell(
                      onTap: () {}, //=> tpvcb.goTo(index),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          widgets[index]],
                      ),
                    ),
                  ),
                ),

              ],
            ))
          ], )
          : Column(
              children: <Widget>[
                Expanded(
                  child: Scrollbar(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: itinerary.legs.length,
                            itemBuilder: (context, index) => SizedBox(
                              child: TextButton(
                                  onPressed: null,
                                  child: Container(
                                    padding: EdgeInsets.all(5),
                                    child: widgets[index],
                                  ),
                                  //onPressed: () =>
                                    //  fitBoundsStep(stepsShapesLatLngs[index])
                                ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => controller
                                .animateCamera(CameraUpdate.newLatLngZoom(
                                    LatLng(
                                      itinerary.legs[itinerary.legs.length - 1].to.latitude,
                                      itinerary.legs[itinerary.legs.length - 1].to.longitude,
                                    ),
                                    18)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),

    );
  }
}