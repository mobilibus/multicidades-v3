import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/addons/twitter/twitter.dart';
import 'package:mobilibus/app/urban/more/information/full_information.dart';
import 'package:mobilibus/base/MOBInformation.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:mobilibus/utils/widgets/saopaulo/SPMetroWidget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Information extends StatefulWidget {
  @override
  _Information createState() => _Information();
}

class _Information extends State<Information> {
  var loadedNews = false;
  var loadedAlerts = false;

  List<MOBInformation> informations = [];

  List<Widget> widgets = [];

  void updateList() {
    if (loadedNews && loadedAlerts) {
      informations.sort((a, b) => a.activeTo.compareTo(b.activeTo));
      setState(() {});
    }
  }

  Future<void> getNews() async {
    http.Response response =
        await MOBApiUtils.getNewsFuture(Utils.project.projectId);
    if (response != null) {
      String body = utf8.decode(response.bodyBytes);
      Iterable list = json.decode(body);
      List<MOBNews> news =
          list.map((model) => MOBNews.fromJson(model)).toList();
      informations.addAll(news);
      loadedNews = true;
    }
    updateList();
  }

  Future<void> getAlerts() async {
    http.Response response =
        await MOBApiUtils.getAlertsFuture(Utils.project.projectId);
    if (response != null) {
      String body = utf8.decode(response.bodyBytes);
      Iterable list = json.decode(body);
      List<MOBAlert> alerts =
          list.map((model) => MOBAlert.fromJson(model)).toList();
      informations.addAll(alerts);
      loadedAlerts = true;
    }
    updateList();
  }

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance()
        .then((prefs) => prefs.setBool('redDot', false));
    Future.delayed(Duration(), () async {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) => AlertDialog(
                backgroundColor: Utils.getColorFromPrimary(context),
                title: Text('Notícias e Alertas',
                    style: TextStyle(
                        color: Utils.getColorByLuminanceTheme(context))),
                content: Text('Carregando, aguarde...',
                    style: TextStyle(
                        color: Utils.getColorByLuminanceTheme(context))),
              ));
      await getNews();
      await getAlerts();
      Navigator.of(context).pop();
    });
  }

  Widget informationsList() => ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: informations.length,
        itemBuilder: (context, index) {
          MOBInformation information = informations[index];

          String title;
          String effect;

          if (information is MOBAlert) {
            MOBAlert alert = information;

            MOBAlertContent alertContent = alert.alertContent;

            List<MOBAlertContentLanguage> alertContentLanguages =
                alertContent.alertContentLanguages;

            title =
                alertContentLanguages[0].title; // alertContentLanguage.title;
            effect = alert.effect;
          } else if (information is MOBNews) {
            MOBNews news = information;

            title = news.title;
          }

          return Container(
            margin: EdgeInsets.all(10),
            child: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: InkWell(
                onTap: () {
                  Widget w = FullInformation(information);
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => w));
                },
                child: ListTile(
                  dense: true,
                  title: Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(
                              information is MOBAlert
                                  ? Icons.warning
                                  : Icons.info,
                              size: 40,
                            ),
                            if (effect != null)
                              Container(
                                width: (MediaQuery.of(context).size.width / 4) *
                                        3 -
                                    20,
                                padding: EdgeInsets.only(bottom: 20, top: 20),
                                child: Text(effect,
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor)),
                              ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width - 60,
                              child:
                                  Text(title, style: TextStyle(fontSize: 18)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  subtitle: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Text('Veja +'),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      );

  @override
  Widget build(BuildContext context) {
    Widget spDiretoDoMetroButton = RaisedButton(
      padding: EdgeInsets.all(20),
      color: Utils.getColorFromPrimary(context),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text('Metrô e Trem Agora\nClique aqui',
              textAlign: TextAlign.center,
              style: TextStyle(color: Utils.getColorByLuminanceTheme(context))),
          Row(
            children: <Widget>[
              Icon(Icons.directions_subway,
                  size: 50, color: Utils.getColorByLuminanceTheme(context)),
              Icon(Icons.directions_railway,
                  size: 50, color: Utils.getColorByLuminanceTheme(context)),
            ],
          ),
        ],
      ),
      onPressed: () => Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => SPMetroWidget())),
    );

    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Utils.globalPrimaryColor,
        centerTitle: true,
        title: Text('Notícias e Alertas', style: textStyle),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: themeColor),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              informations.length > 0
                  ? informationsList()
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(50),
                          child: Text(
                            'Nenhuma notícia ou alerta foi registrado. Tente novamente mais tarde',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Image.asset(
                          'assets/mobilibus/newsalert.png',
                          height: (MediaQuery.of(context).size.height / 4),
                        ),
                      ],
                    ),
              if (false) //(Utils.hasTwitter)
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/twitter.png',
                            height: 50,
                            width: 50,
                          ),
                          Text(
                            '@${Utils.twitterUser}',
                            style: TextStyle(fontSize: 20),
                          ),
                        ],
                      ),
                    ),
                    TwitterViewer(),
                  ],
                ),

              //if (Utils.project.projectId == 531) spDiretoDoMetroButton,
            ],
          ),
        ),
      ),
    );
  }
}
