import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:mobilibus/app/urban/tripplaner/trip_planner.dart';
import 'package:mobilibus/base/MOBPos.dart';
import 'package:mobilibus/utils/DialogUtils.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

final ExactAssetImage assetPos = ExactAssetImage('assets/images/pin_pos.svg');

class FullPointOfSale extends StatefulWidget {
  FullPointOfSale(this.pos) : super();
  final MOBPos pos;

  _FullPointOfSale createState() => _FullPointOfSale(pos);
}

class _FullPointOfSale extends State<FullPointOfSale> {
  _FullPointOfSale(this.pos) : super();
  final MOBPos pos;

  GoogleMapController controller;
  BitmapDescriptor iconPos;
  double zoom = 0.0;
  Marker markerPos;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      String strAssetPos = 'assets/images/pin_pos_gmaps.svg';

      iconPos = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetPos, Size(64, 64), 64));

      markerPos = Marker(
        markerId: MarkerId(pos.hashCode.toString()),
        icon: iconPos,
        position: LatLng(pos.latitude, pos.longitude),
      );
      setState(() {});
    });
  }

  void onMapCreated(mapController) {
    controller = mapController;
    Completer<GoogleMapController>().complete(controller);
    rootBundle.loadString('assets/maps_style_default.json').then((style) {
      controller.setMapStyle(style);
    });
    controller.moveCamera(
        CameraUpdate.newLatLngZoom(LatLng(pos.latitude, pos.longitude), 15.0));
  }

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: themeColor),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Ponto de Venda', style: textStyle),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            myLocationEnabled: true,
            trafficEnabled: false,
            zoomGesturesEnabled: true,
            myLocationButtonEnabled: false,
            mapToolbarEnabled: false,
            minMaxZoomPreference: MinMaxZoomPreference(10, 20),
            mapType: MapType.normal,
            markers: markerPos != null ? Set<Marker>.of([markerPos]) : null,
            onMapCreated: onMapCreated,
            initialCameraPosition: CameraPosition(
              zoom: 15,
              target: LatLng(pos.latitude, pos.longitude),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                child: Container(
                  color: Theme.of(context).primaryColor,
                  child: Container(
                    padding: EdgeInsets.all(20),
                    child: Text(
                      pos.name,
                      style: TextStyle(
                          color: Utils.getColorByLuminanceTheme(context)),
                    ),
                  ),
                ),
              ),
              Container(
                child: RaisedButton(
                  color: Theme.of(context).primaryColor,
                  child: Text('Como chegar',
                      style: TextStyle(
                          color: Utils.getColorByLuminanceTheme(context))),
                  onPressed: () async {
                    try {
                      LocationData locationData = Utils.locationData;
                      double lat = locationData.latitude;
                      double lng = locationData.longitude;
                      String fromName =
                          await MOBApiUtils.getReverseGeocodingDescrition();
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => TripPlanner(
                                fromName,
                                pos.name,
                                Point(lat, lng),
                                Point(pos.latitude, pos.longitude),
                              )));
                    } on PlatformException catch (_) {
                      DialogUtils(context).dialogNeedGPS();
                    }
                  },
                ),
                padding: EdgeInsets.only(left: 20, right: 20),
              ),
            ],
          )
        ],
      ),
    );
  }
}
