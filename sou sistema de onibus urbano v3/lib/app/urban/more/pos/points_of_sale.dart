import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_utils/google_maps_utils.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:mobilibus/app/urban/more/pos/full_point_of_sale.dart';
import 'package:mobilibus/app/urban/tripplaner/trip_planner.dart';
import 'package:mobilibus/base/MOBPos.dart';
import 'package:mobilibus/utils/DialogUtils.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class POS extends StatefulWidget {
  @override
  _POS createState() => _POS();

  static fromJson(model) {}
}

class _POS extends State<POS> {
  List<MOBPos> pointsOfSale = [];
  GoogleMapController controller;
  BitmapDescriptor iconPos;
  Set<Marker> markersSet = Set<Marker>.of([]);
  Map<MOBPos, Marker> markerByPos = {};
  LatLngBounds bounds;
  LatLng center;
  TextEditingController tecFilterPos = TextEditingController();

  bool isSearch = false;
  Icon iconSearch = Icon(Icons.search);

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      String strAssetPos = 'assets/images/v3_pinPOS.svg';

      Uint8List bytes =
          await Utils.bytesFromSvgAsset(context, strAssetPos, Size(64, 64), 64);
      iconPos = BitmapDescriptor.fromBytes(bytes);

      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          Future.delayed(Duration(seconds: 5))
              .then((value) => Navigator.of(context).pop());
          return AlertDialog(
            backgroundColor: Theme.of(context).primaryColor,
            title: Text('Pontos de Venda',
                style: TextStyle(
                  color: Utils.getColorByLuminanceTheme(context),
                )),
            content: Text(
              'Carregando, aguarde...',
              style: TextStyle(
                color: Utils.getColorByLuminanceTheme(context),
              ),
            ),
          );
        },
      );

      await getPos();

      setState(() {});
    });
  }

  void onMapCreated(mapController) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isDark = prefs.getBool('isDark') ?? false;
    controller = mapController;
    await controller.moveCamera(CameraUpdate.newLatLngZoom(
        LatLng(Utils.project.latitude, Utils.project.longitude), 15.0));
    Completer<GoogleMapController>().complete(controller);
    await rootBundle
        .loadString(isDark
            ? 'assets/maps_style_dark.json'
            : 'assets/maps_style_default.json')
        .then((style) => controller.setMapStyle(style));
  }

  Future<void> getPos() async {
    http.Response response = await MOBApiUtils.getPOSFuture();
    if (response != null) {
      String body = utf8.decode(response.bodyBytes);
      Iterable iterable = json.decode(body);

      List<MOBPos> mPos = iterable.map((model) {
        MOBPos pos = MOBPos.fromJson(model);
        double distance = 0.0;

        if (Utils.locationData != null) {
          double lat = Utils.locationData.latitude;
          double lng = Utils.locationData.longitude;
          double mDistance = SphericalUtils.computeDistanceBetween(
              Point(pos.latitude, pos.longitude), Point(lat, lng));
          distance = mDistance;
        }
        return MOBPos.fromJson(model, distance);
      }).toList();

      pointsOfSale = mPos;
      pointsOfSale.sort((p1, p2) => p1.distance.compareTo(p2.distance));
      markersSet = pointsOfSale.map<Marker>((pos) {
        String name = pos.name.replaceAll('--', '\n');
        Marker m = Marker(
          markerId: MarkerId(pos.hashCode.toString()),
          icon: iconPos,
          position: LatLng(pos.latitude, pos.longitude),
          infoWindow: InfoWindow(title: name.split('\n').first),
        );
        markerByPos[pos] = m;
        return m;
      }).toSet();

      buildCenterAndBounds();

      Future.delayed(Duration(seconds: 1), () async {
        Marker marker = markersSet.first;
        await controller
            .animateCamera(CameraUpdate.newLatLngZoom(marker.position, 15));
        await controller.showMarkerInfoWindow(marker.markerId);
      });

      Navigator.of(context).pop();
    } else {
      return;
    }
  }

  void buildCenterAndBounds() {
    double x0, x1, y0, y1;
    for (final pos in pointsOfSale) {
      LatLng latLng = LatLng(pos.latitude, pos.longitude);
      if (x0 == null) {
        x0 = x1 = latLng.latitude;
        y0 = y1 = latLng.longitude;
      } else {
        if (latLng.latitude > x1) x1 = latLng.latitude;
        if (latLng.latitude < x0) x0 = latLng.latitude;
        if (latLng.longitude > y1) y1 = latLng.longitude;
        if (latLng.longitude < y0) y0 = latLng.longitude;
      }
    }

    LatLng ne = LatLng(x1, y1);
    LatLng sw = LatLng(x0, y0);

    bounds = LatLngBounds(northeast: ne, southwest: sw);

    //Calculate center from bounds
    double centerLatBound = (ne.latitude + sw.latitude) / 2;
    double centerLngBound = (ne.longitude + sw.longitude) / 2;

    center = LatLng(centerLatBound, centerLngBound);
  }

  void goToTripPlanner(MOBPos pos) async {
    try {
      LocationData locationData = Utils.locationData;
      double lat = locationData.latitude;
      double lng = locationData.longitude;

      String fromName = await MOBApiUtils.getReverseGeocodingDescrition();
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => TripPlanner(
                fromName,
                pos.name,
                Point(lat, lng),
                Point(pos.latitude, pos.longitude),
              )));
    } catch (e) {
      DialogUtils(context).dialogNeedGPS();
    }
  }

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);

    double height = (MediaQuery.of(context).size.height - kToolbarHeight) / 2;

    String filter = tecFilterPos.text.toLowerCase();
    List<MOBPos> pos = [];
    if (filter.isNotEmpty) {
      pos = List.of(pointsOfSale);
      pos.removeWhere((pos) {
        String name = pos.name.toLowerCase();
        return !name.contains(filter);
      });
    } else
      pos = pointsOfSale;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Utils.globalPrimaryColor,
        centerTitle: true,
        title: isSearch
            ? TextField(
                onChanged: (_) => setState(() {
                  if (pos.isNotEmpty) {
                    MOBPos p = pos.first;
                    MarkerId id = markerByPos[p].markerId;
                    controller.animateCamera(CameraUpdate.newLatLngZoom(
                        LatLng(p.latitude, p.longitude), 15.0));
                    controller.showMarkerInfoWindow(id);
                  }
                }),
                controller: tecFilterPos,
                decoration: InputDecoration(
                  hintText: 'Filtrar pontos de venda',
                ),
              )
            : Text('Pontos de Venda', style: textStyle),
        actions: <Widget>[
          IconButton(
            color: themeColor,
            icon: iconSearch,
            onPressed: () {
              if (this.iconSearch.icon == Icons.search) {
                this.iconSearch = Icon(Icons.close, color: themeColor);
                isSearch = true;
              } else {
                tecFilterPos.text = '';
                this.iconSearch = Icon(Icons.search, color: themeColor);
                isSearch = false;
              }
              setState(() {});
            },
          ),
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: themeColor),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Column(
        children: [
          Container(
            height: height - 50,
            child: GoogleMap(
                markers: markersSet,
                onMapCreated: onMapCreated,
                initialCameraPosition:
                    CameraPosition(target: LatLng(0.0, 0.0))),
          ),
          Container(
            height: height,
            child: pointsOfSale.isNotEmpty
                ? Scrollbar(
                    child: SingleChildScrollView(
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: pos.length,
                        itemBuilder: (context, index) {
                          MOBPos p = pos[index];
                          double d = p.distance;
                          String distance = '';
                          if (d > 1000)
                            distance = (d / 1000).toStringAsFixed(0) + ' km';
                          else
                            distance = d.toStringAsFixed(0) + ' metros';

                          String name = p.name.replaceAll('--', '\n');

                          return Container(
                            margin: EdgeInsets.all(10),
                            child: Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0))),
                              child: InkWell(
                                onLongPress: () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            FullPointOfSale(p))),
                                onTap: () {
                                  LatLng latLng =
                                      LatLng(p.latitude, p.longitude);
                                  controller.animateCamera(
                                      CameraUpdate.newLatLng(latLng));
                                  MarkerId markerId = markersSet
                                      .firstWhere(
                                          (marker) => marker.position == latLng)
                                      .markerId;
                                  controller.showMarkerInfoWindow(markerId);
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      width:
                                          (MediaQuery.of(context).size.width /
                                                  4) *
                                              3,
                                      child: ListTile(
                                        contentPadding: EdgeInsets.all(16.0),
                                        title: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              name,
                                              style: TextStyle(fontSize: 14),
                                            ),
                                            if (d != 0)
                                              Text(
                                                'Distância: $distance',
                                                style: TextStyle(fontSize: 12),
                                              ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    IconButton(
                                        icon: Icon(
                                          Icons.directions_run,
                                          size: 40,
                                        ),
                                        onPressed: () => goToTripPlanner(p)),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  )
                : Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(50),
                          child: Text(
                            'Nenhum ponto de venda foi registrado no momento. Tente novamente mais tarde',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
