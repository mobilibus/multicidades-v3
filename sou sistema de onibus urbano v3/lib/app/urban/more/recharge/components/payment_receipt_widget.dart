import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/data/transdata_payment_recharge_result.dart';

class PaymentReceiptWidget extends StatelessWidget {
  final TransdataPaymentRechargeResult paymentRechargeResult;

  const PaymentReceiptWidget({
    Key key,
    @required this.paymentRechargeResult,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final receiptTextStyle = GoogleFonts.sourceCodePro();

    return Column(
      children: [
        //Image.asset('assets/images/payment_receipt_top.png'),
        Row(
          children: [
            Container(color: Theme.of(context).primaryColor),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 8),
                color: Colors.white,
                child: Column(
                  children: [
                    Text('MOBICARD', style: receiptTextStyle),
                    Text('Rua Mal. Floriano Peixoto, 521', style: receiptTextStyle),
                    Text('CNPJ 30.382.970/0001-60', style: receiptTextStyle),
                    Text('RECIBO DE VENDA DE CRÉDITOS', style: receiptTextStyle),
                    SizedBox(height: 16),
                    Text(paymentRechargeResult.paymentHash, style: receiptTextStyle),
                    Text(
                      paymentRechargeResult.transactionDateTime,
                      style: receiptTextStyle,
                    ),
                    SizedBox(height: 16),
                    Text(
                      paymentRechargeResult.formattedPaymentAmount,
                      style: receiptTextStyle,
                    ),
                    SizedBox(height: 16),
                    Text(
                      'Valor da compra: ${paymentRechargeResult.formattedRechargeAmount}',
                      style: receiptTextStyle,
                    ),
                    SizedBox(height: 16),
                    Text(
                      'Tx. Conveniência: ${paymentRechargeResult.formattedFeeAmount}',
                      style: receiptTextStyle,
                    ),
                    SizedBox(height: 16),
                    Text(
                      'Cartão ${paymentRechargeResult.maskedPaymentCardNumber} - Aut. ${paymentRechargeResult.transactionId}',
                      style: receiptTextStyle,
                    ),
                    SizedBox(height: 16),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Seus créditos possuem validade de 1 ano após sua aquisição, conforme decreto municipal nº 16.747, Art. 12, Parágrafo 1º, de 13  de novembro de 2019.',
                        style: receiptTextStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(color: Theme.of(context).primaryColor),
          ],
        ),
        //Image.asset('assets/images/payment_receipt_bottom.png'),
      ],
    );
  }
}
