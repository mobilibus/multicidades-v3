import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/components/card_statement_register_widget.dart';
import 'package:mobilibus/transdata/data/model/transdata_extract.dart';

class DetailedStatementWidget extends StatelessWidget {
  final List<TransdataTransaction> transactions;

  const DetailedStatementWidget({
    Key key,
    @required this.transactions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextStyle tableHeaderStyle = GoogleFonts.nunito(
      fontSize: 12,
      fontWeight: FontWeight.w600,
      color: Color(0xff212121).withOpacity(0.6),
    );

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(flex: 2, child: Text('Data', style: tableHeaderStyle)),
              Expanded(flex: 2, child: Text('Detalhes', style: tableHeaderStyle)),
              Expanded(flex: 1, child: Text('Valor (R\$)', style: tableHeaderStyle)),
            ],
          ),
          ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: transactions.length,
            separatorBuilder: (_, __) => Divider(),
            itemBuilder: (_, i) => CardStatementRegisterWidget(transaction: transactions[i]),
          ),
        ],
      ),
    );
  }
}
