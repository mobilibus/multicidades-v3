import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TransportCardWidget extends StatelessWidget {
  final VoidCallback onTap;

  const TransportCardWidget({
    Key key,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final labelTextStyle = GoogleFonts.nunito(
      color: Color(0xff212121).withOpacity(0.6),
      fontSize: 14,
      fontWeight: FontWeight.w400,
    );
    final valueTextStyle = GoogleFonts.nunito(
      fontSize: 16,
      fontWeight: FontWeight.w400,
    );

    return GestureDetector(
      child: Container(
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Color(0x212121).withOpacity(0.15)),
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                spreadRadius: 2,
                blurRadius: 5,
                offset: Offset(0, 3),
              )
            ]),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 9,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('CPF', style: labelTextStyle),
                  Text('123.456.789-01', style: valueTextStyle),
                  SizedBox(height: 24),
                  Text('Cartão Transporte', style: labelTextStyle),
                  Text('1234567890', style: valueTextStyle),
                  SizedBox(height: 24),
                  Text('Nascimento', style: labelTextStyle),
                  Text('04/05/2014', style: valueTextStyle)
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: GestureDetector(
                  child: Icon(
                    Icons.delete,
                    color: Color(0xff212121).withOpacity(0.6),
                  ),
                  onTap: () {
                    // TODO Implementar chamada ao endpoint de exclusão de cartão
                  }),
            ),
          ],
        ),
      ),
      onTap: onTap,
    );
  }
}
