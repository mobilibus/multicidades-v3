import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_info_dialog.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_raised_button.dart';
import 'package:mobilibus/app/urban/more/recharge/components/payment_receipt_widget.dart';
import 'package:mobilibus/app/urban/more/recharge/components/widget_to_image_exportation.dart';
import 'package:mobilibus/app/urban/more/recharge/data/recharge_data_singleton.dart';
import 'package:mobilibus/app/urban/more/recharge/data/transdata_payment_recharge_result.dart';
import 'package:mobilibus/app/urban/more/recharge/transport_card_statement_page.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:permission_handler/permission_handler.dart';

class PaymentRechargeSuccessPage extends StatefulWidget {
  final TransdataPaymentRechargeResult paymentRechargeResult;

  const PaymentRechargeSuccessPage({
    Key key,
    @required this.paymentRechargeResult,
  }) : super(key: key);

  @override
  _PaymentRechargeSuccessPageState createState() => _PaymentRechargeSuccessPageState();
}

class _PaymentRechargeSuccessPageState extends State<PaymentRechargeSuccessPage> {
  GlobalKey receiptGlobalKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Utils.getColorFromPrimary(context),
          centerTitle: true,
          elevation: 0,
          title: Text(
            'Pagamento',
            style: GoogleFonts.nunito(fontSize: 20, fontWeight: FontWeight.w700),
          ),
        ),
        backgroundColor: Theme.of(context).primaryColor,
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            padding: const EdgeInsets.all(32),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset('assets/images/successful_result.png'),
                SizedBox(height: 32),
                Text(
                  'Recarga realizada\ncom sucesso!',
                  style: GoogleFonts.nunito(
                    fontSize: 24,
                    fontWeight: FontWeight.w700,
                    color: Theme.of(context).accentColor,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 32),
                WidgetToImageExportation(builder: (key) {
                  this.receiptGlobalKey = key;
                  return PaymentReceiptWidget(paymentRechargeResult: widget.paymentRechargeResult);
                }),
                FlatButton(
                  child: Text(
                    'Clique aqui para salvar',
                    style: GoogleFonts.nunito(
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                  onPressed: _exportReceipt,
                ),
                SizedBox(height: 32),
                CustomRaisedButton(label: 'Ver saldo', onPressed: _redirectToStatement),
              ],
            ),
          ),
        ),
      ),
      onWillPop: _redirectToStatement,
    );
  }

  Future<bool> _redirectToStatement() async {
    final singleton = RechargeDataSingleton.getInstance();
    // TODO Descomentar quando o backend passar a retornar os cartões de transporte salvos
    // Navigator.popUntil(context, ModalRoute.withName('/transport-card-selection'));

    // Navigator.popUntil(context, ModalRoute.withName('/new-transport-card'));
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (_) => TransportCardStatementPage(
          cpf: singleton.cpf,
          cardNumber: singleton.transportCardNumber,
        ),
      ),
      ModalRoute.withName('/new-transport-card'),
    );
    return true;
  }

  Future _exportReceipt() async {
    PermissionStatus permissionStatus = await Permission.storage.status;
    switch (permissionStatus) {
      case PermissionStatus.granted:
        await _saveReceiptImage();
        return;
      case PermissionStatus.permanentlyDenied:
      case PermissionStatus.restricted:
        _openPermanentlyDeniedDialog();
        return;
      default:
        permissionStatus = await Permission.storage.request();
    }

    if (permissionStatus == PermissionStatus.denied) {
      _openDeniedDialog();
    } else {
      _exportReceipt();
    }
  }

  void _openPermanentlyDeniedDialog() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (ctx) => CustomInfoDialog(
        title: 'Sem permissão',
        message:
        'Para que possa salvar o recibo, precisamos que libere o acesso ao armazenamento de arquivos nas configurações do aplicativo.',
        onAfterDismiss: () async {
          await openAppSettings();
        },
      ),
    );
  }

  Future _openDeniedDialog() {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (ctx) => CustomInfoDialog(
        title: 'Sem permissão',
        message:
        'Para que possa salvar o recibo, precisamos da permissão de acesso ao armazenamento de arquivos.',
      ),
    );
  }

  Future _saveReceiptImage() async {
    RenderRepaintBoundary boundary = this.receiptGlobalKey.currentContext.findRenderObject();
    final image = await boundary.toImage(pixelRatio: 1);
    final bytes = await image.toByteData(format: ImageByteFormat.png);
    final pngBytes = bytes.buffer.asUint8List();

    final result = await ImageGallerySaver.saveImage(
      pngBytes,
      name: 'ReciboRecarga_${widget.paymentRechargeResult.transactionDateTime}',
      quality: 100,
    );

    if (result['isSuccess'] == true) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) => CustomInfoDialog(
          title: 'Sucesso',
          message: 'Recibo salvo com sucesso e disponível na sua galeria de imagens.',
        ),
      );
    } else {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) => CustomInfoDialog(
          title: 'Erro',
          message:
          'Não foi possível salvar o recibo. Por favor verifique as permissões de acesso e tente novamente.',
        ),
      );
    }
  }
}
