import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_circular_progress_indicator.dart';
import 'package:mobilibus/app/urban/more/recharge/components/recharge_product_widget.dart';
import 'package:mobilibus/app/urban/more/recharge/controller/product_selection_controller.dart';
import 'package:mobilibus/app/urban/more/recharge/data/recharge_data_singleton.dart';
import 'package:mobilibus/app/urban/more/recharge/payment_amount_page.dart';
import 'package:mobilibus/main.dart';
import 'package:mobilibus/transdata/data/model/transdata_products.dart';
import 'package:mobilibus/utils/Utils.dart';

int countGB = 0;

class ProductSelectionPage extends StatefulWidget {
  @override
  _ProductSelectionPageState createState() => _ProductSelectionPageState();
}

class _ProductSelectionPageState extends State<ProductSelectionPage> {
  ProductSelectionController _controller;
  RechargeDataSingleton singleton = RechargeDataSingleton.getInstance();

  @override
  void initState() {
    super.initState();
    _controller = ProductSelectionController(getPurchaseRepositoryInstance());
    _controller.getAvailableProducts(singleton.cpf, singleton.transportCardNumber);

    setState(() {
      _controller.isLoading;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        centerTitle: true,
        elevation: 0,
        title: Text(
          'Selecionar produto',
          style: GoogleFonts.nunito(fontSize: 20, fontWeight: FontWeight.w700),
        ),
      ),
      backgroundColor: Theme.of(context).accentColor,
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(32),
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            SizedBox(height: 20),
            AnimatedBuilder(
              animation: _controller,
              builder: (_, __) {
                if(_controller.isLoading)
                {
                  return Column(
                    children: [
                      countGB > 0 ? Text('Aguarde... ($countGB de 3)', style: GoogleFonts.nunito(fontSize: 16, fontWeight: FontWeight.w400, height: 1.8),)
                          : Text('Aguarde...', style: GoogleFonts.nunito(fontSize: 16, fontWeight: FontWeight.w400, height: 1.8),),
                      Container(height: 20),
                      CustomCircularProgressIndicator()
                    ],
                  );
                }

                if (_controller.products.value.hasError) {
                  return _drawErrorMessage();
                }

                return Column(
                  children: [
                    Text(
                      'Selecione o produto que deseja adquirir:',
                      style: GoogleFonts.nunito(fontSize: 16, fontWeight: FontWeight.w400, height: 1.8),
                    ),
                    Container(height: 20,),
                    ListView.separated(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: _controller.products.value.data.length,
                      separatorBuilder: (_, __) => SizedBox(height: 16),
                      itemBuilder: (_, i) {
                        final item = _controller.products.value.data[i];
                        return RechargeProductWidget(
                          product: item,
                          onTap: () => _openPaymentAmount(item),
                        );
                      },
                    )
                  ],
                );
              },
            ),
            SizedBox(height: 32),
          ],
        ),
      ),
    );
  }

  Widget _drawErrorMessage() {
    return Container(
      color: Colors.white,
      child: Center(
        child: Text(
          'Erro ao buscar os produtos. Por favor tente novamente.',
          textAlign: TextAlign.center,
          style: GoogleFonts.nunito(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  void _openPaymentAmount(TransdataProduct item) {
    _controller.setChosenProduct(item);
    Navigator.push(context, MaterialPageRoute(builder: (_) => PaymentAmountPage()));
  }
}