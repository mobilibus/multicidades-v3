import 'package:mobilibus/utils/date_number_utils.dart';

class TransdataPaymentRechargeResult {
  RechargeResultStatus status;
  String paymentHash;
  String transactionDateTime;
  int rechargeAmount;
  int feeAmount;
  String maskedPaymentCardNumber;
  int transactionId;

  TransdataPaymentRechargeResult({
    this.status,
    this.paymentHash,
    this.transactionDateTime,
    this.rechargeAmount,
    this.feeAmount,
    this.maskedPaymentCardNumber,
    this.transactionId,
  });

  //String get formattedTransactionDateTime =>
      //DateMOBUtils.getShortDateAndTime(this.transactionDateTime.toString());

  String formattedTransactionDateTime()
  {
    String r = DateMOBUtils.getShortDateAndTime(this.transactionDateTime.toString());

    if(r != null)
    {
      return r;
    }
    else
    {
      return '';
    }
  }

  String get formattedPaymentAmount =>
      NumberUtils.formatAmountAsCurrency(this.rechargeAmount + this.feeAmount);

  String get formattedRechargeAmount => NumberUtils.formatAmountAsCurrency(this.rechargeAmount);

  String get formattedFeeAmount => NumberUtils.formatAmountAsCurrency(this.feeAmount);
}

enum RechargeResultStatus {
  ALL_SUCCESS,
  PAYMENT_ERROR,
  RECHARGE_ERROR,
  UNKNOWN_ERROR,
}
