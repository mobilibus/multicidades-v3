import 'package:flutter/material.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UrbanSettings extends StatefulWidget {
  _UrbanSettings createState() => _UrbanSettings();
}

class _UrbanSettings extends State<UrbanSettings> {
  SharedPreferences sharedPreferences;

  Map<String, String> settings = {
    'Modo Lite': 'Desativa o mapa principal, permancendo horários e favoritos.',
    'Avançado':
        'Fornece informações detalhadas. (Detalhes de linhas, horários, etc).'
  };

  Map<String, bool> settingsValues = {
    'Modo Lite': false,
    'Avançado': true,
  };
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      sharedPreferences = await SharedPreferences.getInstance();
      initSettings();
    });
  }

  void initSettings() async {
    settingsValues['Modo Lite'] =
        sharedPreferences.getBool('urban_settings_lite') ?? false;
    settingsValues['Avançado'] =
        sharedPreferences.getBool('urban_settings_advanced') ?? true;
    setState(() {});
  }

  void updateSettings() async {
    bool litemode = settingsValues['Modo Lite'];
    bool advanced = settingsValues['Avançado'];
    sharedPreferences.setBool('urban_settings_lite', litemode);
    sharedPreferences.setBool('urban_settings_advanced', advanced);

    Utils.liteMode = litemode;
    Utils.advanced = advanced;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    List<String> mSettings = settings.keys.toList();
    return Scaffold(
      appBar: AppBar(title: Text('Configurações')),
      body: ListView.builder(
        itemCount: mSettings.length,
        itemBuilder: (context, index) {
          String title = mSettings[index];
          String subtitle = settings[title];
          bool enabled = settingsValues[title];
          return InkWell(
            onTap: () {
              settingsValues[title] = !enabled;
              updateSettings();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width / 1.2,
                  child: ListTile(
                    title: Text(title),
                    subtitle: Text(subtitle),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 10,
                  child: Checkbox(
                    value: enabled,
                    tristate: false,
                    onChanged: (value) {
                      settingsValues[title] = value;
                      updateSettings();
                    },
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
