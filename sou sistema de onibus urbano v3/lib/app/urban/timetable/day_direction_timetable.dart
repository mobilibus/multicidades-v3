import 'package:flutter/material.dart';
import 'package:mobilibus/app/urban/route/urban_route.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/utils/Utils.dart';

class DayDirectionTimetable extends StatelessWidget {
  DayDirectionTimetable(
      this.departures, this.trips, this.routeId, this.lineColor, this.tripMap)
      : super();

  final List<MOBLineDeparture> departures;
  final List<MOBTrip> trips;
  final int routeId;
  final Color lineColor;
  final Map<int, Map<String, dynamic>> tripMap;

  Widget alertDialogRoute(
          BuildContext context, int tripId, String seqStr, String departure) =>
      AlertDialog(
        backgroundColor: Theme.of(context).primaryColor,
        title: Row(
          children: <Widget>[
            Icon(Icons.departure_board,
                color: Utils.getColorByLuminanceTheme(context)),
            Container(
                padding: EdgeInsets.only(left: 10),
                child: Text(
                  departure,
                  style:
                      TextStyle(color: Utils.getColorByLuminanceTheme(context)),
                )),
          ],
        ),
        content: Text(
          seqStr,
          style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ver no mapa',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context))),
            onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => UrbanRouteViewer(null, routeId, tripId))),
          ),
          FlatButton(
            child: Text('Cancelar',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context))),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();
    Duration duration = Duration(hours: now.hour, minutes: now.minute);
    int indexNextDeparture;
    for (indexNextDeparture = 0;
        indexNextDeparture < departures.length;
        indexNextDeparture++) {
      String dep = departures[indexNextDeparture].dep;
      List<String> split = dep.split(':');
      int h = int.parse(split[0]);
      int m = int.parse(split[1]);
      Duration d = Duration(hours: h, minutes: m);

      if (d.compareTo(duration) > 0) break;
    }
    return
      Container(
      child: GridView.count(
        padding: EdgeInsets.only(bottom: 50),
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 5,
        children: departures.map((d) {
          bool wa = d.wheelchairAccessible;
          int seq = d.seq;
          int tripId = tripMap[seq]['tripId'];
          String desc = tripMap[seq]['desc'];
          String seqStr = '';
          String dep = d.dep;
          String shortName = '';

          for(int count = 0; count < trips.length; count++)
          {

            if(trips[count].seq == seq)
            {
              if(trips[count].shortName != null)
              {
                shortName = trips[count].shortName;
                seqStr = '$seq: $shortName > $desc';
              }
              else {
                seqStr = '$seq: $desc';
              }

              break;
            }
          }

          List<String> split = dep.split(':');
          int hour = int.parse(split[0]);
          int minute = int.parse(split[1]);

          Color color =
              now.hour > hour || (now.hour == hour) && now.minute > minute
                  ? Colors.black
                  : Colors.black;

          bool isNextDeparture = departures.indexOf(d) == indexNextDeparture;

          Color cardColor = isNextDeparture ? Colors.yellow : Colors.white;

          TextStyle normal = TextStyle();
          TextStyle bolded =
              TextStyle(fontWeight: FontWeight.bold, color: color);

          TextStyle textStyle =
              now.hour > hour || (now.hour == hour) && now.minute > minute
                  ? normal
                  : bolded;

          return InkResponse(
            child: Card(
              elevation: 5,
              color: cardColor,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(
                            Icons.accessible_forward,
                            color: wa
                                ? Colors.black
                                : cardColor ==
                                        Colors
                                            .yellow //departures[index].wheelchairAccessible
                                    ? Colors.yellow
                                    : Colors.white,
                            size: 12,
                          ),
                          Text(
                            seq.toString(),
                            style: TextStyle(fontSize: 12),
                            textAlign: TextAlign.end,
                          ),
                        ],
                      ),
                    ),
                    Text(dep, style: textStyle),
                  ],
                ),
              ),
            ),
            onTap: () => showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) =>
                  alertDialogRoute(context, tripId, seqStr, d.dep),
            ),
          );
        }).toList(),
      ),);

  }
}
