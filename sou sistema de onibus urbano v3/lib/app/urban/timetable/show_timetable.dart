import 'dart:convert' as convert;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobilibus/app/urban/timetable/day_direction_timetable.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/utils/DialogUtils.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'widgets/SubtitleInfoWidget.dart';
import 'package:http/http.dart' as http;

class MOBTeste {
  int routeId;
  String shortName;
  String longName;
  String desc;
  int type;
  String color;
  String textColor;
  bool ac;
  int price;
  Timetable timetable;

  MOBTeste(
      {this.routeId,
      this.shortName,
      this.longName,
      this.desc,
      this.type,
      this.color,
      this.textColor,
      this.ac,
      this.price,
      this.timetable});

  MOBTeste.fromJson(Map<String, dynamic> json) {
    routeId = json['routeId'];
    shortName = json['shortName'];
    longName = json['longName'];
    desc = json['desc'];
    type = json['type'];
    color = json['color'];
    textColor = json['textColor'];
    ac = json['ac'];
    price = json['price'];
    timetable = json['timetable'] != null
        ? new Timetable.fromJson(json['timetable'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['routeId'] = this.routeId;
    data['shortName'] = this.shortName;
    data['longName'] = this.longName;
    data['desc'] = this.desc;
    data['type'] = this.type;
    data['color'] = this.color;
    data['textColor'] = this.textColor;
    data['ac'] = this.ac;
    data['price'] = this.price;
    if (this.timetable != null) {
      data['timetable'] = this.timetable.toJson();
    }
    return data;
  }
}

class Timetable {
  List<Directions> directions;
  List<Trips> trips;

  Timetable({this.directions, this.trips});

  Timetable.fromJson(Map<String, dynamic> json) {
    if (json['directions'] != null) {
      directions = new List<Directions>();
      json['directions'].forEach((v) {
        directions.add(new Directions.fromJson(v));
      });
    }
    if (json['trips'] != null) {
      trips = new List<Trips>();
      json['trips'].forEach((v) {
        trips.add(new Trips.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.directions != null) {
      data['directions'] = this.directions.map((v) => v.toJson()).toList();
    }
    if (this.trips != null) {
      data['trips'] = this.trips.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Directions {
  int directionId;
  String desc;
  List<Services> services;

  Directions({this.directionId, this.desc, this.services});

  Directions.fromJson(Map<String, dynamic> json) {
    directionId = json['directionId'];
    desc = json['desc'];
    if (json['services'] != null) {
      services = new List<Services>();
      json['services'].forEach((v) {
        services.add(new Services.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['directionId'] = this.directionId;
    data['desc'] = this.desc;
    if (this.services != null) {
      data['services'] = this.services.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Services {
  int serviceId;
  String desc;
  List<bool> days;
  List<Departures> departures;

  Services({this.serviceId, this.desc, this.days, this.departures});

  Services.fromJson(Map<String, dynamic> json) {
    serviceId = json['serviceId'];
    desc = json['desc'];
    days = json['days'].cast<bool>();
    if (json['departures'] != null) {
      departures = new List<Departures>();
      json['departures'].forEach((v) {
        departures.add(new Departures.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['serviceId'] = this.serviceId;
    data['desc'] = this.desc;
    data['days'] = this.days;
    if (this.departures != null) {
      data['departures'] = this.departures.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Departures {
  String dep;
  String arr;
  int wa;
  int seq;

  Departures({this.dep, this.arr, this.wa, this.seq});

  Departures.fromJson(Map<String, dynamic> json) {
    dep = json['dep'];
    arr = json['arr'];
    wa = json['wa'];
    seq = json['seq'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dep'] = this.dep;
    data['arr'] = this.arr;
    data['wa'] = this.wa;
    data['seq'] = this.seq;
    return data;
  }
}

class Trips {
  int tripId;
  String tripDesc;
  Null shortName;
  int directionId;
  int seq;

  Trips(
      {this.tripId, this.tripDesc, this.shortName, this.directionId, this.seq});

  Trips.fromJson(Map<String, dynamic> json) {
    tripId = json['tripId'];
    tripDesc = json['tripDesc'];
    shortName = json['shortName'];
    directionId = json['directionId'];
    seq = json['seq'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tripId'] = this.tripId;
    data['tripDesc'] = this.tripDesc;
    data['shortName'] = this.shortName;
    data['directionId'] = this.directionId;
    data['seq'] = this.seq;
    return data;
  }
}

class ShowTimetable2 extends StatefulWidget {
  ShowTimetable2(this.line) : super();

  final MOBLine line;

  _ShowTimetable2 createState() => _ShowTimetable2(line);
}

class _ShowTimetable2 extends State<ShowTimetable2>
    with TickerProviderStateMixin {
  _ShowTimetable2(this.line) : super();

  final MOBLine line;

  MOBTimetable timetable;
  bool isFavorited = false;
  MOBDirection selectedDirection;
  Map<int, Map<String, dynamic>> tripMap = {};
  Map<int, bool> tripFilter = {};
  List<int> tripsSeqs = [];
  ScrollController scrollController = ScrollController();
  ExpandableController expandableController =
      ExpandableController(initialExpanded: false);
  final GlobalKey<ScaffoldState> scaffoldKeyMain = GlobalKey<ScaffoldState>();
  TabController tabController;
  bool ac = false;
  List<MOBTeste> mobTeste;

  Future<List<MOBTeste>> getAC() async {
    try {
      List<MOBTeste> list = List();

      int routeId = line.routeId;
      int projectId = Utils.project.projectId;

      final response = await http.get(
          'https://mobilibus.com/api/timetable?project_id=$projectId&route_id=$routeId');

      if (response.statusCode == 200) {
        var decodeJson = jsonDecode(response.body);
        decodeJson.forEach((item) => list.add(MOBTeste.fromJson(item)));

        setState(() {
          ac = list[0].ac;

          if (ac == null) {
            ac = false;
          }
        });
      } else {
        print('erro');
      }
    } catch (e) {
      print('erro');
    }
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      getAC();
      timetable = await Utils.getTimetable(line.routeId);
      List<MOBTrip> trips = timetable.trips;
      for (final trip in trips) {
        int seq = trip.seq;
        int directionId = trip.directionId;
        int tripId = trip.tripId;
        String desc = trip.tripDesc;
        tripMap[seq] = {
          'directionId': directionId,
          'desc': desc,
          'tripId': tripId,
        };
        tripFilter[seq] = false;
      }
      tripsSeqs = tripFilter.keys.toList();
      tripsSeqs.sort();
      if (timetable.directions.length == 1) {
        selectedDirection = timetable.directions.first;
        tabController = TabController(
            length: selectedDirection.services.length, vsync: this);
        goToTabDayOfWeek();
      }

      SharedPreferences prefs = await SharedPreferences.getInstance();
      int projectId = Utils.project.projectId;

      String jsonObject = prefs.getString('timetable_favorites_$projectId');

      Iterable favoritesIterable;
      List<dynamic> favorites = [];
      if (jsonObject != null) {
        favoritesIterable = convert.json.decode(jsonObject);
        favorites = favoritesIterable.toList();
      }

      int routeId = line.routeId;
      var value = favorites.contains(routeId);

      setState(() {
        isFavorited = value == null ? false : value;
      });

      if (trips.length > 0)
        Future.delayed(Duration(seconds: 1), () {
          SnackBar s = SnackBar(
            behavior: SnackBarBehavior.fixed,
            content: Text('Clique no horário e visualize sua rota',
                style: TextStyle(), textAlign: TextAlign.center),
            duration: Duration(seconds: 10),
          );
          scaffoldKeyMain.currentState.showSnackBar(s);
        });

      Future.delayed(Duration(seconds: 2), () {
        MOBApiUtils.mobEvent('line selected', {
          'shortName': line.shortName,
          'longName': line.longName,
          'routeId': line.routeId,
        });
      });
    });
  }

  void goToTabDayOfWeek() => Future.delayed(Duration(seconds: 1), () async {
        int weekDay = DateTime.now().weekday;
        weekDay = weekDay == 7 ? 0 : weekDay;
        SERVICE_DAYS serviceDay = SERVICE_DAYS.values[weekDay];
        int index = 0;
        for (final service in selectedDirection.services)
          if (service.days.contains(serviceDay)) {
            tabController.animateTo(index);
            break;
          } else
            index++;
      });

  void resetFilter() {
    tripFilter.keys.forEach((key) => tripFilter[key] = false);
    tripsSeqs = tripFilter.keys.toList();
    tripsSeqs.sort();
  }

  void favorite() => SharedPreferences.getInstance().then((prefs) {
        int projectId = Utils.project.projectId;

        String jsonObject = prefs.getString('timetable_favorites_$projectId');

        Iterable favoritesIterable;
        List<dynamic> favorites = [];
        if (jsonObject != null) {
          favoritesIterable = convert.json.decode(jsonObject);
          favorites = favoritesIterable.toList();
        }

        int routeId = line.routeId;
        var value = favorites.contains(routeId);

        bool isFavorited = value ?? false;
        isFavorited = !isFavorited;
        this.isFavorited = isFavorited;

        if (isFavorited) {
          favorites.add(line.routeId);
          Future.delayed(Duration(seconds: 2), () {
            MOBApiUtils.mobEvent('line favorited', {
              'shortName': line.shortName,
              'longName': line.longName,
              'routeId': line.routeId,
            });
          });
        } else
          favorites.remove(line.routeId);

        String encoded = convert.json.encode(favorites);
        prefs.setString('timetable_favorites_$projectId', encoded);
        scaffoldKeyMain.currentState.showSnackBar(SnackBar(
          content: isFavorited
              ? Text(
                  'Linha adicionada nos favoritos',
                  style: TextStyle(),
                )
              : Text(
                  'Linha removida dos favoritos',
                  style: TextStyle(),
                ),
          duration: Duration(seconds: 2),
        ));
        setState(() {});
      });

  Widget lineHeader() {
    String textColorStr = line.textColor ?? '#ffffff';
    String backgroundColorStr = line.color ?? '#000000';

    final moneyConvert = new NumberFormat("#,##0.00", "en_US");
    String sPrice = null;

    if (line.price != null) {
      sPrice = moneyConvert.format(line.price);
      sPrice = sPrice.replaceAll('.', ',');
    }

    Color textColor = Utils.getColorFromHex(textColorStr);
    Color backgroundColor = Utils.getColorFromHex(backgroundColorStr);

    Text textShortName = Text(
      line.shortName,
      style: TextStyle(
        color: textColor,
        backgroundColor: backgroundColor,
        fontSize: 28,
      ),
    );
    Text textLongName = Text(
      line.longName,
      textAlign: TextAlign.start,
      style: TextStyle(color: Colors.black),
    );

    bool darkColor = textColor.computeLuminance() > 0.5;
    String imageAsset = Utils.getStopTypeSvg(line.type, darkColor);

    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(10),
          child: Card(
            color: backgroundColor,
            elevation: 5,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            child: Container(
              padding: EdgeInsets.all(5),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            padding: EdgeInsets.all(5),
                            child: SvgPicture.asset(imageAsset, height: 28),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 125,
                            child: textShortName,
                          ),
                        ],
                      ),
                      IconButton(
                        onPressed: favorite,
                        icon: Icon(
                          isFavorited ? Icons.favorite : Icons.favorite_border,
                          color: textColor,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                  line.longName,
                  textAlign: TextAlign.start,
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  if (ac)
                    Container(
                      padding: EdgeInsets.all(5),
                      child: Icon(Icons.ac_unit, color: Color(0xff90dede)),
                    ),
                  Container(width: 10),
                  if (sPrice != null)
                    Card(
                      color: Colors.green,
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Row(
                          children: [
                            Icon(Icons.local_atm, color: Colors.white),
                            Text(
                              ' ' + sPrice,
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }

  Widget lineOptions() {
    List<MOBDirection> directions = timetable?.directions ?? [];
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 5, top: 10),
            child: Text(
              'Selecione um local de saída',
              textAlign: TextAlign.start,
            ),
          ),
          Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.grey),
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            child: Container(
              child: DropdownButton(
                isExpanded: true,
                underline: Container(),
                value: selectedDirection,
                onChanged: (value) => setState(() {
                  selectedDirection = value;
                  tabController = TabController(
                      length: selectedDirection.services.length, vsync: this);
                  resetFilter();
                  goToTabDayOfWeek();
                }),
                items: directions
                    .map((direction) => DropdownMenuItem(
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Text(direction.desc),
                          ),
                          value: direction,
                        ))
                    .toList(),
              ),
            ),
          ),
          if (selectedDirection != null &&
              selectedDirection.services.isNotEmpty)
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: 10),
              height: 40,
              child: InkWell(
                onTap: () => expandableController.toggle(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Confira os destinos'),
                    Icon(Icons.chevron_right),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }

  void dialogSubtitle(double fontSize) => showDialog(
      context: context,
      builder: (context) => AlertDialog(
            backgroundColor: Theme.of(context).primaryColor,
            title: Text('Legenda',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context))),
            content: SubtitleInfoWidget(fontSize),
            actions: <Widget>[
              FlatButton(
                child: Text('Fechar',
                    style: TextStyle(
                        color: Utils.getColorByLuminanceTheme(context))),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          ));

  void scrollToNextDeparture(List<MOBLineDeparture> departures) =>
      Future.delayed(Duration(seconds: 1), () async {
        int indexNextDeparture;
        DateTime now = DateTime.now();
        Duration duration = Duration(hours: now.hour, minutes: now.minute);
        for (indexNextDeparture = 0;
            indexNextDeparture < departures.length;
            indexNextDeparture++) {
          String dep = departures[indexNextDeparture].dep;
          List<String> split = dep.split(':');
          int h = int.parse(split[0]);
          int m = int.parse(split[1]);
          Duration d = Duration(hours: h, minutes: m);

          if (d.compareTo(duration) > 0) break;
        }

        int indexByLine = indexNextDeparture ~/ 5;
        double offset = indexByLine * 90.0;
        scrollController.animateTo(
          offset,
          duration: Duration(seconds: 2),
          curve: Curves.fastOutSlowIn,
        );
      });

  @override
  Widget build(BuildContext context) {
    List<MOBTrip> trips = timetable?.trips ?? [];
    int routeId = line.routeId;
    Color lineColor = Utils.getColorFromHex(line.color ?? '#000000');
    String shortName = line.shortName;

    List<String> subtitles = [];
    if (selectedDirection != null)
      for (final service in selectedDirection.services)
        for (final departure in service.departures) {
          int departureSeq = departure.seq;
          String desc = tripMap[departureSeq]['desc'];
          String subtitle = '';
          String shName = '';

          for (int count = 0; count < timetable.trips.length; count++) {
            if (timetable.trips[count].seq == departureSeq) {
              if (timetable.trips[count].shortName != null) {
                shName = timetable.trips[count].shortName;
                subtitle = '$departureSeq: $shName > $desc';
              } else {
                subtitle = '$departureSeq: $desc';
              }

              break;
            }
          }

          if (!subtitles.contains(subtitle)) {
            subtitles.add(subtitle);
          }
        }

    String subs = '';
    for (final sub in subtitles) subs += '$sub\n';

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      key: scaffoldKeyMain,
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        foregroundColor: Utils.getColorByLuminanceTheme(context),
        centerTitle: true,
        title: Text('Horários'),
        actions: [
          IconButton(
            onPressed: () async {
              await Utils.selectedLineBehaviour(line, false, context, false);

              int projectId = Utils.project.projectId;

              String timetablePath =
                  'timetable_last_download_$projectId\_$routeId';
              SharedPreferences prefs = await SharedPreferences.getInstance();
              String timetableJson = prefs.getString(timetablePath);
              Map<String, dynamic> map = convert.json.decode(timetableJson);
              MOBTimetable timetable = MOBTimetable.fromJson(map);
              List<MOBTrip> trips = timetable.trips;

              String imageAsset = Utils.getStopTypeSvg(line.type, false);

              DialogUtils(context).showRequestTripToView(
                  shortName, trips, imageAsset, routeId, lineColor);
            },
            icon: Icon(Icons.map),
          ),
          IconButton(
              icon: Icon(Icons.info), onPressed: () => dialogSubtitle(16)),
        ],
      ),

      body: NestedScrollView(
        headerSliverBuilder: (context, value) => [
          SliverToBoxAdapter(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                lineHeader(),
                if ((line.desc ?? '').isNotEmpty)
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Card(
                      elevation: 10,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Text(
                          line.desc,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                lineOptions(),
                ExpandablePanel(
                  controller: expandableController,
                  expanded: Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    width: MediaQuery.of(context).size.width,
                    child: Text(subs, style: TextStyle(color: Colors.black)),
                  ),
                  collapsed: null,
                ),
                if (tabController != null && tabController.length > 0)
                  Container(
                    color: Theme.of(context).primaryColor,
                    child: TabBar(
                      isScrollable: tabController.length > 2,
                      controller: tabController,
                      tabs: selectedDirection.services
                          .map((e) => Tab(
                                child: Text(e.desc),
                              ))
                          .toList(),
                    ),
                  ),
              ],
            ),
          )
        ],
        body: selectedDirection != null &&
                tabController != null &&
                selectedDirection.services.isNotEmpty
            ? TabBarView(
                controller: tabController,
                children: selectedDirection.services.map((service) {
                  List<MOBLineDeparture> departures = service.departures;
                  return DayDirectionTimetable(
                      departures, trips, routeId, lineColor, tripMap);
                }).toList(),
              )
            : Center(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(50),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        selectedDirection != null &&
                                selectedDirection.services.isEmpty
                            ? 'Não existem horários disponíveis para essa viagem, tente novamente mais tarde'
                            : 'Selecione um local de saída para visualizar a tabela de horários',
                        textAlign: TextAlign.center,
                      ),
                      Image.asset(
                        selectedDirection != null &&
                                selectedDirection.services.isEmpty
                            ? 'assets/mobilibus/man.png'
                            : 'assets/mobilibus/man.png',
                        height: MediaQuery.of(context).size.height / 3,
                      )
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
