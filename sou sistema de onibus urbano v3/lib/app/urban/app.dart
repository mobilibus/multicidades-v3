import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:mobilibus/app/urban/favorites.dart';
import 'package:mobilibus/app/urban/main_map.dart';
import 'package:mobilibus/app/urban/more.dart';
import 'package:mobilibus/app/urban/timetable_search.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainPage extends StatefulWidget {
  final int selectTab;

  const MainPage({Key key, @required this.selectTab}) : super(key: key);

  @override
  _MainPage createState() => _MainPage();
}

class _MainPage extends State<MainPage> {


  int tabSelected = 0;
  bool hasRedDot = false;
  final PageStorageBucket bucket = PageStorageBucket();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  List<Widget> bottomViews = [];
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    tabSelected = widget.selectTab;

    Timer.periodic(Duration(minutes: 1), (timer) async {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      if (sharedPreferences.containsKey('review')) {
        int millis = sharedPreferences.getInt('review');
        DateTime past = DateTime.fromMillisecondsSinceEpoch(millis);
        DateTime now = DateTime.now();
        Duration duration = now.difference(past);
        if (duration.inDays >= 7) Utils.requestReview = true;
      } 
      
      sharedPreferences.setInt('review', DateTime.now().millisecondsSinceEpoch);
    });
    
    // sharedPreferences = await SharedPreferences.getInstance();
    super.initState();
  }

  void callReview() async {
    double rate = 5.0;
    return await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: Container(
          height: 200,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('O que você acha do app?'),
              RatingBar(
                itemCount: 5,
                tapOnlyMode: true,
                initialRating: 5,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: false,
                onRatingUpdate: (value) => rate = value,
                ratingWidget: RatingWidget(
                    full: Icon(Icons.star, color: Colors.amber),
                    half: Icon(Icons.star, color: Colors.amber),
                    empty: Icon(Icons.star, color: Colors.amber)),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    icon: Icon(Icons.cancel),
                    onPressed: () async {
                      SharedPreferences sharedPreferences =
                          await SharedPreferences.getInstance();
                      sharedPreferences.setInt(
                          'review', DateTime.now().millisecondsSinceEpoch);
                      Utils.requestReview = false;
                      Navigator.of(context).pop();
                    },
                  ),
                  IconButton(
                    icon: Icon(Icons.done),
                    onPressed: () async {
                      MOBApiUtils.mobEvent('app_review', {'review': rate});
                      SharedPreferences sharedPreferences =
                          await SharedPreferences.getInstance();
                      sharedPreferences.setInt(
                          'review', DateTime.now().millisecondsSinceEpoch);
                      Utils.requestReview = false;
                      Navigator.of(context).pop();
                      if (rate >= 5.0) {
                        InAppReview inAppReview = InAppReview.instance;
                        if (await inAppReview.isAvailable()) if (Platform
                            .isAndroid)
                          await inAppReview.requestReview();
                        else if (Platform.isIOS) {
                          inAppReview.openStoreListing(
                              appStoreId: Utils.iosAppStoreId);
                          await inAppReview.requestReview();
                        }
                      } else
                        showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (context) {
                            Future.delayed(Duration(seconds: 1),
                                () => Navigator.of(context).pop());
                            return AlertDialog(title: Text('Obrigado! :)'));
                          },
                        );
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ); 
  }

  @override
  Widget build(BuildContext context) {
    hasRedDot = sharedPreferences?.containsKey('redDot') ?? false
    ? sharedPreferences.getBool('redDot')
    : false;

    bottomViews = <Widget>[
      if (!Utils.liteMode) MyMap(),
      if (Utils.project != null) TimetableSearch(),
      if (Utils.project != null) Favorites(),
      More(),
    ];

    if (Utils.requestReview) {
      Utils.requestReview = !Utils.requestReview;
      callReview();
    }

    return WillPopScope(
      onWillPop: () async {
        if (tabSelected > 0) {
          setState(() => tabSelected = 0);
          return false;
        }
        return true;
      },
      child: Scaffold(
        key: scaffoldKey,
        resizeToAvoidBottomInset: false,
        //resizeToAvoidBottomPadding: false,
        bottomNavigationBar: BottomNavigationBar(
          onTap: (index) => setState(() => tabSelected = index),
          currentIndex: tabSelected,
          type: BottomNavigationBarType.fixed,
          backgroundColor: Theme.of(context).primaryColor,
          selectedItemColor: Utils.globalAccentColor, //Theme.of(context).accentColor,
          unselectedItemColor: Utils.globalUnselectedColor,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          items: [
            if (!Utils.liteMode)
              BottomNavigationBarItem(
                icon: Icon(Icons.map),
                label: 'Mapa',
              ),
            if (Utils.project != null)
              BottomNavigationBarItem(
                icon: Icon(Icons.departure_board),
                label: 'Horários',
              ),
            if (Utils.project != null)
              BottomNavigationBarItem(
                icon: Icon(Icons.favorite),
                label: 'Favoritos',
              ),
            BottomNavigationBarItem(
              label: 'Mais',
              icon: Stack(
                alignment: Alignment.topRight,
                children: [
                  Icon(Icons.menu),
                  if (hasRedDot)
                    Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.all(Radius.circular(5)))
                      ),
                ],
              ),
            ),
          ],
        ),
        
        floatingActionButton: Utils.isMobilibus
        ? Container(
            child: FloatingActionButton(
              heroTag: 'fabReview',
              onPressed: callReview,
              mini: true,
              backgroundColor: Colors.transparent,
              child: Icon(Icons.rate_review),
            ),
          )
        : null,
        body: IndexedStack(
          children: bottomViews,
          index: tabSelected,
        ),
      ),
    );
  }
}
