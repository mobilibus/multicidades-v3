import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobilibus/app/charter/builder/CharterRouteBuilder.dart';
import 'package:mobilibus/app/urban/route/departures/urban_route_departures.dart';
import 'package:mobilibus/app/urban/route/urban_route.dart';
import 'package:mobilibus/base/MOBDeparture.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DepartureWidget extends StatelessWidget {
  DepartureWidget(
    this.context,
    this.departureTrips,
    this.stop,
    this.favoritedMap,
    this.downloadedMap,
    this.callback,
  );
  final BuildContext context;

  final List<MOBDepartureTrip> departureTrips;
  final MOBStop stop;
  final Map<int, bool> favoritedMap;
  final Map<int, bool> downloadedMap;
  final VoidCallback callback;

  void favoriteLine(MOBLine line) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int projectId = Utils.project.projectId;
    int routeId = line.routeId;

    String jsonObject = prefs.getString('timetable_favorites_$projectId');

    Iterable favoritesIterable;
    List<dynamic> favorites = [];
    if (jsonObject != null) {
      favoritesIterable = convert.json.decode(jsonObject);
      favorites = favoritesIterable.toList();
    }

    bool value = favorites.contains(line.routeId);
    bool isFavorited = value ?? false;
    isFavorited = !isFavorited;
    favoritedMap[line.routeId] = isFavorited;
    if (isFavorited) {
      favorites.add(line.routeId);
      MOBApiUtils.mobEvent('line favorited', {
        'shortName': line.shortName,
        'longName': line.longName,
        'routeId': line.routeId,
      });
    } else
      favorites.remove(line.routeId);

    String encoded = convert.json.encode(favorites);
    prefs.setString('timetable_favorites_$projectId', encoded);
    favoritedMap[routeId] = isFavorited;
    Utils.selectedLineBehaviour(line, false, context, false);
    callback.call();
  }

  Widget departuresCard(MOBDepartureTrip mDepartureTrip) {
    Color color = Theme.of(context).primaryColor;
    Color lineColor = Utils.getColorFromHex(mDepartureTrip.color ?? '#000000');
    bool darkColor = lineColor.computeLuminance() < 0.5;
    Color txColor = darkColor ? Colors.white : Colors.black;
    String routeShortName = mDepartureTrip.shortName;
    String routeLongName = mDepartureTrip.longName;
    int routeId = mDepartureTrip.routeId;
    int tripId = mDepartureTrip.tripId;

    String imageAsset = Utils.getStopTypeSvg(stop.stopType, darkColor);

    bool isLineFavorited = favoritedMap[routeId] ?? false;

    bool isInTheProject = true;
    MOBLine line =
        Utils.lines.firstWhere((line) => line.routeId == routeId, orElse: () {
      isInTheProject = false;
      return MOBLine(
        routeId,
        routeShortName,
        routeLongName,
        '',
        -1,
        lineColor.value.toRadixString(16),
        txColor.value.toRadixString(16),
        null,
      );
    });

    double width = MediaQuery.of(context).size.width;

    int countDepartures = mDepartureTrip.departures.length;
    int stopId = stop.stopId;
    List<Widget> actionButtons = [
      Container(
        padding: EdgeInsets.all(10),
        width: width / 2 - 23,
        child: countDepartures > 3
            ? InkWell(
                onTap: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) =>
                        UrbanRouteDepartures(routeId, stop.stopId, tripId))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: width / 4,
                      child: Text('Mais previsões'),
                    ),
                    Icon(Icons.departure_board),
                  ],
                ),
              )
            : null,
      ),
      Container(
        padding: EdgeInsets.all(10),
        width: width / 2 - 25,
        child: InkWell(
          onTap: () => Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => UrbanRouteViewer(stopId, routeId, tripId))),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                width: width / 4,
                child: Text('Mapa da Linha'),
              ),
              Icon(Icons.map),
            ],
          ),
        ),
      ),
    ];

    Widget actionButtonsLayout = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: actionButtons,
    );

    String price = line.price == null
        ? null
        : line.price.toStringAsFixed(2).replaceAll('.', ',');
    String headsign = mDepartureTrip.headsign;

    List<MOBDeparture> departures = mDepartureTrip.departures;

    List<MOBDeparture> departuresToday =
        departures.where((element) => !element.nextDay).toList();
    List<MOBDeparture> departuresNextDay =
        departures.where((element) => element.nextDay).toList();

    List<Widget> departuresWidgetsToday = departuresToday
        .map((departure) => departureDetailed(departure, color))
        .toList();

    List<Widget> departuresWidgetsNextDay = departuresNextDay
        .map((departure) => departureDetailed(departure, color, true))
        .toList();

    if (departuresWidgetsToday.length > 3)
      departuresWidgetsToday = departuresWidgetsToday.sublist(0, 3);

    if (departuresWidgetsNextDay.length > 3)
      departuresWidgetsNextDay = departuresWidgetsNextDay.sublist(0, 3);

    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
          side: BorderSide(width: 1, color: lineColor)),
      borderOnForeground: false,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                color: lineColor,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.all(5),
                        child: SvgPicture.asset(imageAsset, height: 24),
                      ),
                      Text(
                        routeShortName,
                        style: TextStyle(
                          color: txColor,
                          backgroundColor: lineColor,
                          fontSize: 24,
                        ),
                      ),
                    ],
                  ),
                  if (isInTheProject)
                    IconButton(
                      onPressed: () => favoriteLine(line),
                      icon: Icon(
                        isLineFavorited
                            ? Icons.favorite
                            : Icons.favorite_border,
                        color: txColor,
                      ),
                    ),
                ],
              ),
            ),
          ),
          Row(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                width: price != null
                    ? (MediaQuery.of(context).size.width / 4) * 3 - 30
                    : MediaQuery.of(context).size.width - 30,
                child: Text(
                  routeLongName,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 12,
                  ),
                ),
              ),
              if (price != null)
                Container(
                  padding: EdgeInsets.only(top: 5),
                  child: Card(
                    color: Colors.green,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Row(
                        children: [
                          Icon(Icons.local_atm, color: Colors.white),
                          Text(
                           ' ' + price,
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
            width: MediaQuery.of(context).size.width - 30,
            child: Text(
              'Destino: $headsign',
              style: TextStyle(
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.white
                    : Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 12,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: departuresWidgetsToday,
                ),
                if (departuresWidgetsNextDay.isNotEmpty)
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Text(
                          'Amanhã',
                          style: TextStyle(color: Color(0xff708090)),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: departuresWidgetsNextDay,
                      ),
                    ],
                  ),
              ],
            ),
          ),
          if (Utils.isMobilibus)
            Container(
              padding: EdgeInsets.all(5),
              child: Card(
                color: Colors.grey,
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        'routeId=$routeId',
                        style: TextStyle(fontSize: 10),
                      ),
                      Text(
                        'tripId=$tripId',
                        style: TextStyle(fontSize: 10),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Divider(
              height: 1,
              thickness: 1,
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            width: MediaQuery.of(context).size.width,
            child: actionButtonsLayout,
          ),
        ],
      ),
    );
  }

  Widget departureDetailed(MOBDeparture departure, Color color,
      [bool isNextDay = false]) {
    int minutes = departure.minutes;
    int seconds = departure.seconds;
    String vehicleId = departure.vehicleId;
    String strDeparture = '';
    if (departure.nextDay)
      strDeparture = departure.time.substring(0, 5).replaceAll(':', 'h');
    else if (minutes < 2 && !departure.nextDay)
      strDeparture = 'Agora';
    else if (minutes < 60 && !departure.nextDay)
      strDeparture = '$minutes min';
    else
      strDeparture = departure.time.substring(0, 5).replaceAll(':', 'h');

    FontWeight fw = departure.online ? FontWeight.bold : FontWeight.normal;

    IconData icon = Utils.getDepartureIconByStopType(stop.stopType);

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        if (departure.online)
                          Image(
                            image: AssetImage("assets/rt.gif"),
                            height: 20,
                            width: 20,
                          ),
                        Text(
                          strDeparture,
                          style: TextStyle(
                            color: isNextDay ? Color(0xff708090) : null,
                            fontSize: 18,
                            fontWeight: fw,
                          ),
                        ),
                        if (departure.wheelchairAccessible)
                          Icon(
                            Icons.accessible_forward,
                            color: isNextDay ? Color(0xff708090) : Colors.black,
                            size: 18,
                          ),
                      ],
                    ),
                  ],
                ),
                if (vehicleId != null)
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(icon),
                      Text(vehicleId, style: TextStyle(fontSize: 18)),
                    ],
                  ),
              ],
            ),
          ),
          if (Utils.isMobilibus)
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'seconds: $seconds',
                  style: TextStyle(fontSize: 10),
                ),
                Text(
                  'minutes: $minutes',
                  style: TextStyle(fontSize: 10),
                ),
              ],
            ),
        ],
      ),
    );
  }

  Widget departuresCharterCard(MOBDepartureTrip mDepartureTrip) {
    String stopName = stop.name;
    double width = MediaQuery.of(context).size.width * 0.01;
    int routeId = mDepartureTrip.routeId;
    int tripId = mDepartureTrip.tripId;
    return Container(
      padding: EdgeInsets.only(left: width, right: width),
      child: Column(
        children: <Widget>[
          Card(
            elevation: 0,
            color: Color(0xff30394D),
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.white, width: 0.3),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            child: InkWell(
              onTap: () => CharterRouteBuilder(stop, null, routeId, tripId,
                      Theme.of(context).primaryColor, context, false)
                  .init(),
              child: Container(
                padding: EdgeInsets.all(5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 15,
                      child: Text(
                        stopName,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Divider(
                      height: 10.0,
                      color: Theme.of(context).primaryColor,
                    ),
                    listDepartures(mDepartureTrip, stop),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget listDepartures(MOBDepartureTrip departureTrip, MOBStop stop) {
    MOBDeparture departure = departureTrip.departures.first;

    String time = departure.time.substring(0, 5);
    String vehicleId = departureTrip.departures.first.vehicleId;
    String headsign = departureTrip.headsign;
    int tripId = departureTrip.tripId;

    int minutes = departureTrip.departures.first.minutes;
    if (minutes > 2 && minutes < 60)
      time = '$minutes min.';
    else if (minutes < 2) time = 'Aprox.';

    bool isNextDay = departure.nextDay;
    bool isOnline = departure.online;

    double width = MediaQuery.of(context).size.width / 3;

    return InkWell(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '$headsign${Utils.isMobilibus ? ' [$tripId]' : ''}',
              style: TextStyle(fontSize: 12, color: Colors.white),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: width,
                  child: Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (isNextDay)
                          Container(
                            child: Text(
                              'Amanhã, ',
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        Container(
                          child: Text(
                            time,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                if (isOnline)
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          'assets/images/bus.svg',
                          height: 20,
                        ),
                        Container(
                          width: width - 40,
                          padding: EdgeInsets.only(left: 5),
                          child: Text(
                            vehicleId ?? '',
                            style: TextStyle(fontSize: 16, color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) => departureTrips.isEmpty
      ? Center(child: CircularProgressIndicator())
      : ListView.builder(
          padding: EdgeInsets.only(left: 10, right: 10, bottom: 100),
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: departureTrips.length,
          itemBuilder: (context, index) {
            MOBDepartureTrip departureTrip = departureTrips[index];
            Widget departureWidget = Utils.isCharter
                ? departuresCharterCard(departureTrip)
                : departuresCard(departureTrip);
            return departureWidget;
          },
        );
}
