import 'dart:convert';

import 'package:mobilibus/transdata/data/model/transdata_model.dart';

class TransdataTaxesRequest  extends TransdataRequest {
  int cardSerial;
  String cpf;
  List<int> productIds;
  List<int> values;
  TransdataTaxesRequest({
    this.cardSerial,
    this.cpf,
    this.productIds,
    this.values,
  });

  Map<String, dynamic> toMap() {
    return {
      'cardSerial': cardSerial,
      'cpf': cpf,
      'productIds': productIds,
      'values': values,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataTaxesRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataTaxesRequest(
      cardSerial: map['cardSerial'],
      cpf: map['cpf'],
      productIds: List<int>.from(map['productIds']),
      values: List<int>.from(map['values']),
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataTaxesRequest.fromJson(String source) =>
      TransdataTaxesRequest.fromMap(json.decode(source));
}

class TransdataTaxes {
  int errorCode;
  String errorMessage;
  String descricao;
  int valorTotalEmCentavos;
  TransdataTaxes({
    this.errorCode,
    this.errorMessage,
    this.descricao,
    this.valorTotalEmCentavos,
  });

  Map<String, dynamic> toMap() {
    return {
      'errorCode': errorCode,
      'errorMessage': errorMessage,
      'Descricao': descricao,
      'ValorTotalEmCentavos': valorTotalEmCentavos,
    };
  }

  factory TransdataTaxes.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataTaxes(
      errorCode: map['errorCode'],
      errorMessage: map['errorMessage'],
      descricao: map['Descricao'],
      valorTotalEmCentavos: map['ValorTotalEmCentavos'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataTaxes.fromJson(String source) =>
      TransdataTaxes.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransdataTaxes(errorCode: $errorCode, errorMessage: $errorMessage, descricao: $descricao, valorTotalEmCentavos: $valorTotalEmCentavos)';
  }
}
