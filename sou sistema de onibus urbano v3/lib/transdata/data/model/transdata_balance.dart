import 'dart:convert';

import 'package:mobilibus/transdata/data/model/transdata_model.dart';

class TransdataBalancesRequest extends TransdataRequest {
  List<int> cardSerials;
  List<String> cpfs;

  TransdataBalancesRequest({
    this.cardSerials,
    this.cpfs,
  });

  Map<String, dynamic> toMap() {
    return {
      'cardSerials': cardSerials,
      'cpfs': cpfs,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataBalancesRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataBalancesRequest(
      cardSerials: List<int>.from(map['cardSerials']),
      cpfs: List<String>.from(map['cpfs']),
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataBalancesRequest.fromJson(String source) =>
      TransdataBalancesRequest.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransdataHistoryRequest(cardSerials: $cardSerials, cpfs: $cpfs, token: $token, seq: $seq)';
  }
}

class TransdataBalance {
  int errorCode;
  String errorMessage;
  List<TransdataBalanceValue> value;
  TransdataBalance({
    this.errorCode,
    this.errorMessage,
    this.value,
  });

  Map<String, dynamic> toMap() {
    return {
      'errorCode': errorCode,
      'errorMessage': errorMessage,
      'Valor': value?.map((x) => x?.toMap())?.toList(),
    };
  }

  factory TransdataBalance.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataBalance(
      errorCode: map['errorCode'],
      errorMessage: map['errorMessage'],
      value: List<TransdataBalanceValue>.from(
          map['Valor']?.map((x) => TransdataBalanceValue.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataBalance.fromJson(String source) =>
      TransdataBalance.fromMap(json.decode(source));

  @override
  String toString() =>
      'TransdataBalance(errorCode: $errorCode, errorMessage: $errorMessage, Valor: $value)';
}

class TransdataBalanceValue {
  int serieNumber;
  String cpf;
  String balanceDate;
  int balance;
  TransdataBalanceValue({
    this.serieNumber,
    this.cpf,
    this.balanceDate,
    this.balance,
  });

  Map<String, dynamic> toMap() {
    return {
      'NumSerie': serieNumber,
      'CPF': cpf,
      'DataSaldo': balanceDate,
      'SaldoEmCentavos': balance,
    };
  }

  factory TransdataBalanceValue.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataBalanceValue(
      serieNumber: map['NumSerie'],
      cpf: map['CPF'],
      balanceDate: map['DataSaldo'],
      balance: map['SaldoEmCentavos'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataBalanceValue.fromJson(String source) =>
      TransdataBalanceValue.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransdataBalanceValue(serieNumber: $serieNumber, cpf: $cpf, balanceDate: $balanceDate, balance: $balance)';
  }
}
