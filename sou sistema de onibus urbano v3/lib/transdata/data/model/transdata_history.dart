import 'dart:convert';

import 'package:mobilibus/transdata/data/model/transdata_model.dart';

class TransdataHistoryRequest extends TransdataRequest {
  int cardSerial;
  String cpf;
  String startDate;
  String endDate;

  TransdataHistoryRequest({
    this.cardSerial,
    this.cpf,
    this.startDate,
    this.endDate,
  });

  Map<String, dynamic> toMap() {
    return {
      'cardSerial': cardSerial,
      'cpf': cpf,
      'startDate': startDate,
      'endDate': endDate,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataHistoryRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataHistoryRequest(
      cardSerial: map['cardSerial'],
      cpf: map['cpf'],
      startDate: map['startDate'],
      endDate: map['endDate'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataHistoryRequest.fromJson(String source) =>
      TransdataHistoryRequest.fromMap(json.decode(source));
}

class TransdataHistoryData {
  int errorCode;
  String errorMessage;
  List<TransdataHistory> valor;
  TransdataHistoryData({
    this.errorCode,
    this.errorMessage,
    this.valor,
  });

  Map<String, dynamic> toMap() {
    return {
      'errorCode': errorCode,
      'errorMessage': errorMessage,
      'Valor': valor?.map((x) => x?.toMap())?.toList(),
    };
  }

  factory TransdataHistoryData.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
  
    return TransdataHistoryData(
      errorCode: map['errorCode'],
      errorMessage: map['errorMessage'],
      valor: List<TransdataHistory>.from(map['Valor']?.map((x) => TransdataHistory?.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataHistoryData.fromJson(String source) => TransdataHistoryData.fromMap(json.decode(source));
}

class TransdataHistory {
  String idPedido;
  String dataPedido;
  String dataStatus;
  String descricaoStatus;
  int valorTotalPedidoEmCentavos;
  String cpf;
  int numSerie;
  TransdataHistory({
    this.idPedido,
    this.dataPedido,
    this.dataStatus,
    this.descricaoStatus,
    this.valorTotalPedidoEmCentavos,
    this.cpf,
    this.numSerie,
  });

  Map<String, dynamic> toMap() {
    return {
      'IdPedido': idPedido,
      'DataPedido': dataPedido,
      'DataStatus': dataStatus,
      'DescricaoStatus': descricaoStatus,
      'ValorTotalPedidoEmCentavos': valorTotalPedidoEmCentavos,
      'Cpf': cpf,
      'NumSerie': numSerie,
    };
  }

  factory TransdataHistory.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataHistory(
      idPedido: map['IdPedido'],
      dataPedido: map['DataPedido'],
      dataStatus: map['DataStatus'],
      descricaoStatus: map['DescricaoStatus'],
      valorTotalPedidoEmCentavos: map['ValorTotalPedidoEmCentavos'],
      cpf: map['Cpf'],
      numSerie: map['NumSerie'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataHistory.fromJson(String source) =>
      TransdataHistory.fromMap(json.decode(source));
}

class TransdataHistoryDetailRequest extends TransdataRequest {
  int id;
  
  TransdataHistoryDetailRequest({
    this.id,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataHistoryDetailRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataHistoryDetailRequest(
      id: map['id'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataHistoryDetailRequest.fromJson(String source) =>
      TransdataHistoryDetailRequest.fromMap(json.decode(source));

  @override
  String toString() =>
      'TransdataHistoryDetailRequest(id: $id, token: $token, seq: $seq)';
}

class TransdataHistoryDetail {
  int errorCode;
  String errorMessage;
  int serieNumber;
  int tax;
  String cpf;
  String externalPurchaseId;
  String appNameAndVersion;
  String statusDate;
  String statusDescription;
  List<TransdataHistoryDetailItem> items;
  TransdataHistoryDetail({
    this.errorCode,
    this.errorMessage,
    this.serieNumber,
    this.tax,
    this.cpf,
    this.externalPurchaseId,
    this.appNameAndVersion,
    this.statusDate,
    this.statusDescription,
    this.items,
  });

  Map<String, dynamic> toMap() {
    return {
      'errorCode': errorCode,
      'errorMessage': errorMessage,
      'NumeroSerie': serieNumber,
      'TaxasEmCentavos': tax,
      'CPF': cpf,
      'IDcompraExterno': externalPurchaseId,
      'AppNome_Versao': appNameAndVersion,
      'DataStatus': statusDate,
      'descricaoStatus': statusDescription,
      'Itens': items?.map((x) => x?.toMap())?.toList(),
    };
  }

  factory TransdataHistoryDetail.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataHistoryDetail(
      errorCode: map['errorCode'],
      errorMessage: map['errorMessage'],
      serieNumber: map['NumeroSerie'],
      tax: map['TaxasEmCentavos'],
      cpf: map['CPF'],
      externalPurchaseId: map['IDcompraExterno'],
      appNameAndVersion: map['AppNome_Versao'],
      statusDate: map['DataStatus'],
      statusDescription: map['descricaoStatus'],
      items: List<TransdataHistoryDetailItem>.from(
          map['Itens']?.map((x) => TransdataHistoryDetailItem.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataHistoryDetail.fromJson(String source) =>
      TransdataHistoryDetail.fromMap(json.decode(source));
}

class TransdataHistoryDetailItem {
  int product;
  int unityValue;
  int purchaseValue;
  TransdataHistoryDetailItem({
    this.product,
    this.unityValue,
    this.purchaseValue,
  });

  Map<String, dynamic> toMap() {
    return {
      'Produto': product,
      'ValorUnitarioEmCentavos': unityValue,
      'ValorCompraEmCentavos': purchaseValue,
    };
  }

  factory TransdataHistoryDetailItem.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataHistoryDetailItem(
      product: map['Produto'],
      unityValue: map['ValorUnitarioEmCentavos'],
      purchaseValue: map['ValorCompraEmCentavos'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataHistoryDetailItem.fromJson(String source) =>
      TransdataHistoryDetailItem.fromMap(json.decode(source));
}
