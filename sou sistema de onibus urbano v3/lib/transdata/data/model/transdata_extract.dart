import 'dart:convert';

import 'package:mobilibus/transdata/data/model/transdata_model.dart';
import 'package:mobilibus/utils/date_number_utils.dart';

class TransdataExtractRequest extends TransdataRequest {
  int cardSerial;
  String cpf;
  String startDate;
  String endDate;

  TransdataExtractRequest({
    this.cardSerial,
    this.cpf,
    this.startDate,
    this.endDate,
  });

  Map<String, dynamic> toMap() {
    return {
      'cardSerial': cardSerial,
      'cpf': cpf,
      'startDate': startDate,
      'endDate': endDate,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataExtractRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataExtractRequest(
      cardSerial: map['cardSerial'],
      cpf: map['cpf'],
      startDate: map['startDate'],
      endDate: map['endDate'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataExtractRequest.fromJson(String source) =>
      TransdataExtractRequest.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransdataExtractRequest(cardSerial: $cardSerial, cpf: $cpf, startDate: $startDate, endDate: $endDate, token: $token, seq: $seq)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TransdataExtractRequest &&
        o.cardSerial == cardSerial &&
        o.cpf == cpf &&
        o.startDate == startDate &&
        o.endDate == endDate &&
        o.token == token &&
        o.seq == seq;
  }

  @override
  int get hashCode {
    return cardSerial.hashCode ^
    cpf.hashCode ^
    startDate.hashCode ^
    endDate.hashCode ^
    token.hashCode ^
    seq.hashCode;
  }
}

class TransdataExtract {
  final int errorCode;
  final String errorMessage;
  final int currentBalance;
  final List<TransdataTransaction> items;
  TransdataExtract({
    this.errorCode,
    this.errorMessage,
    this.currentBalance,
    this.items,
  });

  Map<String, dynamic> toMap() {
    return {
      'errorCode': errorCode,
      'errorMessage': errorMessage,
      'currentBalance': currentBalance,
      'Itens': items?.map((x) => x?.toMap())?.toList(),
    };
  }

  factory TransdataExtract.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    List<TransdataTransaction> items;
    if (map['Itens'] != null) {
      items = List<TransdataTransaction>.from(
        map['Itens']?.map((x) => TransdataTransaction.fromMap(x)),
      );
    }

    return TransdataExtract(
      errorCode: map['errorCode'],
      errorMessage: map['errorMessage'],
      currentBalance: map['currentBalance'],
      items: items,
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataExtract.fromJson(String source) => TransdataExtract.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransdataExtract(errorCode: $errorCode, errorMessage: $errorMessage, SaldoAtualEmCentavos: $currentBalance, Itens: $items)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TransdataExtract &&
        o.errorCode == errorCode &&
        o.errorMessage == errorMessage &&
        o.currentBalance == currentBalance &&
        o.items == items;
  }

  @override
  int get hashCode {
    return errorCode.hashCode ^ errorMessage.hashCode ^ currentBalance.hashCode ^ items.hashCode;
  }
}

class TransdataTransaction {
  final String date;
  final String description;
  final int value;
  TransdataTransaction({
    this.date,
    this.description,
    this.value,
  });

  Map<String, dynamic> toMap() {
    return {
      'Data': date,
      'Descricao': description,
      'ValorEmCentavos': value,
    };
  }

  factory TransdataTransaction.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataTransaction(
      date: map['Data'],
      description: map['Descricao'],
      value: map['ValorEmCentavos'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataTransaction.fromJson(String source) =>
      TransdataTransaction.fromMap(json.decode(source));

  @override
  String toString() =>
      'TransdataTransaction(Data: $date, Descricao: $description, ValorEmCentavos: $value)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TransdataTransaction &&
        o.date == date &&
        o.description == description &&
        o.value == value;
  }

  @override
  int get hashCode => date.hashCode ^ description.hashCode ^ value.hashCode;

  bool isCredit() {
    // TODO Validar se é essa descrição mesmo
    final List<String> creditDescriptionsList = [
      'Créditos no Cartao',
      'Créditos no Cartão',
    ];
    if (creditDescriptionsList.contains(description)) {
      return true;
    }

    return false;
  }

  String get formattedValue {
    int amount = this.value;
    if (!isCredit()) {
      amount *= -1;
    }

    return NumberUtils.formatAmount(amount);
  }
}
