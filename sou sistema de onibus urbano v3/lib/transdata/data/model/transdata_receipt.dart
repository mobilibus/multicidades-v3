import 'dart:convert';

import 'package:mobilibus/transdata/data/model/transdata_model.dart';

class TransdataReceiptRequest extends TransdataRequest {
  int id;

  TransdataReceiptRequest({
    this.id,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataReceiptRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataReceiptRequest(
      id: map['id'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataReceiptRequest.fromJson(String source) =>
      TransdataReceiptRequest.fromMap(json.decode(source));
}

class TransdataReceipt {
  int errorCode;
  String errorMessage;
  int numero;
  String serie;
  String chaveNF; //todo!
  TransdataReceipt({
    this.errorCode,
    this.errorMessage,
    this.numero,
    this.serie,
    this.chaveNF,
  });

  Map<String, dynamic> toMap() {
    return {
      'errorCode': errorCode,
      'errorMessage': errorMessage,
      'Numero': numero,
      'Serie': serie,
      'ChaveNF': chaveNF,
    };
  }

  factory TransdataReceipt.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataReceipt(
      errorCode: map['errorCode'],
      errorMessage: map['errorMessage'],
      numero: map['Numero'],
      serie: map['Serie'],
      chaveNF: map['ChaveNF'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataReceipt.fromJson(String source) =>
      TransdataReceipt.fromMap(json.decode(source));
}
