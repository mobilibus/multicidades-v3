import 'dart:convert';

import 'package:mobilibus/transdata/data/model/transdata_model.dart';

class TransdataCardBuyRequest extends TransdataRequest {
  int idBuyer;

  TransdataCardBuyRequest({
    this.idBuyer,
  });

  Map<String, dynamic> toMap() {
    return {
      'idBuyer': idBuyer,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataCardBuyRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataCardBuyRequest(
      idBuyer: map['idBuyer'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataCardBuyRequest.fromJson(String source) =>
      TransdataCardBuyRequest.fromMap(json.decode(source));
}

class TransdataCardBuyResponse {
  int errorCode;
  String errorMessage;
  List<TransdataCardBuyResume> value;
  TransdataCardBuyResponse({
    this.errorCode,
    this.errorMessage,
    this.value,
  });

  Map<String, dynamic> toMap() {
    return {
      'errorCode': errorCode,
      'errorMessage': errorMessage,
      'value': value?.map((x) => x?.toMap())?.toList(),
    };
  }

  factory TransdataCardBuyResponse.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataCardBuyResponse(
      errorCode: map['errorCode'],
      errorMessage: map['errorMessage'],
      value: List<TransdataCardBuyResume>.from(
          map['value']?.map((x) => TransdataCardBuyResume.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataCardBuyResponse.fromJson(String source) =>
      TransdataCardBuyResponse.fromMap(json.decode(source));
}

class TransdataCardBuyResume {
  String idPedido;
  String dataPedido;
  String dataStatus;
  String descricaoStatus;
  int valorTotalPedidoEmCentavos;
  String cpf;
  int numSerie;
  TransdataCardBuyResume({
    this.idPedido,
    this.dataPedido,
    this.dataStatus,
    this.descricaoStatus,
    this.valorTotalPedidoEmCentavos,
    this.cpf,
    this.numSerie,
  });

  Map<String, dynamic> toMap() {
    return {
      'IdPedido': idPedido,
      'DataPedido': dataPedido,
      'DataStatus': dataStatus,
      'DescricaoStatus': descricaoStatus,
      'ValorTotalPedidoEmCentavos': valorTotalPedidoEmCentavos,
      'Cpf': cpf,
      'NumSerie': numSerie,
    };
  }

  factory TransdataCardBuyResume.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataCardBuyResume(
      idPedido: map['IdPedido'],
      dataPedido: map['DataPedido'],
      dataStatus: map['DataStatus'],
      descricaoStatus: map['DescricaoStatus'],
      valorTotalPedidoEmCentavos: map['ValorTotalPedidoEmCentavos'],
      cpf: map['Cpf'],
      numSerie: map['NumSerie'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataCardBuyResume.fromJson(String source) =>
      TransdataCardBuyResume.fromMap(json.decode(source));
}

class TransdataNewPurchaseRequest extends TransdataRequest {
  int projectId;
  int numeroSerie;
  int taxasEmCentavos;
  String cpf;
  String iDComprador;
  String appNomeAndVersao;
  List<TransdataNewPurchaseItemRequest> items;

  TransdataNewPurchaseRequest({
    this.projectId,
    this.numeroSerie,
    this.taxasEmCentavos,
    this.cpf,
    this.iDComprador,
    this.appNomeAndVersao,
    this.items,
  });

  Map<String, dynamic> toMap() {
    return {
      'projectId': projectId,
      'NumeroSerie': numeroSerie,
      'TaxasEmCentavos': taxasEmCentavos,
      'CPF': cpf,
      'IDComprador': iDComprador,
      'AppNome_Versao': appNomeAndVersao,
      'Itens': items?.map((i) => i.toMap())?.toList(),
      'token': token,
      'seq': seq,
    };
  }

  String toJson() => json.encode(toMap());

  @override
  String toString() {
    return 'TransdataNewPurchaseRequest(projectId: $projectId, numeroSerie: $numeroSerie, taxasEmCentavos: $taxasEmCentavos, cpf: $cpf, iDComprador: $iDComprador, appNomeAndVersao: $appNomeAndVersao, items: $items)';
  }
}

class TransdataNewPurchaseItemRequest {
  final int product;
  final int valorUnitarioEmCentavos;
  final int valorCompraEmCentavos;
  TransdataNewPurchaseItemRequest({
    this.product,
    this.valorUnitarioEmCentavos,
    this.valorCompraEmCentavos,
  });

  Map<String, dynamic> toMap() {
    return {
      'Produto': product,
      'ValorUnitarioEmCentavos': valorUnitarioEmCentavos,
      'ValorCompraEmCentavos': valorCompraEmCentavos,
    };
  }

  factory TransdataNewPurchaseItemRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataNewPurchaseItemRequest(
      product: map['Produto'],
      valorUnitarioEmCentavos: map['ValorUnitarioEmCentavos'],
      valorCompraEmCentavos: map['ValorCOmpraEmCentavos'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataNewPurchaseItemRequest.fromJson(String source) =>
      TransdataNewPurchaseItemRequest.fromMap(json.decode(source));
}

class TransdataNewPurchase {
  int transactionId;
  TransdataNewPurchase({
    this.transactionId,
  });

  Map<String, dynamic> toMap() {
    return {
      'transactionId': transactionId,
    };
  }

  factory TransdataNewPurchase.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataNewPurchase(
      transactionId: map['transactionId'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataNewPurchase.fromJson(String source) =>
      TransdataNewPurchase.fromMap(json.decode(source));
}

class TransdataPurchaseRequest extends TransdataRequest {
  int transactionId;
  String ccNumber;
  String ccExpiration; //MM/YYYY
  String ccCvv;
  String ccOwner;

  TransdataPurchaseRequest({
    this.transactionId,
    this.ccNumber,
    this.ccExpiration,
    this.ccCvv,
    this.ccOwner,
  });

  Map<String, dynamic> toMap() {
    return {
      'transactionId': transactionId,
      'ccNumber': ccNumber,
      'ccExpiration': ccExpiration,
      'ccCvv': ccCvv,
      'ccOwner': ccOwner,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataPurchaseRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataPurchaseRequest(
      transactionId: map['transactionId'],
      ccNumber: map['ccNumber'],
      ccExpiration: map['ccExpiration'],
      ccCvv: map['ccCvv'],
      ccOwner: map['ccOwner'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataPurchaseRequest.fromJson(String source) =>
      TransdataPurchaseRequest.fromMap(json.decode(source));
}

class TransdataPaymentResponse {
  TransdataPaymentResponse({
    this.status,
    this.payment,
    this.chargeOnCard,
  });

  final String status;
  final TransdataPaymentResultData payment;
  final TransdataChargeOnCard chargeOnCard;

  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'payment': payment?.toMap(),
      'chargeOnCard': chargeOnCard?.toMap(),
    };
  }

  factory TransdataPaymentResponse.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataPaymentResponse(
      status: map['status'],
      payment: TransdataPaymentResultData.fromMap(map['payment']),
      chargeOnCard: TransdataChargeOnCard.fromMap(map['chargeOnCard']),
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataPaymentResponse.fromJson(String source) =>
      TransdataPaymentResponse.fromMap(json.decode(source));
}

class TransdataPaymentResultData {
  TransdataPaymentResultData({
    this.status,
    this.message,
    this.info,
    this.statusCode,
    this.fee,
  });

  final String status;
  final String message;
  final String info;
  final int statusCode;
  final int fee;

  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
      'info': info,
      'statusCode': statusCode,
      'fee': fee,
    };
  }

  factory TransdataPaymentResultData.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataPaymentResultData(
      status: map['status'],
      message: map['message'],
      info: map['info'],
      statusCode: map['statusCode'],
      fee: map['fee'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataPaymentResultData.fromJson(String source) =>
      TransdataPaymentResultData.fromMap(json.decode(source));
}

class TransdataChargeOnCard {
  TransdataChargeOnCard({
    this.status,
    this.message,
  });

  final String status;
  final String message;

  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
    };
  }

  factory TransdataChargeOnCard.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataChargeOnCard(
      status: map['status'],
      message: map['message'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataChargeOnCard.fromJson(String source) =>
      TransdataChargeOnCard.fromMap(json.decode(source));
}

class TransdataCheckPurchaseRequest extends TransdataRequest {
  int transactionId;

  TransdataCheckPurchaseRequest({
    this.transactionId,
  });

  Map<String, dynamic> toMap() {
    return {
      'transactionId': transactionId,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataCheckPurchaseRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataCheckPurchaseRequest(
      transactionId: map['transactionId'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataCheckPurchaseRequest.fromJson(String source) =>
      TransdataCheckPurchaseRequest.fromMap(json.decode(source));
}
