import 'dart:convert';

import 'package:mobilibus/transdata/data/model/transdata_model.dart';

class TransdataUserDefaultRequest extends TransdataRequest {
  int cardSerial;
  String cpf;

  TransdataUserDefaultRequest({
    this.cardSerial,
    this.cpf,
  });

  Map<String, dynamic> toMap() {
    return {
      'cardSerial': cardSerial,
      'cpf': cpf,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataUserDefaultRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataUserDefaultRequest(
      cardSerial: map['cardSerial'],
      cpf: map['cpf'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataUserDefaultRequest.fromJson(String source) =>
      TransdataUserDefaultRequest.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransdataUserDefaultBody(cardSerial: $cardSerial, cpf: $cpf, token: $token, seq: $seq)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TransdataUserDefaultRequest &&
        o.cardSerial == cardSerial &&
        o.cpf == cpf &&
        o.token == token &&
        o.seq == seq;
  }

  @override
  int get hashCode {
    return cardSerial.hashCode ^ cpf.hashCode ^ token.hashCode ^ seq.hashCode;
  }
}

class TransdataUserCpfRequest extends TransdataRequest {
  int userId;
  String cpf;

  TransdataUserCpfRequest({
    this.userId,
    this.cpf,
  });

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'cpf': cpf,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataUserCpfRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataUserCpfRequest(
      userId: map['userId'],
      cpf: map['cpf'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataUserCpfRequest.fromJson(String source) =>
      TransdataUserCpfRequest.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransdataUserCpfRequest(userId: $userId, cpf: $cpf, token: $token, seq: $seq)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TransdataUserCpfRequest &&
        o.userId == userId &&
        o.cpf == cpf &&
        o.token == token &&
        o.seq == seq;
  }

  @override
  int get hashCode {
    return userId.hashCode ^ cpf.hashCode ^ token.hashCode ^ seq.hashCode;
  }
}

class TransdataUserByIdRequest extends TransdataRequest {
  int userId;
  int cardSerial;

  TransdataUserByIdRequest({
    this.userId,
    this.cardSerial,
  });

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'cardSerial': cardSerial,
      'token': token,
      'seq': seq,
    };
  }

  String toJson() => json.encode(toMap());

  @override
  String toString() {
    return 'TransdataUserById(userId: $userId, cardSerial: $cardSerial, token: $token, seq: $seq)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TransdataUserByIdRequest &&
        o.userId == userId &&
        o.cardSerial == cardSerial &&
        o.token == token &&
        o.seq == seq;
  }

  @override
  int get hashCode {
    return userId.hashCode ^
        cardSerial.hashCode ^
        token.hashCode ^
        seq.hashCode;
  }
}

class TransdataUser {
  int errorCode;
  String errorMessage;
  int type;
  int subType;
  String typeStr;
  String subTypeStr;
  String name;
  int registerId;
  bool blockedCard;
  int logicSerial;
  String registerDate;
  int physicalSerial;
  TransdataUser({
    this.errorCode,
    this.errorMessage,
    this.type,
    this.subType,
    this.typeStr,
    this.subTypeStr,
    this.name,
    this.registerId,
    this.blockedCard,
    this.logicSerial,
    this.registerDate,
    this.physicalSerial,
  });

  TransdataUser copyWith({
    int errorCode,
    String errorMessage,
    int type,
    int subType,
    String typeStr,
    String subTypeStr,
    String name,
    int registerId,
    bool blockedCard,
    int logicSerial,
    String registerDate,
    int physicalSerial,
  }) {
    return TransdataUser(
      errorCode: errorCode ?? this.errorCode,
      errorMessage: errorMessage ?? this.errorMessage,
      type: type ?? this.type,
      subType: subType ?? this.subType,
      typeStr: typeStr ?? this.typeStr,
      subTypeStr: subTypeStr ?? this.subTypeStr,
      name: name ?? this.name,
      registerId: registerId ?? this.registerId,
      blockedCard: blockedCard ?? this.blockedCard,
      logicSerial: logicSerial ?? this.logicSerial,
      registerDate: registerDate ?? this.registerDate,
      physicalSerial: physicalSerial ?? this.physicalSerial,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'errorCode': errorCode,
      'errorMessage': errorMessage,
      'Tipo': type,
      'SubTipo': subType,
      'TipoStr': typeStr,
      'SubTipoStr': subTypeStr,
      'Nome': name,
      'CadastroId': registerId,
      'CartaoBloqueado': blockedCard,
      'SerialLogico': logicSerial,
      'DataCadastro': registerDate,
      'SerialFisico': physicalSerial,
    };
  }

  factory TransdataUser.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataUser(
      errorCode: map['errorCode'],
      errorMessage: map['errorMessage'],
      type: map['Tipo'],
      subType: map['SubTipo'],
      typeStr: map['TipoStr'],
      subTypeStr: map['SubTipoStr'],
      name: map['Nome'],
      registerId: map['CadastroId'],
      blockedCard: map['CartaoBloqueado'],
      logicSerial: map['SerialLogico'],
      registerDate: map['DataCadastro'],
      physicalSerial: map['SerialFisico'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataUser.fromJson(String source) =>
      TransdataUser.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransdataUserDefault(errorCode: $errorCode, errorMessage: $errorMessage, type: $type, subType: $subType, typeStr: $typeStr, subTypeStr: $subTypeStr, name: $name, registerId: $registerId, blockedCard: $blockedCard, logicSerial: $logicSerial, registerDate: $registerDate, physicalSerial: $physicalSerial)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TransdataUser &&
        o.errorCode == errorCode &&
        o.errorMessage == errorMessage &&
        o.type == type &&
        o.subType == subType &&
        o.typeStr == typeStr &&
        o.subTypeStr == subTypeStr &&
        o.name == name &&
        o.registerId == registerId &&
        o.blockedCard == blockedCard &&
        o.logicSerial == logicSerial &&
        o.registerDate == registerDate &&
        o.physicalSerial == physicalSerial;
  }

  @override
  int get hashCode {
    return errorCode.hashCode ^
        errorMessage.hashCode ^
        type.hashCode ^
        subType.hashCode ^
        typeStr.hashCode ^
        subTypeStr.hashCode ^
        name.hashCode ^
        registerId.hashCode ^
        blockedCard.hashCode ^
        logicSerial.hashCode ^
        registerDate.hashCode ^
        physicalSerial.hashCode;
  }
}
