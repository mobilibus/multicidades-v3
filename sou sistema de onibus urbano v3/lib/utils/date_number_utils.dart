import 'package:intl/intl.dart';

class DateMOBUtils {
  static String getExtendedDateAndTime(String date) {
    var dt = DateTime.parse(date);
    return "${DateFormat.yMMMd('pt_BR').format(dt)} às ${DateFormat.Hm().format(dt)}";
  }

  static String getShortDateAndTime(String date) {
    var dt = DateTime.parse(date);
    return "${DateFormat.yMd('pt_BR').format(dt)} ${DateFormat.Hm().format(dt)}";
  }

  static String getShortDate(String date) {
    var dt = DateTime.parse(date);
    return DateFormat.yMd('pt_BR').format(dt);
  }

  static String getExtractDefaultStartDate(String balanceDate) {
    DateFormat formatBalance = DateFormat(('yyyy-MM-dd dd:mm:ss'));
    DateFormat formatResult = DateFormat(('yyyy-MM-dd 00:00:00'));
    if (balanceDate == null || balanceDate.isEmpty) {
      balanceDate = formatBalance.format(DateTime.now());
    }
    DateTime dt = DateTime.parse(balanceDate);
    return formatResult.format(dt);
  }

  static String getExtractDefaultEndDate(String balanceDate) {
    DateFormat formatBalance = DateFormat(('yyyy-MM-dd dd:mm:ss'));
    DateFormat formatResult = DateFormat(('yyyy-MM-dd 00:00:00'));
    if (balanceDate == null || balanceDate.isEmpty) {
      balanceDate = formatBalance.format(DateTime.now());
    }
    DateTime dt = DateTime.parse(balanceDate);
    return formatResult.format(dt.add(Duration(days: 1)));
  }

  static String getNextDate(String currentDate) {
    var dt = DateTime.parse(currentDate);
    DateFormat format = DateFormat(('yyyy-MM-dd 00:00:00'));
    return format.format(dt.add(Duration(days: 1)));
  }

  static String getPreviousDate(String currentDate) {
    var dt = DateTime.parse(currentDate);
    DateFormat format = DateFormat(('yyyy-MM-dd 00:00:00'));
    return format.format(dt.subtract(Duration(days: 1)));
  }

  static bool isValidCardExpiration(String date) {
    DateTime dt;
    try {
      dt = DateFormat('dd/MM/yyyy hh:mm:ss').parse('01/$date 23:59:59');
    } catch (e) {
      return false;
    }
    return (dt != null) && dt.isAfter(DateTime.now());
  }
}

class NumberUtils {
  static String formatAmount(int amount) {
    double d = amount / 100;
    var f = NumberFormat("########.00", "pt_BR");
    return f.format(d);
  }

  static String formatAmountAsCurrency(int amount) {
    var d = amount / 100;
    var f = NumberFormat("#######0.00", "pt_BR");
    return "R\$ ${f.format(d)}";
  }
}
