import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobilibus/app/select_city_urban.dart';
import 'package:mobilibus/app/urban/app.dart';
import 'package:mobilibus/app/urban/route/urban_route.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/base/MOBProject.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Utils.dart';

class DialogUtils {
  DialogUtils(this.context) {
    _themeColor = Utils.isLightTheme(context)
        ? Colors.black
        : Utils.getColorFromPrimary(context);
  }
  final BuildContext context;
  Color _themeColor;

  void showAbout(String version) {
    String customText = '';
    if (Utils.isBus2)
      customText =
          'Este é o Bus2, uma solução desenvolvida para levar informação do transporte público em tempo real, facilitando seus deslocamentos no dia-a-dia.\n\n';
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            'Sobre nós',
            style: TextStyle(
              color: Utils.getColorByLuminanceTheme(context),
            ),
          ),
          content: Scrollbar(
            child: SingleChildScrollView(
              child: Wrap(
                children: <Widget>[
                  Text(
                    '$customText\Versão do app: $version\n\nMobilibus - Todos os direitos reservados',
                    style: TextStyle(
                      color: Utils.getColorByLuminanceTheme(context),
                    ),
                  ),
                  Divider(
                    color: Theme.of(context).accentColor,
                    thickness: 1,
                  )
                ],
              ),
            ),
          ),
          actions: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                FlatButton(
                  onPressed: () async {
                    const url =
                        'http://app.mobilibus.com/politica-de-privacidade.html';
                    if (await canLaunch(url))
                      await launch(url);
                    else
                      throw 'Could not launch $url';
                  },
                  child: Text(
                    'Política de Privacidade',
                    style: TextStyle(
                      color: Utils.getColorByLuminanceTheme(context),
                    ),
                  ),
                ),
                FlatButton(
                  onPressed: () async {
                    const url = 'http://app.mobilibus.com/termos-de-uso.html';
                    if (await canLaunch(url))
                      await launch(url);
                    else
                      throw 'Could not launch $url';
                  },
                  child: Text(
                    'Termos de uso',
                    style: TextStyle(
                      color: Utils.getColorByLuminanceTheme(context),
                    ),
                  ),
                ),
                FlatButton(
                  onPressed: () async {
                    const url = 'http://mobilibus.com.br';
                    if (await canLaunch(url))
                      await launch(url);
                    else
                      throw 'Could not launch $url';
                  },
                  child: Text(
                    'Nosso site',
                    style: TextStyle(
                      color: Utils.getColorByLuminanceTheme(context),
                    ),
                  ),
                ),
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(
                    'Fechar',
                    style: TextStyle(
                      color: Utils.getColorByLuminanceTheme(context),
                    ),
                  ),
                ),
              ],
            )
          ],
        );
      },
    );
  }

  void dialogNeedGPS() => showDialog(
      context: context,
      builder: (c) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          backgroundColor: Theme.of(c).primaryColor,
          title: Text('Localização',
              style: TextStyle(color: Utils.getColorByLuminanceTheme(context))),
          actions: <Widget>[
            FlatButton(
              child: Text('OK',
                  style: TextStyle(
                      color: Utils.getColorByLuminanceTheme(context))),
              onPressed: () => Navigator.pop(c),
            )
          ],
          content: Text(
              'Precisamos que você de permissão de GPS para obtermos sua localização e calcularmos sua rota.',
              style: TextStyle(color: Utils.getColorByLuminanceTheme(context))),
        );
      });

  void showNetError() => showDialog(
        context: context,
        builder: (c) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          backgroundColor: Theme.of(c).primaryColor,
          title: Text('Erro!'),
          content: Text(
            'Verifique sua conexão com a internet.',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(c).pop(),
              child: Text('OK',
                  style: TextStyle(
                      color: Utils.getColorByLuminanceTheme(context))),
            )
          ],
        ),
      );

  void showTripError() async => showDialog(
        context: context,
        builder: (c) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          backgroundColor: Theme.of(c).primaryColor,
          title: Text(
            'Erro!',
            style: TextStyle(
                color: Utils.getColorByLuminanceTheme(context), fontSize: 20),
          ),
          content: Text(
            'Não localizamos nenhuma opção com estes parâmetros. Por favor, refaça a sua busca.',
            style: TextStyle(
                color: Utils.getColorByLuminanceTheme(context), fontSize: 20),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(c).pop(),
              child: Text(
                'OK',
                style: TextStyle(
                    color: Utils.getColorByLuminanceTheme(context),
                    fontSize: 20),
              ),
            )
          ],
        ),
      );

  void dialogNoPast(bool isDialogOpen) => showDialog(
        context: context,
        barrierDismissible: false,
        builder: (c) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            backgroundColor: Theme.of(c).primaryColor,
            title: Text('Planejador de viagem',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context))),
            content: Text('Bus2 não é o DeLorean 🚗⏳',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context))),
            actions: <Widget>[
              FlatButton(
                child: Text('Fechar', style: TextStyle(color: _themeColor)),
                onPressed: () {
                  Navigator.of(c).pop();
                  isDialogOpen = false;
                },
              )
            ],
          );
        },
      );

  void dialogMaxFuture() => showDialog(
        context: context,
        builder: (c) {
          DateTime maxDateTime = DateTime.now().add(Duration(days: 14));
          int maxDay = maxDateTime.day;
          int month = maxDateTime.month;
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            backgroundColor: Theme.of(context).primaryColor,
            title: Text(
              'Planejador de viagem',
              style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
            ),
            content: Text(
              'Informe uma data entre hoje e dia $maxDay/$month',
              style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Fechar',
                  style:
                      TextStyle(color: Utils.getColorByLuminanceTheme(context)),
                ),
                onPressed: () => Navigator.of(c).pop(),
              )
            ],
          );
        },
      );

  void showTripNoResult() => showDialog(
        context: context,
        builder: (c) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          backgroundColor: Theme.of(context).primaryColor,
          title: Text('Erro'),
          content: Text('Nenhum resultado encontrado',
              style: TextStyle(color: Utils.getColorByLuminanceTheme(context))),
          actions: <Widget>[
            FlatButton(
                child: Text(
                  'Fechar',
                  style:
                      TextStyle(color: Utils.getColorByLuminanceTheme(context)),
                ),
                onPressed: () => Navigator.of(c).pop())
          ],
        ),
      );

  Future<int> showDialogRequestUpdate() async => await showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            'Atualização',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          content: Text(
            'Existe uma atualização disponível para a tabela de horários desta linha. Deseja atualizar?',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Sim',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              ),
              onPressed: () => Navigator.of(context).pop(1),
            ),
            FlatButton(
              child: Text(
                'Não',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              ),
              onPressed: () => Navigator.of(context).pop(0),
            ),
            FlatButton(
              child: Text(
                'Cancelar',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              ),
              onPressed: () => Navigator.of(context).pop(-1),
            ),
          ],
        ),
      );

  Future<void> timetableNeedEthernetDialog(String timetable, MOBLine line,
          bool navigate, bool isAccessibility) =>
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          backgroundColor: Utils.getColorFromPrimary(context),
          title: Text(
            'Atenção!',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          content: Text(
            'Conecte-se à internet para receber a tabela horária atualizada desta linha.',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(
                'OK',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              ),
            )
          ],
        ),
      );

  void noTimetable() => showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            'Tabela indisponível',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          content: Text(
            'Não há tabela previamente salva. Conecte-se a internet e tente novamente.',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Cancelar',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      );

  void showProjectUnavailable() => showDialog(
        context: context,
        barrierDismissible: false,
        useRootNavigator: false,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            'Serviço indisponível',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          content: Text(
            'Infelizmente não atendemos a sua região no momento. :(\nMas você pode acessar outras cidades disponíveis! :)',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Clique aqui e confira!',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => SelectCityUrban()));
              },
            ),
            FlatButton(
              child: Text(
                'Cancelar',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => MainPage(selectTab: 0)));
              },
            ),
          ],
        ),
      );

  void showRequestProjectChange(
      List<MOBProject> projects, Function(int) function) {
    List<Widget> actions = projects
        .map(
          (project) => FlatButton(
            child: Text(
              project.name,
              style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
            ),
            onPressed: () {
              Navigator.of(context).pop();
              function(project.projectId);
            },
          ),
        )
        .toList();

    actions.add(
      FlatButton(
        child: Text(
          'Cancelar',
          style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
        ),
        onPressed: () => Navigator.of(context).pop(),
      ),
    );
    showDialog(
      context: context,
      barrierDismissible: false,
      useRootNavigator: false,
      builder: (BuildContext context) => AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20))),
        backgroundColor: Theme.of(context).primaryColor,
        content: Text(
          'Você está numa cidade diferente. Deseja usar as configurações dela?',
          style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
        ),
        actions: actions,
      ),
    );
  }

  void showRequestTripToView(String shortName, List<MOBTrip> trips,
          String imageAsset, int routeId, Color lineColor) =>
      trips.length == 1
          ? {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) =>
                      UrbanRouteViewer(null, routeId, trips.first.tripId)))
            }
          : showModalBottomSheet(
              context: context,
              builder: (context) => Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            side: BorderSide(width: 2, color: lineColor),
                          ),
                          child: Container(
                            padding: EdgeInsets.all(5),
                            child: Row(
                              children: <Widget>[
                                SvgPicture.asset(imageAsset, height: 20),
                                Text('  $shortName'),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Text('Selecione a viagem desejada'),
                      ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: <Widget>[
                          ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: trips.length,
                            itemBuilder: (context, index) {
                              MOBTrip trip = trips[index];
                              String desc = ''; //trip.tripDesc;

                              if (trips[index].shortName != null) {
                                shortName = trips[index].shortName;
                                desc = '$shortName > ' + trip.tripDesc;
                              } else {
                                desc = trip.tripDesc;
                              }

                              return Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                color: Theme.of(context).primaryColor,
                                child: InkWell(
                                  onTap: () async {
                                    int tripId = trip.tripId;
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                UrbanRouteViewer(
                                                    null, routeId, tripId)));
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(10),
                                    child: Text(
                                      desc,
                                      style: TextStyle(
                                          color: Utils.getColorByLuminanceTheme(
                                              context)),
                                    ), //, style: TextStyle(color: Colors.white),),
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            );
}
