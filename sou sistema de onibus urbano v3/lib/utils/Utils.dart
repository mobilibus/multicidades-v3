import 'dart:async';
import 'dart:convert' as convert;
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_utils/google_maps_utils.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:mobilibus/accessibility/timetable/exit_timetable_accessibility.dart';
import 'package:mobilibus/app/urban/timetable/show_timetable.dart';
import 'package:mobilibus/base/MOBCharterCompany.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/base/MOBProject.dart';
import 'package:mobilibus/base/otp/OTPPlan.dart';
import 'package:mobilibus/beacon/beacon.dart';
import 'package:mobilibus/traccar/traccar.dart';
import 'package:mobilibus/utils/DialogUtils.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:twitter_api/twitter_api.dart';

enum SERVICE_DAYS {
  SUNDAY,
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY,
}

class Utils {
  static User firebaseUser;
  static BeaconService beaconService;
  static TraccarService traccarService;
  static final GoogleSignIn googleSignIn = GoogleSignIn();
  static final FirebaseAuth auth = FirebaseAuth.instance;
  static LocationData locationData;
  static List<MOBLine> lines = [];
  static MOBProject project;
  static const String twitterUser = 'bus2app';
  static bool isMobilibus = false;
  static bool fullTimetable = false;
  static bool hasTwitter = true;
  static bool isCharter = false;
  static CHARTER_TYPE charterType = CHARTER_TYPE.LOGIN;
  static const bool isMultipleCities = false;
  static const bool isBus2 = false;
  static bool showAds = false;
  static bool requestChangeProject = true;
  static int updateDelay = 20;
  static const Duration timeoutDuration = Duration(seconds: 30);
  static bool requestReview = false;
  static String iosAppStoreId = '1576792227'; //1487690526 : bus2
  static String uuid = '';
  static bool getPhoneInfo = false;
  static int globalProjectID = 0;
  static String globalNameProject = 'SOU - Sistema de Ônibus Urbano';
  static String globalStrSplash = 'assets/sou/sou_splash.webp';
  static String globalStrHeader = 'assets/sou/sou_header_$globalProjectID.webp';
  static const Color globalPrimaryColor = Color(0xffffd100);
  static const Color globalAccentColor = Color(0xff666666);
  static const Color globalUnselectedColor = Color(0xffbcbcc2);
  static const String globalApplicationId = "br.com.sou.app";
  static const String globalLandingPage = "sou";

  static bool liteMode = false;
  static bool advanced = true;

  static twitterApi mTwitterApi = twitterApi(
    consumerKey: 'VXhmIBUHz6Yn61INkjymbxZ4F',
    consumerSecret: '0BdadX8AKZGhMa4f7D2qp7txPX0GlF8gdtOOiReyeCniz7U81H',
    token: '1261049012536446979-ejOGuiM2woGO6FLvyN4oTnBJf193bT',
    tokenSecret: 'D5DheHWF8SjXZvoov8AtUy412ZH8aMp9OhgPpoGxYGlt5',
  );

  static const String adAndroidBus2 = 'ca-app-pub-8130943127451476/4665838206';
  static const String adIosBus2 = 'ca-app-pub-8130943127451476/5866897989';

  static double lastLatitude = 0.0;
  static double lastLongitude = 0.0;

  static Future<User> handleSignIn(BuildContext context) async {
    try {
      final GoogleSignInAccount googleUser = await googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      firebaseUser = (await auth.signInWithCredential(credential)).user;
      return firebaseUser;
    } catch (e) {
      if (Utils.isMobilibus)
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  content: Text(e.toString()),
                ));
    }
    return null;
  }

  static Future<void> downloadFirebasePreferences(Reference sr) async {
    Uint8List data = await sr.getData(1024 * 1024);
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (data != null) {
      String jsonString = convert.utf8.decode(data);
      Map<String, dynamic> map = convert.json.decode(jsonString);
      map.forEach((key, value) {
        if (value is bool)
          sharedPreferences.setBool(key, value);
        else if (value is int)
          sharedPreferences.setInt(key, value);
        else if (value is double)
          sharedPreferences.setDouble(key, value);
        else if (value is String)
          sharedPreferences.setString(key, value);
        else if (value is List) {
          String jsonString = convert.json.encode(value);
          sharedPreferences.setString(key, jsonString);
        }
      });
    }
  }

  static Future<void> uploadFirebasePreferences(
      Reference sr, String fileName) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final Directory directory = await getTemporaryDirectory();
    String path = directory.path;
    String fileNameJson = '$path/$fileName';
    File file = File(fileNameJson);
    file.createSync(recursive: true);

    Map<String, dynamic> json = {};
    sharedPreferences
        .getKeys()
        .forEach((key) => json[key] = sharedPreferences.get(key));

    String jsonString = convert.json.encode(json);

    file.writeAsStringSync(jsonString);

    await sr.putFile(file).whenComplete(() => null);

    file.deleteSync(recursive: true);
  }

  static String getBannerAdUnitId() =>
      Platform.isIOS ? adIosBus2 : adAndroidBus2;

  static Color getColorByLuminanceTheme(BuildContext context) =>
      isLightTheme(context) ? Colors.black : Colors.white;

  static Color getColorByLuminanceThemeSecondary(BuildContext context) =>
      isLightThemeSecondary(context) ? Colors.black : Colors.white;

  static Color getColorByLuminanceThemeReverse(BuildContext context) =>
      isLightTheme(context) ? Colors.black : Colors.white;

  static bool isLightTheme(BuildContext context) =>
      Theme.of(context).primaryColor.computeLuminance() > 0.5;

  static bool isLightThemeSecondary(BuildContext context) =>
      globalAccentColor.computeLuminance() > 0.5;

  static Color getColorByLuminance(String colorStr) {
    Color color = getColorFromHex(colorStr);
    int r = color.red;
    int g = color.green;
    int b = color.blue;
    return (((r * 299) + (g * 587) + (b * 114)) / 1000) > 140
        ? Colors.black
        : Colors.white;
  }

  static Color getColorFromHex(String stringHex) =>
      Color(int.parse('FF' + stringHex.replaceAll('#', ''), radix: 16));

  static Color getColorFromPrimary(BuildContext context) =>
      Theme.of(context).primaryColor == Colors.white
          ? Colors.black
          : Theme.of(context).primaryColor;

  static getColorFromPrimaryReverse(BuildContext context) =>
      getColorByLuminanceTheme(context) == Colors.black
          ? Colors.black
          : Colors.white;

  static Future<bool> canDownloadProjects() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var time = prefs.getInt('projects_last_download');
    int lastDownload =
        time == null ? 0 : prefs.getInt('projects_last_download');

    DateTime lastSaved = DateTime.fromMillisecondsSinceEpoch(lastDownload);
    DateTime now = DateTime.now();
    int diffHours = now.difference(lastSaved).inHours;
    bool canDownload = diffHours >= 12;
    return canDownload;
  }

  static Future<void> downloadTimetableList(MOBProject project) async {
    int projectId = Utils.project.projectId;
    http.Response response = await (Utils.fullTimetable
        ? MOBApiUtils.getTimetableFuture(projectId)
        : MOBApiUtils.getRoutesTimetableFuture());
    if (response != null) {
      String json = convert.utf8.decode(response.bodyBytes);
      Iterable iterable = convert.json.decode(json);
      Utils.lines = iterable.map((item) => MOBLine.fromJson(item)).toList();

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('timetable_$projectId', json);
    }
  }

  static Future<MOBProject> selectedProject(int projectId) async {
    projectId = globalProjectID;

    SharedPreferences prefs = await SharedPreferences.getInstance();

    http.Response response =
        await MOBApiUtils.getProjectDetails(projectId, false);

    if (response != null) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> json = convert.json.decode(jsonString);
      Utils.project = MOBProject.fromJson(json);

      MOBApiUtils.setApiUri(Utils.project.apiUri);

      Utils.lines.clear();

      prefs.setString('selectedProject', jsonString);
      prefs.setInt(
          'selectedProjectTime', DateTime.now().millisecondsSinceEpoch);

      Utils.globalStrHeader = 'assets/sou/sou_header_$projectId.webp';

      return project;
    } else
      return null;
  }

  static IconData getDepartureIconByStopType(int stopType) {
    IconData icon;
    switch (stopType) {
      case 0:
        break;
      case 1:
        icon = Icons.directions_subway;
        break;
      case 2:
        icon = Icons.directions_transit;
        break;
      case 3:
        icon = Icons.directions_bus;
        break;
      case 4:
        icon = Icons.directions_boat;
        break;
      case 5:
        break;
      case 6:
        break;
      case 7:
        break;
      default:
        icon = Icons.directions_bus;
    }
    return icon;
  }

  static String getIDByStopType(int stopType) {
    switch (stopType) {
      case 0:
        break;
      case 1:
        return 'railway 1';
        break;
      case 2:
        return 'subway 2';
        break;
      case 3:
        return 'bus 3';
        break;
      case 4:
        return 'boat 4';
        break;
      case 5:
        break;
      case 6:
        break;
      case 7:
        return 'tram 7';
        break;
      case 8:
        return 'pointOfSale';
        break;
      default:
        return 'bus default';
    }
    return '$stopType ${DateTime.now().microsecondsSinceEpoch}';
  }

  static String getDayOfTheWeek(int weekDay) {
    String dayOfWeek = '';
    switch (weekDay) {
      case 1:
        dayOfWeek = 'Seg';
        break;
      case 2:
        dayOfWeek = 'Ter';
        break;
      case 3:
        dayOfWeek = 'Qua';
        break;
      case 4:
        dayOfWeek = 'Qui';
        break;
      case 5:
        dayOfWeek = 'Sex';
        break;
      case 6:
        dayOfWeek = 'Sáb';
        break;
      case 7:
        dayOfWeek = 'Dom';
        break;
    }
    return dayOfWeek;
  }

  static String getDayOfTheWeekName(int dayOfWeek) {
    String name = '';
    switch (dayOfWeek) {
      case 1:
        name = 'MONDAY';
        break;
      case 2:
        name = 'TUESDAY';
        break;
      case 3:
        name = 'WEDNESDAY';
        break;
      case 4:
        name = 'THURDSDAY';
        break;
      case 5:
        name = 'FRIDAY';
        break;
      case 6:
        name = 'SATURDAY';
        break;
      default:
        name = 'SUNDAY';
        break;
    }
    return name;
  }

  static Future<void> selectedLineBehaviour(
      MOBLine line, bool accessibility, BuildContext context,
      [bool navigate = true]) async {
    int projectId = Utils.project.projectId;
    int routeId = line.routeId;

    String timetablePath = 'timetable_last_download_$projectId\_$routeId';
    String lastDownloadTime =
        'timetable_last_download_time_$projectId\_$routeId';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String jsonTimetable = prefs.getString(timetablePath);

    DateTime now = DateTime.now();

    var time = prefs.getInt(lastDownloadTime);
    int lastDownload = time == null ? 0 : prefs.getInt(lastDownloadTime);
    DateTime lastSaved = DateTime.fromMillisecondsSinceEpoch(lastDownload);

    int diffHours = now.difference(lastSaved).inHours;

    bool canDownload = diffHours >= 12; //12 hours
    if (canDownload) {
      http.Response response =
          await MOBApiUtils.getTimetableByRouteFuture(line.routeId);

      if (response != null) {
        String body = convert.utf8.decode(response.bodyBytes);
        Iterable list = convert.json.decode(body);
        Map<String, dynamic> map = list.first['timetable'];
        jsonTimetable = convert.json.encode(map);
        line = MOBLine.fromLine(line);

        DateTime now = DateTime.now();
        prefs.setInt(lastDownloadTime, now.millisecondsSinceEpoch);
        prefs.setString(timetablePath, jsonTimetable);
      }
    }
    if (jsonTimetable != null) {
      Map<String, dynamic> map = convert.json.decode(jsonTimetable);

      MOBTimetable newTimetable = MOBTimetable.fromJson(map);
      MOBLine newLine = MOBLine.fromLine(line);
      line = newLine;
      if (navigate) {
        if (accessibility) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      ExitTimetableAccessibility(line, newTimetable)));
        } else
          await Navigator.push(context,
              MaterialPageRoute(builder: (context) => ShowTimetable2(line)));
      }
    } else
      await DialogUtils(context).timetableNeedEthernetDialog(
          jsonTimetable, line, navigate, accessibility);
  }

  static String getOtpParamsUrl(
    DateTime dateTime,
    String otp,
    String port,
    String fromParam,
    String toParam,
    String timeParam,
    String dateParam,
    double sliderValue,
    bool arriveBy,
    bool wheelChair,
    List<OTP_MODE> modes,
  ) {
    String mode = '';
    for (final m in modes) {
      String value = m.toString().replaceAll('OTP_MODE.', '');
      mode += '$value,';
    }
    mode = mode.substring(0, mode.length - 1);

    String plan = 'otp/routers/default/plan';
    String params = 'module=planner';
    params = '$params&fromPlace=$fromParam';
    params = '$params&toPlace=$toParam';
    params = '$params&time=$timeParam';
    params = '$params&date=$dateParam';
    params = '$params&mode=$mode';
    params = '$params&maxWalkDistance=$sliderValue';
    params = '$params&arriveBy=$arriveBy';
    params = '$params&wheelchair=$wheelChair';
    params = '$params&locale=pt_BR';
    return '$otp:$port/$plan?$params';
  }

  static Future<Uint8List> bytesFromSvgAsset(
      BuildContext context, String assetName, Size size, int width) async {
    String svgString =
        await DefaultAssetBundle.of(context).loadString(assetName);
    DrawableRoot drawableRoot = await svg.fromSvgString(svgString, null);
    ui.Picture picture = drawableRoot.toPicture();
    //MediaQueryData queryData = MediaQuery.of(context);
    //double devicePixelRatio = queryData.devicePixelRatio;
    double w = size.width; // * devicePixelRatio;
    double h = size.height; // * devicePixelRatio;
    ui.Image image = await picture.toImage(w.toInt(), h.toInt());
    ByteData data = await image.toByteData(format: ui.ImageByteFormat.png);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  static String directionByDegrees(double degrees) {
    String direction = SphericalUtils.getCardinal(degrees);
    var val = ((degrees / 22.5) + 0.5).floor();
    var arr = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];
    direction = arr[(val % 8)];
    if (direction == 'N') {
      direction = 'ao norte';
    } else if (direction == 'S') {
      direction = 'ao sul';
    } else if (direction == 'E') {
      direction = 'a leste';
    } else if (direction == 'W') {
      direction = 'a oeste';
    } else if (direction == 'NE') {
      direction = 'a nordeste';
    } else if (direction == 'SE') {
      direction = 'a sudeste';
    } else if (direction == 'SW') {
      direction = 'a sudoeste';
    } else if (direction == 'NW') {
      direction = 'a noroeste';
    } else
      direction = '';

    return direction;
  }

  static Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  static Future<MOBTimetable> getTimetable(int routeId) async {
    int projectId = project.projectId;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String jsonTimetable =
        prefs.getString('timetable_last_download_$projectId\_$routeId') ?? '[]';
    Map<String, dynamic> map = convert.json.decode(jsonTimetable);
    return MOBTimetable.fromJson(map);
  }

  static String getStopTypeSvg(int stopType, bool darkColor) {
    String imageAsset = 'assets/images';
    switch (stopType) {
      case 0: //tram
        imageAsset += '/tram' + (darkColor ? '' : '_black');
        break;
      case 1: //subway
        imageAsset += '/subway' + (darkColor ? '' : '_black');
        break;
      case 2: //train
        imageAsset += '/train' + (darkColor ? '' : '_black');
        break;
      case 3: //bus
        imageAsset += '/bus' + (darkColor ? '' : '_black');
        break;
      case 4: //ferry
        imageAsset += '/boat' + (darkColor ? '' : '_black');
        break;
      case 5:
        imageAsset += '/' + (darkColor ? '' : '_black');
        break;
      case 6:
        imageAsset += '/' + (darkColor ? '' : '_black');
        break;
      case 7:
        imageAsset += '/' + (darkColor ? '' : '_black');
        break;
    }
    imageAsset += '.svg';
    return imageAsset;
  }

  static List<SERVICE_DAYS> getDaysStringfied(List<bool> days) {
    List<SERVICE_DAYS> daysStringfied = [];
    for (int i = 0; i < days.length; i++)
      if (days[i])
        switch (i) {
          case 0:
            daysStringfied.add(SERVICE_DAYS.SUNDAY);
            break;
          case 1:
            daysStringfied.add(SERVICE_DAYS.MONDAY);
            break;
          case 2:
            daysStringfied.add(SERVICE_DAYS.TUESDAY);
            break;
          case 3:
            daysStringfied.add(SERVICE_DAYS.WEDNESDAY);
            break;
          case 4:
            daysStringfied.add(SERVICE_DAYS.THURSDAY);
            break;
          case 5:
            daysStringfied.add(SERVICE_DAYS.FRIDAY);
            break;
          case 6:
            daysStringfied.add(SERVICE_DAYS.SATURDAY);
            break;
        }
    return daysStringfied;
  }

  static String getModeName(OTP_MODE otpMode) {
    String modeName;
    switch (otpMode) {
      case OTP_MODE.WALK:
        modeName = 'A Pé';
        break;
      case OTP_MODE.BICYCLE:
        modeName = 'Bicicleta';
        break;
      case OTP_MODE.TRAM:
        modeName = 'VLT';
        break;
      case OTP_MODE.SUBWAY:
        modeName = 'Metrô';
        break;
      case OTP_MODE.RAIL:
        modeName = 'Trem';
        break;
      case OTP_MODE.TRAINISH:
        modeName = 'Sem trem';
        break;
      case OTP_MODE.BUS:
        modeName = 'Ônibus';
        break;
      case OTP_MODE.FERRY:
        modeName = 'Balsa';
        break;
      case OTP_MODE.TRANSIT:
        modeName = 'Qualquer transporte público';
        break;
      case OTP_MODE.LEG_SWITCH:
        modeName = 'Troca';
        break;
      case OTP_MODE.CAR:
        modeName = 'Carro';
        break;
      case OTP_MODE.CABLE_CAR:
        modeName = 'Bondinho';
        break;
      case OTP_MODE.GONDOLA:
        modeName = 'Gôndola';
        break;
      case OTP_MODE.FUNICULAR:
        modeName = 'Funicular';
        break;
      case OTP_MODE.BUSISH:
        modeName = 'Sem ônibus';
        break;
      case OTP_MODE.CUSTOM_MOTOR_VEHICLE:
        modeName = 'Veículo personalizado';
        break;
    }
    return modeName;
  }

  static IconData getModeIcon(OTP_MODE otpMode) {
    IconData modeIcon;
    switch (otpMode) {
      case OTP_MODE.WALK:
        modeIcon = Icons.directions_walk;
        break;
      case OTP_MODE.BICYCLE:
        modeIcon = Icons.directions_bike;
        break;
      case OTP_MODE.TRAM:
        modeIcon = Icons.tram;
        break;
      case OTP_MODE.SUBWAY:
        modeIcon = Icons.subway;
        break;
      case OTP_MODE.RAIL:
        modeIcon = Icons.directions_railway;
        break;
      case OTP_MODE.TRAINISH:
        modeIcon = Icons.train;
        break;
      case OTP_MODE.BUS:
        modeIcon = Icons.directions_bus;
        break;
      case OTP_MODE.FERRY:
        modeIcon = Icons.car_repair;
        break;
      case OTP_MODE.TRANSIT:
        modeIcon = Icons.directions_transit;
        break;
      case OTP_MODE.LEG_SWITCH:
        modeIcon = Icons.transfer_within_a_station;
        break;
      case OTP_MODE.CAR:
        modeIcon = Icons.car_rental;
        break;
      case OTP_MODE.CABLE_CAR:
        modeIcon = Icons.directions_railway;
        break;
      case OTP_MODE.GONDOLA:
        modeIcon = Icons.directions_boat;
        break;
      case OTP_MODE.FUNICULAR:
        modeIcon = Icons.directions_railway;
        break;
      case OTP_MODE.BUSISH:
        modeIcon = Icons.directions_bus;
        break;
      case OTP_MODE.CUSTOM_MOTOR_VEHICLE:
        modeIcon = Icons.car_rental;
        break;
    }
    return modeIcon;
  }
}
