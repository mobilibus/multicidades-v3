import 'dart:async';
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/utils/Utils.dart';

class SPMetroWidget extends StatefulWidget {
  _SPMetroWidget createState() => _SPMetroWidget();
}

class _IntegrationSP {
  final String atualizacao;
  final String codigo;
  final String cor;
  final String descricao;
  final String estacao1;
  final String estacao2;
  final String geracao;
  final int id;
  final String mensagemSituacao;
  final String nome;
  final String posicao;
  final String statusOperacao;
  final String tipo;
  final String titulo;

  _IntegrationSP(
    this.atualizacao,
    this.codigo,
    this.cor,
    this.descricao,
    this.estacao1,
    this.estacao2,
    this.geracao,
    this.id,
    this.mensagemSituacao,
    this.nome,
    this.posicao,
    this.statusOperacao,
    this.tipo,
    this.titulo,
  );

  factory _IntegrationSP.fromJson(Map<String, dynamic> json) => _IntegrationSP(
        json['Atualizacao'] ?? null,
        json['Codigo'] ?? null,
        json['Cor'] ?? null,
        json['Descricao'] ?? null,
        json['Estacao1'] ?? null,
        json['Estacao2'] ?? null,
        json['Geracao'] ?? null,
        json['Id'] ?? 0,
        json['MensagemSituacao'] ?? null,
        json['Nome'] ?? null,
        json['Posicao'] ?? null,
        json['StatusOperacao'] ?? null,
        json['Tipo'] ?? null,
        json['Title'] ?? null,
      );
}

class _SPMetroWidget extends State<SPMetroWidget> {
  String viamobilidade =
      'https://www.viamobilidade.com.br/_vti_bin/SituacaoService.svc/GetAllSituacao';
  List<_IntegrationSP> integrationSPlist = [];
  Timer timer;
  int seconds = 30;
  bool isGrid = false;

  @override
  void dispose() {
    if (timer != null) timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 1), (t) async {
      seconds <= 0 ? updateTram() : seconds--;
      setState(() {});
    });
    updateTram();
  }

  void updateTram() async {
    http.Response response =
        await http.get(viamobilidade).timeout(Utils.timeoutDuration);
    if (response != null) {
      String json = convert.utf8.decode(response.bodyBytes);
      Iterable iterable = convert.json.decode(json);
      integrationSPlist =
          iterable.map((model) => _IntegrationSP.fromJson(model)).toList();

      integrationSPlist.sort((isp1, isp2) {
        int code1 = int.parse(isp1.codigo);
        int code2 = int.parse(isp2.codigo);
        return code1.compareTo(code2);
      });

      seconds = 30;
      setState(() {});
    }
  }

  String getTypeAsset(String type) {
    String asset = 'assets/sp';
    switch (type.toLowerCase()) {
      case 'c':
        asset = '$asset/sp_cptm.jpg';
        break;
      case 'viaquatro':
        asset = '$asset/sp_viaquatro.png';
        break;
      case 'viamobilidade':
        asset = '$asset/sp_viamobilidade.jpg';
        break;
      case 'metro':
        asset = '$asset/sp_metro.jpg';
        break;
    }
    return asset;
  }

  Color getTypeColor(String color) {
    Color colorBg;
    switch (color) {
      case 'azul':
        colorBg = Color(0xff285083);
        break;
      case 'verde':
        colorBg = Color(0xff006d58);
        break;
      case 'vermelha':
        colorBg = Color(0xffdf3f31);
        break;
      case 'amarela':
        colorBg = Color(0xffffd400);
        break;
      case 'lilas':
        colorBg = Color(0xff8c3583);
        break;
      case 'rubi':
        colorBg = Color(0xffa8034f);
        break;
      case 'diamante':
        colorBg = Color(0xffa39e8c);
        break;
      case 'esmeralda':
        colorBg = Color(0xff00aa9e);
        break;
      case 'turquesa':
        colorBg = Color(0xff00829b);
        break;
      case 'coral':
        colorBg = Color(0xfff55f1a);
        break;
      case 'safira':
        colorBg = Color(0xff1c146b);
        break;
    }
    return colorBg;
  }

  Color getStatusColor(String status) {
    Color color;
    switch (status.toLowerCase()) {
      case 'operação normal':
        color = Colors.green;
        break;
      case 'operação parcial':
      case 'velocidade reduzida':
      case 'operação diferenciada':
        color = Colors.yellow;
        break;
      case 'operação encerrada':
      case 'paralisada':
      case 'operação paralisada':
        color = Colors.red;
        break;
    }
    return color;
  }

  String getEmojiStatus(String status) {
    String emoji;
    switch (status.toLowerCase()) {
      case 'operação normal':
        emoji = '😎';
        break;
      case 'operação parcial':
      case 'velocidade reduzida':
      case 'operação diferenciada':
        emoji = '⚠️';
        break;
      case 'operação encerrada':
      case 'paralisada':
      case 'operação paralisada':
        emoji = '🚫';
        break;
    }
    return emoji;
  }

  Widget getWidgetMetro(BuildContext context, int index) {
    _IntegrationSP isp = integrationSPlist[index];

    /*
          String update = isp.atualizacao;
          String generated = isp.geracao;
          int id = isp.id;
          String name = isp.nome;
          String position = isp.posicao;
          String description = isp.descricao;
          String title = isp.titulo;
          */

    String code = isp.codigo;
    String status = isp.statusOperacao;
    String type = code == '5' ? 'viamobilidade' : isp.tipo;

    String color = isp.cor;
    String station1 = isp.estacao1.toString().replaceAll('null', '');
    String station2 = isp.estacao2.toString().replaceAll('null', '');
    String msg = isp.mensagemSituacao.toString().replaceAll('null', '');

    String asset = getTypeAsset(type);
    Color colorBg = getTypeColor(color);
    //Color statusColor = getStatusColor(status);

    if (code == '13')
      colorBg = Color(0xff00b052);
    else if (code == '15')
      colorBg = Color(0xffc0c0c0);
    else if (code == '17') colorBg = Color(0xffefba00);

    return ListTile(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.asset(
                asset,
                height: 30,
              ),
              Container(
                padding: EdgeInsets.only(
                  left: 10,
                ),
                child: Text(
                  '   $code   ',
                  style: TextStyle(
                    color: colorBg != null
                        ? Utils.getColorByLuminance(
                            colorBg.value.toRadixString(16))
                        : Colors.black,
                    backgroundColor: colorBg,
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
          Text(
            getEmojiStatus(status),
            textAlign: TextAlign.end,
            style: TextStyle(fontSize: 30),
          ),
        ],
      ),
      subtitle: Text('$msg $station1 $station2\n$status'),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget present = isGrid
        ? GridView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 20, bottom: 20),
            physics: NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 2.0),
            itemCount: integrationSPlist.length,
            itemBuilder: (context, index) => Card(
                  elevation: 10.0,
                  child: getWidgetMetro(context, index),
                ))
        : ListView.separated(
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 20, bottom: 20),
            physics: NeverScrollableScrollPhysics(),
            separatorBuilder: (context, index) => Divider(thickness: 3),
            itemCount: integrationSPlist.length,
            itemBuilder: (context, index) => getWidgetMetro(context, index),
          );
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Direto do Metrô'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(isGrid ? Icons.view_list : Icons.grid_on),
            onPressed: () => setState(() => isGrid = !isGrid),
          ),
          Row(
            children: <Widget>[
              Text('$seconds\s...'),
              Icon(Icons.timer),
            ],
          )
        ],
      ),
      body: Scrollbar(
        child: SingleChildScrollView(child: present),
      ),
    );
  }
}
