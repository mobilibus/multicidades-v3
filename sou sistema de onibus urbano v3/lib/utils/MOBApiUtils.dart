import 'dart:convert' as convert;
import 'dart:math';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_utils/google_maps_utils.dart';
import 'package:http/http.dart' as http;
import 'package:imei_plugin/imei_plugin.dart';
import 'package:location/location.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/base/otp/OTPGeocode.dart';
import 'package:mobilibus/base/otp/OTPPlan.dart';
import 'package:mobilibus/base/otp/OTPReverseGeocode.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:permission_handler/permission_handler.dart' as ph;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sim_info/sim_info.dart';
import 'package:wifi_info_flutter/wifi_info_flutter.dart' as wi;

bool readCustomHeader = false;

abstract class MOBApiUtils {
  static String mobBase = 'https://mobilibus.com/api/';
  static String mobBaseDev =
      'https://1zsojhwd0k.execute-api.us-east-1.amazonaws.com/dev/';

  static Map<String, String> headers = {
    'Content-Type': 'application/json',
    'Connection': 'keep-alive',
    'Accept': 'application/json',
    'Cache-Control': 'no-cache',
    'Accept-Encoding': 'gzip, deflate, br',
    'x-mob-package': '',
    'x-mob-version': '',
    'x-mob-build': '',
    'x-mob-project-id': '',
    'x-mob-uuid': '',
    'x-mob-device-name': '',
    'x-mob-os-name': '',
  };

  static void setApiUri(String uri) {
    mobBase = uri;
  }

  static Future<Map<String, String>> customHeader(String event,
      [Map<String, String> data]) async {
    Map<String, dynamic> mH = convert.json.decode(convert.json.encode(headers));
    Map<String, String> h = {'x-mob-app-event': event};
    if (data != null)
      data.forEach((key, value) => h['x-mob-app-$key'] = '$value');

    mH.forEach((key, value) => h['$key'] = '$value');

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token') ?? null;

    if (token != null) h['x-mob-phone-fcm'] = token;

    try {
      Connectivity c = Connectivity();
      Map<ph.Permission, ph.PermissionStatus> map =
          await [ph.Permission.phone].request();

      ph.PermissionStatus permissionStatus = map[ph.Permission.phone];

      bool hasPhonePermission = permissionStatus.isGranted;
      if (hasPhonePermission) {
        ConnectivityResult connectivityResult = await c.checkConnectivity();

        switch (connectivityResult) {
          case ConnectivityResult.wifi:
            wi.WifiInfo wifiInfo = wi.WifiInfo();
            h['x-mob-phone-connection-mode'] = 'wifi';
            h['x-mob-phone-wifi-bssid'] = await wifiInfo.getWifiBSSID();
            h['x-mob-phone-wifi-ip'] = await wifiInfo.getWifiIP();
            h['x-mob-phone-wifi-name'] = await wifiInfo.getWifiName();

            switch (await wifiInfo.getLocationServiceAuthorization()) {
              case wi.LocationAuthorizationStatus.notDetermined:
                h['x-mob-phone-wifi-gps'] = 'not determined';
                break;
              case wi.LocationAuthorizationStatus.restricted:
                h['x-mob-phone-wifi-gps'] = 'restricted';
                break;
              case wi.LocationAuthorizationStatus.denied:
                h['x-mob-phone-wifi-gps'] = 'denied';
                break;
              case wi.LocationAuthorizationStatus.authorizedAlways:
                h['x-mob-phone-wifi-gps'] = 'authorized always';
                break;
              case wi.LocationAuthorizationStatus.authorizedWhenInUse:
                h['x-mob-phone-wifi-gps'] = 'authorized when in use';
                break;
              case wi.LocationAuthorizationStatus.unknown:
                h['x-mob-phone-wifi-gps'] = 'unknown';
                break;
            }

            break;
          case ConnectivityResult.mobile:
            h['x-mob-phone-connection-mode'] = 'mobile';
            break;
          case ConnectivityResult.none:
            h['x-mob-phone-connection-mode'] = 'none';
            break;
        }

        String allowsVOIP = await SimInfo.getAllowsVOIP;
        String carrierName = await SimInfo.getCarrierName;
        String isoCountryCode = await SimInfo.getIsoCountryCode;
        String mobileCountryCode = await SimInfo.getMobileCountryCode;
        String mobileNetworkCode = await SimInfo.getMobileNetworkCode;

        Map<String, String> sim = {
          'x-mob-phone-allows-VOIP': allowsVOIP,
          'x-mob-phone-carrier-name': carrierName,
          'x-mob-phone-iso-country-code': isoCountryCode,
          'x-mob-phone-mobile-country-code': mobileCountryCode,
          'x-mob-phone-mobile-network-code': mobileNetworkCode,
        };

        h.addAll(sim);

        h['x-mob-phone-imei'] = await ImeiPlugin.getImei();
        h['x-mob-phone-uuid'] = await ImeiPlugin.getId();
      }
    } catch (e) {}

    LocationData locationData = Utils.locationData;
    if (locationData != null) {
      double accuracy = locationData.accuracy;
      double latitude = locationData.latitude;
      double longitude = locationData.longitude;

      h['x-mob-phone-gps-accuracy'] = '$accuracy';
      h['x-mob-phone-gps-latitude'] = '$latitude';
      h['x-mob-phone-gps-longitude'] = '$longitude';
    }

    return h;
  }

  static String getUrl(String path) {
    return '${Utils.isMobilibus ? mobBaseDev : mobBase}$path';
  }

  static Future<void> mobEvent(String event,
      [Map<String, dynamic> data]) async {
    Map<String, String> mHeaders = await customHeader(event);

    String jsonBody = convert.json.encode(
      {
        'event': event,
        if (data != null) 'data': data,
      },
    );

    String url = getUrl('send-event');

    await http
        .post(url, body: jsonBody, headers: mHeaders)
        .timeout(Utils.timeoutDuration);

    readCustomHeader = true;
  }

  static Future<http.Response> loginCharter(String login, int agencyId) async {
    try {
      Map<String, dynamic> map = {'matricula': login, 'agency_id': agencyId};
      String json = convert.json.encode(map);

      Map<String, String> mHeaders = await customHeader('charter login');

      String url = getUrl('charter-login');

      return http
          .post(
            url,
            body: json,
            headers: mHeaders,
          )
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> getAgencies(int projectId,
      [bool isCompanySelectOnly = false]) async {
    try {
      Map<String, String> mHeaders = await customHeader('charter get agencies');

      String url =
          getUrl('agencies?project_id=$projectId&routes=$isCompanySelectOnly');

      return http.get(url, headers: mHeaders).timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> getProjectDetails(
      int projectId, bool isCharter) async {
    try {
      String urbanCharter = isCharter ? 'charter' : 'urban';

      Map<String, String> mHeaders =
          await customHeader('get $urbanCharter project details');

      String url = getUrl('project-details?project_id=$projectId');

      return await http
          .get(
            url,
            headers: mHeaders,
          )
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> getUrbanProjects() async {
    try {
      Map<String, String> mHeaders = await customHeader('get urban projects');

      bool isDev = Utils.isMobilibus;
      String url = getUrl('projects?dev=$isDev');

      return await http
          .get(
            url,
            headers: mHeaders,
          )
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> getCharterProjects() async {
    try {
      Map<String, String> mHeaders = await customHeader('get charter projects');

      bool isDev = Utils.isMobilibus;
      String url = getUrl('projects?charter=true&dev=$isDev');

      return await http
          .get(
            url,
            headers: mHeaders,
          )
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> getTimetableFuture(int projectId) async {
    try {
      headers['x-mob-project-id'] = '$projectId';

      Map<String, String> mHeaders =
          await customHeader('get complete timetable');

      String url = getUrl('timetable?project_id=$projectId');

      return await http
          .get(
            url,
            headers: mHeaders,
          )
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> getTimetableByRouteFuture(int routeId) async {
    try {
      int projectId = Utils.project.projectId;
      headers['x-mob-project-id'] = '$projectId';

      Map<String, String> mHeaders =
          await customHeader('get timetable by routeId');

      String url = getUrl('timetable?project_id=$projectId&route_id=$routeId');

      return await http
          .get(
            url,
            headers: mHeaders,
          )
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> getRoutesTimetableFuture() async {
    try {
      int projectId = Utils.project.projectId;
      headers['x-mob-project-id'] = '$projectId';

      Map<String, String> mHeaders =
          await customHeader('get routes by projectId');

      String url = getUrl('routes?project_id=$projectId');

      return await http
          .get(url, headers: mHeaders)
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> getGeocodeFuture(
      String text, String otpUri) async {
    try {
      String geocode = 'geocode?address=$text';
      text = text.replaceAll(' ', '+');
      String url = '$otpUri/otp/$geocode';

      Map<String, String> mHeaders = await customHeader('geocode');

      // FROM REQUEST TO URL
      return await http
          .get(url, headers: mHeaders)
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> getStopsFuture(String request) async {
    try {
      Map<String, String> mHeaders = await customHeader('get stops');

      return await http
          .get(request, headers: mHeaders)
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return e;
    }
  }

  static Future<OTPGeocode> getGeocode(dynamic value, String otpUri) async {
    http.Response response = await getGeocodeFuture(value, otpUri);
    String body = convert.utf8.decode(response.bodyBytes);
    Map<String, dynamic> map = convert.json.decode(body);
    OTPGeocode geocode = OTPGeocode.fromJson(map);
    return geocode;
  }

  static Future<List<OTPGeocodeResult>> suggestionCallback(
      String value, String otp) async {
    OTPGeocode geocode = await getGeocode(value, otp);
    return Future<List<OTPGeocodeResult>>.value(geocode.results);
  }

  static Future<List<MOBLine>> downloadRoutesTimetable() async {
    List<MOBLine> lines = [];
    var response = await getRoutesTimetableFuture();
    if (response != null) {
      int projectId = Utils.project.projectId;

      String body = convert.utf8.decode(response.bodyBytes);
      Iterable list = convert.json.decode(body);
      lines = list.map((model) => MOBLine.fromJson(model)).toList();
      DateTime now = DateTime.now();
      SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.setString('timetable_$projectId', body);
      prefs.setInt(
          'timetable_last_download_$projectId', now.millisecondsSinceEpoch);
    }
    return lines;
  }

  static String getStopsInViewUrl(GMULatLngBounds bounds, [List<int> tripIds]) {
    Point ne = bounds.northEast;
    Point sw = bounds.southWest;
    String neLat = ne.x.toStringAsFixed(6);
    String neLng = ne.y.toStringAsFixed(6);
    String swLat = sw.x.toStringAsFixed(6);
    String swLng = sw.y.toStringAsFixed(6);

    String baseUrl = getUrl('stops-in-view');
    String neParam = 'ne=$neLat,$neLng';
    String swParam = 'sw=$swLat,$swLng';

    String params = '$neParam&$swParam';

    if (tripIds != null && tripIds.isNotEmpty) {
      params += '&trip_ids=';
      tripIds.forEach((id) {
        String concat = id == tripIds.last ? '' : ',';
        params += '$id$concat';
        return params;
      });
    }

    String url = '$baseUrl?$params';
    return url;
  }

  static Future<http.Response> getDepartures(int stopId,
      {int routeId, int tripId, bool isBubbleOverlayOpen}) async {
    try {
      String url = getUrl('departures?stop_id=$stopId');

      if (routeId != null && tripId != null)
        url += '&route_id=$routeId&trip_id=$tripId';

      Map<String, String> mHeaders = await customHeader('departures');

      if (isBubbleOverlayOpen != null)
        mHeaders['x-mob-app-isBubbleOverlayOpen'] = '$isBubbleOverlayOpen';

      return await http
          .get(url, headers: mHeaders)
          .timeout(Utils.timeoutDuration);
    } on Exception {
      return null;
    }
  }

  static Future<http.Response> getNewsFuture(int projectId) async {
    try {
      headers['x-mob-project-id'] = '$projectId';

      Map<String, String> mHeaders = await customHeader('news');

      String url = getUrl('news?project_id=$projectId');

      return await http
          .get(
            url,
            headers: mHeaders,
          )
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> getAlertsFuture(int projectId) async {
    try {
      headers['x-mob-project-id'] = '$projectId';

      Map<String, String> mHeaders = await customHeader('alerts');

      String url = getUrl('alerts?project_id=$projectId');

      return await http
          .get(
            url,
            headers: mHeaders,
          )
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }

  static Future<http.Response> sendMessage(
      int projectId, String body, BuildContext context) async {
    try {
      headers['x-mob-project-id'] = '$projectId';

      Map<String, String> mHeaders = await customHeader('send message');

      String url = getUrl('send-message');

      return await http
          .post(
            url,
            headers: mHeaders,
            body: body,
          )
          .catchError(
            (onError) => showDialog(
              context: context,
              builder: (dialogContext) => AlertDialog(
                title: Text('Sucesso!'),
                content: Text('Recebemos sua mensagem, obrigado!'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Fechar'),
                    onPressed: () {
                      Navigator.of(dialogContext).pop();
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ),
            ),
          );
    } on Exception {
      return null;
    }
  }

  static Future<String> getReverseGeocodingDescrition(
      [double latitude, double longitude]) async {
    String otpUri = Utils.project.otpUri;

    String base = '$otpUri/otp/geocode/reverse';

    Map<String, String> mHeaders = await customHeader('reverse geocode');

    LocationData locationData = Utils.locationData;

    double lat = latitude == null ? locationData.latitude : latitude;
    double lng = longitude == null ? locationData.longitude : longitude;

    String strLat = lat.toStringAsFixed(4);
    String strLng = lng.toStringAsFixed(4);
    String url = '$base?lat=$strLat&lon=$strLng';
    try {
      http.Response response =
          await http.get(url, headers: mHeaders).timeout(Utils.timeoutDuration);
      if (response != null) {
        String body = convert.utf8.decode(response.bodyBytes);
        Map<String, dynamic> json = convert.json.decode(body);
        OTPReverseGeocode reverseGeocode = OTPReverseGeocode.fromJson(json);
        if (reverseGeocode.results.isNotEmpty)
          return reverseGeocode.results.first.description;
      }
    } catch (e) {}

    return null;
  }

  static Future<OTPPlan> getTripplanner(
    Point from,
    Point to,
    bool isArriveBy,
    String dateParam,
    String timeParam, {
    int maxWalkDistance = 1000,
    bool wheelchair = false,
    List<OTP_MODE> modes = const [
      OTP_MODE.WALK,
      OTP_MODE.TRANSIT,
    ],
  }) async {
    String fromParam = from.x.toString() + ',' + from.y.toString();
    String toParam = to.x.toString() + ',' + to.y.toString();

    String mode = '';
    for (final m in modes) {
      String value = m.toString().replaceAll('OTP_MODE.', '');
      mode += '$value,';
    }
    mode = mode.substring(0, mode.length - 1);

    String otpUri = Utils.project.otpUri;
    String plan = 'otp/routers/default/plan';
    String base = '$otpUri/$plan';
    String params = 'module=planner';
    params = '$params&fromPlace=$fromParam';
    params = '$params&toPlace=$toParam';
    params = '$params&date=$dateParam';
    params = '$params&time=$timeParam';
    params = '$params&mode=$mode';
    params = '$params&maxWalkDistance=$maxWalkDistance';
    params = '$params&arriveBy=$isArriveBy';
    params = '$params&wheelchair=$wheelchair';
    params = '$params&locale=pt_BR';

    String request = '$base?$params';

    Map<String, String> mHeaders = await customHeader('tripplanner');

    http.Response response = await http
        .get(request, headers: mHeaders)
        .timeout(Utils.timeoutDuration);
    if (response != null) {
      String body = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> planner = convert.json.decode(body);
      OTPPlan plan = planner.containsKey('plan')
          ? OTPPlan.fromJson(planner['plan'], '', body)
          : null;

      Map<String, dynamic> event = {
        'arriveBy': isArriveBy,
        'date': dateParam,
        'time': timeParam,
        'from': {
          'lat': from.x,
          'lng': from.y,
        },
        'to': {
          'lat': to.x,
          'lng': to.y,
        },
      };
      mobEvent('tripplanner', event);

      return plan;
    }
    return null;
  }

  static Future<http.Response> getTripDetailsFuture(int tripId) async {
    try {
      Map<String, String> mHeaders = await customHeader('get trip details');

      String url = getUrl('trip-details?trip_id=$tripId');

      return await http
          .get(
            url,
            headers: mHeaders,
          )
          .timeout(Utils.timeoutDuration);
    } on Exception {
      return null;
    }
  }

  static Future<http.Response> getPOSFuture() async {
    try {
      int projectId = Utils.project.projectId;

      Map<String, String> mHeaders = await customHeader('points of sale');

      String url = getUrl('points-of-sale?project_id=$projectId');

      return await http
          .get(
            url,
            headers: mHeaders,
          )
          .timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }
}
