import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/base/otp/OTPPlan.dart';
import 'package:mobilibus/app/urban/tripplaner/trip_planner.dart';

import 'Utils.dart';

abstract class TripplanerUtils {
  static Future<void> decodePlan(
    OTPPlan plan,
    List<List<Widget>> tripDetails,
    List<Widget> listItineraries,
    List<OTPItinerary> itineraries,
    BuildContext context,
  ) async {
    List<OTPItinerary> mItineraries = plan.itineraries;
    Color color = Utils.isLightTheme(context)
        ? Colors.black
        : Utils.getColorFromPrimary(context);

    timeStartGB.clear();
    timeEndGB.clear();
    durationDistanceGB.clear();
    for (final itinerary in mItineraries)
    {

      itineraries.add(itinerary);
      List<OTPLeg> legs = itinerary.legs;

      DateTime startDateTime =
          DateTime.fromMillisecondsSinceEpoch(itinerary.startTime);
      DateTime endDateTime =
          DateTime.fromMillisecondsSinceEpoch(itinerary.endTime);

      String timeStart =
          DateFormat('HH:mm').format(startDateTime).replaceAll(':', 'h');
      String timeEnd =
          DateFormat('HH:mm').format(endDateTime).replaceAll(':', 'h');

      //String dayOfWeek = Utils.getDayOfTheWeek(startDateTime.weekday);

      int duration = itinerary.duration ~/ 60;
      double distance = 0.0;
      Icon legIcon;
      Widget legText;
      List<Widget> mLegs = [];
      for (final leg in legs) {
        distance += leg.distance;
        OTP_MODE mode = leg.mode;
        bool realtime = leg.realTime ?? false;

        switch (mode) {
          case OTP_MODE.WALK:
            legIcon = Icon(Icons.directions_walk, color: color);
            String duration = (leg.duration ~/ 60).toString();
            if (int.parse(duration) == 0) duration = '<1';
            legText = Container(
              padding: EdgeInsets.only(top: 5, bottom: 5),
              child: Text('$duration\min'),
            );
            break;
          case OTP_MODE.CAR:
          case OTP_MODE.CABLE_CAR:
          case OTP_MODE.GONDOLA:
          case OTP_MODE.FUNICULAR:
          case OTP_MODE.BUSISH:
          case OTP_MODE.CUSTOM_MOTOR_VEHICLE:
          case OTP_MODE.BUS:
          case OTP_MODE.SUBWAY:
          case OTP_MODE.TRAINISH:
          case OTP_MODE.BICYCLE:
          case OTP_MODE.RAIL:
          case OTP_MODE.TRAM:
          case OTP_MODE.FERRY:
          case OTP_MODE.TRANSIT:
          case OTP_MODE.LEG_SWITCH:
            String textColorStr = leg.routeTextColor;
            String backgroundColorStr = leg.routeColor;
            String route = leg.route;
            Color textColor = Color(
                int.parse('FF' + textColorStr.replaceAll('#', ''), radix: 16));
            Color backgroundColor = Color(int.parse(
                'FF' + backgroundColorStr.replaceAll('#', ''),
                radix: 16));

            IconData mIcon;
            if (mode == OTP_MODE.BUS)
              mIcon = Icons.directions_bus;
            else if (mode == OTP_MODE.SUBWAY)
              mIcon = Icons.directions_subway;
            else
              mIcon = Icons.directions_railway;

            legIcon = Icon(mIcon, color: color);

            legText = Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0))),
              color: backgroundColor,
              child: Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Text(route, style: TextStyle(color: textColor)),
              ),
            );
            break;
        }

        Widget legItem = Wrap(
          crossAxisAlignment: WrapCrossAlignment.end,
          children: <Widget>[
            Column(
              children: [
                if (realtime)
                  Image.asset(
                    'assets/rt.gif',
                    width: 20,
                  )
                else
                  Container(
                    height: 20,
                    width: 20,
                  ),
                legIcon,
                legText,
              ],
            ),
            if (legs.indexOf(leg) < legs.length - 1)
              Container(
                padding: EdgeInsets.only(bottom: 20),
                child: Icon(Icons.navigate_next),
              ),
          ],
        );
        mLegs.add(legItem);
      }

      distance /= 1000.0;
      String strDistance = distance.toStringAsFixed(0).replaceAll('.', ',');

      TextStyle textStyle = TextStyle(fontSize: 20);

      timeStartGB.add(timeStart);
      timeEndGB.add(timeEnd);
      durationDistanceGB.add('$duration\min - $strDistance\km');

      List<Widget> tripDetailsWidgets = <Widget>[
        Row(
          children: [
            Text(timeStart, style: textStyle),
            Icon(Icons.chevron_right),
            Text(timeEnd, style: textStyle),
          ],
        ),
        Text('$duration\min - $strDistance\km', style: textStyle),
      ];

      tripDetails.add(tripDetailsWidgets);
      Widget itineraryWidget = Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: tripDetailsWidgets,
          ),
          Wrap(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 10),
                child: Wrap(
                  crossAxisAlignment: WrapCrossAlignment.end,
                  children: mLegs,
                ),
              ),
            ],
          )
        ],
      );
      listItineraries.add(itineraryWidget);
    }
  }
}
