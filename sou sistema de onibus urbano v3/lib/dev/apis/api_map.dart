import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as gmaps;
import 'package:google_maps_utils/google_maps_utils.dart';

class ApiMap extends StatefulWidget {
  ApiMap(this.trueReturnBoundsFalseReturnsLatLng);
  final bool trueReturnBoundsFalseReturnsLatLng;
  _ApiMap createState() => _ApiMap(trueReturnBoundsFalseReturnsLatLng);
}

class _ApiMap extends State<ApiMap> {
  _ApiMap(this.trueReturnBoundsFalseReturnsLatLng) : super();
  final bool trueReturnBoundsFalseReturnsLatLng;
  GMULatLngBounds bounds;
  gmaps.LatLng center;
  gmaps.GoogleMapController controller;
  gmaps.CameraPosition cameraPosition;
  double zoom = 0;
  double radius = 1000;

  void onMapCreated(onMapCreated) {
    controller = onMapCreated;
    Completer<gmaps.GoogleMapController>().complete(controller);
    rootBundle
        .loadString('assets/maps_style_default.json')
        .then((style) => onMapCreated.setMapStyle(style));
    setState(() {});
  }

  void onCameraMove(onCameraMove) {
    cameraPosition = onCameraMove;
    zoom = onCameraMove.zoom;
    center = cameraPosition.target;
    controller.getVisibleRegion().then((onValue) {
      double latitude = onCameraMove.target.latitude;
      double longitude = onCameraMove.target.longitude;
      bounds = SphericalUtils.toBounds(latitude, longitude, radius);
      setState(() {});
    });
  }

  Widget detailsCard() {
    String neLat = bounds.northEast.x.toStringAsFixed(6);
    String neLng = bounds.northEast.y.toStringAsFixed(6);

    String swLat = bounds.southWest.x.toStringAsFixed(6);
    String swLng = bounds.southWest.y.toStringAsFixed(6);

    String distance = '';
    if (radius < 1000)
      distance = '$radius (metros)';
    else
      distance = '${radius ~/ 1000} (km)';

    return Container(
      padding: EdgeInsets.all(5),
      child: Card(
        elevation: 5,
        child: Container(
          height: 150,
          padding: EdgeInsets.all(5),
          child: Column(
            children: <Widget>[
              Text('Raio: $distance'),
              Slider(
                max: 5000,
                min: 500,
                value: radius,
                onChanged: (value) {
                  radius = double.parse(value.toStringAsFixed(0));
                  setState(() {});
                },
              ),
              Text('Zoom: $zoom'),
              Text('NE: $neLat, $neLng\nSW: $swLat, $swLng'),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Set<gmaps.Marker> markers = Set.from([
      if (cameraPosition != null && bounds != null)
        gmaps.Marker(
          markerId: gmaps.MarkerId(bounds.hashCode.toString()),
          position: cameraPosition.target,
        )
    ]);
    Set<gmaps.Circle> circles = Set.from([
      if (cameraPosition != null && bounds != null)
        gmaps.Circle(
          circleId: gmaps.CircleId(bounds.hashCode.toString()),
          center: cameraPosition.target,
          radius: radius,
        )
    ]);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.pop(
            context, trueReturnBoundsFalseReturnsLatLng ? bounds : center),
        child: Icon(Icons.done),
      ),
      body: Stack(
        children: <Widget>[
          gmaps.GoogleMap(
            initialCameraPosition:
                gmaps.CameraPosition(target: gmaps.LatLng(0.0, 0.0)),
            markers: markers,
            circles: circles,
            compassEnabled: false,
            mapToolbarEnabled: false,
            myLocationEnabled: false,
            myLocationButtonEnabled: false,
            trafficEnabled: false,
            zoomControlsEnabled: false,
            onMapCreated: onMapCreated,
            onCameraMove: onCameraMove,
          ),
          if (bounds != null) detailsCard(),
        ],
      ),
    );
  }
}
