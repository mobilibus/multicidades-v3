import 'dart:async';
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NegativeDepartures extends StatefulWidget {
  _NegativeDepartures createState() => _NegativeDepartures();
}

class _NegativeDepartures extends State<NegativeDepartures> {
  List<Map<String, dynamic>> data = [];
  List<String> dates = [];

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), getItems);
  }

  void getItems() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    Iterable<String> keys = sp
        .getKeys()
        .where((element) => element.contains('negative_departures_seconds_'));
    for (final k in keys) {
      String date = k
          .split('negative_departures_seconds_')
          .last
          .replaceAll('T', ' ')
          .split('.')
          .first;
      DateFormat df = DateFormat('yyyy-MM-dd HH:mm:ss');
      DateFormat mDf = DateFormat('dd/MM/yyyy HH:mm:ss');
      dates.add(mDf.format(df.parse(date)));
      String json = sp.getString(k) ?? '[]';
      Iterable i = convert.json.decode(json);
      data.addAll(i.map((e) => e as Map<String, dynamic>).toList());
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Partidas Negativas'),
        actions: [
          IconButton(
            icon: Icon(Icons.delete),
            onPressed: () async {
              SharedPreferences sp = await SharedPreferences.getInstance();
              Iterable<String> keys = sp.getKeys().where((element) =>
                  element.contains('negative_departures_seconds_'));
              for (final k in keys) await sp.remove(k);
              dates.clear();
              data.clear();
              getItems();
            },
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          Map<String, dynamic> mData = data[index];
          String shortName = mData['shortName'];
          String longName = mData['longName'];
          int stopId = mData['stopId'];

          List times = mData['times'];
          return ListTile(
            title: Text('stopId: $stopId\n$shortName - $longName'),
            subtitle: ListView.builder(
              itemCount: times.length,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                Map<String, dynamic> time = times[index];
                int seconds = time['seconds'];
                return Text('Segundos: $seconds');
              },
            ),
          );
        },
      ),
    );
  }
}
