import 'dart:async';
import 'dart:convert' as convert;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_utils/google_maps_utils.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/base/MOBProject.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/dev/departuresdev/widgets/departures_view_dev.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class DevDepartures extends StatefulWidget {
  _DevDepartures createState() => _DevDepartures();
}

class _DevDepartures extends State<DevDepartures> {
  GoogleMapController googleMapController;
  CameraPosition cameraPosition;

  List<SelectableProject> projects = [];
  bool showProjects = true;
  List<Marker> markersList = [];
  LatLngBounds visibleBounds;
  double zoom = 0.0;
  bool isNextDay = false;
  TextEditingController tfLat = TextEditingController(text: '0.0');
  TextEditingController tfLng = TextEditingController(text: '0.0');

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController tecProject = TextEditingController();
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      try {
        http.Response response = await MOBApiUtils.getUrbanProjects();
        if (response != null) {
          String jsonString = convert.utf8.decode(response.bodyBytes);
          Iterable iterable = convert.json.decode(jsonString);
          projects =
              iterable.map((e) => SelectableProject.fromJson(e)).toList();
          projects.sort((a, b) => a.name.compareTo(b.name));
          setState(() {});
        }
      } catch (e) {}
    });
  }

  void mapMoveEnded() async {
    visibleBounds = await googleMapController.getVisibleRegion();
    GMULatLngBounds mBounds = GMULatLngBounds(
        Point(visibleBounds.northeast.latitude,
            visibleBounds.northeast.longitude),
        Point(visibleBounds.southwest.latitude,
            visibleBounds.southwest.longitude));

    Utils.lastLatitude =
        double.parse(cameraPosition.target.latitude.toStringAsFixed(6));
    Utils.lastLongitude =
        double.parse(cameraPosition.target.longitude.toStringAsFixed(6));

    String url = MOBApiUtils.getStopsInViewUrl(mBounds);

    http.Response response = await MOBApiUtils.getStopsFuture(url);
    String body = convert.utf8.decode(response.bodyBytes);
    Iterable list = convert.json.decode(body);
    List<MOBStop> stops = list.map((model) => MOBStop.fromJson(model)).toList();

    stops.removeWhere((stop) => stop == null);
    stops.removeWhere((stop) => stop.stopType == 8);

    for (final stop in stops) {
      LatLng latLng = LatLng(stop.latitude, stop.longitude);
      Marker m = markerBuild(stop, latLng);
      markersList.add(m);
    }
    setState(() {});
  }

  Marker markerBuild(MOBStop stop, LatLng latLng) {
    int stopType = stop.stopType;
    String idStopType = Utils.getIDByStopType(stopType);
    return Marker(
      markerId: MarkerId(stop.hashCode.toString() + idStopType),
      position: latLng,
      onTap: () async {
        if (stopType != 8) showDepartures(stop);
        await googleMapController.moveCamera(CameraUpdate.newLatLng(latLng));
      },
    );
  }

  void showDepartures(MOBStop stop) async {
    DepartureWidgetDev dpView = DepartureWidgetDev(stop);
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) => Container(
        height: MediaQuery.of(context).size.height - kToolbarHeight,
        child: dpView,
      ),
    );
  }

  void onMapCreated(onMapCreatead) async {
    googleMapController = onMapCreatead;
    Completer<GoogleMapController>().complete(googleMapController);
    await rootBundle
        .loadString('assets/maps_style_default.json')
        .then((style) => googleMapController.setMapStyle(style));
  }

  void onCameraMove(CameraPosition cameraPosition) {
    zoom = cameraPosition.zoom;
    this.cameraPosition = cameraPosition;
    setState(() {});
  }

  void onCameraIdle() {
    if (zoom >= 15.0) mapMoveEnded();
    setState(() {});
  }

  Widget cardGoToLatLng() => Container(
        child: Card(
          child: Container(
            padding: EdgeInsets.all(2),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(2),
                  width: MediaQuery.of(context).size.width / 3,
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Latitude'),
                    controller: tfLat,
                    keyboardType: TextInputType.numberWithOptions(
                        decimal: true, signed: true),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(2),
                  width: MediaQuery.of(context).size.width / 3,
                  child: TextField(
                    decoration: InputDecoration(labelText: 'Longitude'),
                    controller: tfLng,
                    keyboardType: TextInputType.numberWithOptions(
                        decimal: true, signed: true),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.gps_fixed),
                  onPressed: () async {
                    try {
                      double lat =
                          double.parse(tfLat.text.replaceAll(',', '.'));
                      double lng =
                          double.parse(tfLng.text.replaceAll(',', '.'));

                      LatLng latLng = LatLng(lat, lng);

                      await googleMapController.animateCamera(
                          CameraUpdate.newLatLngZoom(latLng, 20));
                    } catch (e) {}
                  },
                ),
              ],
            ),
          ),
        ),
      );

  void goToProject(SelectableProject urban) async {
    if (!showProjects) return;
    http.Response response =
        await MOBApiUtils.getProjectDetails(urban.projectId, false);
    if (response != null) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> json = convert.json.decode(jsonString);
      Utils.project = MOBProject.fromJson(json);
      double lat = Utils.project.latitude;
      double lng = Utils.project.longitude;
      LatLng latLng = LatLng(lat, lng);
      googleMapController.animateCamera(CameraUpdate.newLatLngZoom(latLng, 10));
      showProjects = false;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Marker> markers = zoom < 15.0 ? [] : markersList;
    Set<Marker> markersSet = Set<Marker>.of(markers);

    List<SelectableProject> projects = List.of(this.projects);
    String text = tecProject.text.toLowerCase();
    if (text.isNotEmpty) {
      projects?.removeWhere((project) {
        String name = project.name.toLowerCase();
        String city = project.city.toLowerCase();
        String country = project.country.toLowerCase();
        return !name.contains(text) &&
            !city.contains(text) &&
            !country.contains(text);
      });
      if (projects.length == 1) {
        SelectableProject urban = projects.first;
        goToProject(urban);
      }
    }

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: showProjects
            ? TextField(
                decoration: InputDecoration(hintText: 'Filtrar'),
                controller: tecProject,
                onChanged: (_) => setState(() {}),
              )
            : Text('Dev Departures'),
        actions: <Widget>[
          IconButton(
            icon: Icon(!showProjects ? Icons.location_city : Icons.block),
            onPressed: () => setState(
              () {
                tecProject.clear();
                showProjects = !showProjects;
              },
            ),
          ),
        ],
      ),
      body: Stack(
        alignment: Alignment.topRight,
        children: <Widget>[
          GoogleMap(
            markers: markersSet,
            zoomControlsEnabled: false,
            myLocationButtonEnabled: false,
            buildingsEnabled: false,
            mapToolbarEnabled: false,
            myLocationEnabled: false,
            compassEnabled: false,
            rotateGesturesEnabled: false,
            trafficEnabled: false,
            onMapCreated: onMapCreated,
            onCameraMove: onCameraMove,
            onCameraIdle: onCameraIdle,
            initialCameraPosition: CameraPosition(target: LatLng(0.0, 0.0)),
          ),
          if (!showProjects)
            cardGoToLatLng()
          else
            Container(
              padding: EdgeInsets.only(right: 5, top: 5, bottom: 80),
              width: 320,
              child: Card(
                elevation: 10,
                color: Colors.amber,
                child: Container(
                  padding: EdgeInsets.only(left: 5, right: 5),
                  child: ListView.builder(
                    padding: EdgeInsets.only(bottom: 5, top: 5),
                    itemCount: projects.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      SelectableProject urban = projects[index];
                      String name = urban.name;
                      String city = urban.city;
                      return Card(
                        elevation: 5,
                        child: InkWell(
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              '$name - $city',
                              textAlign: TextAlign.center,
                            ),
                          ),
                          onTap: () => goToProject(urban),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
