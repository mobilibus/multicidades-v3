import 'package:clipboard_manager/clipboard_manager.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesList extends StatefulWidget {
  _SharedPreferencesList createState() => _SharedPreferencesList();
}

class _SharedPreferencesList extends State<SharedPreferencesList> {
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      prefs = await SharedPreferences.getInstance();
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    Set<String> keys = prefs
            ?.getKeys()
            ?.where((element) => !element.startsWith('_'))
            ?.toSet() ??
        Set<String>();

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, () => setState(() {})),
        ),
        title: Text('Configurações salvas'),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.delete),
        onPressed: () async {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          await prefs.clear();
          setState(() {});
        },
      ),
      body: Scrollbar(
        child: ListView.builder(
          padding: EdgeInsets.only(bottom: 100),
          itemCount: keys.length,
          itemBuilder: (context, index) {
            String key = keys.elementAt(index);
            String content = prefs.get(key).toString();
            return Container(
              padding: EdgeInsets.all(10),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: ListTile(
                    title: Text(key),
                    subtitle: Text(content.length > 100
                        ? content.substring(0, 99)
                        : content),
                    onTap: () => showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        title: Text(key),
                        content: Container(
                          height: 100,
                          width: 320,
                          child: ListView(
                            children: <Widget>[
                              Text(content),
                            ],
                          ),
                        ),
                        actions: <Widget>[
                          RaisedButton(
                            child: Text('Copiar'),
                            onPressed: () =>
                                ClipboardManager.copyToClipBoard(content),
                          ),
                          RaisedButton(
                            child: Text('Apagar'),
                            onPressed: () {
                              prefs.remove(key);
                              Navigator.of(context).pop();
                              setState(() {});
                            },
                          ),
                          RaisedButton(
                            child: Text('Fechar'),
                            onPressed: () => Navigator.of(context).pop(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
