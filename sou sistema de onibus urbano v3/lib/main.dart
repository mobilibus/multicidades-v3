import 'dart:async';
import 'dart:convert';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'dart:developer';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_utils/google_maps_utils.dart';
import 'package:http/http.dart';
import 'package:location/location.dart';
import 'package:mobilibus/accessibility/main_accessibility.dart';
import 'package:mobilibus/app/urban/app.dart';
import 'package:mobilibus/app/urban/more/information/information.dart';
import 'package:mobilibus/base/MOBProject.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/intro/UrbanIntro.dart';
import 'package:mobilibus/traccar/traccar.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:mobilibus/utils/widgets/NotificationWebView.dart';
import 'package:package_info/package_info.dart';
import 'package:permission_handler/permission_handler.dart' as ph;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'app/urban/more/recharge/new_transport_card_page.dart';
import 'package:mobilibus/transdata/data/purchase_repository.dart';
import 'package:mobilibus/transdata/data/transdata_api.dart';

import 'app/urban/save_load_projectId_local/save_load_projectId_local.dart';
import 'choose_city.dart';

final String strSplash = Utils.globalStrSplash;

FirebaseMessaging _firebaseMessaging;
BuildContext _context;

PurchaseRepository _purchaseRepository;

PurchaseRepository getPurchaseRepositoryInstance() {
  if (_purchaseRepository == null) {
    _purchaseRepository = PurchaseRepository(TransdataApiImp());
  }

  return _purchaseRepository;
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SharedPreferences prefs = await SharedPreferences.getInstance();

  Utils.liteMode = prefs.getBool('urban_settings_lite') ?? false;
  Utils.advanced = prefs.getBool('urban_settings_advanced') ?? true;

  await Firebase.initializeApp();
  _firebaseMessaging = FirebaseMessaging();

  String emergencyKey = 'emergencyClear1';
  bool emergencyClear = prefs.getBool(emergencyKey) ?? false;
  if (!emergencyClear) {
    prefs.clear();
    prefs.setBool(emergencyKey, true);
  }

  bool isDark = false; //sharedPreferences.getBool('isDark') ?? false;
  Brightness brightness = isDark ? Brightness.dark : Brightness.light;

  Color primary = Utils.globalPrimaryColor;
  Color accent = Utils.globalAccentColor;

  ThemeData themeData = ThemeData(
    fontFamily: GoogleFonts.openSans().fontFamily,
    primaryColor: primary,
    brightness: brightness,
    colorScheme: ColorScheme.fromSwatch().copyWith(secondary: accent),
  );

  //bool acceptedApp = sharedPreferences.getBool('acceptedApp') ?? false;

  MaterialApp materialApp = MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: themeData,
    home: UrbanIntro(),
    title: Utils.globalNameProject,
    routes: {
      '/new-transport-card': (_) => NewTransportCardPage(),
    },
  );

  //Location()
  //.onLocationChanged
  //.listen((locationData) => Utils.locationData = locationData);

  //Utils.beaconService = BeaconService();
  Utils.traccarService = TraccarService();

  if (prefs.containsKey('selectedProject')) {
    String projectSaved = prefs.getString('selectedProject');
    Map<String, dynamic> map = json.decode(projectSaved);
    Utils.project = MOBProject.fromJson(map);

    http.Response response =
        await MOBApiUtils.getProjectDetails(Utils.project.projectId, false);

    if (response != null) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> json = convert.json.decode(jsonString);
      Utils.project = MOBProject.fromJson(json);

      prefs.setString('selectedProject', jsonString);
      prefs.setInt(
          'selectedProjectTime', DateTime.now().millisecondsSinceEpoch);

      MOBApiUtils.setApiUri(Utils.project.apiUri);
    }
  }

  Map<String, dynamic> map = MOBApiUtils.headers;

  ///
  /// Load device informations
  ///

  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  PackageInfo packageInfo = await PackageInfo.fromPlatform();

  map['x-mob-package'] = packageInfo.packageName;
  map['x-mob-version'] = packageInfo.version;
  map['x-mob-build'] = packageInfo.buildNumber;
  if (Platform.isAndroid) {
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    Utils.uuid = androidInfo.androidId;
    String brand = androidInfo.brand;
    String board = androidInfo.board;
    String device = androidInfo.device;
    map['x-mob-uuid'] = Utils.uuid;
    map['x-mob-device-name'] = '$brand - $board - $device';
    map['x-mob-os-name'] = 'android';
  } else if (Platform.isIOS) {
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    Utils.uuid = iosInfo.identifierForVendor;
    map['x-mob-device-name'] = iosInfo.utsname.machine;
    map['x-mob-os-name'] = 'ios';
  }
  map['x-mob-uuid'] = Utils.uuid;

  runApp(materialApp);

  Admob.initialize();
}

class Splash extends StatefulWidget {
  State<StatefulWidget> createState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    debugPaintSizeEnabled = false;
    return _Splash();
  }
}

class _Splash extends State<Splash> {
  bool connectivityResultGB = false;
  bool isProgress = false;
  bool actInternet = false;

  @override
  void initState() {
    MOBApiUtils.mobEvent('splash');
    super.initState();
    SaveLoadSharedLocalStorageViewModel().getProjectId();

    Future.delayed(Duration(), () async {
      _getActiveInternet();

      if (Platform.isAndroid) {
        try {
          await [
            ph.Permission.location,
            ph.Permission.locationAlways,
            ph.Permission.locationWhenInUse,
            ph.Permission.accessMediaLocation,
            ph.Permission.phone,
          ].request();
        } catch (e) {}
      }

      MediaQuery.of(context).accessibleNavigation
          ? loadApp()
          : Future.delayed(Duration(seconds: 2), () => loadApp());
      fcmSetup(context);
    });
  }

  void _saveActInternet() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setBool('actInternet', true);
    } catch (e) {
      print('erro');
    }
  }

  void _getActiveInternet() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      actInternet = prefs.getBool('actInternet');
    } catch (e) {
      print('erro');
    }

    if (actInternet == null) {
      actInternet = false;
    }

    print('erro');
  }

  void loadApp() async {
    bool hasPermission =
        await Location().hasPermission() == PermissionStatus.granted;

    var data = Utils.locationData;
    double lat = -19.747257; //data.latitude;
    double lng = -47.942001; //data.longitude;
    GMULatLngBounds bounds = SphericalUtils.toBounds(lat, lng, 750);
    String url = MOBApiUtils.getStopsInViewUrl(bounds);

    var connectivityResult = await (Connectivity().checkConnectivity());

    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      if (!actInternet) {
        _saveActInternet();
      }

      if (Utils.globalProjectID == 0) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ChooseCity()));
      }
      Response response = await MOBApiUtils.getStopsFuture(url);
      if (response != null) {
        String jsonString = utf8.decode(response.bodyBytes);
        Iterable iterable = json.decode(jsonString);
        List<MOBStop> stops =
            iterable.map((stop) => MOBStop.fromJson(stop)).toList();
        if (stops.isNotEmpty) {
          MOBStop stop = stops.firstWhere((stop) => stop.stopType != 8);
          int projectId =
              Utils.globalProjectID; //stop.projectId; //ID DO PROJETO
          Utils.project = await Utils.selectedProject(projectId);
        }
      }
      if (Utils.globalProjectID != 0) {
        loadUrban();
      }
    } else {
      setState(() {
        connectivityResultGB = true;
      });
    }
  }

  void loadUrban() {
    MOBApiUtils.headers['x-mob-project-id'] =
        Utils.project.projectId.toString();
    Widget w = MediaQuery.of(context).accessibleNavigation
        ? MainPageAccessibility()
        : MainPage(selectTab: 0);
    log("Estamos usando aqui tbm oh: Main LOAD YRBNA()");
    Navigator.pushAndRemoveUntil(
        context, MaterialPageRoute(builder: (context) => w), (r) => false);
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          alignment: Alignment.bottomRight,
          children: [
            if (connectivityResultGB && actInternet)
              AlertDialog(
                backgroundColor: Colors.white, //Theme.of(context).primaryColor,
                title: Row(
                  children: <Widget>[
                    Icon(Icons.wifi_off_outlined,
                        color: Colors
                            .black /*Utils.getColorByLuminanceTheme(context)*/),
                    Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text('Você está sem Internet',
                            style: TextStyle(
                                color: Colors
                                    .black) //TextStyle(color: Utils.getColorByLuminanceTheme(context)),
                            )),
                  ],
                ),
                content: Text(
                  'Deseja visualizar a tabela Offline de seus favoritos?',
                  style: TextStyle(
                      color: Colors
                          .black /*Utils.getColorByLuminanceTheme(context)*/),
                ),
                actions: <Widget>[
                  FlatButton(
                      child: Text('Não, Obrigado',
                          style: TextStyle(
                              color: Colors
                                  .black /*Utils.getColorByLuminanceTheme(context)*/)),
                      onPressed: () {
                        setState(() {
                          connectivityResultGB = false;
                          isProgress = true;
                        });

                        MOBApiUtils.headers['x-mob-project-id'] = Utils
                            .globalProjectID
                            .toString(); //Utils.project.projectId.toString();
                        Widget w = MediaQuery.of(context).accessibleNavigation
                            ? MainPageAccessibility()
                            : MainPage(selectTab: 0);

                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => w));
                      }),
                  FlatButton(
                      child: Text('Ok',
                          style: TextStyle(
                              color: Colors
                                  .black /*Utils.getColorByLuminanceTheme(context)*/)),
                      onPressed: () {
                        setState(() {
                          connectivityResultGB = false;
                          isProgress = true;
                        });

                        MOBApiUtils.headers['x-mob-project-id'] = Utils
                            .globalProjectID
                            .toString(); //Utils.project.projectId.toString();
                        Widget w = MediaQuery.of(context).accessibleNavigation
                            ? MainPageAccessibility()
                            : MainPage(selectTab: 2);
                        log("Está indo para o \n${MediaQuery.of(context).accessibleNavigation}\, se for false então e o MainPage(2)");
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => w));
                      }),
                ],
              ),

            if (connectivityResultGB && !actInternet)
              AlertDialog(
                backgroundColor: Colors.white, //Theme.of(context).primaryColor,
                title: Row(
                  children: <Widget>[
                    Icon(Icons.wifi_off_outlined,
                        color: Colors
                            .black /*Utils.getColorByLuminanceTheme(context)*/),
                    Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text('Você está sem Internet',
                            style: TextStyle(
                                color: Colors
                                    .black) //TextStyle(color: Utils.getColorByLuminanceTheme(context)),
                            )),
                  ],
                ),
                content: Text(
                  'Hum.. .Vejo aqui que você não tem uma conexão ativa à Internet no momento. ' +
                      'Infelizmente é necessário para o primeiro acesso. ' +
                      'Volte quando estiver com uma conexão ativa à internet.',
                  style: TextStyle(
                      color: Colors
                          .black /*Utils.getColorByLuminanceTheme(context)*/),
                ),
                actions: <Widget>[
                  /*
                  FlatButton(
                      child: Text(
                          'Não, Obrigado',
                          style:
                          TextStyle(color: Colors.black/*Utils.getColorByLuminanceTheme(context)*/)),
                      onPressed: (){

                        setState(() {
                          connectivityResultGB = false;
                          isProgress = true;
                        });

                        MOBApiUtils.headers['x-mob-project-id'] = Utils.globalProjectID.toString();//Utils.project.projectId.toString();
                        Widget w = MediaQuery.of(context).accessibleNavigation
                            ? MainPageAccessibility()
                            : MainPage(0);

                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => w));
                      }
                  ),
                  */
                  FlatButton(
                      child: Text('Ok',
                          style: TextStyle(
                              color: Colors
                                  .black /*Utils.getColorByLuminanceTheme(context)*/)),
                      onPressed: () {
                        SystemChannels.platform
                            .invokeMethod('SystemNavigator.pop');
/*
                        setState(() {
                          connectivityResultGB = false;
                          isProgress = true;
                        });

                        MOBApiUtils.headers['x-mob-project-id'] = Utils.globalProjectID.toString();//Utils.project.projectId.toString();
                        Widget w = MediaQuery.of(context).accessibleNavigation
                            ? MainPageAccessibility()
                            : MainPage(2);

                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => w));
                        */
                      }),
                ],
              ),

            if (isProgress)
              Center(
                child: CircularProgressIndicator(
                    backgroundColor: Theme.of(context).primaryColor),
              ),

            //(Utils.isCharter)
            /*
             Container(
                padding: EdgeInsets.all(10),
                child: Image.asset(
                  'assets/header/metrocard_header.webp',
                  width: MediaQuery.of(context).size.width / 1.5,
                ),
              ),
            */
          ],
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage(strSplash),
          ),
        ),
      ),
    );
  }

  Future<void> fcmSetup(BuildContext context) async {
    _context = context;
    try {
      bool isIOS = Theme.of(context).platform == TargetPlatform.iOS;
      if (isIOS) {
        _firebaseMessaging?.requestNotificationPermissions(
            IosNotificationSettings(sound: true, badge: true, alert: true));
        _firebaseMessaging.onIosSettingsRegistered
            ?.listen((IosNotificationSettings settings) {});
      }

      _firebaseMessaging?.configure(
        onLaunch: onLaunchMessageHandler,
        onMessage: onMessageMessageHandler,
        onResume: onResumeMessageHandler,
        onBackgroundMessage: isIOS ? null : onBackgroundMessageHandler,
      );
      String token = await _firebaseMessaging.getToken();
      (await SharedPreferences.getInstance()).setString('token', token);
      await _firebaseMessaging.setAutoInitEnabled(true);
    } on Exception {}
  }

  Widget alertDialogRoute() => AlertDialog(
        backgroundColor: Theme.of(context).primaryColor,
        title: Row(
          children: <Widget>[
            Icon(Icons.wifi_off_outlined,
                color: Utils.getColorByLuminanceTheme(context)),
            Container(
                padding: EdgeInsets.only(left: 10),
                child: Text(
                  'Você está sem Internet.',
                  style:
                      TextStyle(color: Utils.getColorByLuminanceTheme(context)),
                )),
          ],
        ),
        content: Text(
          'Deseja visualizar a tabela Offline de seus favoritos?',
          style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context))),
            onPressed: () {}, //=> Navigator.of(context).push(MaterialPageRoute(
            //builder: (context) => UrbanRouteViewer(null, routeId, tripId))),
          ),
          FlatButton(
            child: Text('Não, Obrigado.',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context))),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
      );
}

Future<dynamic> onBackgroundMessageHandler(Map<String, dynamic> message) async {
  if (message.containsKey('data')) {
    final dynamic data = message['data'];
    print("onBackgroundMessageMessageHandler data: $data");
  }

  if (message.containsKey('notification')) {
    final dynamic notification = message['notification'];
    print("onBackgroundMessageMessageHandler notification: $notification");
  }
  return message;
}

Future<dynamic> onLaunchMessageHandler(var message) async {
  loadPush(message);
  return message;
}

Future<dynamic> onMessageMessageHandler(var message) async {
  return message;
}

Future<dynamic> onResumeMessageHandler(var message) async {
  loadPush(message);
  return message;
}

void loadPush(var message) async {
  var data = message['data'];
  if (data.containsKey('redDot')) {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool('redDot', true);
  }
  if (data.containsKey('projectId')) {
    Future.delayed(Duration(seconds: 1), () async {
      int projectId = int.parse(data['projectId']);
      String url = data['url'] ?? null;
      String urlName = data['url_name'] ?? null;
      int mProjectId = Utils?.project ?? 0;
      if (projectId == mProjectId) {
        if (url != null && url.contains('https://'))
          Navigator.push(
              _context,
              MaterialPageRoute(
                  builder: (context) => NotificationWebView(urlName, url)));
        else if (await canLaunch(url))
          launch(url);
        else
          Navigator.push(
              _context, MaterialPageRoute(builder: (context) => Information()));
      }
    });
  }
}
