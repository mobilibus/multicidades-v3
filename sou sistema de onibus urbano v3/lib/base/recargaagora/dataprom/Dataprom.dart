class Dataprom {
  final List<DatapromTicketing> ticketings;

  Dataprom(this.ticketings);

  factory Dataprom.fromJson(Map<String, dynamic> json) {
    List<DatapromTicketing> ticketings = json.containsKey('bilhetagens')
        ? (json['bilhetagens'] as Iterable)
            .map((e) => DatapromTicketing.fromJson(e))
            .toList()
        : [];
    return Dataprom(ticketings);
  }
}

class DatapromTicketing {
  final String id;
  final String name;
  final DatapromCity city;
  final String cardNumberRegex;
  final List<DatapromTransportCardType> transportCardTypes;

  DatapromTicketing(this.id, this.name, this.city, this.cardNumberRegex,
      this.transportCardTypes);

  factory DatapromTicketing.fromJson(Map<String, dynamic> json) {
    String id = json['codigo'] ?? '-1';
    String name = json['nome'] ?? '';
    DatapromCity city = json.containsKey('cidade')
        ? DatapromCity.fromJson(json['cidade'])
        : null;
    String cardNumberRegex = json['regexNumeroCartao'] ?? '';
    List<DatapromTransportCardType> transportCardTypes =
        json.containsKey('tiposCartaoTransporte')
            ? (json['tiposCartaoTransporte'] as Iterable)
                .map((e) => DatapromTransportCardType.fromJson(e))
                .toList()
            : [];
    return DatapromTicketing(
        id, name, city, cardNumberRegex, transportCardTypes);
  }
}

class DatapromCity {
  final String id;
  final String name;
  final String stateInitials;
  final String countryInitials;

  DatapromCity(this.id, this.name, this.stateInitials, this.countryInitials);

  factory DatapromCity.fromJson(Map<String, dynamic> json) {
    String id = json['codigo'] ?? '-1';
    String name = json['nome'] ?? '';
    String stateInitials = json['siglaEstado'] ?? '';
    String countryInitials = json['sigraPais'] ?? '';

    return DatapromCity(id, name, stateInitials, countryInitials);
  }
}

class DatapromTransportCardType {
  final String id;
  final String name;

  DatapromTransportCardType(this.id, this.name);

  factory DatapromTransportCardType.fromJson(Map<String, dynamic> json) {
    String id = json['codigo'] ?? '-1';
    String name = json['nome'] ?? '';
    return DatapromTransportCardType(id, name);
  }
}
