enum INFORMATION_TYPE {
  NEWS,
  ALERTS,
}

enum ALERT_EFFECT {
  NO_SERVICE,
  REDUCED_SERVICE,
  SIGNIFICANT_DELAYS,
  DETOUR,
  ADDITIONAL_SERVICE,
  MODIFIED_SERVICE,
  OTHER_EFFECT,
  UNKNOWN_EFFECT,
  STOP_MOVED,
  MISSING_EFFECT,
}

class MOBInformation {
  final int informationId;
  final int activeFrom;
  final int activeTo;
  final int timezoneOffset;

  MOBInformation(
      this.informationId, this.activeFrom, this.activeTo, this.timezoneOffset);
}

class MOBNews extends MOBInformation {
  final int newsId;
  final String title;
  final String content;
  final int activeFrom;
  final int activeTo;
  final int timezoneOffset;

  MOBNews(this.newsId, this.title, this.content, this.activeFrom, this.activeTo,
      this.timezoneOffset)
      : super(newsId, activeFrom, activeTo, timezoneOffset);

  factory MOBNews.fromJson(Map<String, dynamic> json) {
    int newsId = json['newsId'] ?? 0;
    String title = json['title'] ?? null;
    String content = json['content'] ?? null;
    int activeFrom = json['activeFrom'] ?? 0;
    int activeTo = json['activeTo'] ?? 0;
    final int timezoneOffset = json['timezoneOffset'] ?? 0;
    return MOBNews(
        newsId, title, content, activeFrom, activeTo, timezoneOffset);
  }
}

class MOBAlert extends MOBInformation {
  final int alertId;
  final String effect;
  final String cause;
  final int activeFrom;
  final int activeTo;
  final int timezoneOffset;
  final MOBAlertContent alertContent;
  final MOBAlertAffectedEntities alertAffectedEntities;

  MOBAlert(
    this.alertId,
    this.effect,
    this.activeFrom,
    this.activeTo,
    this.cause,
    this.timezoneOffset,
    this.alertContent,
    this.alertAffectedEntities,
  ) : super(alertId, activeFrom, activeTo, timezoneOffset);

  factory MOBAlert.fromJson(Map<String, dynamic> json) {
    MOBAlertContent alertContent =
        MOBAlertContent.fromJson(json['content'] ?? {});
    MOBAlertAffectedEntities alertAffectedEntities =
        MOBAlertAffectedEntities.fromJson(json['affectedEntities'] ?? {});

    int alertId = json['alertId'] ?? 0;
    int timezoneOffset = json['timezoneOffset'] ?? 0;
    String effect = json['effect'] ?? null;
    String cause = json['caise'] ?? null;

    int activeFrom = json['activeFrom'] ?? 0;
    int activeTo = json['activeTo'] ?? 0;

    ALERT_EFFECT alertEffect;

    switch (effect) {
      case 'NO_SERVICE':
        //alertEffect = ALERT_EFFECT.NO_SERVICE;
        effect = 'SEM SERVIÇO';
        break;
      case 'REDUCED_SERVICE':
        //alertEffect = ALERT_EFFECT.REDUCED_SERVICE;
        effect = 'SERVIÇO REDUZIDO';
        break;
      case 'SIGNIFICANT_DELAYS':
        //alertEffect = ALERT_EFFECT.SIGNIFICANT_DELAYS;
        effect = 'ATRASO NA OPERAÇÃO';
        break;
      case 'DETOUR':
        //alertEffect = ALERT_EFFECT.DETOUR;
        effect = 'DESVIO';
        break;
      case 'ADDITIONAL_SERVICE':
        //alertEffect = ALERT_EFFECT.ADDITIONAL_SERVICE;
        effect = 'NOVA OPERAÇÃO';
        break;
      case 'MODIFIED_SERVICE':
        //alertEffect = ALERT_EFFECT.MODIFIED_SERVICE;
        effect = 'SERVIÇO MODIFICADO';
        break;
      case 'OTHER_EFFECT':
        //alertEffect = ALERT_EFFECT.OTHER_EFFECT;
        effect = 'INFORMAÇÃO';
        break;
      case 'UNKNOWN_EFFECT':
        //alertEffect = ALERT_EFFECT.UNKNOWN_EFFECT;
        effect = 'INFORMAÇÃO';
        break;
      case 'STOP_MOVED':
        //alertEffect = ALERT_EFFECT.STOP_MOVED;
        effect = 'PONTO DE PARADA MODIFICADO';
        break;
      default:
        //alertEffect = ALERT_EFFECT.MISSING_EFFECT;
        effect = 'INFORMAÇÃO';
        break;
    }

    return MOBAlert(alertId, effect, activeFrom, activeTo, cause,
        timezoneOffset, alertContent, alertAffectedEntities);
  }
}

class MOBAlertContent {
  final List<MOBAlertContentLanguage> alertContentLanguages;

  MOBAlertContent(this.alertContentLanguages);

  factory MOBAlertContent.fromJson(Map<String, dynamic> json) {
    List<String> languages = json.keys.map((e) => e).toList();
    List<MOBAlertContentLanguage> alertContentLanguages = languages.map((e) {
      List<String> contents =
          (json[e] as Iterable).map((e) => e as String).toList();
      return MOBAlertContentLanguage.fromJson(contents);
    }).toList();
    return MOBAlertContent(alertContentLanguages);
  }
}

class MOBAlertContentLanguage {
  final String language;
  final String title;
  final String content;
  final String url;

  MOBAlertContentLanguage(this.language, this.title, this.content, this.url);

  factory MOBAlertContentLanguage.fromJson(List<String> contents) {
    String language = contents[0];
    String title = contents[1];
    String content = contents[2];
    String url = contents[3];

    return MOBAlertContentLanguage(language, title, content, url);
  }
}

class MOBAlertAffectedEntities {
  final List<int> agencyIds;
  final List<int> routeIds;
  final List<int> routeTypes;
  final List<MOBAlertStop> stops;

  MOBAlertAffectedEntities(
      this.agencyIds, this.routeIds, this.routeTypes, this.stops);

  factory MOBAlertAffectedEntities.fromJson(Map<String, dynamic> json) {
    List<int> agencyIds =
        ((json['agencyIds'] ?? []) as Iterable).map((e) => e as int).toList();
    List<int> routeIds =
        ((json['routeIds'] ?? []) as Iterable).map((e) => e as int).toList();
    List<int> routeTypes =
        ((json['routeTypes'] ?? []) as Iterable).map((e) => e as int).toList();

    List<MOBAlertStop> stops = ((json['stops'] ?? []) as Iterable)
        .map((e) => MOBAlertStop.fromJson(e))
        .toList();

    return MOBAlertAffectedEntities(agencyIds, routeIds, routeTypes, stops);
  }
}

class MOBAlertStop {
  final int stopId;
  final String code;
  final String name;

  MOBAlertStop(this.stopId, this.code, this.name);

  factory MOBAlertStop.fromJson(Map<String, dynamic> json) {
    final int stopId = json['stopId'] ?? 0;
    final String code = json['code'] ?? null;
    final String name = json['name'] ?? null;

    return MOBAlertStop(stopId, code, name);
  }
}
