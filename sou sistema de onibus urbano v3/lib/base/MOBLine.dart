import 'package:mobilibus/utils/Utils.dart';

class MOBLine {
  final int routeId;
  final String shortName;
  final String longName;
  final String desc;
  final int type;
  final String color;
  final String textColor;
  final double price;

  MOBLine(this.routeId, this.shortName, this.longName, this.desc, this.type,
      this.color, this.textColor, this.price);

  factory MOBLine.fromJson(Map<String, dynamic> json) => MOBLine(
        json['routeId'] ?? 0,
        json['shortName'] ?? null,
        json['longName'] ?? null,
        json['desc'] ?? null,
        json['type'] ?? 0,
        json['color'] ?? null,
        json['textColor'] ?? null,
        json['price']?.toDouble() ?? null,
      );

  factory MOBLine.fromLine(MOBLine line) => MOBLine(
        line.routeId,
        line.shortName,
        line.longName,
        line.desc,
        line.type,
        line.color,
        line.textColor,
        line.price,
      );
}

class MOBTimetable {
  final List<MOBTrip> trips;
  final List<MOBDirection> directions;

  MOBTimetable(this.trips, this.directions);

  factory MOBTimetable.fromJson(Map<String, dynamic> json) => MOBTimetable(
        (json['trips'] as Iterable)
                ?.map((model) => MOBTrip?.fromJson(model))
                ?.toList() ??
            [],
        (json['directions'] as Iterable)
                ?.map((model) => MOBDirection?.fromJson(model))
                ?.toList() ??
            [],
      );
}

class MOBTrip {
  final int tripId;
  final String tripDesc;
  final String shortName;
  final int directionId;
  final int seq;

  MOBTrip(
      this.tripId, this.tripDesc, this.shortName, this.directionId, this.seq);

  factory MOBTrip.fromJson(Map<String, dynamic> json) => MOBTrip(
        json['tripId'] ?? 0,
        json['tripDesc'] ?? null,
        json['shortName'] ?? null,
        json['directionId'] ?? 0,
        json['seq'] ?? 0,
      );
}

class MOBDirection {
  final int directionId;
  final String desc;
  final List<MOBService> services;

  MOBDirection(this.directionId, this.desc, this.services);

  factory MOBDirection.fromJson(Map<String, dynamic> json) => MOBDirection(
        json['directionId'] ?? 0,
        json['desc'] ?? null,
        (json['services'] as Iterable)
                ?.map((model) => MOBService.fromJson(model))
                ?.toList() ??
            [],
      );
}

class MOBService {
  final int serviceId;
  final String desc;
  final List<SERVICE_DAYS> days;
  final List<MOBLineDeparture> departures;

  MOBService(this.serviceId, this.desc, this.days, this.departures);

  factory MOBService.fromJson(Map<String, dynamic> json) => MOBService(
        json['serviceId'] ?? 0,
        json['desc'] ?? null,
        Utils.getDaysStringfied((json['days'] as Iterable)?.cast<bool>() ?? []),
        (json['departures'] as Iterable)
                ?.map((model) => MOBLineDeparture?.fromJson(model))
                ?.toList() ??
            [],
      );
}

class MOBLineDeparture {
  final String dep;
  final String arr;
  final int seq;
  final bool wheelchairAccessible;

  MOBLineDeparture(this.dep, this.arr, this.seq, this.wheelchairAccessible);

  factory MOBLineDeparture.fromJson(Map<String, dynamic> json) =>
      MOBLineDeparture(
        json['dep'] ?? null,
        json['arr'] ?? null,
        json['seq'] ?? 0,
        json['wa'] == 1 ?? false,
      );
}
