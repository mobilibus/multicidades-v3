class MOBPos {
  final String name;
  final double latitude;
  final double longitude;
  final double distance;

  MOBPos(this.name, this.latitude, this.longitude, this.distance);

  factory MOBPos.fromJson(Map<String, dynamic> json, [double distance = 0.0]) {
    String name = json['name'] ?? null;
    double lat = json['lat'] ?? 0.0;
    double lng = json['lng'] ?? 0.0;
    return MOBPos(name, lat, lng, distance);
  }
}
