import 'package:intl/intl.dart';
import 'package:mobilibus/utils/Utils.dart';

class MOBTripDetails {
  final String tripName;
  final String routeName;
  final String shape;
  final List<MOBTripDetailsService> services;
  final List<MOBTripDetailsStop> stops;
  final List<MOBTripDetailsVehicle> vehicles;
  final int tzOffset;

  MOBTripDetails(
    this.tripName,
    this.routeName,
    this.shape,
    this.services,
    this.stops,
    this.vehicles,
    this.tzOffset,
  );

  factory MOBTripDetails.fromJson(Map<String, dynamic> json) {
    String tripName = json['tripName'] ?? null;
    String routeName = json['routeName'] ?? null;
    String shape = json['shape'] ?? null;
    int tzOffset = json['tzOffset'] ?? 0;
    List<MOBTripDetailsService> services =
        ((json['services'] ?? []) as Iterable)
            .map((model) => MOBTripDetailsService.fromJson(model))
            .toList();
    List<MOBTripDetailsStop> stops = ((json['stops'] ?? []) as Iterable)
        .map((model) => MOBTripDetailsStop?.fromJson(model))
        .toList();
    List<MOBTripDetailsVehicle> vehicles =
        ((json['vehicles'] ?? []) as Iterable)
            .map((model) => MOBTripDetailsVehicle.fromJson(model))
            .toList();
    return MOBTripDetails(
      tripName,
      routeName,
      shape,
      services,
      stops,
      vehicles,
      tzOffset,
    );
  }
}

class MOBTripDetailsService {
  final String name;
  final List<String> departures;
  final List<SERVICE_DAYS> days;

  MOBTripDetailsService(this.name, this.departures, this.days);

  factory MOBTripDetailsService.fromJson(Map<String, dynamic> json) {
    String name = json['name'] ?? null;
    List<String> departures = ((json['departures'] ?? []) as Iterable)
        .map((e) => e as String)
        .toList();
    List<SERVICE_DAYS> days =
        Utils.getDaysStringfied((json['days'] as Iterable)?.cast<bool>() ?? []);
    return MOBTripDetailsService(name, departures, days);
  }
}

class MOBTripDetailsStop {
  final int stopId;
  final double latitude;
  final double longitude;
  final String name;
  final int previousStopSeconds;

  MOBTripDetailsStop(
    this.stopId,
    this.latitude,
    this.longitude,
    this.name,
    this.previousStopSeconds,
  );

  factory MOBTripDetailsStop.fromJson(Map<String, dynamic> json) {
    int stopId = json['stopId'] ?? 0;
    double latitude = json['lat'] ?? 0.0;
    double longitude = json['lng'] ?? 0.0;
    String name = json['name'] ?? null;
    int previousStopSeconds = json['int'] ?? null;
    return MOBTripDetailsStop(
        stopId, latitude, longitude, name, previousStopSeconds);
  }
}

class MOBTripDetailsVehicle {
  final String vehicleId;
  final String positionTime;
  final double latitude;
  final double longitude;
  final int percTravelled;
  final int heading;
  final DateTime startTime;
  final int delay;
  final int seq;

  MOBTripDetailsVehicle(
    this.vehicleId,
    this.positionTime,
    this.latitude,
    this.longitude,
    this.percTravelled,
    this.heading,
    this.startTime,
    this.delay,
    this.seq,
  );

  factory MOBTripDetailsVehicle.fromJson(Map<String, dynamic> json) {
    String vehicleId = json['vehicleId'] ?? null;
    String positionTime = json['positionTime'].replaceAll('T', ' ').replaceAll('.000Z', '') ?? null;
    double latitude = json['lat'] ?? 0.0;
    double longitude = json['lng'] ?? 0.0;
    int percTravelled = json['percTravelled'] ?? 0;
    int heading = json['heading'] ?? 0;
    DateTime startTime = DateFormat('HH:mm').parse(json['startTime']) ?? null;
    int delay = json['delay'] ?? null;
    int seq = json['seq'] ?? null;
    return MOBTripDetailsVehicle(vehicleId, positionTime, latitude, longitude,
        percTravelled, heading, startTime, delay, seq);
  }
}
