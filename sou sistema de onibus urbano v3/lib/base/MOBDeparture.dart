import 'package:instant/instant.dart' as i;
import 'package:mobilibus/utils/Utils.dart';

class MOBDepartures {
  final String stop;
  final int tzOffset;
  final List<MOBDepartureTrip> departures;
  final List<MOBDepartureTrip> departuresToday;
  final List<MOBDepartureTrip> departuresNextDay;

  final Map<int, List<MOBDepartureTrip>> mapDeparturesToday = {};
  final Map<int, List<MOBDepartureTrip>> mapDeparturesNextDay = {};

  MOBDepartures(this.stop, this.tzOffset, this.departures, this.departuresToday,
      this.departuresNextDay) {
    for (final dt in departuresToday) {
      int tripId = dt.tripId;
      if (!mapDeparturesToday.containsKey(tripId))
        mapDeparturesToday[tripId] = [dt];
      else
        mapDeparturesToday[tripId].add(dt);
    }
    for (final dt in departuresNextDay) {
      int tripId = dt.tripId;
      if (!mapDeparturesNextDay.containsKey(tripId))
        mapDeparturesNextDay[tripId] = [dt];
      else
        mapDeparturesNextDay[tripId].add(dt);
    }
  }

  factory MOBDepartures.fromJson(Map<String, dynamic> json,
      [int routeId, int tripId]) {
    int tzOffset = json['tzOffset'] ?? 0;
    String stopName = json['stopName'] ?? null;

    List<MOBDepartureTrip> trips = ((json['trips'] ?? []) as Iterable)
        .map((model) => MOBDepartureTrip.fromJson(model, tzOffset))
        .toList();

    List<MOBDepartureTrip> departures = trips
        .map((trip) => MOBDepartureTrip.fromJson(trip.toMap(), tzOffset))
        .toList();

    if (tripId != null && tripId > -1)
      departures.removeWhere((departure) => departure.tripId != tripId);

    if (routeId != null && routeId > -1)
      departures.removeWhere((departure) => departure.routeId != routeId);

    List<MOBDepartureTrip> departuresToday = departures
        .map((e) => MOBDepartureTrip.fromJson(e.toMap(), tzOffset))
        .toList();
    List<MOBDepartureTrip> departuresNextDay = departures
        .map((e) => MOBDepartureTrip.fromJson(e.toMap(), tzOffset))
        .toList();

    departuresToday.forEach((departure) =>
        departure.departures.removeWhere((departure) => departure.nextDay));
    departuresToday.removeWhere((departure) => departure.departures.isEmpty);

    departuresNextDay.forEach((departure) =>
        departure.departures.removeWhere((departure) => !departure.nextDay));
    departuresNextDay.removeWhere((departure) => departure.departures.isEmpty);

    return MOBDepartures(
        stopName, tzOffset, departures, departuresToday, departuresNextDay);
  }
}

class MOBDepartureTrip {
  final int routeId;
  final int tripId;
  final String shortName;
  final String longName;
  final String color;
  final String headsign;
  final List<MOBDeparture> departures;

  MOBDepartureTrip(
    this.routeId,
    this.tripId,
    this.shortName,
    this.longName,
    this.color,
    this.headsign,
    this.departures,
  ) {
    departures.removeWhere((departureLine) {
      if (departureLine.online) return false;
      int departureSeconds = departureLine.seconds;
      int removeSeconds = Utils?.project?.realtimeDeparturesOffset ?? 20;
      return departureSeconds <= removeSeconds;
    });
  }

  factory MOBDepartureTrip.fromJson(Map<String, dynamic> json, int tzOffset) {
    int routeId = json['routeId'] ?? 0;
    int tripId = json['tripId'] ?? 0;
    String shortName = json['shortName'] ?? null;
    String longName = json['longName'] ?? null;
    String color = json['color'] ?? null;
    String headsign = json['headsign'] ?? null;
    List<MOBDeparture> departures = json.containsKey('departures')
        ? (json['departures'] as Iterable)
            .map((model) => MOBDeparture?.fromJson(model, tzOffset))
            .toList()
        : [];
    return MOBDepartureTrip(
        routeId, tripId, shortName, longName, color, headsign, departures);
  }

  Map<String, dynamic> toMap() => {
        'routeId': routeId,
        'tripId': tripId,
        'shortName': shortName,
        'longName': longName,
        'color': color,
        'headsign': headsign,
        'departures': departures.map((departure) => departure.toMap()).toList()
      };
}

class MOBDeparture {
  final String time;
  final bool wheelchairAccessible;
  final bool nextDay;
  final int seconds;
  final int secondsDelay;
  final int minutes;
  final bool online;
  final String vehicleId;
  final String gpsTime;
  final int bearing;
  final int delay;

  MOBDeparture(
    this.time,
    this.wheelchairAccessible,
    this.nextDay,
    this.seconds,
    this.secondsDelay,
    this.minutes,
    this.online,
    this.vehicleId,
    this.gpsTime,
    this.bearing,
    this.delay,
  );

  factory MOBDeparture.fromJson(Map<String, dynamic> json, int tzOffset) {
    int minutes = 0;
    int seconds = 0;
    int secondsDelay = 0;
    bool nextDay = json['nextDay'] ?? false;
    String time = json['time'] ?? null;
    if (time.isNotEmpty) {
      List<String> split = time.split(':');
      int h = int.parse(split[0]);
      int m = int.parse(split[1]);
      int s = int.parse(split[2]);

      DateTime now = i.curDateTimeByUtcOffset(offset: tzOffset.toDouble());

      Duration dNow =
          Duration(hours: now.hour, minutes: now.minute, seconds: now.second);
      Duration busTime = Duration(hours: h, minutes: m, seconds: s);

      int secondsNow = dNow.inSeconds;
      int secondsTime = busTime.inSeconds + (nextDay ? 86400 : 0);

      seconds = secondsTime - secondsNow;
      secondsDelay = secondsTime - secondsNow;
      if (secondsDelay < 0) secondsDelay *= -1;
      if (seconds < 0) {
        if (nextDay)
          seconds *= -1;
        else
          seconds = 0;
      }

      minutes = seconds ~/ 60;
    }

    bool wheelchairAccessible = json['wa'] ?? false;
    bool online = json.containsKey('vehicleId')
        ? json['vehicleId'] != null && (json['vehicleId'] as String).isNotEmpty
        : false;
    String vehicleId = json['vehicleId'] ?? null;
    String gpsTime = json['gpsTime'] ?? null;
    int bearing = json['bearing'] ?? 0;
    int delay = json['delay'] ?? 0;
    return MOBDeparture(time, wheelchairAccessible, nextDay, seconds,
        secondsDelay, minutes, online, vehicleId, gpsTime, bearing, delay);
  }

  Map<String, dynamic> toMap() => {
        'time': time,
        'wa': wheelchairAccessible,
        'nextDay': nextDay,
        'online': online,
        'vehicleId': vehicleId,
        'gpsTime': gpsTime,
        'bearing': bearing,
        'delay': delay,
      };
}
