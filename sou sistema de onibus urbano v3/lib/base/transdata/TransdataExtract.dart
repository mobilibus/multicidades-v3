import 'package:intl/intl.dart';
import 'package:mobilibus/base/transdata/TransdataRef.dart';

class TransdataExtract extends TransdataRef {
  final List<TransdataExtractItem> items;
  final int currentBalanceInCents;

  TransdataExtract(int errorCode, String errorMessage, this.items,
      this.currentBalanceInCents)
      : super(errorCode, errorMessage);

  factory TransdataExtract.fromJson(Map<String, dynamic> map) {
    int errorCode = map['errorCode'];
    String errorMessage = map['errorMessage'];
    List<TransdataExtractItem> items = ((map['Itens'] ?? []) as Iterable)
        .map((e) => TransdataExtractItem.fromJson(e))
        .toList();
    int currentBalanceInCents = map['SaldoAtualEmCentavos'];
    return TransdataExtract(
        errorCode, errorMessage, items, currentBalanceInCents);
  }
}

class TransdataExtractItem {
  final DateTime date;
  final String description;
  final int valueInCents;

  TransdataExtractItem(this.date, this.description, this.valueInCents);

  factory TransdataExtractItem.fromJson(Map<String, dynamic> map) {
    String mDate = (map['Data'] ?? '1970-01-01 00:00:00').replaceAll('T', ' ');
    DateTime date = DateFormat('yyyy-MM-dd HH:mm:ss').parse(mDate);
    String description = map['Descricao'];
    int valueInCents = map['ValorEmCentavos'];
    return TransdataExtractItem(date, description, valueInCents);
  }
}
