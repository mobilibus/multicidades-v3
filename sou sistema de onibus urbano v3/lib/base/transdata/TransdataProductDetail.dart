import 'package:intl/intl.dart';
import 'package:mobilibus/base/transdata/TransdataRef.dart';

class TransdataProductDetail extends TransdataRef {
  final int serialNumber;
  final int taxInCents;
  final String cpf;
  final String externalBuyerId;
  final String versionAppName;
  final DateTime status;
  final String description;
  final List<TransdataProductDetailItem> items;

  TransdataProductDetail(
    int errorCode,
    String errorMessage,
    this.serialNumber,
    this.taxInCents,
    this.cpf,
    this.externalBuyerId,
    this.versionAppName,
    this.status,
    this.description,
    this.items,
  ) : super(errorCode, errorMessage);

  factory TransdataProductDetail.fromJson(Map<String, dynamic> map) {
    DateFormat df = DateFormat('yyyy-MM-dd HH:mm:ss');

    int errorCode = map['errorCode'];
    String errorMessage = map['errorMessage'];
    int serialNumber = map['NumeroSerie'];
    int taxInCents = map['TaxasEmCentavos'];
    String cpf = map['CPF'];
    String externalBuyerId = map['IDCompraExterno'];
    String versionAppName = map['AppNome_Versao'];
    DateTime status = df.parse(map['DataStatus'] ?? '1970-01-01 00:00:00');
    String description = map['DescricaoStatus'];
    List<TransdataProductDetailItem> items = ((map['Itens'] ?? []) as Iterable)
        .map((e) => TransdataProductDetailItem.fromJson(e))
        .toList();

    return TransdataProductDetail(
        errorCode,
        errorMessage,
        serialNumber,
        taxInCents,
        cpf,
        externalBuyerId,
        versionAppName,
        status,
        description,
        items);
  }
}

class TransdataProductDetailItem {
  final int product;
  final int valueInCents;
  final int valueBuyInCents;

  TransdataProductDetailItem(
      this.product, this.valueInCents, this.valueBuyInCents);

  factory TransdataProductDetailItem.fromJson(Map<String, dynamic> map) {
    int product = map['Produto'];
    int valueInCents = map['ValorUnitarioEmCentavos'];
    int valueBuyInCents = map['ValorCompraEmCentavos'];
    return TransdataProductDetailItem(product, valueInCents, valueBuyInCents);
  }
}
