class MOBTraccarDevice {
  final int id;
  final MOBTraccarDeviceAttributes traccarDeviceAttributes;
  final int groupId;
  final String name;
  final String uniqueId;
  final String status;
  final String lastUpdate;
  final int positionId;
  final List<MOBTraccarDeviceGeofenceId> geofenceIds;
  final String phone;
  final String model;
  final String contact;
  final String category;
  final bool disabled;

  MOBTraccarDevice(
    this.id,
    this.traccarDeviceAttributes,
    this.groupId,
    this.name,
    this.uniqueId,
    this.status,
    this.lastUpdate,
    this.positionId,
    this.geofenceIds,
    this.phone,
    this.model,
    this.contact,
    this.category,
    this.disabled,
  );

  factory MOBTraccarDevice.fromJson(Map<String, dynamic> json) {
    int id = json['id'] ?? null;
    MOBTraccarDeviceAttributes attributes = json.containsKey('attributes')
        ? MOBTraccarDeviceAttributes.fromJson(json['attributes'])
        : null;
    int groupId = json['groupId'] ?? null;
    String name = json['name'] ?? null;
    String uniqueId = json['uniqueId'] ?? null;
    String status = json['status'] ?? null;
    String lastUpdate = json['lastUpdate'] ?? null;
    int positionId = json['positionId'] ?? null;
    List<MOBTraccarDeviceGeofenceId> geofenceIds =
        json.containsKey('geofenceIds')
            ? (json['geofenceIds'] as Iterable)
                .map((map) => MOBTraccarDeviceGeofenceId.fromJson(map))
                .toList()
            : null;
    String phone = json['phone'] ?? null;
    String model = json['model'] ?? null;
    String contact = json['contact'] ?? null;
    String category = json['category'] ?? null;
    bool disabled = json['disabled'] ?? false;

    return MOBTraccarDevice(
      id,
      attributes,
      groupId,
      name,
      uniqueId,
      status,
      lastUpdate,
      positionId,
      geofenceIds,
      phone,
      model,
      contact,
      category,
      disabled,
    );
  }
}

class MOBTraccarDeviceGeofenceId {
  MOBTraccarDeviceGeofenceId();
  factory MOBTraccarDeviceGeofenceId.fromJson(Map<String, dynamic> json) {
    return MOBTraccarDeviceGeofenceId();
  }
}

class MOBTraccarDeviceAttributes {
  final String projectId;
  final String carro;
  final String gmt;

  MOBTraccarDeviceAttributes(this.projectId, this.carro, this.gmt);

  factory MOBTraccarDeviceAttributes.fromJson(Map<String, dynamic> json) {
    String projectId = json['projectId'] ?? null;
    String carro = json['carro'] ?? null;
    String gmt = json['gmt'] ?? null;
    return MOBTraccarDeviceAttributes(projectId, carro, gmt);
  }
}
