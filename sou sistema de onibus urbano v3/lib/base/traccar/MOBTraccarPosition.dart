class MOBTraccarPosition {
  final int id;
  final MOBTraccarPositionAttributes traccarPositionAttributes;
  final int deviceId;
  final String type;
  final String protocol;
  final String serverTime;
  final String deviceTime;
  final String fixTime;
  final bool outdated;
  final bool valid;
  final double latitude;
  final double longitude;
  final double altitude;
  final double speed;
  final double course;
  final String address;
  final double accuracy;
  final MOBTraccarPositionNetwork traccarPositionNetwork;

  MOBTraccarPosition(
    this.id,
    this.traccarPositionAttributes,
    this.deviceId,
    this.type,
    this.protocol,
    this.serverTime,
    this.deviceTime,
    this.fixTime,
    this.outdated,
    this.valid,
    this.latitude,
    this.longitude,
    this.altitude,
    this.speed,
    this.course,
    this.address,
    this.accuracy,
    this.traccarPositionNetwork,
  );

  factory MOBTraccarPosition.fromJson(Map<String, dynamic> json) {
    int id = json['id'] ?? null;
    MOBTraccarPositionAttributes traccarPositionAttributes =
        json.containsKey('attributes')
            ? MOBTraccarPositionAttributes.fromJson(json['attributes'])
            : null;
    int deviceId = json['deviceId'] ?? null;
    String type = json['type'] ?? null;
    String protocol = json['protocol'] ?? null;
    String serverTime = json['serverTime'] ?? null;
    String deviceTime = json['deviceTime'] ?? null;
    String fixTime = json['fixTime'] ?? null;
    bool outdated = json['outdated'] ?? false;
    bool valid = json['valid'] ?? false;
    double latitude = json['latitude'] ?? null;
    double longitude = json['longitude'] ?? null;
    double altitude = json['altitude'] ?? null;
    double speed = json['speed'] ?? null;
    double course = json['course'] ?? null;
    String address = json['address'] ?? null;
    double accuracy = json['accuracy'] ?? null;
    MOBTraccarPositionNetwork traccarPositionNetwork =
        json.containsKey('network')
            ? MOBTraccarPositionNetwork.fromJson(json['network'] ?? {})
            : null;

    return MOBTraccarPosition(
      id,
      traccarPositionAttributes,
      deviceId,
      type,
      protocol,
      serverTime,
      deviceTime,
      fixTime,
      outdated,
      valid,
      latitude,
      longitude,
      altitude,
      speed,
      course,
      address,
      accuracy,
      traccarPositionNetwork,
    );
  }
}

class MOBTraccarPositionAttributes {
  final int hdop;
  final double odometer;
  final double batteryLevel;
  final bool ignition;
  final int input;
  final int output;
  final String type;
  final double distance;
  final double totalDistance;
  final bool motion;

  MOBTraccarPositionAttributes(
    this.hdop,
    this.odometer,
    this.batteryLevel,
    this.ignition,
    this.input,
    this.output,
    this.type,
    this.distance,
    this.totalDistance,
    this.motion,
  );

  factory MOBTraccarPositionAttributes.fromJson(Map<String, dynamic> json) {
    int hdop = json['hdop'] ?? null;
    double odometer = json['odometer'] ?? null;
    double batteryLevel = json.containsKey('batteryLevel')
        ? json['batteryLevel'].toDouble()
        : null;
    bool ignition = json['ignition'] ?? false;
    int input = json['input'] ?? null;
    int output = json['output'] ?? null;
    String type = json['type'] ?? null;
    double distance = json['distance'] ?? null;
    double totalDistance = json['totalDistance'] ?? null;
    bool motion = json['motion'] ?? false;
    return MOBTraccarPositionAttributes(hdop, odometer, batteryLevel, ignition,
        input, output, type, distance, totalDistance, motion);
  }
}

class MOBTraccarPositionNetwork {
  final String radioType;
  final bool considerIp;
  final List<MOBTraccarPositionCellTower> traccarPositionCellTowers;

  MOBTraccarPositionNetwork(
    this.radioType,
    this.considerIp,
    this.traccarPositionCellTowers,
  );

  factory MOBTraccarPositionNetwork.fromJson(Map<String, dynamic> json) {
    String radioType = json['radioType'] ?? null;
    bool considerIp = json['considerIp'] ?? false;
    List<MOBTraccarPositionCellTower> traccarPositionCellTowers =
        json.containsKey('cellTowers')
            ? (json['cellTowers'] as Iterable)
                .map((map) => MOBTraccarPositionCellTower.fromJson(map))
                .toList()
            : null;

    return MOBTraccarPositionNetwork(
        radioType, considerIp, traccarPositionCellTowers);
  }
}

class MOBTraccarPositionCellTower {
  final int cellId;
  final int locationAreaCode;
  final int mobileCountryCode;
  final int mobileNetworkCode;

  MOBTraccarPositionCellTower(
    this.cellId,
    this.locationAreaCode,
    this.mobileCountryCode,
    this.mobileNetworkCode,
  );
  factory MOBTraccarPositionCellTower.fromJson(Map<String, dynamic> json) {
    int cellId = json['cellId'] ?? null;
    int locationAreaCode = json['locationAreaCode'] ?? null;
    int mobileCountryCode = json['mobileCountryCode'] ?? null;
    int mobileNetworkCode = json['mobileNetworkCode'] ?? null;
    return MOBTraccarPositionCellTower(
        cellId, locationAreaCode, mobileCountryCode, mobileNetworkCode);
  }
}
