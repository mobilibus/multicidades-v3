import 'package:flutter/material.dart';

class ClaytonGame extends StatefulWidget {
  _ClaytonGame createState() => _ClaytonGame();
}

class _ClaytonGame extends State<ClaytonGame> {
  TextEditingController teController = TextEditingController();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  void createAPI() {
    teController.clear();
    setState(() {});
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text('API criada com sucesso!'),
    ));
  }

  void clearCache() {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text('Cache limpado com sucesso!'),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Clayton Game'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text('Criar API'),
          TextField(
            controller: teController,
            onChanged: (text) => setState(() {}),
            decoration: InputDecoration(hintText: 'https://mobilibus.com/api/'),
          ),
          if (teController.text.isNotEmpty)
            RaisedButton(
              onPressed: createAPI,
              child: Text('Criar'),
            ),
          Container(
            padding: EdgeInsets.only(top: 50),
            child: RaisedButton(
              onPressed: clearCache,
              child: Text('Limpar cache'),
            ),
          ),
        ],
      ),
    );
  }
}
