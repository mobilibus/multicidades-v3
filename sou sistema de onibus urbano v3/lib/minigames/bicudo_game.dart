import 'package:flutter/material.dart';

class BicudoGame extends StatefulWidget {
  _BicudoGame createState() => _BicudoGame();
}

class _BicudoGame extends State<BicudoGame> {
  List<String> items = [
    'https://cdn.pixabay.com/photo/2014/09/12/18/20/coca-cola-443123_960_720.png',
    'https://cdn.pixabay.com/photo/2012/04/13/01/51/hamburger-31775_960_720.png',
    'https://cdn.pixabay.com/photo/2013/07/13/01/24/french-fries-155679_960_720.png',
    'https://cdn.pixabay.com/photo/2017/02/01/00/42/bus-2028647_960_720.png',
    'https://cdn.pixabay.com/photo/2014/04/03/10/08/airliner-309920_960_720.png',
    'https://pbs.twimg.com/profile_images/1209530094252806144/aWRjTor4_400x400.jpg',
    'https://pbs.twimg.com/profile_images/1297326885408575488/AtKWAD_m_400x400.jpg',
  ];

  Map<int, int> indexValue = {};

  void love(int index) => setState(() => indexValue[index]++);
  void unlove(int index) => setState(() =>
      indexValue[index] == 0 ? indexValue[index] = 0 : indexValue[index]--);

  @override
  void initState() {
    for (int i = 0; i < items.length; i++) indexValue[i] = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Bicudo Game')),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return Container(
            padding: EdgeInsets.all(5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: (MediaQuery.of(context).size.width / 2) - 5,
                  child: Image.network(
                    items[index],
                    height: indexValue[index].toDouble(),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width / 2 - 5,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(
                            Icons.remove,
                            size: 40,
                          ),
                          onPressed: () => unlove(index)),
                      Text(
                        (indexValue[index]).toString(),
                        style: TextStyle(fontSize: 30),
                      ),
                      IconButton(
                          icon: Icon(
                            Icons.favorite,
                            size: 40,
                          ),
                          onPressed: () => love(index))
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
