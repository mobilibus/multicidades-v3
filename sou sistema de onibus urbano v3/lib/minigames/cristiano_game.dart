import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

class CristianoGame extends StatefulWidget {
  _CristianoGame createState() => _CristianoGame();
}

class _CristianoGame extends State<CristianoGame> {
  FlutterTts flutterTts = FlutterTts();
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () {
      flutterTts.setLanguage('pt-BR');
    });
  }

  @override
  Widget build(BuildContext context) {
    List<String> speaks = [
      'O cliente pediu',
      'Então, vamos lá',
      'Mau, caráter',
      'Só tem louco',
      'To devendo para eles',
      'Vai ficar falando? Vou te dar farpas',
      'Precisa limpar o cache!',
      'Sabia que sou o chefe?',
      '12 horas para atualizar? Nada disso, 30 dias!',
      'Conta do google ta abaixando?',
      'Falou para alterar o aplicativo?',
      'O que? Modificar o que eu fiz?',
      'Hoje a noite a costela é por minha conta! Costela dura! K K K',
    ];
    return Scaffold(
      appBar: AppBar(title: Text('Cristiano Game')),
      body: ListView.builder(
        itemCount: speaks.length,
        itemBuilder: (context, index) => ListTile(
          title: Text(speaks[index]),
          onTap: () async {
            await flutterTts.stop();
            flutterTts.speak(speaks[index]);
          },
        ),
      ),
    );
  }
}
