import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobilibus/recargaagora/module/recarga_detalhes.dart';
import 'package:mobilibus/recargaagora/module/token_request.dart';
import 'package:mobilibus/base/recargaagora/dataprom/Dataprom.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromTransportCard.dart';
import 'package:mobilibus/base/recargaagora/mobilibus/MobilibusAPIs.dart';
import 'package:mobilibus/utils/Utils.dart';

class SelectValue extends StatefulWidget {
  SelectValue(
      this.transportCardUser, this.ticketing, this.cardNumber, this.cardType);
  final DatapromTransportCardUser transportCardUser;
  final DatapromTicketing ticketing;
  final String cardNumber;
  final String cardType;
  _SelectValue createState() =>
      _SelectValue(transportCardUser, ticketing, cardNumber, cardType);
}

class _SelectValue extends State<SelectValue> {
  _SelectValue(
      this.transportCardUser, this.ticketing, this.cardNumber, this.cardType)
      : super();
  final DatapromTransportCardUser transportCardUser;
  final DatapromTicketing ticketing;
  final String cardNumber;
  final String cardType;

  double value = 15.0;
  TextEditingController valueTec = TextEditingController(text: '15,00');
  bool isLoading = false;
  bool isGimaveProduction = true;
  bool isDatapromProduction = true;

  void confirm() async {
    value = double.parse(valueTec.text.replaceAll(',', '.'));
    if (!kDebugMode && value < 15 || value > 500) return;

    isLoading = true;
    setState(() {});
    try {
      bool hasToken = await MobilibusAPIs.doesAppContainsSavedToken();
      if (hasToken) {
        String cardMask = await MobilibusAPIs.getCreditCardMask();
        String token = await MobilibusAPIs.getCreditCardToken();
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => TokenRequest(
              cardMask,
              token,
              cardNumber,
              value,
              transportCardUser,
              cardType,
              isDatapromProduction,
              isGimaveProduction,
            )));
      } else
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => RecargaDetalhes(
              transportCardUser,
              cardNumber,
              cardType,
              value,
              isDatapromProduction,
              isGimaveProduction,
            )));
    } catch (e) {
      TextStyle textStyle = TextStyle(color: Colors.white);
      showDialog(
        context: context,
        builder: (BuildContext context) =>  AlertDialog(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text('Erro', style: textStyle),
          content: Text(e, style: textStyle),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Fechar', style: textStyle))
          ],
        ),
      );
    }
    isLoading = false;
    setState(() {});
  }

  Widget extraButton(double amount) => Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.all(Radius.circular(40))),
      child: Container(
        child: InkWell(
            child: Text('R\$ ${amount.toStringAsFixed(0)}',
                style: TextStyle(color: Colors.white, fontSize: 20)),
            onTap: () {
              value = amount;
              valueTec.text = value.toStringAsFixed(2).replaceAll('.', ',');
              setState(() {});
            }),
      ));

  Widget mainButton(bool isAdd) => Container(
    padding: EdgeInsets.all(10),
    decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.all(Radius.circular(60))),
    child: InkWell(
      child: Icon(
        isAdd ? Icons.add : Icons.remove,
        color: Colors.white,
        size: 40,
      ),
      onTap: () => isAdd ? addValue(1) : removeValue(1),
    ),
  );

  void addValue(int add) {
    value += add;
    if (value > 500) value = 500;
    valueTec.text = value.toStringAsFixed(2).replaceAll('.', ',');
    setState(() {});
  }

  void removeValue(int remove) {
    value -= remove;
    if (value < 15) value = 15;
    valueTec.text = value.toStringAsFixed(2).replaceAll('.', ',');
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Valor', style: textStyle),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: themeColor,
          ),
          onPressed: () => Navigator.pop(context, null),
        ),
      ),
      body: isLoading
          ? Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('Aguarde...'),
            CircularProgressIndicator(
                strokeWidth: 3,
                valueColor: AlwaysStoppedAnimation<Color>(
                    Utils.isLightTheme(context)
                        ? Utils.getColorFromPrimary(context)
                        : Colors.black))
          ],
        ),
      )
          : Scrollbar(
        child: SingleChildScrollView(
          child: Center(
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  Text(
                    'Selecione o valor de sua recarga.',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 25),
                  ),
                  Text(
                    'Mínimo de R\$ 15,00 e máximo de R\$ 500,00',
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width / 5,
                          child: Text(
                            'R\$',
                            style: TextStyle(fontSize: 50),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width -
                              (MediaQuery.of(context).size.width / 3),
                          child: TextField(
                            keyboardType: TextInputType.number,
                            style: TextStyle(fontSize: 50),
                            controller: valueTec,
                            onChanged: (text) {
                              double newValue =
                              double.parse(text.replaceAll(',', '.'));
                              if (newValue < 15)
                                newValue = 15;
                              else if (newValue > 500) newValue = 500;
                              value = newValue;
                              setState(() {});
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        mainButton(true),
                        mainButton(false),
                      ],
                    ),
                  ),
                  Container(child: Divider(thickness: 0.1)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      extraButton(20),
                      extraButton(30),
                      extraButton(50),
                      extraButton(80),
                    ],
                  ),
                  Container(child: Divider(thickness: 1)),
                  Container(
                    padding: EdgeInsets.all(20),
                    child: RaisedButton(
                      color: Theme.of(context).primaryColor,
                      onPressed: confirm,
                      child: Text(
                        'CONFIRMAR',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
