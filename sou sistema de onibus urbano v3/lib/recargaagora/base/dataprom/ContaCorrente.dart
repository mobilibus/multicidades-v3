class ContaCorrente {
  final List<Registro> registros;
  ContaCorrente(this.registros);

  factory ContaCorrente.fromJson(Map<String, dynamic> json) {
    List<Registro> registros = json.containsKey('registros')
        ? (json['registros'] as Iterable)
            .map((e) => Registro.fromJson(e))
            .toList()
        : [];
    registros = registros.reversed.toList();
    return ContaCorrente(registros);
  }
}

class Registro {
  final String dataOperacao;
  final int numeroCarteira;
  final String operacao;
  final String codigoLinha;
  final String linha;
  final String trecho;
  final String empresa;
  final String codigoExternoVeiculo;
  final String matriculaCobrador;
  final String nomeCobrador;
  final double valor;
  final double saldoCartao;
  final double saldoAgendado;

  Registro(
      this.dataOperacao,
      this.numeroCarteira,
      this.operacao,
      this.codigoLinha,
      this.linha,
      this.trecho,
      this.empresa,
      this.codigoExternoVeiculo,
      this.matriculaCobrador,
      this.nomeCobrador,
      this.valor,
      this.saldoCartao,
      this.saldoAgendado);

  factory Registro.fromJson(Map<String, dynamic> json) {
    String dataOperacao = json['dataOperacao'] ?? '-';
    int numeroCarteira = json['numeroCarteira'] ?? -1;
    String operacao = json['operacao'] ?? '-';
    String codigoLinha = json['codigoLinha'] ?? '-';
    String linha = json['linha'] ?? '-';
    String trecho = json['trecho'] ?? '-';
    String empresa = json['empresa'] ?? '-';
    String codigoExternoVeiculo = json['codigoExternoVeiculo'] ?? '-';
    String matriculaCobrador = json['matriculaCobrador'] ?? '-';
    String nomeCobrador = json['nomeCobrador'] ?? '-';
    double valor = double.parse(json['valor'].toString()) ?? 0;
    double saldoCartao = double.parse(json['saldoCartao'].toString()) ?? 0;
    double saldoAgendado = double.parse(json['saldoAgendado'].toString()) ?? 0;

    if (dataOperacao.isEmpty) dataOperacao = '-';
    if (operacao.isEmpty) operacao = '-';
    if (codigoLinha.isEmpty) codigoLinha = '-';
    if (linha.isEmpty) linha = '-';
    if (trecho.isEmpty) trecho = '-';
    if (empresa.isEmpty) empresa = '-';
    if (codigoExternoVeiculo.isEmpty) codigoExternoVeiculo = '-';
    if (matriculaCobrador.isEmpty) matriculaCobrador = '-';
    if (nomeCobrador.isEmpty) nomeCobrador = '-';

    if (dataOperacao.length >= 19) {
      List<String> splitDateTime = dataOperacao.substring(0, 19).split('T');
      String time = splitDateTime[1];
      List<String> split = splitDateTime[0].split('-');

      String date = '${split[2]}/${split[1]}/${split[0]}';
      dataOperacao = '$date - $time';
    }

    return Registro(
        dataOperacao,
        numeroCarteira,
        operacao,
        codigoLinha,
        linha,
        trecho,
        empresa,
        codigoExternoVeiculo,
        matriculaCobrador,
        nomeCobrador,
        valor,
        saldoCartao,
        saldoAgendado);
  }
}
