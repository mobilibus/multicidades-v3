enum STATUS {
  OPENED,
  DENIED,
  CLOSED,
  CANCELLED,
}

class MobilibusTransaction {
  final double amount;
  final String tokenDataprom;
  final int createTime;
  final int lastUpdate;
  final int cardType;
  final String tokenEucard;
  final String personalCard;
  final int transactionId;
  final STATUS status;

  MobilibusTransaction(
    this.amount,
    this.tokenDataprom,
    this.createTime,
    this.lastUpdate,
    this.cardType,
    this.tokenEucard,
    this.personalCard,
    this.transactionId,
    this.status,
  );

  factory MobilibusTransaction.fromJson(Map<String, dynamic> json) {
    double amount = json['amount'] ?? 0.0;
    String tokenDataprom = json['tokenDataprom'] ?? '';
    int createTime = json['createTime'] ?? -1;
    int lastUpdate = json['lastUpdate'] ?? -1;
    int cardType = json['cardType'] ?? -1;
    String tokenEucard = json['tokenEucard'] ?? '';
    String personalCard = json['personalCard'] ?? '';
    int transactionId = json['transactionId'] ?? -1;
    String statusStr = json['status'] ?? '';
    STATUS status;
    switch (statusStr) {
      case 'OPENED':
        status = STATUS.OPENED;
        break;
      case 'DENIED':
        status = STATUS.DENIED;
        break;
      case 'CLOSED':
        status = STATUS.CLOSED;
        break;
      case 'CANCELLED':
        status = STATUS.CANCELLED;
        break;
    }

    return MobilibusTransaction(amount, tokenDataprom, createTime, lastUpdate,
        cardType, tokenEucard, personalCard, transactionId, status);
  }
}
