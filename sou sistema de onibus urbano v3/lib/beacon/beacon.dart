import 'dart:async';
import 'dart:convert' as convert;
import 'dart:io';

import 'package:beacon_broadcast/beacon_broadcast.dart';
import 'package:flutter_beacon/flutter_beacon.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';

class BeaconService {
  final List<Region> regions = List.of(
    [
      Platform.isAndroid
          ? Region(identifier: 'beacon')
          : Region(
              identifier: 'Apple Airlocate',
              proximityUUID: 'E2C56DB5-DFFB-48D2-B060-D0F5A71096E0',
            )
    ],
    growable: false,
  );

  Timer _timer;
  Map<String, DateTime> dateTimeByBeacon = {};
  List<Beacon> mBeacons = [];
  bool isAdvertising = false;
  BeaconBroadcast beaconBroadcast;
  StreamSubscription<RangingResult> rangingBeacon;

  BeaconService() {
    if (Platform.isAndroid)
      Future.delayed(Duration(seconds: 2), () async {
        try {
          flutterBeacon.initializeScanning;

          rangingBeacon = flutterBeacon
              .ranging(regions)
              .listen((rangingResult) => _updateBeacons(rangingResult.beacons));

          _timer = Timer(Duration(minutes: 1), _sendEvent);
        } catch (e) {}

        try {
          beaconBroadcast = BeaconBroadcast();

          BeaconStatus transmissionSupportStatus =
              await beaconBroadcast.checkTransmissionSupported();

          switch (transmissionSupportStatus) {
            case BeaconStatus.supported:
              //'You are good to go, you can advertise as a beacon';
              await beaconBroadcast
                  .setIdentifier('broadcast iBeacon')
                  .setUUID('E2C56DB5-DFFB-48D2-B060-D0F5A71096E0')
                  .setLayout('m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24')
                  .setManufacturerId(0x004c)
                  .setMajorId(1)
                  .setMinorId(100)
                  .start();
              break;
            case BeaconStatus.notSupportedMinSdk:
              //'Your Android system version is too low (min. is 21)';
              break;
            case BeaconStatus.notSupportedBle:
              //'Your device does not support BLE';
              break;
            case BeaconStatus.notSupportedCannotGetAdvertiser:
              //'Either your chipset or driver is incompatible';
              break;
          }
        } catch (e) {}
      });
  }
  void stop() {
    beaconBroadcast.stop();
    rangingBeacon.cancel();
    flutterBeacon.close;
    _timer?.cancel();
  }

  void _sendEvent() {
    if (mBeacons.isNotEmpty) {
      List data = mBeacons.map((b) => b.toJson).toList();
      String beaconListJson = convert.json.encode(data);
      MOBApiUtils.mobEvent('beacon event', {'beacons': beaconListJson});
      mBeacons.clear();
    }
  }

  void _updateBeacons(List<Beacon> beacons) {
    beacons.forEach((beacon) {
      dateTimeByBeacon[beacon.proximityUUID] = DateTime.now();
      if (!_containsBeacon(beacon))
        mBeacons.add(beacon);
      else if (_containsBeacon(beacon))
        mBeacons[beacons.indexWhere(
            (b) => b.proximityUUID == beacon.proximityUUID)] = beacon;
    });
  }

  bool _containsBeacon(Beacon beacon) {
    for (final mBeacon in mBeacons)
      if (mBeacon.proximityUUID == beacon.proximityUUID) return true;
    return false;
  }
}
