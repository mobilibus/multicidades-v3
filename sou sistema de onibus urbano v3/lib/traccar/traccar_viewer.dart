import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobilibus/base/traccar/MOBTraccarDevice.dart';
import 'package:mobilibus/base/traccar/MOBTraccarUtils.dart';
import 'package:mobilibus/traccar/traccar_viewer_track.dart';
import 'package:mobilibus/utils/Utils.dart';

class TraccarViewer extends StatefulWidget {
  _TraccarViewer createState() => _TraccarViewer();
}

class _TraccarViewer extends State<TraccarViewer> {
  Timer timer;
  bool loaded = false;
  bool lite = false;
  List<MOBTraccarDevice> devices = [];
  TextEditingController tecFilterTraccar = TextEditingController();
  Map<int, bool> checkedTraccar = {};

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      timer = Timer.periodic(
          Duration(seconds: Utils.updateDelay), (timer) => updateAll());
      updateAll();
    });
  }

  void updateAll() async {
    await getDevices();

    setState(() => loaded = true);
  }

  Future<void> getDevices() async =>
      devices = await MOBTraccarUtils.getTraccarDevices();

  void seeAllOnline() {
    List<MOBTraccarDevice> traccarDevices =
        devices.where((device) => device.status != 'offline').toList();
    selectedDevices(traccarDevices);
  }

  void seeAllOffline() {
    List<MOBTraccarDevice> traccarDevices =
        devices.where((device) => device.status == 'offline').toList();
    selectedDevices(traccarDevices);
  }

  void selectedDevices([List<MOBTraccarDevice> traccarDevices]) async {
    List<MOBTraccarDevice> mTtraccarDevices = [];
    if (traccarDevices == null) {
      List<int> ids =
          checkedTraccar.keys.where((key) => checkedTraccar[key]).toList();

      mTtraccarDevices =
          devices.where((device) => ids.contains(device.id)).toList();
    } else
      mTtraccarDevices = traccarDevices;

    await showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) => Container(
        height: MediaQuery.of(context).size.height - kToolbarHeight,
        child: TraccarViewerTrack(mTtraccarDevices),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    String text = tecFilterTraccar.text.toLowerCase();
    List<MOBTraccarDevice> mDevices = [];
    if (text.isNotEmpty) {
      mDevices = List.of(devices);
      mDevices.removeWhere((device) {
        String name = device.name ?? '';
        String status = device.status;
        String strId = device.id.toString();
        String strPositionId = device.positionId.toString();
        String lastUpdate = device.lastUpdate.toString();
        return lite
            ? !name.toLowerCase().contains(text)
            : !name.toLowerCase().contains(text) &&
                !strId.contains(text) &&
                !strPositionId.contains(text) &&
                !lastUpdate.contains(text) &&
                !status.toLowerCase().contains(text);
      });
    } else
      mDevices = devices;

    bool hasSelectedTraccar = false;
    for (final selected in checkedTraccar.values)
      if (selected) {
        hasSelectedTraccar = true;
        break;
      }

    List<int> keys =
        checkedTraccar.keys.where((key) => checkedTraccar[key]).toList();

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Container(
          padding: EdgeInsets.all(5),
          child: TextField(
            controller: tecFilterTraccar,
            onChanged: (text) => setState(() {}),
            decoration: InputDecoration(
              hintText: 'Filtrar',
              hintStyle:
                  TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              border: OutlineInputBorder(),
            ),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () => setState(() => lite = !lite),
            icon: Icon(
              lite ? Icons.list : Icons.filter_list,
              size: 16,
            ),
          ),
          IconButton(
            onPressed: seeAllOnline,
            icon: Icon(
              Icons.signal_wifi_4_bar,
              size: 16,
            ),
          ),
          IconButton(
            onPressed: seeAllOffline,
            icon: Icon(
              Icons.signal_wifi_off,
              size: 16,
            ),
          ),
        ],
      ),
      floatingActionButton: hasSelectedTraccar
          ? FloatingActionButton(
              onPressed: selectedDevices,
              child: Icon(Icons.track_changes),
            )
          : null,
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Scrollbar(
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: mDevices.length,
              padding: EdgeInsets.only(top: hasSelectedTraccar ? 50 : 0),
              itemBuilder: (context, index) {
                MOBTraccarDevice traccarDevice = mDevices[index];
                int id = traccarDevice.id;
                String lastUpdate = traccarDevice.lastUpdate;
                String status = traccarDevice.status;
                String name = traccarDevice.name;

                Color color = traccarDevice.status == 'offline'
                    ? Colors.grey[350]
                    : Colors.green[400];

                int positionId = traccarDevice.positionId;
                double width = (MediaQuery.of(context).size.width / 4);

                bool checked = checkedTraccar[id] ?? false;

                return Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    color: color,
                    child: InkWell(
                      onTap: () =>
                          setState(() => checkedTraccar[id] = !checked),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Row(
                          children: [
                            Container(
                              width: width * 3,
                              child: lite
                                  ? Text('Nome=$name')
                                  : Wrap(
                                      spacing: 16.0,
                                      runSpacing: 10.0,
                                      children: [
                                        Text('ID=$id'),
                                        Text('Position ID=$positionId'),
                                        Text('Nome=$name'),
                                        Text('Status=$status'),
                                        Text('Last Update=$lastUpdate'),
                                      ],
                                    ),
                            ),
                            Container(
                              width: width - 48,
                              child: Checkbox(
                                  value: checked,
                                  onChanged: (value) => setState(
                                      () => checkedTraccar[id] = value)),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          if (keys.isNotEmpty)
            Container(
              height: 45,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemCount: keys.length,
                itemBuilder: (context, index) {
                  int id = keys[index];
                  MOBTraccarDevice traccarDevice =
                      devices.firstWhere((device) => device.id == id);
                  String name = traccarDevice.name;
                  return Chip(
                    label: Text(name),
                    backgroundColor: Theme.of(context).accentColor,
                    onDeleted: () => setState(() => checkedTraccar[id] = false),
                  );
                },
              ),
            ),
          if (!loaded)
            Center(
              child: CircularProgressIndicator(
                strokeWidth: 5,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
              ),
            ),
        ],
      ),
    );
  }
}
