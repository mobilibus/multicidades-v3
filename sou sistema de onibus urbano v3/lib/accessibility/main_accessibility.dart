import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:mobilibus/accessibility/stops/near_stops_accessibility.dart';
import 'package:mobilibus/accessibility/timetable/search_timetable_accessibility.dart';
import 'package:mobilibus/accessibility/tripplanner/search_tripplanner_accessibility.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainPageAccessibility extends StatefulWidget {
  _MainPageAccessibility createState() => _MainPageAccessibility();
}

class _MainPageAccessibility extends State<MainPageAccessibility> {
  FlutterTts flutterTts = FlutterTts();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () {
      flutterTts.setLanguage('pt-BR');
      flutterTts.speak(
          'Este é o modo de acessibilidade, selecione uma das opções disponíveis.');
    });
    Future.delayed(Duration(), () async {
      if (Utils.project != null)
        await Utils.downloadTimetableList(Utils.project);
      if (Utils.lines.isEmpty) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        int projectId = Utils.project.projectId;
        String json = prefs.getString('timetable_$projectId') ?? '[]';
        Iterable iterable = convert.json.decode(json);
        Utils.lines = iterable.map((e) => MOBLine.fromJson(e)).toList();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    TextStyle textStyle =
        TextStyle(color: Utils.getColorByLuminanceTheme(context));
    double height = MediaQuery.of(context).size.height / 5;
    EdgeInsets edge = EdgeInsets.only(top: 10);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            //mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 60, //height,
              ),
              Image.asset(
                'assets/mobilibus/access.png',
                height: (MediaQuery.of(context).size.height / 4),
                semanticLabel:
                    'Imagem de um homem cego junto a um cão guia, segurando o bastão na mão esquerda e a corda da coleira na direita.',
              ),
              Container(
                height: 20, //height,
              ),
              Container(
                height: 100, //height,
                child: Semantics(
                  button: true,
                  child: RaisedButton(
                    color: color,
                    child: Text('Pontos de Ônibus Próximos', style: textStyle),
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NearStopsAccessibility())),
                  ),
                ),
              ),
              Container(
                padding: edge,
                height: 100, //height,
                child: Semantics(
                  button: true,
                  child: RaisedButton(

                    color: color,
                    child: Text('Horários de Partida', style: textStyle),
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => TimetableAcessibility())),
                  ),
                ),
              ),
              /*
              Container(
                padding: edge,
                height: 100, //height,
                child: Semantics(
                  button: true,
                  child: RaisedButton(
                    color: color,
                    child: Text('Como Chegar', style: textStyle),
                    onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                SearchTripplannerAccessibility())),
                  ),
                ),
              ),
              */
              Container(
                height: 30, //height,
              ),
              Text('Este é o Modo de Acessibilidade, disponível para usuários ' +
                  'com deficiência visual que tenham recursos de conversão de texto em voz ativos no aparelho.',
                  style: TextStyle(
                      color: Colors.black, fontSize: 14), textAlign: TextAlign.center,),
            ],
          )
        ],
      ),
    );
  }
}
