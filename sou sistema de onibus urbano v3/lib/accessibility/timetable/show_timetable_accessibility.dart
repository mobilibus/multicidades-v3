import 'package:flutter/material.dart';
import 'package:mobilibus/base/MOBLine.dart';

class ShowTimetableAccessibility extends StatefulWidget {
  ShowTimetableAccessibility(
      this.line, this.timetable, this.direction, this.service);

  final MOBLine line;
  final MOBTimetable timetable;
  final MOBDirection direction;
  final MOBService service;

  _ShowTimetableAccessibility createState() =>
      _ShowTimetableAccessibility(line, timetable, direction, service);
}

class _ShowTimetableAccessibility extends State<ShowTimetableAccessibility> {
  _ShowTimetableAccessibility(
      this.line, this.timetable, this.direction, this.service)
      : super();

  final MOBLine line;
  final MOBTimetable timetable;
  final MOBDirection direction;
  final MOBService service;

  List<MOBLineDeparture> departures;
  List<MOBTrip> trips;
  Map<int, String> seqs = {};

  @override
  void initState() {
    trips = timetable.trips;
    departures = service.departures;
    for (final trip in trips) seqs[trip.seq] = trip.tripDesc;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String desc = service.desc;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Horários de $desc'),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: departures.length,
        itemBuilder: (context, index) {
          MOBLineDeparture departure = departures[index];

          String dep = departure.dep;
          int seq = departure.seq;
          String desc = seqs[seq];

          return ListTile(
            title: Text(dep),
            subtitle: Text('Trajeto: $desc'),
            isThreeLine: true,
            dense: true,
            enabled: true,
          );
        },
      ),
    );
  }
}
