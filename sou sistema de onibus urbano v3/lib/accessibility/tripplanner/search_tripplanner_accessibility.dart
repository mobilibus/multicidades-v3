import 'dart:convert' as convert;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_speech/flutter_speech.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:mobilibus/app/urban/tripplaner/trip_planner.dart';
import 'package:mobilibus/base/otp/OTPGeocode.dart';
import 'package:mobilibus/base/otp/OTPPlan.dart';

import 'package:mobilibus/app/urban/tripplaner/viewer/trip_planner_viewer.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/TripplanerUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class SearchTripplannerAccessibility extends StatefulWidget {
  _SearchTripplannerAccessibility createState() =>
      _SearchTripplannerAccessibility();
}

class _SearchTripplannerAccessibility
    extends State<SearchTripplannerAccessibility> {
  FlutterTts flutterTts = FlutterTts();
  TextEditingController searchController = TextEditingController();
  SpeechRecognition speech = SpeechRecognition();
  bool speechRecognitionAvailable = false;
  String currentLocale = 'pt_BR';
  bool isListening = false;
  String transcription = '';

  @override
  void initState() {
    super.initState();

    flutterTts.setLanguage('pt-BR');

    speech
        .activate('pt_BR')
        .then((res) => setState(() => speechRecognitionAvailable = res))
        .then((onValue) {
      speech.setAvailabilityHandler(
          (bool result) => setState(() => speechRecognitionAvailable = result));
      speech.setRecognitionStartedHandler(
          () => setState(() => isListening = true));
      speech.setRecognitionResultHandler((String text) {
        setState(() {
          searchController.text = text;
          if (transcription.isNotEmpty && !isListening) search();
        });
      });
      speech.setRecognitionCompleteHandler((listening) => setState(() {
            searchController.text = listening;
            transcription = listening;
            isListening = false;
          }));
      speech.setErrorHandler(() {
        transcription = '';
        isListening = false;
        setState(() {});
      });

      speech.activate(currentLocale).then((res) {
        setState(() => setState(() => speechRecognitionAvailable = res));
      });
    });
  }

  void search() async {
    await flutterTts.speak('Buscando!');
    http.Response response =
        await MOBApiUtils.getGeocodeFuture(transcription, Utils.project.otpUri);
    isListening = false;
    String body = convert.utf8.decode(response.bodyBytes);
    Map<String, dynamic> map = convert.json.decode(body);
    OTPGeocode geocode = OTPGeocode.fromJson(map);
    if (geocode.results.length > 0) {
      LocationData locationData = Utils.locationData;

      OTPGeocodeResult geocodeResult = geocode.results[0];
      List<Widget> listItineraries = [];
      List<List<Widget>> tripDetails = [];
      DateTime dateTime = DateTime.now();

      DateFormat dateFormatDate = DateFormat('MM-dd-yyyy');
      DateFormat dateFormatTime = DateFormat('HH:mm');

      String date = dateFormatDate.format(dateTime);
      String time = dateFormatTime.format(dateTime);

      bool isArriveBy = false;
      OTPPlan plan = await MOBApiUtils.getTripplanner(
        Point(locationData.latitude, locationData.longitude),
        Point(geocodeResult.latitude, geocodeResult.longitude),
        isArriveBy,
        date,
        time,
      );

      if (plan == null) return;

      TripplanerUtils.decodePlan(
          plan, tripDetails, listItineraries, [], context);
      List<OTPItinerary> itineraries = plan.itineraries;
      OTPItinerary i = itineraries[0];
      List<Widget> widgetTripDetails = tripDetails[0];
      String detailsTP = '';
      TripPlannerViewer w = TripPlannerViewer(
        i,
        widgetTripDetails,
        false,
        timeStartGB[0],
        timeEndGB[0],
        durationDistanceGB[0],
        destiny: transcription,
      );
      Navigator.push(context, MaterialPageRoute(builder: (context) => w));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Planejador de Viagem'),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          RaisedButton(
            child: Column(
              children: <Widget>[
                Text(
                  'Clique aqui para usar o comando de voz.',
                  style: TextStyle(
                      color: isListening
                          ? Colors.green
                          : Utils.getColorByLuminanceTheme(context)),
                ),
                Icon(
                  Icons.mic,
                  color: isListening
                      ? Colors.green
                      : Utils.getColorByLuminanceTheme(context),
                ),
              ],
            ),
            color: isListening ? Colors.green : Theme.of(context).primaryColor,
            onPressed: isListening
                ? null
                : () => speech.activate(currentLocale).then((_) => speech
                    .listen()
                    .then((result) => setState(() => isListening = result))),
          ),
          TextField(
            controller: searchController,
            onChanged: (text) => setState(() {
              transcription = text;
            }),
            decoration: InputDecoration(
                hintText: 'Digite o Local desejado, e após, confirme.',
                border: OutlineInputBorder()),
          ),
          RaisedButton(
            color: Theme.of(context).primaryColor,
            child: Text(
              'Confirmar',
              style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
            ),
            onPressed: () => search(),
          ),
        ],
      ),
    );
  }
}
