import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobilibus/addons/recargaagora/module/recarga_detalhes.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromTransportCard.dart';
import 'package:mobilibus/base/recargaagora/gimave/EucaturAPIs.dart';
import 'package:mobilibus/utils/Utils.dart';

class TokenRequest extends StatefulWidget {
  TokenRequest(
      this.cardMask,
      this.token,
      this.transactionId,
      this.rechargeId,
      this.cardNumber,
      this.amount,
      this.transportCardUser,
      this.cardType,
      this.isDatapromProduction,
      this.isGimaveProduction);
  final String cardMask;
  final String token;
  final int transactionId;
  final String rechargeId;
  final String cardNumber;
  final double amount;
  final DatapromTransportCardUser transportCardUser;
  final String cardType;
  final bool isDatapromProduction;
  final bool isGimaveProduction;
  _TokenRequest createState() => _TokenRequest(
        cardMask,
        token,
        transactionId,
        rechargeId,
        cardNumber,
        amount,
        transportCardUser,
        cardType,
        isDatapromProduction,
        isGimaveProduction,
      );
}

class _TokenRequest extends State<TokenRequest> {
  _TokenRequest(
      this.cardMask,
      this.token,
      this.transactionId,
      this.rechargeId,
      this.cardNumber,
      this.amount,
      this.transportCardUser,
      this.cardType,
      this.isDatapromProduction,
      this.isGimaveProduction)
      : super();
  final String cardMask;
  final String token;
  final int transactionId;
  final String rechargeId;
  final String cardNumber;
  final double amount;
  final DatapromTransportCardUser transportCardUser;
  final String cardType;
  final bool isDatapromProduction;
  final bool isGimaveProduction;

  bool isTokenPaying = false;
  int duration = 10;
  Timer timer;

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(
        Duration(seconds: 1),
        () => timer = Timer.periodic(Duration(seconds: 1), (t) {
              duration--;
              setState(() {});
              if (duration == 0) t.cancel();
            }));
  }

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: Colors.white);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Pagamento por token', style: textStyle),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: themeColor,
          ),
          onPressed: () => Navigator.pop(context, null),
        ),
      ),
      body: isTokenPaying
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('Aguarde... Finalizando transação...'),
                  CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Utils.isLightTheme(context)
                              ? Utils.getColorFromPrimary(context)
                              : Colors.black))
                ],
              ),
            )
          : Container(
              padding: EdgeInsets.all(30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('Cartão: $cardMask'),
                  Text(
                    'Você já realizou um pagamento com este cartão anteriormente, deseja efetuar o pagamento com o mesmo? Não será necessário informar os dados novamente.',
                    textAlign: TextAlign.center,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                          color: Theme.of(context).primaryColor,
                          onPressed: () async {
                            if (duration > 0) return;
                            isTokenPaying = true;
                            setState(() {});
                            await EucaturAPIs.payWithToken(
                              context,
                              //transactionId,
                              //rechargeId,
                              cardNumber,
                              amount,
                              isDatapromProduction,
                              isGimaveProduction,
                            );
                            isTokenPaying = false;
                            setState(() {});
                          },
                          child: Text(
                            duration > 0 ? 'Aguarde... ($duration)' : 'SIM',
                            style: textStyle,
                          )),
                      RaisedButton(
                          color: Theme.of(context).primaryColor,
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                                    builder: (context) => RecargaDetalhes(
                                          transportCardUser,
                                          cardNumber,
                                          cardType,
                                          rechargeId,
                                          transactionId,
                                          amount,
                                          isDatapromProduction,
                                          isGimaveProduction,
                                        )));
                          },
                          child: Text('Não', style: textStyle)),
                    ],
                  ),
                ],
              ),
            ),
    );
  }
}
