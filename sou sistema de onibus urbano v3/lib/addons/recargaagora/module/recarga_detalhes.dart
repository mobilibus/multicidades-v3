import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/addons/recargaagora/module/detalheswidgets/card_address.dart';
import 'package:mobilibus/addons/recargaagora/module/detalheswidgets/card_details.dart';
import 'package:mobilibus/addons/recargaagora/module/detalheswidgets/holder_details.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromTransportCard.dart';
import 'package:mobilibus/base/recargaagora/gimave/EucaturAPIs.dart';
import 'package:mobilibus/utils/Utils.dart';

class RecargaDetalhes extends StatefulWidget {
  RecargaDetalhes(
      this.transportCardUser,
      this.cardNumber,
      this.cardType,
      this.rechargeId,
      this.transactionId,
      this.amount,
      this.isDatapromProduction,
      this.isGimaveProduction);
  final DatapromTransportCardUser transportCardUser;
  final String cardNumber;
  final String cardType;
  final String rechargeId;
  final int transactionId;
  final double amount;
  final bool isDatapromProduction;
  final bool isGimaveProduction;
  _RecargaDetalhes createState() => _RecargaDetalhes(
        transportCardUser,
        cardNumber,
        cardType,
        rechargeId,
        transactionId,
        amount,
        isDatapromProduction,
        isGimaveProduction,
      );
}

class _RecargaDetalhes extends State<RecargaDetalhes>
    with TickerProviderStateMixin {
  _RecargaDetalhes(
      this.transportCardUser,
      this.datapromCardNumber,
      this.cardType,
      this.rechargeId,
      this.transactionId,
      this.amount,
      this.isDatapromProduction,
      this.isGimaveProduction)
      : super();
  final DatapromTransportCardUser transportCardUser;
  final String datapromCardNumber;
  final String cardType;
  final String rechargeId;
  final int transactionId;
  final double amount;
  final bool isDatapromProduction;
  final bool isGimaveProduction;

  bool isLoading = false;

  Map<String, String> formMap = {};
  List<GlobalKey<FormState>> formKeys = [
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
  ];

  void done() async {
    setState(() {});

    bool form1 = formKeys[0].currentState.validate();
    bool form2 = formKeys[1].currentState.validate();
    bool form3 = formKeys[2].currentState.validate();

    if (form1 && form2 && form3) {
      isLoading = true;
      setState(() {});
      try {
        String name = formMap['name'];
        String email = formMap['email'];
        String cpf = formMap['cpf'];
        String phone = formMap['phone'];

        String cardFlag = formMap['cardFlag'];
        String cardNumber = formMap['cardNumber'];
        String cardCvv = formMap['cardCvv'];
        String expireMonth = formMap['expireMonth'];
        String expireYear = formMap['expireYear'];

        String zipcode = formMap['zipcode'];
        String place = formMap['place'];
        String country = formMap['country'];
        String state = formMap['state'];
        String city = formMap['city'];
        String neighborhood = formMap['neighborhood'];
        String street = formMap['street'];
        String placeNumber = formMap['placeNumber'];

        DateTime bornDate = transportCardUser.bornDate;
        String strBornDate = DateFormat('yyyy-MM-dd').format(bornDate);

        DateTime now = DateTime.now();
        String createdAt = DateFormat('yyyy-MM-dd').format(now);

        ///JSON

        Map<String, dynamic> address = {
          'street': street,
          'additional_details': place,
          'number': int.parse(placeNumber),
          'neighborhood': neighborhood,
          'city': city,
          'state': state,
          'zip_code': zipcode,
          'country': country,
        };

        Map<String, dynamic> eucaturPay = {
          'amount': amount,
          'currency': 'BRL',
          'method': 'CREDIT_CARD',
          'metadata': {
            'cpf': cpf,
            'dataprom_token': rechargeId,
            'dataprom_card': cardNumber,
            'dataprom_cardCode': cardType,
            'dataprom_borndate': strBornDate,
          },
          'customer': {
            'created_at': createdAt,
            'email': email,
            'name': name,
            'type': 'PRIVATE',
            'cpf': cpf,
            'phone': phone,
            'address': address,
          },
          'seller': {
            'id': '6024',
            'name': 'Cade_Meu_Onibus_Manaus',
            'created_at': createdAt,
          },
          'shopping_cart': [
            {
              'amount': amount,
              'description': 'Recarga Sinetram',
              'quantity': 1,
            }
          ],
          'credit_card': {
            'holder_name': name,
            'brand': cardFlag,
            'create_token': true,
            'installments': 1,
            'number': cardNumber,
            'expiry_month': int.parse(expireMonth),
            'expiry_year': int.parse(expireYear),
            'cvv': cardCvv,
            'billing': {
              'type': 'PRIVATE',
              'identity_code': cpf,
              'full_name': name,
              'phone': phone,
              'address': address,
            }
          }
        };

        Map<String, dynamic> map = await EucaturAPIs.pay(
          eucaturPay,
          //transactionId,
          //rechargeId,
          datapromCardNumber,
          isDatapromProduction,
          isGimaveProduction,
        );

        TextStyle textStyle = TextStyle(color: Colors.white);

        showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            backgroundColor: Theme.of(context).primaryColor,
            title: Text(map.isEmpty ? 'Sucesso!' : 'Erro', style: textStyle),
            content: Container(
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Text(
                      map.isEmpty
                          ? 'Pagamento realizado com sucesso! Você pode conferir no histórico! Foi criado um token com o seu cartão, assim você poderá efetuar recarga de maneira mais rápida!'
                          : map['causa'],
                      style: textStyle),
                  Text(
                    map.isEmpty ? '' : map['erro'],
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    map.isEmpty ? '' : map['antifraud'],
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    if (map.isEmpty) Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                  child: Text('Fechar', style: textStyle))
            ],
          ),
        );
      } on Exception {} finally {
        isLoading = false;
        setState(() {});
      }
    }
  }

  Widget header(String name) => Container(
        color: Theme.of(context).primaryColor,
        padding: EdgeInsets.all(20),
        child: Center(
          child: Text(
            name,
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Pagamento', style: textStyle),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: themeColor,
          ),
          onPressed: () => Navigator.pop(context, null),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: isLoading ? null : done,
        child: isLoading
            ? CircularProgressIndicator(
                strokeWidth: 3,
                valueColor: AlwaysStoppedAnimation<Color>(
                    Utils.isLightTheme(context)
                        ? Utils.getColorFromPrimary(context)
                        : Colors.black))
            : Icon(
                Icons.done,
                size: 50,
              ),
      ),
      body: isLoading
          ? Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                  Text('Aguarde... Finalizando transação...'),
                  CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Utils.isLightTheme(context)
                              ? Utils.getColorFromPrimary(context)
                              : Colors.black))
                ]))
          : Scrollbar(
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(bottom: 100),
                  child: Column(
                    children: [
                      header('Dados do Titular do Cartão'),
                      HolderDetails(formMap, formKeys[0]),
                      header('Dados do Cartão'),
                      CardDetails(formMap, formKeys[1]),
                      header('Endereço de fatura do Cartão'),
                      CardAddress(formMap, formKeys[2]),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
