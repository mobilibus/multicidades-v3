import 'package:cpfcnpj/cpfcnpj.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/base/recargaagora/dataprom/Dataprom.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromAPIs.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromTransportCard.dart';
import 'package:mobilibus/base/recargaagora/mobilibus/MobilibusAPIs.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'module/recarga_agora.dart';

class RecargaAgoraLogin extends StatefulWidget {
  _RecargaAgoraLogin createState() => _RecargaAgoraLogin();
}

class _RecargaAgoraLogin extends State<RecargaAgoraLogin> {
  TextEditingController datapromCardTec = TextEditingController();
  TextEditingController datapromCPFTec = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  String day = '00';
  String month = '00';
  String year = '0000';

  String datapromCard = '';
  String datapromCPF = '';

  bool isLoginIn = false;

  Dataprom dataprom;
  DatapromTicketing bilhetagem;

  bool isGimaveProduction = Utils.isMobilibus ? false : true;
  bool isDatapromProduction = Utils.isMobilibus ? false : true;
  bool validateCPFandBornDate = Utils.isMobilibus ? false : true;

  final TextEditingController tecBornDay = TextEditingController();
  final TextEditingController tecBornMonth = TextEditingController();
  final TextEditingController tecBornYear = TextEditingController();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      DateTime now = DateTime.now();
      SharedPreferences sp = await SharedPreferences.getInstance();

      datapromCard = sp.getString('recargaagora_dataprom_card') ?? '';
      datapromCPF = sp.getString('recargaagora_dataprom_cpf') ?? '';

      datapromCardTec.text = datapromCard;
      datapromCPFTec.text = datapromCPF;

      day = sp.getString('recargaagora_dataprom_birthdate_day') ??
          (now.day < 10 ? '0${now.day}' : now.day.toString());
      month = sp.getString('recargaagora_dataprom_birthdate_month') ??
          (now.month < 10 ? '0${now.month}' : now.month.toString());
      year = sp.getString('recargaagora_dataprom_birthdate_year') ??
          (now.year < 10 ? '0${now.year}' : now.year.toString());
      tecBornDay.text = day.toString();
      tecBornMonth.text = month.toString();
      tecBornYear.text = year.toString();

      try {
        dataprom = await DatapromAPIs.getSystem(isDatapromProduction);
        if (dataprom != null) bilhetagem = dataprom.ticketings.first;
      } catch (e) {
        TextStyle textStyle = TextStyle(color: Colors.white);

        showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            backgroundColor: Theme.of(context).primaryColor,
            title: Text('Erro', style: textStyle),
            content: Text(e, style: textStyle),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('Fechar', style: textStyle))
            ],
          ),
        );
      }
      setState(() {});
    });
  }

  void validate() async {
    if (formKey.currentState.validate() || !validateCPFandBornDate) {
      isLoginIn = true;
      setState(() {});

      try {
        String cardNumber = datapromCardTec.text;
        DatapromTransportCard transportCard =
            await DatapromAPIs.getDatapromCard(
                cardNumber, isDatapromProduction);
        DatapromTransportCardUser transportCardUser =
            transportCard.cardTransportUser;
        DateTime bornDate = transportCardUser.bornDate;
        String selectedDate = '$day/$month/$year';

        String dataNascimentoFormat = DateFormat('dd/MM/yyyy').format(bornDate);

        String cpf = transportCardUser.document;
        String selectedCpf = datapromCPFTec.text;

        if (validateCPFandBornDate) if (cpf.isEmpty)
          throw 'Inconsistência de dados. Verifique o cadastro no Sinetram.';

        if (validateCPFandBornDate == false ||
            (validateCPFandBornDate &&
                dataNascimentoFormat == selectedDate &&
                cpf == selectedCpf)) {
          if (validateCPFandBornDate) {
            SharedPreferences sp = await SharedPreferences.getInstance();

            if (sp.containsKey('recargaagora_dataprom_card') &&
                sp.getString('recargaagora_dataprom_card') != cardNumber)
              MobilibusAPIs.clearTokens();

            sp.setString('recargaagora_dataprom_card', cardNumber);
            sp.setString('recargaagora_dataprom_cpf', cpf);
            sp.setString('recargaagora_dataprom_birthdate_day', day);
            sp.setString('recargaagora_dataprom_birthdate_month', month);
            sp.setString('recargaagora_dataprom_birthdate_year', year);
          }

          String error = await Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => RecargaAgora(
                    transportCardUser,
                    cardNumber,
                    bilhetagem,
                    transportCard,
                    isDatapromProduction,
                    isGimaveProduction,
                  )));
          if (error != null && error.isNotEmpty) {
            TextStyle style = TextStyle(color: Colors.white);
            showDialog(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  backgroundColor: Theme.of(context).primaryColor,
                  title: Text('Erro', style: style),
                  content: Text(error, style: style),
                  actions: [
                    FlatButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text('Fechar', style: style),
                    ),
                  ],
                ));
          }
        } else
          throw 'Dados informados não conferem.';
      } catch (e) {
        TextStyle textStyle = TextStyle(color: Colors.white);
        print(e);
        showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            backgroundColor: Theme.of(context).primaryColor,
            title: Text('Erro', style: textStyle),
            content: Text(e, style: textStyle),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('Fechar', style: textStyle))
            ],
          ),
        );
      }
      isLoginIn = false;
      setState(() {});
    }
  }

  void changedDate(DateTime dateTime) async {
    day = dateTime.day < 10 ? '0${dateTime.day}' : dateTime.day.toString();
    month =
        dateTime.month < 10 ? '0${dateTime.month}' : dateTime.month.toString();
    year = dateTime.year < 10 ? '0${dateTime.year}' : dateTime.year.toString();
    tecBornDay.text = '$day';
    tecBornMonth.text = '$month';
    tecBornYear.text = '$year';
    setState(() {});
  }

  Widget devWidgets() => Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Sistema (API)'),
              Text('Teste / Produção'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Dataprom (${isDatapromProduction ? 'Produção' : 'Teste'})'),
              Switch(
                  value: isDatapromProduction,
                  onChanged: (value) =>
                      setState(() => isDatapromProduction = value)),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Eucatur (${isGimaveProduction ? 'Produção' : 'Teste'})'),
              Switch(
                  value: isGimaveProduction,
                  onChanged: (value) =>
                      setState(() => isGimaveProduction = value)),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Validar CPF e Data de Nascimento'),
              Switch(
                  value: validateCPFandBornDate,
                  onChanged: (value) =>
                      setState(() => validateCPFandBornDate = value)),
            ],
          ),
        ],
      );

  Widget manualDateInput() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            width: 50,
            child: Column(
              children: [
                Text('Dia'),
                TextFormField(
                  onChanged: (text) {
                    if (text.isNotEmpty) {
                      int digit = int.parse(text);
                      day = digit < 10 ? '0$digit' : text;
                      setState(() {});
                    }
                  },
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  controller: tecBornDay,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 50,
            child: Column(
              children: [
                Text('Mês'),
                TextFormField(
                  onChanged: (text) {
                    if (text.isNotEmpty) {
                      int digit = int.parse(text);
                      month = digit < 10 ? '0$digit' : text;
                      setState(() {});
                    }
                  },
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  controller: tecBornMonth,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 70,
            child: Column(
              children: [
                Text('Ano'),
                TextFormField(
                  onChanged: (text) {
                    if (text.isNotEmpty) {
                      int digit = int.parse(text);
                      year = digit < 10 ? '0$digit' : text;
                      setState(() {});
                    }
                  },
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  controller: tecBornYear,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                ),
              ],
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor, fontSize: 20);

    return Scaffold(
      appBar: AppBar(
        title: Text('Recarga de Créditos', style: textStyle),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: themeColor,
          ),
          onPressed: () => Navigator.pop(context, null),
        ),
      ),
      floatingActionButton: isLoginIn
          ? null
          : FloatingActionButton.extended(
              heroTag: 'heroLoginDone',
              icon: Icon(Icons.done),
              onPressed: validate,
              label: Text('Entrar'),
            ),
      body: isLoginIn
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('Aguarde...'),
                  CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Utils.isLightTheme(context)
                              ? Utils.getColorFromPrimary(context)
                              : Colors.black))
                ],
              ),
            )
          : Scrollbar(
              child: SingleChildScrollView(
                padding: EdgeInsets.only(bottom: 100),
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Form(
                    key: formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0))),
                              child: Container(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.all(10),
                                      child: Text('Cartão Transporte e CPF'),
                                    ),
                                    if (Utils.isMobilibus) devWidgets(),
                                    Theme(
                                      data: ThemeData(
                                          hintColor:
                                              Theme.of(context).accentColor),
                                      child: TextFormField(
                                        validator: (text) => text.isEmpty
                                            ? 'Campo obrigatório'
                                            : null,
                                        controller: datapromCardTec,
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor),
                                        inputFormatters: [
                                          FilteringTextInputFormatter.digitsOnly
                                        ],
                                        keyboardType: TextInputType.number,
                                        decoration: InputDecoration(
                                          hintText: 'Número Cartão Transporte',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding:
                                          EdgeInsets.only(left: 30, right: 30),
                                      child: Divider(
                                        color: Theme.of(context).primaryColor,
                                        thickness: 1,
                                      ),
                                    ),
                                    Theme(
                                      data: ThemeData(
                                          hintColor:
                                              Theme.of(context).accentColor),
                                      child: TextFormField(
                                        validator: (text) => text.isEmpty
                                            ? 'Campo obrigatório'
                                            : !CPF.isValid(text)
                                                ? 'CPF inválido'
                                                : null,
                                        controller: datapromCPFTec,
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor),
                                        inputFormatters: [
                                          FilteringTextInputFormatter.digitsOnly
                                        ],
                                        keyboardType: TextInputType.number,
                                        decoration: InputDecoration(
                                          hintText: 'CPF',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0))),
                              child: Container(
                                padding: EdgeInsets.all(10),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.all(10),
                                      child: Text('Data de Nascimento'),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(10),
                                      child: manualDateInput(),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(10),
                                      child: Card(
                                        elevation: 5,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10.0))),
                                        color: Theme.of(context).accentColor,
                                        child: InkWell(
                                          onTap: () =>
                                              DatePicker.showDatePicker(
                                            context,
                                            minTime: DateTime(1900, 1, 1),
                                            maxTime: DateTime(9999, 12, 31),
                                            onConfirm: changedDate,
                                            currentTime: DateTime.now(),
                                            locale: LocaleType.pt,
                                          ),
                                          child: Container(
                                            padding: EdgeInsets.all(10),
                                            child: Wrap(
                                              alignment: WrapAlignment.center,
                                              crossAxisAlignment:
                                                  WrapCrossAlignment.center,
                                              children: <Widget>[
                                                Text('Selecionar data ',
                                                    style: textStyle),
                                                Icon(Icons.calendar_today),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
