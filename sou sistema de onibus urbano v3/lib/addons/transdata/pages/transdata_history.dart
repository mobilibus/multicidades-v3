import 'dart:convert' as convert;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:mobilibus/addons/transdata/services/extract_service.dart';
import 'package:mobilibus/addons/transdata/services/product_detail_service.dart';
import 'package:mobilibus/base/transdata/TransdataAPIs.dart';
import 'package:mobilibus/base/transdata/TransdataExtract.dart';
import 'package:mobilibus/base/transdata/TransdataHistory.dart';
import 'package:mobilibus/base/transdata/TransdataUser.dart';

class TransdataHistoryPage extends StatefulWidget {
  final TransdataAPIs api;
  final TransdataUser user;
  TransdataHistoryPage(this.api, this.user);

  _TransdataHistoryPage createState() => _TransdataHistoryPage(api, user);
}

class _TransdataHistoryPage extends State<TransdataHistoryPage>
    with AutomaticKeepAliveClientMixin {
  _TransdataHistoryPage(this.api, this.user);
  final TransdataAPIs api;
  final TransdataUser user;

  TransdataHistory transdataHistory;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 1), getHistory);
  }

  @override
  bool get wantKeepAlive => false;

  void getHistory() async {
    String cpf = user.cpf;
    int cardSerial = user.physicalSerial;
    DateTime dateTime = DateTime.now();

    DateFormat dateFormat = DateFormat('yyyy-MM-dd');

    String endDate = dateFormat.format(dateTime);
    String startDate =
        dateFormat.format(dateTime.subtract(Duration(days: 365)));

    startDate = '$startDate 00:00:00';
    endDate = '$endDate 23:59:59';

    transdataHistory =
        await api.getHistory(cardSerial, cpf, startDate, endDate);
    setState(() {});
  }

  void goExtract() async {
    DateTime dateStart = DateTime.now().subtract(Duration(days: 1));
    DateTime dateEnd = DateTime.now();

    var result = await showDialog(
      context: context,
      builder: (context) => StatefulBuilder(
        builder: (context, setState) => AlertDialog(
          title: Text('Selecione o periodo de tempo que deseja ver o extrato.'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Column(
                children: [
                  Text('Início'),
                  Container(
                    height: 100,
                    child: CupertinoDatePicker(
                      onDateTimeChanged: (dateTime) =>
                          setState(() => dateStart = dateTime),
                      initialDateTime:
                          DateTime.now().subtract(Duration(days: 1)),
                      mode: CupertinoDatePickerMode.date,
                    ),
                  ),
                ],
              ),
              Divider(
                height: 20,
              ),
              Column(
                children: [
                  Text('Fim'),
                  Container(
                    height: 100,
                    child: CupertinoDatePicker(
                      onDateTimeChanged: (dateTime) =>
                          setState(() => dateEnd = dateTime),
                      initialDateTime: DateTime.now(),
                      mode: CupertinoDatePickerMode.date,
                    ),
                  ),
                ],
              ),
            ],
          ),
          actions: [
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () => Navigator.of(context).pop(null),
            ),
            FlatButton(
              child: Text('Consultar'),
              onPressed: () => Navigator.of(context).pop(0),
            ),
          ],
        ),
      ),
    );

    if (result == null) return;

    int cardSerial = user.physicalSerial;
    String cpf = user.cpf;

    DateFormat df = DateFormat('yyyy-MM-dd');

    String startDate = df.format(dateStart);
    startDate = '$startDate 00:00:00';

    String endDate = df.format(dateEnd);
    endDate = '$endDate 23:59:59';

    http.Response response =
        await api.userCardExtract(cardSerial, cpf, startDate, endDate);
    if (response != null && response.statusCode == 200) {
      String json = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(json);
      TransdataExtract transdataExtract = TransdataExtract.fromJson(map);

      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ExtractService(api, user, transdataExtract)));
    }
  }

  void goTaxes() async {
    int cardSerial = user.logicalSerial;
    String cpf = user.cpf;

    //api.getTaxes(cardSerial, cpf, productIds, values)
  }

  void onTapProduct(TransdataHistoryItem item) {
    String id = item.id;
    Widget page = ProductDetailService(id, user, api);
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => page));
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    List<TransdataHistoryItem> items = transdataHistory?.items ?? [];
    DateFormat df = DateFormat('dd/MM/yyyy - HH:mm:ss');
    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          items.isEmpty
              ? Center(
                  child: transdataHistory == null
                      ? CircularProgressIndicator()
                      : Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              'Nenhum item encontrado',
                              textAlign: TextAlign.center,
                            ),
                            IconButton(
                                icon: Icon(Icons.refresh),
                                onPressed: getHistory)
                          ],
                        ),
                )
              : ListView.builder(
                  padding: EdgeInsets.only(bottom: 100),
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    TransdataHistoryItem item = items[index];

                    String real = (item.totalInCents / 100)
                        .toStringAsFixed(2)
                        .replaceAll('.', ',');

                    String requested = df.format(item.requested);
                    String validated = df.format(item.status);

                    return ListTile(
                      onTap: () => onTapProduct(item),
                      title: Text('R\$ $real'),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Solicitado: $requested'),
                          Text('Confirmado: $validated'),
                        ],
                      ),
                    );
                  },
                ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              RaisedButton(
                color: Theme.of(context).accentColor,
                child: Text('Extrato'),
                onPressed: goExtract,
              ),
              RaisedButton(
                color: Theme.of(context).accentColor,
                child: Text('Taxas'),
                onPressed: goTaxes,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
