import 'dart:async';
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:mobilibus/base/transdata/TransdataAPIs.dart';
import 'package:mobilibus/base/transdata/TransdataUser.dart';

class TransdataHomePage extends StatefulWidget {
  final TransdataAPIs api;
  final TransdataUser user;
  TransdataHomePage(this.api, this.user);

  _TransdataHomePage createState() => _TransdataHomePage(api, user);
}

class _TransdataHomePage extends State<TransdataHomePage>
    with AutomaticKeepAliveClientMixin {
  _TransdataHomePage(this.api, this.user);
  final TransdataAPIs api;
  final TransdataUser user;

  TransdataUserBalances userBalances;
  Timer timer;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

    timer = Timer.periodic(Duration(seconds: 5), (timer) => getBalance());

    getBalance();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void getBalance() async {
    String cpf = user.cpf;
    int cardSerial = user.logicalSerial;

    http.Response response = await api.getBalances([cpf], [cardSerial]);
    if (response != null && response.statusCode == 200) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(jsonString);
      userBalances = TransdataUserBalances.fromJson(map);

      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    String typeName = user.typeName;
    int type = user.type;

    String subtypeName = user.subtypeName;
    int subtype = user.subtype;

    int id = user.id;

    String date = DateFormat('dd/MM/yyyy - HH:mm:ss').format(user.date);

    TransdataUserBalanceItem balance =
        (userBalances != null && userBalances.balances.isNotEmpty)
            ? userBalances.balances.first
            : null;

    String real;
    if (balance != null) {
      int cents = balance.balanceInCents;
      real = (cents / 100).toStringAsFixed(2).replaceAll('.', ',');
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('ID: $id'),
                Text('Tipo: $type - $typeName'),
                Text('Subtipo: $subtype - $subtypeName'),
                Text('Data de Cadastro: $date'),
              ],
            ),
            if (balance != null)
              ListTile(
                title: Text('Saldo'),
                subtitle: Text('R\$ $real'),
              ),
          ],
        ),
      ),
    );
  }
}
