import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/base/transdata/TransdataAPIs.dart';
import 'package:mobilibus/base/transdata/TransdataProduct.dart';
import 'package:mobilibus/base/transdata/TransdataUser.dart';

class TransdataProducts extends StatefulWidget {
  TransdataProducts(this.api, this.user);
  final TransdataAPIs api;
  final TransdataUser user;

  _TransdataProducts createState() => _TransdataProducts(api, user);
}

class _TransdataProducts extends State<TransdataProducts>
    with AutomaticKeepAliveClientMixin {
  _TransdataProducts(this.api, this.user);
  final TransdataAPIs api;
  final TransdataUser user;

  TransdataProduct transdataProduct;

  Map<int, int> quantity = {};

  int selectedIndex;

  bool loading = true;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 1), getProducts);
  }

  void getProducts() async {
    int cardSerial = user.physicalSerial;
    int userId = user.id;
    String cpf = user.cpf;
    transdataProduct = await api.getRegisteredProducts(userId, cardSerial);
    if (transdataProduct != null) clear();

    loading = false;

    setState(() {});
  }

  void clear() {
    selectedIndex = null;
    for (int i = 0; i < transdataProduct.ret.length; i++) quantity[i] = 0;
    setState(() {});
  }

  void done(String total) async {
    int cardSerial = user.physicalSerial;
    String cpf = user.cpf;

    TransdataProductItem item = transdataProduct.ret[selectedIndex];

    int taxInCents = item.tax;

    int length = quantity[selectedIndex];
    List<TransdataProductItem> items = List.filled(length, item);

    http.Response response =
        await api.newPurchase(cardSerial, cpf, taxInCents, items);
    if (response != null && response.statusCode == 200) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(jsonString);

      TransdataProductItemCreated itemCreated =
          TransdataProductItemCreated.fromJson(map);

      clear();

      if (itemCreated.errorCode != 0) return;

      await showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Deseja efetuar a compra?'),
          content: Text('Total: R\$ $total'),
          actions: [
            FlatButton(
              child: Text('Confirmar'),
              onPressed: () => buy(itemCreated),
            ),
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () => cancel(itemCreated),
            ),
          ],
        ),
      );
    }
  }

  void buy(TransdataProductItemCreated itemCreated) async {
    String id = itemCreated.id;

    http.Response response = await api.confirmPurchase(id);
    if (response != null && response.statusCode == 200) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(jsonString);
      TransdataProductItemEnded ended = TransdataProductItemEnded.fromJson(map);
      Navigator.of(context).pop();
      await finish(ended);
    }
  }

  void cancel(TransdataProductItemCreated itemCreated) async {
    String id = itemCreated.id;

    List<PRODUCT_CANCEL> reasons = PRODUCT_CANCEL.values;

    int reason = await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Motivo'),
        content: Container(
          height: 300,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: reasons.length,
            itemBuilder: (context, index) {
              PRODUCT_CANCEL reason = reasons[index];
              String reasonName;
              switch (reason) {
                case PRODUCT_CANCEL.EXPIRED:
                  reasonName = 'Pedido Expirado';
                  break;
                case PRODUCT_CANCEL.DENIED:
                  reasonName = 'Pagamento Recusado';
                  break;
                case PRODUCT_CANCEL.CANCELLED_BY_INTEGRATOR:
                  reasonName = 'Cancelado pelo Integrador';
                  break;
                case PRODUCT_CANCEL.OTHER:
                  reasonName = 'Outros';
                  break;
              }
              return ListTile(
                title: Text(reasonName),
                onTap: () =>
                    Navigator.of(context).pop(api.getCancelIdByReason(reason)),
              );
            },
          ),
        ),
      ),
    );
    http.Response response = await api.cancelPurchase(id, reason ?? 99);
    if (response != null && response.statusCode == 200) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(jsonString);
      TransdataProductItemEnded ended = TransdataProductItemEnded.fromJson(map);
      Navigator.of(context).pop();
      await finish(ended);
    }
  }

  Future<void> finish(TransdataProductItemEnded ended) async {
    if (ended.errorCode != 0)
      await showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Erro'),
          content: Text(ended.errorMessage),
          actions: [],
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    double total = 0.0;
    List<TransdataProductItem> ret = transdataProduct?.ret ?? [];
    for (int i = 0; i < ret.length; i++) {
      int q = quantity[i] ?? 0;
      if (q < 0 || q == null)
        quantity[i] = 0;
      else if (q > 0) {
        TransdataProductItem item = ret[i];
        total += (item.unitValue / 100) * q;
      }
    }
    String mTotal = total.toStringAsFixed(2).replaceAll('.', ',');
    return Scaffold(
      floatingActionButton: total == 0.0
          ? null
          : FloatingActionButton.extended(
              onPressed: () => done(mTotal),
              label: Text('R\$: $mTotal'),
              icon: Icon(Icons.done),
            ),
      body: transdataProduct == null || transdataProduct.ret.isEmpty
          ? Center(
              child: loading
                  ? CircularProgressIndicator()
                  : Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text('Não foi possível encontrar produtos'),
                        IconButton(
                          icon: Icon(Icons.refresh),
                          onPressed: getProducts,
                        ),
                      ],
                    ))
          : selectedIndex != null
              ? Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(ret[selectedIndex].name),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Card(
                                color: Colors.green[400],
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  child: Text(
                                      'Valor: R\$ ${(ret[selectedIndex].unitValue / 100).toStringAsFixed(2).replaceAll('.', ',')}'),
                                ),
                              ),
                              Card(
                                color: Colors.yellow,
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  child: Text(
                                      'Taxa: R\$ ${(ret[selectedIndex].tax / 100).toStringAsFixed(2).replaceAll('.', ',')}'),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          RaisedButton(
                            child: Icon(
                              Icons.arrow_drop_up,
                              size: 40,
                            ),
                            onPressed: () =>
                                setState(() => quantity[selectedIndex]++),
                          ),
                          Text(
                            quantity[selectedIndex].toString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 40),
                          ),
                          RaisedButton(
                            child: Icon(
                              Icons.arrow_drop_down,
                              size: 40,
                            ),
                            onPressed: () =>
                                setState(() => quantity[selectedIndex]--),
                          ),
                        ],
                      ),
                    ),
                    RaisedButton(
                      color: Colors.red[200],
                      child: Text('Cancelar'),
                      onPressed: clear,
                    ),
                  ],
                )
              : ListView.builder(
                  padding: EdgeInsets.only(bottom: 100),
                  itemCount: ret.length,
                  itemBuilder: (context, index) {
                    TransdataProductItem item = ret[index];
                    String name = item.name;
                    int daysToEnableBuy = item.daysToEnableBuy;
                    //String minValue = (item.minValue / 100).toStringAsFixed(2).replaceAll('.', ',');
                    //String maxValue = (item.maxValue / 100).toStringAsFixed(2).replaceAll('.', ',');
                    String tax = (item.tax / 100)
                        .toStringAsFixed(2)
                        .replaceAll('.', ',');
                    String value = (item.unitValue / 100)
                        .toStringAsFixed(2)
                        .replaceAll('.', ',');
                    return Container(
                      padding: EdgeInsets.all(5),
                      child: Card(
                        color: daysToEnableBuy != 0
                            ? Colors.grey
                            : selectedIndex == index
                                ? Colors.amber
                                : null,
                        child: InkWell(
                          onTap: daysToEnableBuy != 0
                              ? null
                              : () {
                                  clear();
                                  selectedIndex = index;
                                },
                          child: Container(
                            padding: EdgeInsets.all(5),
                            width: MediaQuery.of(context).size.width - 80,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    if (daysToEnableBuy > 0)
                                      Text(
                                        '(Produto indisponível por $daysToEnableBuy dia(s))',
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    Text(name),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Card(
                                      color: Colors.green[400],
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        child: Text('Valor: R\$ $value'),
                                      ),
                                    ),
                                    Card(
                                      color: Colors.yellow,
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        child: Text('Taxa: R\$ $tax'),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
    );
  }
}
