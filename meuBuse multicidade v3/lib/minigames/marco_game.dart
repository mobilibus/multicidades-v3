import 'package:flutter/material.dart';

class MarcoGame extends StatefulWidget {
  _MarcoGame createState() => _MarcoGame();
}

class _MarcoGame extends State<MarcoGame> {
  List<String> features = [
    'Remover partidas',
    'Novo formulário 100%',
    'Corrigir monitoramento',
    'Pequeno ajuste',
    'Alô é Marco?',
    'Vou viajar',
    'Esse botão ta lindo',
    'Importar feed',
    '@Clayton reinicia para mim?',
    'Não derrubar editor',
    'Jovem, podes ver esse botão para mim?',
    'Vou ver no banco de dados',
    'Beacon',
    'Conta google jovem!',
    '♥️ google ♥️ moovit ♥️',
    'Realtime ta off',
    'RT3 ta off',
    'Adrian, o JSON não ta funcionando',
    '200 abas, 32gb de ram',
    'Londrina ECO NOJINHO!',
    'Calma jovem!',
  ];
  List<Widget> widgets = [];

  void addButton() {
    Widget widget = Text('Botão ${widgets.length}');
    widgets.add(widget);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Marco Game'),
      ),
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          GridView.builder(
            padding: EdgeInsets.only(bottom: 100),
            physics: ClampingScrollPhysics(),
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
            itemCount: widgets.length,
            itemBuilder: (context, index) => RaisedButton(
              onPressed: () => showDialog(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  content: Text(features[index]),
                ),
              ),
              child: widgets[index],
            ),
          ),
          if (widgets.length != features.length)
            Container(
              padding: EdgeInsets.all(20),
              child: FloatingActionButton.extended(
                onPressed: addButton,
                label: Text('Adicionar botão do Marco'),
                icon: Icon(Icons.add),
              ),
            ),
        ],
      ),
    );
  }
}
