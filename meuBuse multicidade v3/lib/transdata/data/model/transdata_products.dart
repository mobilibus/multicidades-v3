import 'dart:convert';

import 'package:mobilibus/transdata/data/model/transdata_model.dart';

class TransdataCpfProductsRequest extends TransdataRequest {
  int cardSerial;
  String cpf;
  TransdataCpfProductsRequest({
    this.cardSerial,
    this.cpf,
  });

  Map<String, dynamic> toMap() {
    return {
      'cardSerial': cardSerial,
      'cpf': cpf,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataCpfProductsRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataCpfProductsRequest(
      cardSerial: map['cardSerial'],
      cpf: map['cpf'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataCpfProductsRequest.fromJson(String source) =>
      TransdataCpfProductsRequest.fromMap(json.decode(source));
}

class TransdataRegisteredProductRequest extends TransdataRequest {
  int cardSerial;
  String userId;

  TransdataRegisteredProductRequest({
    this.cardSerial,
    this.userId,
  });

  Map<String, dynamic> toMap() {
    return {
      'cardSerial': cardSerial,
      'userId': userId,
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataRegisteredProductRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataRegisteredProductRequest(
      cardSerial: map['cardSerial'],
      userId: map['userId'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataRegisteredProductRequest.fromJson(String source) =>
      TransdataRegisteredProductRequest.fromMap(json.decode(source));
}

class TransdataProductResponse {
  int errorCode;
  String errorMessage;
  List<TransdataProduct> produtos;
  TransdataProductResponse({
    this.errorCode,
    this.errorMessage,
    this.produtos,
  });

  Map<String, dynamic> toMap() {
    return {
      'errorCode': errorCode,
      'errorMessage': errorMessage,
      'ret': produtos?.map((x) => x?.toMap())?.toList(),
    };
  }

  factory TransdataProductResponse.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataProductResponse(
      errorCode: map['errorCode'],
      errorMessage: map['errorMessage'],
      produtos: List<TransdataProduct>.from(
          map['ret']?.map((x) => TransdataProduct.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataProductResponse.fromJson(String source) =>
      TransdataProductResponse.fromMap(json.decode(source));
}

class TransdataProduct {
  int produtoId;
  String nome;
  int tarifa;
  int valorUnitario;
  int valorMin;
  int valorMax;
  int diasParaHabilitarCompra;
  String mensagem;
  int saldoMaximoCartao;
  TransdataProduct({
    this.produtoId,
    this.nome,
    this.tarifa,
    this.valorUnitario,
    this.valorMin,
    this.valorMax,
    this.diasParaHabilitarCompra,
    this.mensagem,
    this.saldoMaximoCartao,
  });

  Map<String, dynamic> toMap() {
    return {
      'ProdutoId': produtoId,
      'Nome': nome,
      'Tarifa': tarifa,
      'ValorUnitario': valorUnitario,
      'ValorMin': valorMin,
      'ValorMax': valorMax,
      'DiasParaHabilitarCompra': diasParaHabilitarCompra,
      'Mensagem': mensagem,
      'SaldoMaximoCartao': saldoMaximoCartao,
    };
  }

  factory TransdataProduct.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataProduct(
      produtoId: map['ProdutoId'],
      nome: map['Nome'],
      tarifa: map['Tarifa'],
      valorUnitario: map['ValorUnitario'],
      valorMin: map['ValorMin'],
      valorMax: map['ValorMax'],
      diasParaHabilitarCompra: map['DiasParaHabilitarCompra'],
      mensagem: map['Mensagem'],
      saldoMaximoCartao: map['SaldoMaximoCartao'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataProduct.fromJson(String source) =>
      TransdataProduct.fromMap(json.decode(source));
}
