import 'package:intl/intl.dart';
import 'package:mobilibus/app/urban/more/recharge/data/balance_extract_model.dart';
import 'package:mobilibus/app/urban/more/recharge/data/data_response.dart';
import 'package:mobilibus/app/urban/more/recharge/data/recharge_data_singleton.dart';
import 'package:mobilibus/app/urban/more/recharge/data/user_transport_card_model.dart';
import 'package:mobilibus/transdata/data/model/transdata_balance.dart';
import 'package:mobilibus/transdata/data/model/transdata_extract.dart';
import 'package:mobilibus/transdata/data/model/transdata_products.dart';
import 'package:mobilibus/transdata/data/model/transdata_purchase.dart';
import 'package:mobilibus/transdata/data/model/transdata_taxes.dart';
import 'package:mobilibus/transdata/data/model/transport_card_local_model.dart';
import 'package:mobilibus/transdata/data/transdata_api.dart';
import 'package:mobilibus/utils/date_number_utils.dart';
import 'package:mobilibus/utils/formatting_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PurchaseRepository {
  final TransdataApi _transdataApi;
  static const _KEY_CURRENT_TRANSPORT_CARD_DATA = 'CURRENT_TRANSPORT_CARD_DATA';

  PurchaseRepository(this._transdataApi);

  Future<BalanceExtractModel> getBalanceFromUser(String cpf, int cardNumber) async {
    TransdataBalancesRequest balanceRequest = TransdataBalancesRequest(
      cpfs: [cpf],
      cardSerials: [cardNumber],
    );

    final balanceValue = DataResponse<List<TransdataBalanceValue>>();
    final cardExtract = DataResponse<List<TransdataTransaction>>();

    try {
      final response = await _transdataApi.getBalances(balanceRequest);
      if (response.errorCode != 0 || response.value == null || response.value.isEmpty) {
        throw Exception();
      }

      balanceValue.data = response.value;
      balanceValue.hasError = false;
    } catch (e) {
      balanceValue.data = null;
      balanceValue.hasError = true;
    }

    String balanceDate = balanceValue.data != null
        ? balanceValue.data.first.balanceDate
        : DateFormat(('yyyy-MM-dd HH:mm:ss')).format(DateTime.now());
    String startDate = DateMOBUtils.getExtractDefaultStartDate(balanceDate);
    String endDate = DateMOBUtils.getExtractDefaultEndDate(balanceDate);

    try {
      final response = await getCardExtractFromPeriod(cpf, cardNumber, startDate, endDate);
      if (response.errorCode != 0 || response.items == null) {
        throw Exception();
      }

      cardExtract.data = response.items;
      cardExtract.hasError = false;
    } catch (e) {
      cardExtract.data = null;
      cardExtract.hasError = true;
    }

    BalanceExtractModel result = BalanceExtractModel(
      transportCard: UserTransportCardModel(cpf: cpf, cardNumber: cardNumber),
      balanceValue: balanceValue,
      cardExtract: cardExtract,
    );

    return result;
  }

  Future<TransdataExtract> getCardExtractFromDate(String cpf, int cardNumber, String date) async {
    String endDate = DateMOBUtils.getNextDate(date);
    return await getCardExtractFromPeriod(cpf, cardNumber, date, endDate);
  }

  Future<TransdataExtract> getCardExtractFromPeriod(
      String cpf, int cardNumber, String startDate, String endDate) async {
    TransdataExtractRequest requestData = TransdataExtractRequest(
      cpf: cpf,
      cardSerial: cardNumber,
      startDate: startDate,
      endDate: endDate,
    );
    TransdataExtract result = await _transdataApi.cardExtract(requestData);

    return result;
  }

  Future<DataResponse<List<TransdataProduct>>> getAvailableProducts(
      String cpf, int cardNumber) async {
    final requestData = TransdataCpfProductsRequest(cpf: cpf, cardSerial: cardNumber);

    DataResponse<List<TransdataProduct>> result = DataResponse();
    try {
      TransdataProductResponse response = await _transdataApi.getCpfProducts(requestData);
      if (response.errorCode != 0 || response.produtos == null || response.produtos.isEmpty) {
        throw Exception();
      }

      result.data = response.produtos;
      result.hasError = false;
    } catch (e) {
      result.data = null;
      result.hasError = true;
    }

    return result;
  }

  Future<TransdataTaxes> getProductTax(String cpf, int cardNumber, int productId, int taxId) async {
    final requestData = TransdataTaxesRequest(
      cpf: cpf,
      cardSerial: cardNumber,
      productIds: [productId],
      values: [taxId],
    );

    TransdataTaxes response = await _transdataApi.getTaxes(requestData);
    return response;
  }

  Future<TransdataNewPurchase> startNewPurchase(RechargeDataSingleton orderData) async {
    final requestData = TransdataNewPurchaseRequest(
      cpf: orderData.cpf,
      numeroSerie: orderData.transportCardNumber,
      appNomeAndVersao: '${Constants.APP_NAME}-${Constants.APP_VERSION}',
      iDComprador: Constants.BUYER_ID,
      projectId: Constants.PROJECT_ID,
      taxasEmCentavos: orderData.productTaxAmount,
      items: [
        TransdataNewPurchaseItemRequest(
          product: orderData.product.produtoId,
          valorUnitarioEmCentavos: orderData.product.valorUnitario,
          valorCompraEmCentavos: orderData.rechargeAmount,
        ),
      ],
    );
    final response = await _transdataApi.newPurchase(requestData);
    return response;
  }

  Future<TransdataPaymentResponse> sendTransactionPayment(
      int transactionId, RechargeDataSingleton orderData) async {
    final requestData = TransdataPurchaseRequest(
      transactionId: transactionId,
      ccOwner: orderData.paymentCardOwner,
      ccNumber: orderData.paymentCardNumber,
      ccExpiration: orderData.paymentCardExpiration,
      ccCvv: orderData.paymentCardCVV,
    );

    final response = await _transdataApi.doPayment(requestData);
    return response;
  }

  Future<TransdataPaymentResponse> checkPurchaseManually(int transactionId) async {
    final requestData = TransdataCheckPurchaseRequest(transactionId: transactionId);

    TransdataPaymentResponse response = await _transdataApi.checkPurchase(requestData);
    return response;
  }

  Future saveCurrentTransportCardData(String cpf, int cardNumber) async {
    final data = TransportCardLocalModel(userCPF: cpf, transportCardNumber: cardNumber);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(_KEY_CURRENT_TRANSPORT_CARD_DATA, data.toJson());
  }

  Future<TransportCardLocalModel> getSavedTransportCardData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final json = prefs.getString(_KEY_CURRENT_TRANSPORT_CARD_DATA);
    TransportCardLocalModel model;
    try {
      model = TransportCardLocalModel.fromJson(json);
    } catch (e) {
      print(e);
    }

    return model;
  }
}
