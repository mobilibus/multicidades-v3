import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:url_launcher/url_launcher.dart';

class TwitterViewer extends StatefulWidget {
  _TwitterViewer createState() => _TwitterViewer();
}

class _TwitterViewer extends State<TwitterViewer> {
  List<Map<String, String>> tweets = [];

  void request() {
    Future twitterRequest = Utils.mTwitterApi.getTwitterRequest(
      'GET',
      'statuses/user_timeline.json',
      options: {
        'screen_name': Utils.twitterUser,
        'count': '100',
        'trim_user': 'true',
        'exclude_replies': 'true',
        'include_rts': 'true',
      },
    );

    twitterRequest.then((response) {
      if (response != null) {
        String content = convert.utf8.decode(response.bodyBytes);
        Iterable iterable = convert.json.decode(content);
        tweets = iterable.map((map) {
          String text = map['text'];
          List urls = map['entities']['urls'];
          String url = urls.isEmpty ? '' : urls.first['url'];
          return {
            'text': text,
            'url': url,
          };
        }).toList();
        setState(() {});
      }
    });
  }

  @override
  void initState() {
    super.initState();
    request();
  }

  @override
  Widget build(BuildContext context) => ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: tweets.length,
        itemBuilder: (context, index) {
          Map<String, String> tweet = tweets[index];
          String text = tweet['text'].split('…').first;
          String url = tweet['url'];
          return Container(
            margin: EdgeInsets.all(10),
            child: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: InkWell(
                onTap: () => launch(url),
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: ListTile(
                    title: Text('$text...'),
                    subtitle: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text('Veja +'),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      );
}
