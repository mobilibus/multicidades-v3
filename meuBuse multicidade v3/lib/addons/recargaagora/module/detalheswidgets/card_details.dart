import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobilibus/base/recargaagora/gimave/EucaturAPIs.dart';

import 'customwidget/custom_text_form_field.dart';

class CardDetails extends StatefulWidget {
  CardDetails(this.formMap, this.formKey) : super();
  final Map<String, dynamic> formMap;
  final GlobalKey<FormState> formKey;
  _CardDetails createState() => _CardDetails(formMap, formKey);
}

class _CardDetails extends State<CardDetails> {
  _CardDetails(this.formMap, this.formKey) : super();
  final Map<String, dynamic> formMap;
  final GlobalKey<FormState> formKey;

  CARD card;
  TextEditingController tecCardNumber = TextEditingController();
  TextEditingController tecCardCvv = TextEditingController();
  TextEditingController tecExpireMonth = TextEditingController();
  TextEditingController tecExpireYear = TextEditingController();

  Timer timer;

  @override
  void initState() {
    super.initState();
    Future.delayed(
        Duration(seconds: 1),
        () => timer = Timer.periodic(
            Duration(milliseconds: 500), (timer) => setState(() {})));
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void cardTypeListener(String text) {
    String cardNumber = text;
    card = EucaturAPIs.getCardType(cardNumber);
    formMap['cardFlag'] = card.toString().replaceAll('CARD.', '');
    setState(() {});
  }

  void selectCard() {
    TextStyle textStyle = TextStyle(color: Colors.white);
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
            backgroundColor: Theme.of(context).primaryColor,
            title: Text('Selecione a bandeira', style: textStyle),
            actions: [
              FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text('Fechar', style: textStyle),
              )
            ],
            content: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: ListView.builder(
                    itemCount: CARD.values.length,
                    itemBuilder: (context, index) {
                      CARD c = CARD.values[index];
                      String name = c.toString().replaceAll('CARD.', '');
                      return InkWell(
                          onTap: () {
                            card = c;
                            setState(() {});
                            Navigator.of(context).pop();
                          },
                          child: Container(
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                Text(name, style: textStyle),
                                Container(
                                    padding: EdgeInsets.all(10),
                                    child: Card(
                                      child: Container(
                                        padding: EdgeInsets.all(10),
                                        child: Image.asset(
                                          EucaturAPIs.getAssetByCardType(c),
                                          height: 50,
                                          width: 50,
                                        ),
                                      ),
                                    ))
                              ])));
                    }))));
  }

  Widget bigScreenCardDetails() => Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              CustomTextFormField(
                LengthLimitingTextInputFormatter(4),
                'CVV',
                TextInputType.number,
                tecCardCvv,
                tecCardCvv.text.length == 3 || tecCardCvv.text.length == 4,
                'O CVV possui 3 ou 4 dígitos',
                formKey,
                width: MediaQuery.of(context).size.width / 3,
                textCapitalization: TextCapitalization.none,
              ),
              CustomTextFormField(
                LengthLimitingTextInputFormatter(2),
                'Mês',
                TextInputType.number,
                tecExpireMonth,
                tecExpireMonth.text.length == 2,
                'Use 2 dígitos',
                formKey,
                width: MediaQuery.of(context).size.width / 3,
                textCapitalization: TextCapitalization.none,
              ),
              CustomTextFormField(
                LengthLimitingTextInputFormatter(2),
                'Ano',
                TextInputType.number,
                tecExpireYear,
                tecExpireYear.text.length == 2,
                'Use 2 dígitos',
                formKey,
                width: MediaQuery.of(context).size.width / 3,
                textCapitalization: TextCapitalization.none,
              ),
            ],
          ),
        ],
      );

  Widget smallScreenCardDetails(bool halfContainer) => Column(
        children: <Widget>[
          Row(
            children: [
              CustomTextFormField(
                LengthLimitingTextInputFormatter(4),
                'CVV',
                TextInputType.number,
                tecCardCvv,
                tecCardCvv.text.length == 3 || tecCardCvv.text.length == 4,
                'O CVV possui 3 ou 4 dígitos',
                formKey,
                width: MediaQuery.of(context).size.width / 3,
                textCapitalization: TextCapitalization.none,
              ),
              CustomTextFormField(
                LengthLimitingTextInputFormatter(2),
                'Mês',
                TextInputType.number,
                tecExpireMonth,
                tecExpireMonth.text.length == 2,
                'Use 2 dígitos',
                formKey,
                width: MediaQuery.of(context).size.width / 3,
                textCapitalization: TextCapitalization.none,
              ),
              CustomTextFormField(
                LengthLimitingTextInputFormatter(2),
                'Ano',
                TextInputType.number,
                tecExpireYear,
                tecExpireYear.text.length == 2,
                'Use 2 dígitos',
                formKey,
                width: MediaQuery.of(context).size.width / 3,
                textCapitalization: TextCapitalization.none,
              ),
            ],
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    formMap['cardNumber'] = tecCardNumber.text;
    formMap['cardCvv'] = tecCardCvv.text;
    formMap['expireMonth'] = tecExpireMonth.text;
    formMap['expireYear'] = tecExpireYear.text;

    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    return Container(
      child: Scrollbar(
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 1.5,
                        padding: EdgeInsets.all(10),
                        child: Text(
                          'Selecione a bandeira:',
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 3,
                          height: 70,
                          padding: EdgeInsets.all(10),
                          child: InkWell(
                              onTap: selectCard,
                              child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          color: Colors.grey, width: 1),
                                      borderRadius: BorderRadius.circular(12)),
                                  child: card != null
                                      ? Container(
                                          padding: EdgeInsets.all(5),
                                          child: Image.asset(
                                              EucaturAPIs.getAssetByCardType(
                                                  card)))
                                      : Center(
                                          child: Text('Clique aqui',
                                              textAlign: TextAlign.center)))))
                    ]),
                CustomTextFormField(
                  LengthLimitingTextInputFormatter(16),
                  'Número do cartão',
                  TextInputType.number,
                  tecCardNumber,
                  tecCardNumber.text.length >= 14 &&
                      tecCardNumber.text.length <= 16,
                  'Informe todos os dígitos do cartão',
                  formKey,
                  textCapitalization: TextCapitalization.none,
                  onChanged: cardTypeListener,
                ),
                devicePixelRatio < 2.5
                    ? smallScreenCardDetails(false)
                    : bigScreenCardDetails(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
