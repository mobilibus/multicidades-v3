import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/addons/recargaagora/module/history.dart';
import 'package:mobilibus/addons/recargaagora/module/select_value.dart';
import 'package:mobilibus/base/recargaagora/dataprom/Dataprom.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromAPIs.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromChekingAccount.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromTransportCard.dart';
import 'package:mobilibus/base/recargaagora/mobilibus/MobilibusAPIs.dart';
import 'package:mobilibus/base/recargaagora/mobilibus/MobilibusTransaction.dart';
import 'package:mobilibus/utils/Utils.dart';

class RecargaAgora extends StatefulWidget {
  RecargaAgora(
      this.usuarioCartaoTransporte,
      this.cardNumber,
      this.bilhetagem,
      this.cartaoTransporte,
      this.isDatapromProduction,
      this.isGimaveProduction);
  final DatapromTransportCardUser usuarioCartaoTransporte;
  final String cardNumber;
  final DatapromTicketing bilhetagem;
  final DatapromTransportCard cartaoTransporte;
  final bool isDatapromProduction;
  final bool isGimaveProduction;

  _RecargaAgora createState() => _RecargaAgora(
        usuarioCartaoTransporte,
        cardNumber,
        bilhetagem,
        cartaoTransporte,
        isDatapromProduction,
        isGimaveProduction,
      );
}

class _RecargaAgora extends State<RecargaAgora> {
  _RecargaAgora(this.transportCardUser, this.cardNumber, this.ticketing,
      this.transportCard, this.isDatapromProduction, this.isGimaveProduction)
      : super();
  final DatapromTransportCardUser transportCardUser;
  final String cardNumber;
  final DatapromTicketing ticketing;
  final DatapromTransportCard transportCard;
  final bool isDatapromProduction;
  final bool isGimaveProduction;

  DatapromChekingAccount checkingAccount;
  double totalMoney = 0.0;
  double moneyMaxRecharge = 0.0;
  DateTime lastDateUpdate;
  int page = 1;
  Timer timer;

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    List<DatapromBalance> balances = transportCard.balances;
    lastDateUpdate = balances.first.balanceDate;
    balances.forEach((saldo) {
      totalMoney += saldo.balance;
      moneyMaxRecharge = saldo.maxRechargeValue;

      DateTime date = saldo.balanceDate;
      if (date.isAfter(lastDateUpdate)) lastDateUpdate = saldo.balanceDate;
    });

    if (balances.length > 1)
      balances.removeWhere((saldo) => saldo.transportCardTypeCode == '1');
    super.initState();
    updateContaCorrente();
    timer = Timer.periodic(Duration(minutes: 1), (_) => updateContaCorrente());
  }

  Widget cardUser(String name, String recharge, Color color) => Container(
        width: MediaQuery.of(context).size.width / 1.5,
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Text(name,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).accentColor)),
            Text(
              recharge,
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold, color: color),
            ),
          ],
        ),
      );
  Widget cardHistory() => InkWell(
        onTap: () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => History(
                  transportCardUser,
                  cardNumber,
                  ticketing,
                  isDatapromProduction,
                  isGimaveProduction,
                ))),
        child: Card(
          color: Theme.of(context).accentColor,
          child: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Icon(Icons.history, color: Colors.white),
                Text('Histórico', style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
        ),
      );

  Widget cardContaCorrente() {
    if (checkingAccount == null)
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('Carregando...'),
            CircularProgressIndicator(
                strokeWidth: 3,
                valueColor: AlwaysStoppedAnimation<Color>(
                    Utils.isLightTheme(context)
                        ? Utils.getColorFromPrimary(context)
                        : Colors.black))
          ],
        ),
      );
    List<DatapromRecord> records = checkingAccount.records;
    List<DatapromBalance> balances = transportCard.balances;
    List<DatapromTransportCardType> cardTypeIds = ticketing.transportCardTypes;
    DatapromTransportCardUser transportCardUser =
        transportCard.cardTransportUser;

    String id = '-1';
    String cardType = '';

    if (balances.isNotEmpty) {
      id = balances
          .firstWhere((element) => element.transportCardTypeCode != '-1')
          .transportCardTypeCode;
      cardType = cardTypeIds.firstWhere((element) => element.id == id).name;
    } else
      Navigator.of(context).pop(
          'Usuário com dados cadastrais incompletos ou carteira indisponível');

    double width = MediaQuery.of(context).size.width;
    double leftPaneWidth = width / 5;
    double rightPaneWidth = (width - leftPaneWidth - 48);

    Widget transactions = ListView.builder(
      shrinkWrap: true,
      padding: EdgeInsets.only(top: 10),
      physics: NeverScrollableScrollPhysics(),
      itemCount: records.length,
      itemBuilder: (context, index) {
        DatapromRecord registro = records[index];

        DateTime operationDate = registro.operationDate;
        String date = DateFormat('dd/MM/yyyy - HH:mm:ss').format(operationDate);

        String lineCode = registro.lineCode;
        String line = registro.externalVehicleCode;
        double mValue = registro.value;
        String value = mValue.toStringAsFixed(2).replaceAll('.', ',');
        String operation = registro.operation;
        TextStyle textStyle = TextStyle(fontSize: 8);

        return Container(
          color: index % 2 == 0 ? Colors.grey[200] : null,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: leftPaneWidth,
                child: Text(
                  date.replaceAll(' - ', '\n'),
                  style: TextStyle(fontSize: 10),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                width: rightPaneWidth,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      width: rightPaneWidth / 4,
                      child: Text(
                        operation,
                        style: textStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      width: rightPaneWidth / 4,
                      child: Text(
                        lineCode,
                        style: textStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      width: rightPaneWidth / 4,
                      child: Text(
                        line,
                        style: textStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      width: rightPaneWidth / 4,
                      child: Text(
                        '$value',
                        style: TextStyle(
                          fontSize: 10,
                          color: mValue < 0 ? Colors.red : Colors.green,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );

    TextStyle textStyle = TextStyle(fontSize: 8, fontWeight: FontWeight.bold);
    List<String> tabsNames = [
      'Operação',
      'Linha',
      'Ônibus',
      'Valor (R\$)',
    ];

    Widget transactionTitle = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Container(
          width: leftPaneWidth,
          child: Text(
            'Data\\Hora',
            style: textStyle,
            textAlign: TextAlign.center,
          ),
        ),
        Container(
          width: rightPaneWidth,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: tabsNames
                .map((e) => Container(
                    width: rightPaneWidth / 4,
                    child: Text(
                      e,
                      textAlign: TextAlign.center,
                      style: textStyle,
                    )))
                .toList(),
          ),
        )
      ],
    );

    TextStyle textStyleHeader = TextStyle(
        fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white);

    DateFormat dateFormat = DateFormat('dd/MM/yyyy HH:mm:ss');

    DatapromBalance datapromBalance = transportCard.balances.first;
    String balance = totalMoney.toStringAsFixed(2).replaceAll('.', ',');
    String dateLastUpdate = dateFormat.format(lastDateUpdate);
    String maxRecharge =
        moneyMaxRecharge.toStringAsFixed(2).replaceAll('.', ',');

    return Card(
      color: Theme.of(context).primaryColor,
      elevation: 5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            child: Text(cardType, style: textStyleHeader),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Saldo',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold)),
                    Text('R\$ $balance', style: textStyleHeader),
                    Text('Última atualização:',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold)),
                    Text('$dateLastUpdate',
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold)),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width / 2 - 30,
                      child: Text(
                        'Clique no cifrão para recarregar.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Theme.of(context).accentColor,
                          borderRadius: BorderRadius.all(Radius.circular(60))),
                      child: IconButton(
                        color: Colors.white,
                        iconSize: 60,
                        icon: Icon(Icons.attach_money),
                        onPressed: () => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) => SelectValue(
                                    transportCardUser,
                                    ticketing,
                                    cardNumber,
                                    datapromBalance.transportCardTypeCode))),
                      ),
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width / 2 - 30,
                            child: Text(
                                'Valor máximo de recarga: R\$ $maxRecharge',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            color: Colors.white,
            child: Center(
              child: Column(
                children: <Widget>[
                  Text(
                    'Extrato de uso últimos 5 dias',
                    style: TextStyle(
                      fontSize: 20,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                  transactionTitle,
                  transactions,
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> updateContaCorrente() async {
    checkingAccount = await DatapromAPIs.getDatapromCardDetails(
      cardNumber,
      isDatapromProduction,
    );
    setState(() {});

    while (true) {
      List<MobilibusTransaction> transactions =
          await MobilibusAPIs.getTransactions(cardNumber, page);
      if (transactions.isEmpty) {
        page = 1;
        break;
      } else {
        page++;
        for (final transaction in transactions) {
          STATUS status = transaction.status;
          String eucardToken = transaction.tokenEucard;
          if (status != STATUS.CLOSED && status != STATUS.CANCELLED) {
            DateTime dateTime =
                DateTime.fromMillisecondsSinceEpoch(transaction.createTime);
            DateTime now = DateTime.now();
            bool isNotSameDay = now.day != dateTime.day;
            if (isNotSameDay)
              MobilibusAPIs.updateTransaction(
                STATUS.CANCELLED,
                transaction.transactionId,
                tokenDataprom: transaction.tokenDataprom,
                tokenEucard: null,
              );
          } else if (status == STATUS.CANCELLED && eucardToken != null) {
            MobilibusAPIs.updateTransaction(
              STATUS.CANCELLED,
              transaction.transactionId,
              tokenDataprom: transaction.tokenDataprom,
              tokenEucard: null,
            );
          }

          /* TEMP CODE */
          if (status == STATUS.CLOSED &&
              eucardToken != null &&
              eucardToken == '(não criado)')
            MobilibusAPIs.updateTransaction(
              STATUS.CANCELLED,
              transaction.transactionId,
              tokenDataprom: transaction.tokenDataprom,
              tokenEucard: null,
            );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(
        color: themeColor, fontFamily: GoogleFonts.catamaran().fontFamily);

    DatapromTransportCardUser usuarioCartaoTransporte =
        transportCard.cardTransportUser;

    String nome = usuarioCartaoTransporte.name;
    bool podeEfetuarRecarga = transportCard.canRecharge;
    String podeEfetuarRecargaString = podeEfetuarRecarga
        ? 'PODE EFETUAR RECARGA'
        : 'NÃO PODE EFETUAR RECARGA';

    Color podeEfetuarRecargaColor =
        podeEfetuarRecarga ? Colors.green : Colors.red;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Cartões', style: textStyle),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: themeColor),
          onPressed: () => Navigator.pop(context, null),
        ),
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(top: 5, left: 5, right: 5, bottom: 100),
            child: Center(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      cardUser(nome, podeEfetuarRecargaString,
                          podeEfetuarRecargaColor),
                      cardHistory(),
                    ],
                  ),
                  cardContaCorrente(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
