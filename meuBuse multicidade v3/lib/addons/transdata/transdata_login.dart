import 'package:cpfcnpj/cpfcnpj.dart';
import 'package:encrypt/encrypt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobilibus/addons/transdata/transdata_logged.dart';
import 'package:mobilibus/base/transdata/Transdata.dart';
import 'package:mobilibus/base/transdata/TransdataAPIs.dart';
import 'package:mobilibus/base/transdata/TransdataUser.dart';
import 'package:mobilibus/utils/Utils.dart';

class TransdataLogin extends StatefulWidget {
  _TransdataLogin createState() => _TransdataLogin();
}

class _TransdataLogin extends State<TransdataLogin> {
  Transdata transdata;
  Encrypter encrypter;
  RSA rsa;

  TransdataAPIs api;
  LOGIN loginType = LOGIN.DEFAULT;

  GlobalKey<FormState> formKeyID = GlobalKey<FormState>();
  GlobalKey<FormState> formKeyDOCUMENT = GlobalKey<FormState>();
  GlobalKey<FormState> formKeySerialNumber = GlobalKey<FormState>();
  TextEditingController tecID = TextEditingController();
  TextEditingController tecCPF = TextEditingController();
  TextEditingController tecSerialNumber = TextEditingController();
  PageController pageController = PageController(viewportFraction: 0.9);

  bool loading = false;

  bool isDev = true;

  final List<Map<String, dynamic>> cards = [
    {
      'login': LOGIN.DEFAULT,
      'cpf': '172.690.108-46',
      'number': 200724833,
    },
    {
      'login': LOGIN.DEFAULT,
      'cpf': '713.568.151-70',
      'number': 615542853,
    },
    {
      'login': LOGIN.DEFAULT,
      'cpf': '952.235.463-50',
      'number': 2070485612,
    },
    {
      'login': LOGIN.DEFAULT,
      'cpf': '138.362.771-15',
      'number': 4202265844,
    },
    {
      'login': LOGIN.DEFAULT,
      'cpf': '276.870.868-21',
      'number': 4197086868,
    },
    {
      'login': LOGIN.DEFAULT,
      'cpf': '74.116.153/0001-35',
      'number': 4200781892,
    },
    {
      'login': LOGIN.DEFAULT,
      'cpf': '399.144.218-30',
      'number': 3524896004,
    },
  ];

  @override
  void initState() {
    super.initState();
    handshake();
  }

  Future<void> handshake() async {
    String path = 'assets/transdata';

    String mPublicKey = isDev ? 'transdataPublic' : 'transdataPublicProd';
    String mPrivateKey = isDev ? 'transdataPrivate' : 'transdataPrivateProd';
    String publicPem = await rootBundle.loadString('$path/$mPublicKey.pem');
    String privatePem = await rootBundle.loadString('$path/$mPrivateKey.pem');

    final publicKey = RSAKeyParser().parse(publicPem);
    final privateKey = RSAKeyParser().parse(privatePem);

    rsa = RSA(publicKey: publicKey, privateKey: privateKey);

    encrypter = Encrypter(rsa);

    setState(() => loading = true);
    do await connect(); while (transdata == null);
    setState(() => loading = false);

    api = TransdataAPIs(transdata, encrypter, isDev);

    setState(() {});
  }

  Future<void> connect() async => transdata =
      await TransdataAPIs(transdata, encrypter, isDev).handShake(isDev);

  void selectCard(Map<String, dynamic> card) async {
    loginType = card['login'];

    String cpf = card['cpf'];
    int userId = card['id'];
    int cardSerial = card['number'];

    tecCPF.text = cpf;
    tecSerialNumber.text = cardSerial.toString();

    setState(() {});

    Future.delayed(Duration(seconds: 1), login);
  }

  void showCards() => showModalBottomSheet(
        context: context,
        builder: (context) => ListView.builder(
          shrinkWrap: true,
          itemCount: cards.length,
          itemBuilder: (context, index) {
            Map<String, dynamic> card = cards[index];
            String cpf = card['cpf'];
            int number = card['number'];
            return ListTile(
              title: Text('$number'),
              subtitle: Text(cpf),
              onTap: () {
                Navigator.of(context).pop();
                selectCard(card);
              },
            );
          },
        ),
      );

  Widget loginByType(LOGIN login) {
    Widget widget;
    switch (login) {
      case LOGIN.DEFAULT:
        widget = Form(
          key: formKeySerialNumber,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextFormField(
                validator: (text) => text.isEmpty ? 'Campo obrigatório' : null,
                controller: tecSerialNumber,
                decoration: InputDecoration(hintText: 'N𐄙 de Série do Cartão'),
              ),
              TextFormField(
                validator: (text) => text.isEmpty ? 'Campo obrigatório' : null,
                controller: tecCPF,
                onChanged: (text) => setState(() {}),
                decoration: InputDecoration(hintText: 'CPF/CNPJ'),
              ),
            ],
          ),
        );
        break;
      case LOGIN.CPF:
        widget = Form(
          key: formKeyDOCUMENT,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextFormField(
                validator: (text) => text.isEmpty ? 'Campo obrigatório' : null,
                controller: tecCPF,
                decoration: InputDecoration(hintText: 'CPF'),
              ),
              TextFormField(
                validator: (text) => text.isEmpty ? 'Campo obrigatório' : null,
                controller: tecID,
                onChanged: (text) => setState(() {}),
                decoration: InputDecoration(hintText: 'ID de Cadastro'),
              ),
            ],
          ),
        );
        break;
      case LOGIN.ID:
        widget = Form(
          key: formKeyID,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextFormField(
                validator: (text) => text.isEmpty ? 'Campo obrigatório' : null,
                controller: tecSerialNumber,
                decoration: InputDecoration(hintText: 'N𐄙 de Série do Cartão'),
              ),
              TextFormField(
                validator: (text) => text.isEmpty ? 'Campo obrigatório' : null,
                controller: tecID,
                onChanged: (text) => setState(() {}),
                decoration: InputDecoration(hintText: 'ID de Cadastro'),
              ),
            ],
          ),
        );
        break;
    }

    return widget;
  }

  void login() async {
    loading = true;
    setState(() {});
    try {
      TransdataUser user;
      switch (loginType) {
        case LOGIN.DEFAULT:
          if (formKeySerialNumber.currentState.validate()) {
            int serial = int.parse(tecSerialNumber.text);
            String cpf = tecCPF.text;

            user = await api.readUserDefault(serial.toString(), cpf);
          }
          break;
        case LOGIN.CPF:
          if (formKeyDOCUMENT.currentState.validate()) {
            String cpf = tecCPF.text;
            int userId = int.parse(tecID.text);

            //user = await api.readUserByCPF(cpf, userId);
          }
          break;
        case LOGIN.ID:
          if (formKeyID.currentState.validate()) {
            int serial = int.parse(tecSerialNumber.text);
            int userId = int.parse(tecID.text);

            //user = await api.readUserByID(serial, userId);
          }
          break;
      }
      if (user != null && user.errorCode != 1)
        await Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => TransdataLogged(api, user)));
      else
        throw Exception('Usuário não encontrado');
    } on Exception catch (e) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(e.toString()),
          actions: [
            FlatButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Fechar'),
            ),
          ],
        ),
      );
    }

    loading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (CPF.isValid(tecCPF.text))
      tecCPF.text = CPF.format(tecCPF.text);
    else if (CNPJ.isValid(tecCPF.text)) tecCPF.text = CNPJ.format(tecCPF.text);

    return Scaffold(
      appBar: AppBar(
        title: Text(isDev ? 'Teste' : 'Produção'),
        actions: !Utils.isMobilibus
            ? null
            : [
                if (transdata != null && transdata.version != null)
                  IconButton(
                    icon: Icon(Icons.credit_card),
                    onPressed: showCards,
                  ),
                IconButton(
                  icon: Icon(Icons.sync),
                  onPressed: () async {
                    isDev = isDev ? false : true;
                    transdata = null;
                    await handshake();
                  },
                ),
              ],
      ),
      floatingActionButton: loading
          ? null
          : (transdata == null || transdata.version == null)
              ? null
              : FloatingActionButton.extended(
                  onPressed: login,
                  label: Text('Entrar'),
                ),
      body: (transdata == null || transdata.version == null)
          ? null
          : SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  transdata == null
                      ? Center(child: CircularProgressIndicator())
                      : loading
                          ? Center(child: CircularProgressIndicator())
                          : Container(
                              padding: EdgeInsets.all(10),
                              child: Text('Versão: ${transdata.version}'),
                            ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Container() ??
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: LOGIN.values
                              .map(
                                (e) => Card(
                                  color: loginType == e
                                      ? Theme.of(context).primaryColor
                                      : Colors.grey[300],
                                  child: InkWell(
                                    onTap: () {
                                      loginType = e;
                                      pageController.animateToPage(
                                          LOGIN.values.indexOf(e),
                                          duration: Duration(milliseconds: 300),
                                          curve: Curves.easeIn);
                                    },
                                    child: Container(
                                        padding: EdgeInsets.all(5),
                                        child: Text(e == LOGIN.DEFAULT
                                            ? 'SERIAL + CPF'
                                            : e == LOGIN.CPF
                                                ? 'CPF + ID'
                                                : 'SERIAL + ID')),
                                  ),
                                ),
                              )
                              .toList(),
                        ),
                  ),
                  Container(
                    height: 215,
                    child: PageView(
                      onPageChanged: (index) =>
                          setState(() => loginType = LOGIN.values[index]),
                      controller: pageController,
                      children: LOGIN.values
                          .sublist(0, 1)
                          .map(
                            (e) => Container(
                              child: Card(
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  child: loginByType(e),
                                ),
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
