import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mobilibus/base/traccar/MOBTraccarDevice.dart';
import 'package:mobilibus/base/traccar/MOBTraccarPosition.dart';
import 'package:mobilibus/base/traccar/MOBTraccarUtils.dart';

class TraccarViewerTrack extends StatefulWidget {
  TraccarViewerTrack(this.traccarDevices) : super();
  final List<MOBTraccarDevice> traccarDevices;
  _TraccarViewerTrack createState() => _TraccarViewerTrack(traccarDevices);
}

class _TraccarViewerTrack extends State<TraccarViewerTrack> {
  _TraccarViewerTrack(this.traccarDevices) : super();
  final List<MOBTraccarDevice> traccarDevices;

  Timer timer;
  GoogleMapController gMapController;
  List<MOBTraccarPosition> positions = [];
  @override
  void initState() {
    timer = Timer.periodic(Duration(seconds: 20), (timer) => updateDevice());
    super.initState();
    updateDevice();
    Future.delayed(Duration(seconds: 3), () => bounds());
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void updateDevice() async {
    positions = await MOBTraccarUtils.getTraccarPositions(traccarDevices);
    setState(() {});
  }

  void bounds() {
    double x0, x1, y0, y1;
    for (final position in positions) {
      LatLng latLng = LatLng(position.latitude, position.longitude);
      if (x0 == null) {
        x0 = x1 = latLng.latitude;
        y0 = y1 = latLng.longitude;
      } else {
        if (latLng.latitude > x1) x1 = latLng.latitude;
        if (latLng.latitude < x0) x0 = latLng.latitude;
        if (latLng.longitude > y1) y1 = latLng.longitude;
        if (latLng.longitude < y0) y0 = latLng.longitude;
      }
    }

    LatLng ne = LatLng(x1, y1);
    LatLng sw = LatLng(x0, y0);

    LatLngBounds bounds = LatLngBounds(northeast: ne, southwest: sw);
    gMapController.animateCamera(CameraUpdate.newLatLngBounds(bounds, 50));
  }

  @override
  Widget build(BuildContext context) {
    Set<Marker> markers = positions.map((position) {
      String protocol = position?.protocol ?? '';
      String deviceTime = position?.deviceTime ?? '';
      double lat = position?.latitude ?? 0.0;
      double lng = position?.longitude ?? 0.0;

      deviceTime = deviceTime.isNotEmpty ? deviceTime.substring(0, 19) : '';
      List<String> dateTime =
          deviceTime.isNotEmpty ? deviceTime.split('T') : ['', ''];

      String date = dateTime[0];
      String time = dateTime[1];

      List<String> dateSplit = date.isNotEmpty ? date.split('-') : ['', '', ''];
      String day = dateSplit[2];
      String month = dateSplit[1];
      String year = dateSplit[0];

      date = '$day/$month/$year';

      String id = position?.id?.toString() ?? '';

      LatLng latLng = LatLng(lat, lng);

      MOBTraccarDevice traccarDevice = traccarDevices
          .firstWhere((device) => device.positionId == position.id);

      String name = traccarDevice.name;

      MarkerId markerId = MarkerId(id);
      return Marker(
        markerId: markerId,
        position: latLng,
        onTap: () => gMapController
            .animateCamera(CameraUpdate.newLatLngZoom(latLng, 10)),
        infoWindow: InfoWindow(
          title: '$name - $id - $protocol',
          snippet: '$time - $date',
        ),
      );
    }).toSet();

    return Scaffold(
      appBar: AppBar(
        title: Text('Tracking'),
        centerTitle: true,
      ),
      body: Stack(
        alignment: Alignment.topRight,
        children: [
          GoogleMap(
            markers: markers,
            mapToolbarEnabled: false,
            zoomControlsEnabled: false,
            onMapCreated: (googleMapController) async {
              gMapController = googleMapController;
              Completer<GoogleMapController>().complete(gMapController);
              await rootBundle
                  .loadString('assets/maps_style_default.json')
                  .then((style) => gMapController.setMapStyle(style));
            },
            initialCameraPosition: CameraPosition(target: LatLng(0.0, 0.0)),
          ),
        ],
      ),
    );
  }
}
