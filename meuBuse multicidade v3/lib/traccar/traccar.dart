import 'dart:async';
import 'dart:io';

import 'package:location/location.dart';
import 'package:mobilibus/base/traccar/MOBTraccarUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class TraccarService {
  Timer _timer;

  TraccarService() {
    if (Platform.isAndroid)
      Future.delayed(Duration(seconds: 2), () {
        _timer = Timer.periodic(Duration(seconds: Utils.updateDelay), (timer) {
          if (Utils.beaconService.mBeacons.isNotEmpty) _getLocation();
        });
        _getLocation();
      });
  }

  void stop() {
    _timer?.cancel();
  }

  void _getLocation() async {
    Location location = Location();
    if ((await location.hasPermission() == PermissionStatus.granted) &&
        (await location.serviceEnabled())) {
      LocationData locationData = Utils.locationData;
      double bearing = locationData.heading;
      double lat = locationData.latitude;
      double lng = locationData.longitude;
      double speed = locationData.speed;
      Utils.beaconService.mBeacons.forEach((beacon) {
        String uuid = beacon.proximityUUID;
        String macAddress = beacon.macAddress;
        String id = '$uuid' '_' '$macAddress';
        MOBTraccarUtils.sendTraccar(id, lat, lng, speed, bearing);
      });
    }
  }
}
