import 'package:flutter/material.dart';
import 'package:mobilibus/accessibility/timetable/show_timetable_accessibility.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/utils/Utils.dart';

class DayTimetableAccessibility extends StatefulWidget {
  DayTimetableAccessibility(this.line, this.direction);

  final MOBLine line;
  final MOBDirection direction;

  _DayTimetableAccessibility createState() =>
      _DayTimetableAccessibility(line, direction);
}

class _DayTimetableAccessibility extends State<DayTimetableAccessibility> {
  _DayTimetableAccessibility(this.line, this.direction) : super();

  final MOBLine line;
  final MOBDirection direction;
  MOBTimetable timetable;

  List<MOBService> services;

  @override
  void initState() {
    Future.delayed(Duration(),
        () async => timetable = await Utils.getTimetable(line.routeId));
    services = direction.services;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Selecione o dia da semana'),
        centerTitle: true,
      ),
      body: ListView(
        physics: AlwaysScrollableScrollPhysics(),
        children: <Widget>[
          if (line.desc != null && line.desc.isNotEmpty)
            Container(
              padding: EdgeInsets.all(10),
              child: Text(line.desc),
            ),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: services.length,
            itemBuilder: (context, index) {
              MOBService service = services[index];
              String desc = service.desc;

              return ListTile(
                title: Text(desc),
                dense: true,
                enabled: true,
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ShowTimetableAccessibility(
                              line, timetable, direction, service)));
                },
              );
            },
          )
        ],
      ),
    );
  }
}
