import 'package:flutter/material.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/utils/Utils.dart';

class TimetableAcessibility extends StatefulWidget {
  _TimetableAcessibility createState() => _TimetableAcessibility();
}

class _TimetableAcessibility extends State<TimetableAcessibility> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        title: Text('Selecione a linha'),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: Utils.lines.length,
        itemBuilder: (context, index) {
          MOBLine line = Utils.lines[index];
          String shortName = line.shortName;
          String longName = line.longName;

          return ListTile(
            title: Text('$shortName - $longName'),
            dense: true,
            enabled: true,
            onTap: () => Utils.selectedLineBehaviour(line, true, context, true),
          );
        },
      ),
    );
  }
}
