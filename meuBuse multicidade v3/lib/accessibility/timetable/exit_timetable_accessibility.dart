import 'package:flutter/material.dart';
import 'package:mobilibus/accessibility/timetable/day_timetable_accessibility.dart';
import 'package:mobilibus/base/MOBLine.dart';

class ExitTimetableAccessibility extends StatefulWidget {
  ExitTimetableAccessibility(this.line, this.timetable);

  final MOBLine line;
  final MOBTimetable timetable;
  _ExitTimetableAccessibility createState() =>
      _ExitTimetableAccessibility(line, timetable);
}

class _ExitTimetableAccessibility extends State<ExitTimetableAccessibility> {
  _ExitTimetableAccessibility(this.line, this.timetable) : super();
  final MOBLine line;
  final MOBTimetable timetable;

  List<MOBDirection> directions;

  @override
  void initState() {
    directions = timetable.directions;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Selecione a saída'),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: directions.length,
        itemBuilder: (context, index) {
          MOBDirection direction = directions[index];
          String desc = direction.desc;

          return ListTile(
            title: Text(desc),
            dense: true,
            enabled: true,
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        DayTimetableAccessibility(line, direction))),
          );
        },
      ),
    );
  }
}
