import 'dart:async';
import 'dart:convert' as convert;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_maps_utils/google_maps_utils.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/accessibility/stops/departures_stop_accessibility.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class NearStopsAccessibility extends StatefulWidget {
  _NearStopsAccessibility createState() => _NearStopsAccessibility();
}

class _NearStopsAccessibility extends State<NearStopsAccessibility> {
  LocationData locationData;
  Map<Point, double> markersMap = {}; //position | distance
  List<MOBStop> stops = [];

  double radiusInMeters = 300.0;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      try {
        locationData = Utils.locationData;
        getStops(locationData);
      } catch (e) {
        print(e);
      }
    });
  }

  void getStops(LocationData locationData) async {
    double latitude = locationData.latitude;
    double longitude = locationData.longitude;
    GMULatLngBounds bounds = SphericalUtils.toBounds(latitude, longitude, 750);

    String url = MOBApiUtils.getStopsInViewUrl(bounds);

    http.Response response = await MOBApiUtils.getStopsFuture(url);
    if (response != null) {
      if (response != null) {
        String body = convert.utf8.decode(response.bodyBytes);
        Iterable list = convert.json.decode(body);
        List<MOBStop> stopsList =
            list.map((model) => MOBStop.fromJson(model)).toList();

        stopsList.removeWhere((test) => test == null);

        for (final stop in stopsList) {
          int stopType = stop.stopType;

          if(stop.projectId == Utils.globalProjectID) { //FILTRA OS PONTOS PARA O PROJETO ESPECÍFICO
            //8 is Point of Sale
            if (stopType < 8) {
              Point stopLatLng = Point(stop.latitude, stop.longitude);
              if (!markersMap.containsKey(stopLatLng)) {
                Point myPos = Point(latitude, longitude);

                double distance =
                SphericalUtils.computeDistanceBetween(myPos, stopLatLng);
                double degrees = SphericalUtils.computeHeading(
                    myPos, stopLatLng);
                stop.distance = distance;
                stop.degrees = degrees;
                markersMap[stopLatLng] = distance;
                stops.add(stop);
              }
            }
          }
        }

        stops.sort((a, b) => a.distance.compareTo(b.distance));

        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        title: Text('Pontos de Ônibus próximos'),
        centerTitle: true,
      ),
      body: markersMap.isEmpty
          ? Center(
              child: Container(
                width: 300,
                child: Text(
                    'É necessário estar com a localização ativada para encontrarmos pontos de ônibus próximos a você'),
              ),
            )
          : ListView.builder(
              itemCount: markersMap.length,
              itemBuilder: (context, index) {
                MOBStop stop = stops[index];
                String name = stop.name;
                String distance = stop.distance.toStringAsFixed(0);
                double degrees = stop.degrees;
                String direction = SphericalUtils.getCardinal(degrees);
                var val = ((degrees / 22.5) + 0.5).floor();
                var arr = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];
                direction = arr[(val % 8)];

                if (direction == 'N') {
                  direction = 'Norte';
                } else if (direction == 'S') {
                  direction = 'Sul';
                } else if (direction == 'E') {
                  direction = 'Leste';
                } else if (direction == 'W') {
                  direction = 'Oeste';
                } else if (direction == 'NE') {
                  direction = 'Nordeste';
                } else if (direction == 'SE') {
                  direction = 'Sudeste';
                } else if (direction == 'SW') {
                  direction = 'Sudoeste';
                } else if (direction == 'NW') {
                  direction = 'Noroeste';
                }

                return ListTile(
                  title: Text(name),
                  subtitle: Text('Distância: $distance metros para $direction'),
                  dense: true,
                  enabled: true,
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DeparturesAccessibility(
                              Utils.project.projectId, stop.stopId, name))),
                );
              },
            ),
    );
  }
}
