import 'package:clipboard_manager/clipboard_manager.dart';
import 'package:flutter/material.dart';
import 'package:mobilibus/addons/recargaagora/recarga_agora_login.dart';
import 'package:mobilibus/addons/transdata/transdata_login.dart';
import 'package:mobilibus/addons/twitter/twitter.dart';
import 'package:mobilibus/beacon/beacon_viewer.dart';
import 'package:mobilibus/dev/apis/api_list.dart';
import 'package:mobilibus/dev/departuresdev/dev_departures.dart';
import 'package:mobilibus/dev/negative_departures.dart';
import 'package:mobilibus/dev/shared_preferences_list.dart';
import 'package:mobilibus/traccar/traccar_viewer.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DevTools extends StatefulWidget {
  _DevTools createState() => _DevTools();
}

class _DevTools extends State<DevTools> with TickerProviderStateMixin {
  Map<String, dynamic> mobilibus = {
    'Departures Dev': {
      'icon': Icons.code,
      'widget': DevDepartures(),
    },
    'Configurações salvas': {
      'icon': Icons.save,
      'widget': SharedPreferencesList(),
    },
    'Partidas negativas': {
      'icon': Icons.save,
      'widget': NegativeDepartures(),
    },
    'App APIs': {
      'icon': Icons.cloud,
      'widget': ApiList(),
    },
  };
  Map<String, dynamic> addons = {
    'Beacons': {
      'icon': Icons.bluetooth_audio,
      'widget': BeaconViewer(),
    },
    'Traccar': {
      'icon': Icons.wifi,
      'widget': TraccarViewer(),
    },
    'Twitter': {
      'icon': Icons.message,
      'widget': TwitterViewer(),
    },
    'Recarga Agora\nDataprom + Gimave': {
      'icon': Icons.attach_money,
      'widget': RecargaAgoraLogin(),
    },
    'Transdata': {
      'icon': Icons.attach_money,
      'widget': TransdataLogin(),
    },
  };

  String token = '';
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  TabController tabController;

  @override
  void initState() {
    tabController = TabController(length: 3, vsync: this);
    super.initState();
    Future.delayed(Duration(), () async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      token = prefs.getString('token') ?? null;
      setState(() {});
    });
  }

  Widget listFeatures(List<String> titles, List<dynamic> data) =>
      ListView.builder(
        shrinkWrap: true,
        itemCount: titles.length,
        itemBuilder: (context, index) {
          var d = data[index];
          Widget w = d['widget'];
          IconData iconData = d['icon'];
          return Container(
            padding: EdgeInsets.all(5),
            child: Card(
              color: (index % 2 == 0)
                  ? Theme.of(context).accentColor
                  : Theme.of(context).primaryColor,
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Container(
                padding: EdgeInsets.all(5),
                child: ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(iconData),
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        width: MediaQuery.of(context).size.width - 110,
                        child: Text(titles[index]),
                      ),
                      Icon(Icons.chevron_right)
                    ],
                  ),
                  onTap: () {
                    if (w is TwitterViewer || w is SharedPreferencesList) {
                      double height =
                          MediaQuery.of(context).size.height - kToolbarHeight;
                      showModalBottomSheet(
                        context: context,
                        isScrollControlled: true,
                        enableDrag: false,
                        builder: (context) => Container(
                          height: height,
                          child: w is TwitterViewer
                              ? Column(
                                  children: [
                                    AppBar(
                                        title: Text('@' + Utils.twitterUser)),
                                    Scrollbar(
                                      child: SingleChildScrollView(
                                        child: Container(
                                          height: height - kToolbarHeight,
                                          child: w,
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              : w,
                        ),
                      );
                    } else
                      Navigator.push(
                          context, MaterialPageRoute(builder: (context) => w));
                  },
                ),
              ),
            ),
          );
        },
      );

  Widget tokenWidget() => Container(
        padding: EdgeInsets.all(10),
        child: Wrap(
          crossAxisAlignment: WrapCrossAlignment.end,
          alignment: WrapAlignment.end,
          children: <Widget>[
            Text(
              token ?? '',
              style: TextStyle(fontSize: 12),
            ),
            IconButton(
              onPressed: () {
                ClipboardManager.copyToClipBoard(token ?? '');
                scaffoldKey.currentState.showSnackBar(SnackBar(
                  content: Text('Copiado para a área de transferência'),
                ));
              },
              icon: Icon(Icons.content_copy),
            ),
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    Widget mobFeatures =
        listFeatures(mobilibus.keys.toList(), mobilibus.values.toList());
    Widget addonFeatures =
        listFeatures(addons.keys.toList(), addons.values.toList());

    Widget token = Container(
      child: AlertDialog(
        title: Text(
          'Firebase Push Token',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        content: tokenWidget(),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Dev Tools'),
        bottom: TabBar(
          controller: tabController,
          tabs: [
            Tab(child: Text('MOB')),
            Tab(child: Text('Addons')),
            Tab(child: Text('Token')),
          ],
        ),
      ),
      body: DefaultTabController(
        length: 3,
        child: TabBarView(
          controller: tabController,
          children: [
            mobFeatures,
            addonFeatures,
            token,
          ],
        ),
      ),
    );
  }
}
