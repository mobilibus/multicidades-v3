import 'package:flutter/material.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

import 'api_viewer.dart';

class ApiList extends StatefulWidget {
  _ApiList createState() => _ApiList();
}

class _ApiList extends State {
  static String baseUrl =
      Utils.isMobilibus ? MOBApiUtils.mobBaseDev : MOBApiUtils.mobBase;

  Map<String, Map<String, Map<String, dynamic>>> apisMap = {
    'Urbano': {
      'Tabela de horários\n(Completo)': {
        'api': API.TIMETABLE_FULL,
        'url': '$baseUrl/timetable?project_id=param1',
        'icon': Icons.access_time,
      },
      'Tabela de horários\n(Parcial)': {
        'api': API.TIMETABLE_PARTIAL,
        'url': '$baseUrl/routes?project_id=param1',
        'icon': Icons.timer,
      },
      'Tabela de horários\n(Projeto + Linha)': {
        'api': API.TIMETABLE_BY_LINE,
        'url': '$baseUrl/timetable?project_id=param1&route_id=param2',
        'icon': Icons.timelapse,
      },
      'Pontos de Venda': {
        'api': API.POS,
        'url': '$baseUrl/points-of-sale?project_id=param1',
        'icon': Icons.store,
      },
    },
    'Fretamento': {
      'Limpar Cache': {
        'api': API.CLEAR_CACHE,
        'url': '$baseUrl/timetable?force=true&project_id=param1',
        'icon': Icons.delete_forever,
      },
      'Fretamento Agências': {
        'api': API.AGENCIES,
        'url': '$baseUrl/agencies?project_id=param1&routes=param2',
        'icon': Icons.business_center,
      },
      'Fretamento Login': {
        'api': API.LOGIN,
        'url': '$baseUrl/charter-login',
        'icon': Icons.person,
      },
      'Pontos de Ônibus por Login': {
        'api': API.STOPS_LOGIN,
        'url': '$baseUrl/charter-login',
        'icon': Icons.verified_user,
      }
    },
    'Urbano/Fretamento': {
      'Projetos': {
        'api': API.PROJECTS,
        'url': '$baseUrl/projects?dev=param1&charter=param2',
        'icon': Icons.location_city,
      },
      'Detalhes do Projeto': {
        'api': API.PROJECT_DETAILS,
        'url': '$baseUrl/project-details?project_id=param1',
        'icon': Icons.loyalty,
      },
      'Viagem': {
        'api': API.TRIP,
        'url': '$baseUrl/trip-details?routeId=param1&trip_id=param2',
        'icon': Icons.directions,
      },
      'Pontos próximos': {
        'api': API.NEAR_STOPS,
        'url': '$baseUrl/stops-in-view?ne=param1,param2&sw=param3,param4',
        'icon': Icons.near_me,
      },
      'Partidas': {
        'api': API.DEPARTURES,
        'url': '$baseUrl/departures?stop_id=param1',
        'icon': Icons.departure_board,
      },
      'Notícias e Alertas': {
        'api': API.NEWS_ALERTS,
        'url': {
          'news': '$baseUrl/news?project_id=param1',
          'alerts': '$baseUrl/alerts?project_id=param1',
        },
        'icon': Icons.new_releases,
      },
    },
    'OTP': {
      'Reverse': {
        'api': API.REVERSE,
        'url': {
          'otp1': 'http://otp1.mobilibus.com',
          'otp2': 'http://otp2.mobilibus.com',
          'otp3': 'http://otp3.mobilibus.com',
        },
        'icon': Icons.directions_run,
      },
      'OpenTripPlanner': {
        'api': API.OPEN_TRIPPLANER,
        'url': {
          'otp1': 'http://otp1.mobilibus.com',
          'otp2': 'http://otp2.mobilibus.com',
          'otp3': 'http://otp3.mobilibus.com',
        },
        'icon': Icons.directions_walk,
      },
    },
  };
  @override
  Widget build(BuildContext context) {
    List<String> groups = apisMap.keys.toList();

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, () => setState(() {})),
        ),
        title: Text('App APIs'),
        centerTitle: true,
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          child: ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: groups.length,
            itemBuilder: (context, index) {
              String group = groups[index];
              Map<String, dynamic> mapOfGroup = apisMap[group];
              List<dynamic> keysOfGroup = mapOfGroup.keys.toList();
              return Container(
                padding: EdgeInsets.all(10),
                child: Card(
                  color: index % 2 == 0
                      ? Theme.of(context).primaryColor
                      : Theme.of(context).accentColor,
                  elevation: 5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: Column(
                    children: [
                      Text(group,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 30, fontWeight: FontWeight.bold)),
                      ListView.separated(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        separatorBuilder: (context, index) =>
                            Divider(thickness: 1),
                        itemCount: keysOfGroup.length,
                        itemBuilder: (context, index) {
                          String apiName = keysOfGroup[index];
                          Map<String, dynamic> apiMap = mapOfGroup[apiName];
                          API apiType = apiMap['api'];
                          var url = apiMap['url'];
                          IconData icon = apiMap['icon'];
                          Map<String, dynamic> apiUrl = {apiName: url};
                          return InkWell(
                            onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ApiViewer(apiName, apiUrl, apiType))),
                            child: Container(
                              padding: EdgeInsets.all(20),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Icon(icon),
                                  Container(
                                    width: (MediaQuery.of(context).size.width /
                                            5) *
                                        3,
                                    child: Text(apiName,
                                        style: TextStyle(fontSize: 20)),
                                  ),
                                  Icon(Icons.chevron_right)
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
