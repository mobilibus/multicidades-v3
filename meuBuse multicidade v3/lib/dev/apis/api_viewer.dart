import 'dart:convert' as convert;
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_json_widget/flutter_json_widget.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_utils/google_maps_utils.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/base/MOBCharterCompany.dart';
import 'package:mobilibus/base/MOBProject.dart';
import 'package:mobilibus/base/otp/OTPPlan.dart';
import 'package:mobilibus/dev/apis/api_map.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Debouncer.dart';
import 'package:mobilibus/utils/Utils.dart';

enum API {
  PROJECTS,
  PROJECT_DETAILS,
  TIMETABLE_FULL,
  TIMETABLE_PARTIAL,
  TIMETABLE_BY_LINE,
  TRIP,
  NEAR_STOPS,
  DEPARTURES,
  POS,
  NEWS_ALERTS,
  REVERSE,
  OPEN_TRIPPLANER,
  AGENCIES,
  CLEAR_CACHE,
  LOGIN,
  STOPS_LOGIN,
}

class ApiViewer extends StatefulWidget {
  ApiViewer(this.name, this.url, this.api) : super();
  final String name;
  final Map<String, dynamic> url;
  final API api;
  _ApiViewer createState() => _ApiViewer(name, url, api);
}

class _ApiViewer extends State<ApiViewer> {
  _ApiViewer(this.name, this.url, this.api) : super();
  final String name;
  final Map<String, dynamic> url;
  final API api;

  Widget apiWidget;
  Widget jsonViewer;
  bool isLoading = false;
  bool hasError = false;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    if (api == API.STOPS_LOGIN || api == API.CLEAR_CACHE)
      Future.delayed(Duration(), () async {
        http.Response response = await MOBApiUtils.getCharterProjects();
        if (response != null) {
          String jsonString = convert.utf8.decode(response.bodyBytes);
          Iterable iterable = convert.json.decode(jsonString);
          List<MOBProject> projects =
              iterable.map((e) => MOBProject.fromJson(e)).toList();
          if (api == API.STOPS_LOGIN)
            projectsStopsByLogin = projects;
          else if (api == API.CLEAR_CACHE) clearProjects = projects;

          setState(() {});
        }
      });
  }

  TextEditingController tecTimetableFull = TextEditingController();
  Widget timetableWidget() => TextField(
        controller: tecTimetableFull,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            hintText: 'Project ID', border: OutlineInputBorder()),
      );

  TextEditingController tecProjectDetails = TextEditingController();
  Widget projectDetailsWidget() => TextField(
        controller: tecProjectDetails,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            hintText: 'Project ID', border: OutlineInputBorder()),
      );

  TextEditingController tecTimetablePartial = TextEditingController();
  Widget timetablePartialWidget() => TextField(
        controller: tecTimetablePartial,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            hintText: 'Project ID', border: OutlineInputBorder()),
      );

  bool isProjectsDev = false;
  bool isProjectsCharter = false;
  Widget projectsWidget() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            children: <Widget>[
              Text('Dev'),
              Checkbox(
                value: isProjectsDev,
                onChanged: isProjectsCharter
                    ? null
                    : (value) => setState(() => isProjectsDev = value),
              ),
            ],
          ),
          Column(
            children: <Widget>[
              Text('Fretamento'),
              Checkbox(
                value: isProjectsCharter,
                onChanged: (value) => setState(() => isProjectsCharter = value),
              ),
            ],
          ),
        ],
      );

  TextEditingController tecTimetableProjectId = TextEditingController();
  TextEditingController tecTimetableRouteId = TextEditingController();
  Widget timetableLineWidget() => Column(
        children: <Widget>[
          TextField(
            controller: tecTimetableProjectId,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: 'Project ID', border: OutlineInputBorder()),
          ),
          TextField(
            controller: tecTimetableRouteId,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: 'Route ID', border: OutlineInputBorder()),
          ),
        ],
      );

  TextEditingController tecTripRouteId = TextEditingController();
  TextEditingController tecTripTripId = TextEditingController();
  Widget tripWidget() => Column(
        children: <Widget>[
          TextField(
            controller: tecTripRouteId,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: 'Route ID', border: OutlineInputBorder()),
          ),
          TextField(
            controller: tecTripTripId,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: 'Trip ID', border: OutlineInputBorder()),
          ),
        ],
      );

  GMULatLngBounds bounds;
  Widget nearStopsWidget() => Column(
        children: <Widget>[
          RaisedButton(
            onPressed: () async {
              bounds = await showModalBottomSheet(
                context: context,
                enableDrag: false,
                isScrollControlled: true,
                builder: (context) => SafeArea(
                  child: Container(
                    height: MediaQuery.of(context).size.height - kToolbarHeight,
                    child: ApiMap(true),
                  ),
                ),
              );
              setState(() {});
            },
            child: Text('Mapa'),
          ),
          if (bounds != null)
            Text(
                'NE: ${bounds.northEast.x.toStringAsFixed(6)}, ${bounds.northEast.y.toStringAsFixed(6)}')
          else
            Text('NE: '),
          if (bounds != null)
            Text(
                'SW: ${bounds.southWest.x.toStringAsFixed(6)}, ${bounds.southWest.y.toStringAsFixed(6)}')
          else
            Text('SW: '),
        ],
      );

  TextEditingController tecDepartureStopId = TextEditingController();
  Widget departureWidget() => TextField(
        controller: tecDepartureStopId,
        keyboardType: TextInputType.number,
        decoration:
            InputDecoration(hintText: 'Stop ID', border: OutlineInputBorder()),
      );

  TextEditingController tecPointsOfSaleProjectId = TextEditingController();
  Widget pointsOfSaleWidget() => Column(
        children: <Widget>[
          TextField(
            controller: tecPointsOfSaleProjectId,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: 'Project ID', border: OutlineInputBorder()),
          ),
        ],
      );
  TextEditingController tecNewsAlertsProjectId = TextEditingController();
  Widget newsAlertsWidget() => Column(
        children: <Widget>[
          TextField(
            controller: tecNewsAlertsProjectId,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: 'Project ID', border: OutlineInputBorder()),
          ),
        ],
      );

  DateTime dateTime;
  int groupRadio = 0;
  String day = '??',
      month = '??',
      hour = '??',
      minute = '??',
      dayOfWeek = '???',
      port = '',
      otp = '',
      urlOTP = '',
      fromParam = '',
      toParam = '',
      timeParam = '',
      dateParam = '';
  double sliderValue = 0.0,
      fromLat = 0.0,
      fromLng = 0.0,
      toLat = 0.0,
      toLng = 0.0;
  List<OTP_MODE> modes = [];
  bool wheelChair = true, walk = true, transit = true;
  bool arriveBy = false, isDialogOpen = false;
  TextEditingController tecOtpPort = TextEditingController();

  CameraPosition cameraPosition =
      CameraPosition(target: LatLng(0.0, 0.0), zoom: 0);
  GoogleMapController controller;
  GoogleMap googleMap;

  Widget otpWidget() => Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Radio(
                        value: 0,
                        onChanged: (value) =>
                            setState(() => groupRadio = value),
                        groupValue: groupRadio,
                      ),
                      Text('otp1'),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Radio(
                        value: 1,
                        onChanged: (value) =>
                            setState(() => groupRadio = value),
                        groupValue: groupRadio,
                      ),
                      Text('otp2'),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Radio(
                        value: 2,
                        onChanged: (value) =>
                            setState(() => groupRadio = value),
                        groupValue: groupRadio,
                      ),
                      Text('otp3'),
                    ],
                  ),
                ],
              ),
              Container(
                width: 100,
                child: TextField(
                  controller: tecOtpPort,
                  onChanged: (value) => setState(() => port = value),
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      hintText: 'Port', border: OutlineInputBorder()),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text('Arrive By'),
              Checkbox(
                value: arriveBy,
                onChanged: (value) => setState(() => arriveBy = value),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Icon(Icons.directions_walk),
                  Checkbox(
                    value: walk,
                    onChanged: (value) => setState(() => walk = value),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Icon(Icons.directions_transit),
                  Checkbox(
                    value: transit,
                    onChanged: (value) => setState(() => transit = value),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Icon(Icons.accessible_forward),
                  Checkbox(
                    value: wheelChair,
                    onChanged: (value) => setState(() => wheelChair = value),
                  ),
                ],
              ),
            ],
          ),
          Wrap(
            runSpacing: 2.0,
            spacing: 5.0,
            alignment: WrapAlignment.center,
            children: <Widget>[
              RaisedButton(
                child: Text(
                  'De:\n$fromLat,$fromLng',
                  textAlign: TextAlign.center,
                ),
                onPressed: () async {
                  LatLng latLng = await showModalBottomSheet(
                    context: context,
                    enableDrag: false,
                    isScrollControlled: true,
                    builder: (context) => SafeArea(
                      child: Container(
                        height:
                            MediaQuery.of(context).size.height - kToolbarHeight,
                        child: ApiMap(false),
                      ),
                    ),
                  );
                  if (latLng != null) {
                    fromLat = latLng.latitude;
                    fromLng = latLng.longitude;
                    setState(() {});
                  }
                },
              ),
              RaisedButton(
                child: Text(
                  'Para:\n$toLat,$toLng',
                  textAlign: TextAlign.center,
                ),
                onPressed: () async {
                  LatLng latLng = await showModalBottomSheet(
                    context: context,
                    enableDrag: false,
                    isScrollControlled: true,
                    builder: (context) => SafeArea(
                      child: Container(
                        height:
                            MediaQuery.of(context).size.height - kToolbarHeight,
                        child: ApiMap(false),
                      ),
                    ),
                  );
                  if (latLng != null) {
                    toLat = latLng.latitude;
                    toLng = latLng.longitude;
                    setState(() {});
                  }
                },
              ),
              RaisedButton(
                child: Text(
                  'Date: $dayOfWeek $day/$month $hour:$minute',
                  textAlign: TextAlign.center,
                ),
                onPressed: () => showCupertinoModalPopup(
                  context: context,
                  builder: (context) => Container(
                    height: 400,
                    padding: EdgeInsets.all(10),
                    child: Card(
                      elevation: 5,
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: 320,
                              child: CupertinoDatePicker(
                                use24hFormat: true,
                                mode: CupertinoDatePickerMode.dateAndTime,
                                initialDateTime:
                                    DateTime.now().add(Duration(minutes: 1)),
                                minimumDate: DateTime.now(),
                                onDateTimeChanged: (dateTime) {
                                  int dif = DateTime.now()
                                      .difference(dateTime)
                                      .inDays;
                                  if (DateTime.now().isBefore(dateTime) &&
                                      dif < 14) {
                                    day = dateTime.day >= 10
                                        ? dateTime.day.toString()
                                        : '0${dateTime.day}';
                                    month = dateTime.month >= 10
                                        ? dateTime.month.toString()
                                        : '0${dateTime.month}';
                                    String year = dateTime.year >= 10
                                        ? dateTime.year.toString()
                                        : '0${dateTime.year}';

                                    hour = dateTime.hour >= 10
                                        ? dateTime.hour.toString()
                                        : '0${dateTime.hour}';
                                    minute = dateTime.minute >= 10
                                        ? dateTime.minute.toString()
                                        : '0${dateTime.minute}';

                                    dayOfWeek =
                                        Utils.getDayOfTheWeek(dateTime.weekday);

                                    dateParam = '$month-$day-$year';
                                    timeParam = '$hour:$minute';

                                    fromParam = fromLat.toString() +
                                        ',' +
                                        fromLng.toString();
                                    toParam = toLat.toString() +
                                        ',' +
                                        toLng.toString();

                                    modes = [];
                                    if (walk) modes.add(OTP_MODE.WALK);
                                    if (transit) modes.add(OTP_MODE.TRANSIT);

                                    setState(() {});
                                  }
                                },
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                FlatButton(
                                  child: Text('Cancelar'),
                                  onPressed: () => Navigator.of(context).pop(),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Text('Max Walk Distance: ${sliderValue.toStringAsFixed(0)} (m)'),
          Slider(
            max: 5000,
            min: 0,
            divisions: 100,
            value: sliderValue,
            onChanged: (value) => setState(() => sliderValue = value),
          ),
        ],
      );

  Debouncer debouncer = Debouncer(milliseconds: 3000);

  TextEditingController tecOtpAddress = TextEditingController();
  Widget reverseWidget() => Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Radio(
                        value: 0,
                        onChanged: (value) =>
                            setState(() => groupRadio = value),
                        groupValue: groupRadio,
                      ),
                      Text('otp1'),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Radio(
                        value: 1,
                        onChanged: (value) =>
                            setState(() => groupRadio = value),
                        groupValue: groupRadio,
                      ),
                      Text('otp2'),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Radio(
                        value: 2,
                        onChanged: (value) =>
                            setState(() => groupRadio = value),
                        groupValue: groupRadio,
                      ),
                      Text('otp3'),
                    ],
                  ),
                ],
              ),
              Container(
                width: 100,
                child: TextField(
                  controller: tecOtpPort,
                  onChanged: (value) => setState(() => port = value),
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      hintText: 'Port', border: OutlineInputBorder()),
                ),
              ),
            ],
          ),
          TextField(
            controller: tecOtpAddress,
            decoration: InputDecoration(
                hintText: 'Address', border: OutlineInputBorder()),
          ),
        ],
      );

  TextEditingController tecAgencies = TextEditingController();
  bool isCompanyOnly = false;
  Widget charterAgenciesWidget() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: (MediaQuery.of(context).size.width / 3) * 2.2,
            child: TextField(
              controller: tecAgencies,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  hintText: 'Project ID', border: OutlineInputBorder()),
            ),
          ),
          Column(
            children: <Widget>[
              Text('routeIds', textAlign: TextAlign.center),
              Checkbox(
                value: isCompanyOnly,
                onChanged: (value) => setState(() => isCompanyOnly = value),
              ),
            ],
          ),
        ],
      );

  TextEditingController tecLoginNumber = TextEditingController();
  TextEditingController tecAgencyId = TextEditingController();
  Widget charterLoginWidget() => Column(
        children: [
          TextField(
            controller: tecLoginNumber,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: 'Login Number', border: OutlineInputBorder()),
          ),
          TextField(
            controller: tecAgencyId,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: 'Agency ID', border: OutlineInputBorder()),
          ),
        ],
      );

  TextEditingController tecCharterStopsLoginLatitude = TextEditingController();
  TextEditingController tecCharterStopsLoginLongitude = TextEditingController();
  TextEditingController tecCharterStopsLoginLogin = TextEditingController();
  List<MOBProject> projectsStopsByLogin = [];
  double radiusInMeters = 1000.0;
  List<int> routeIds = [];
  List<MOBCharterCompany> companies = [];
  MOBProject selectedProject;
  MOBCharterCompany selectedCompany;
  bool isCharterStopsLoginLoading = false;
  bool isCharterStopsLoginCompanyOnly = false;
  bool isCharterStopsNoLogin = false;
  Widget charterStopsByLoginWidget() => Column(
        children: [
          if (projectsStopsByLogin.isNotEmpty)
            Container(
              height: 60,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: projectsStopsByLogin.length,
                itemBuilder: (context, index) {
                  MOBProject project = projectsStopsByLogin[index];
                  int projectId = project.projectId;
                  String name = project.name;
                  return Card(
                    color:
                        selectedProject == project ? Colors.blue : Colors.white,
                    child: InkWell(
                      onTap: () async {
                        selectedProject = project;
                        http.Response response = await MOBApiUtils.getAgencies(
                            project.projectId, isCharterStopsLoginCompanyOnly);
                        if (response != null) {
                          String jsonString =
                              convert.utf8.decode(response.bodyBytes);
                          Iterable iterable = convert.json.decode(jsonString);
                          companies = iterable
                              .map((e) => MOBCharterCompany.fromJson(
                                  e,
                                  isCharterStopsLoginCompanyOnly
                                      ? CHARTER_TYPE.COMPANY_SELECT
                                      : null))
                              .toList();
                        }
                        setState(() {});
                      },
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Column(
                          children: [
                            Text('$projectId'),
                            Text(name),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              if (!isCharterStopsLoginCompanyOnly && !isCharterStopsNoLogin)
                Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 3 - 10,
                      child: TextField(
                        controller: tecCharterStopsLoginLogin,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            hintText: 'Login', border: OutlineInputBorder()),
                      ),
                    ),
                    RaisedButton.icon(
                      onPressed: () async {
                        String login = tecCharterStopsLoginLogin.text;
                        int agencyId = selectedCompany.agencyId;
                        http.Response response =
                            await MOBApiUtils.loginCharter(login, agencyId);
                        String body = convert.utf8.decode(response.bodyBytes);
                        Map<String, dynamic> map = convert.json.decode(body);
                        Iterable routes = map['routes'] ?? [];
                        map = routes.first;
                        routes = map['tripIds'];
                        setState(() =>
                            routeIds = routes.map((e) => e as int).toList());
                      },
                      icon: Icon(Icons.done),
                      label: Text('Login'),
                    )
                  ],
                ),
              if (!isCharterStopsNoLogin)
                Container(
                  width: MediaQuery.of(context).size.width / 3 - 10,
                  child: Column(
                    children: [
                      Text(
                        'Apenas\nempresa',
                        textAlign: TextAlign.center,
                      ),
                      Checkbox(
                        value: isCharterStopsLoginCompanyOnly,
                        onChanged: (value) => setState(
                            () => isCharterStopsLoginCompanyOnly = value),
                      ),
                    ],
                  ),
                ),
              if (!isCharterStopsLoginCompanyOnly)
                Container(
                  width: MediaQuery.of(context).size.width / 3 - 10,
                  child: Column(
                    children: [
                      Text(
                        'Sem\nLogin',
                        textAlign: TextAlign.center,
                      ),
                      Checkbox(
                        value: isCharterStopsNoLogin,
                        onChanged: (value) =>
                            setState(() => isCharterStopsNoLogin = value),
                      ),
                    ],
                  ),
                ),
            ],
          ),
          if (companies.isNotEmpty &&
              (!isCharterStopsNoLogin || isCharterStopsLoginCompanyOnly))
            Container(
              height: isCharterStopsLoginCompanyOnly ? 80 : 40,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: companies.length,
                itemBuilder: (context, index) {
                  MOBCharterCompany company = companies[index];
                  String name = company.name;
                  List<int> mRouteIds = company.routeIds;
                  return Card(
                    color:
                        selectedCompany == company ? Colors.blue : Colors.white,
                    child: InkWell(
                      onTap: () async {
                        selectedCompany = company;
                        if (isCharterStopsLoginCompanyOnly)
                          routeIds = mRouteIds;
                        setState(() {});
                      },
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Column(
                          children: [
                            Text(name),
                            if (isCharterStopsLoginCompanyOnly)
                              Container(
                                height: 35,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: mRouteIds.length,
                                  itemBuilder: (context, index) {
                                    int tripId = mRouteIds[index];
                                    return Card(
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        child: Text('$tripId'),
                                      ),
                                    );
                                  },
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          if (!isCharterStopsLoginCompanyOnly)
            Container(
              height: 35,
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: routeIds.length,
                itemBuilder: (context, index) {
                  int tripId = routeIds[index];
                  return Card(
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: Text('$tripId'),
                    ),
                  );
                },
              ),
            ),
          Row(
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2 - 15,
                child: TextField(
                  controller: tecCharterStopsLoginLatitude,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      hintText: 'Latitude', border: OutlineInputBorder()),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2 - 15,
                child: TextField(
                  controller: tecCharterStopsLoginLongitude,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      hintText: 'Longitude', border: OutlineInputBorder()),
                ),
              ),
            ],
          ),
        ],
      );

  int selectedClearProjectId = 0;
  List<MOBProject> clearProjects = [];
  Widget charterClearCacheWidget() => Container(
        height: 70,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: clearProjects.length,
          itemBuilder: (context, index) {
            MOBProject project = clearProjects[index];
            int projectId = project.projectId;
            String name = project.name;
            return Container(
              width: 200,
              child: ListTile(
                tileColor:
                    selectedClearProjectId == projectId ? Colors.amber : null,
                title: Text(name),
                subtitle: Text('ID: $projectId'),
                onTap: () => setState(() => selectedClearProjectId = projectId),
              ),
            );
          },
        ),
      );

  void doApi() async {
    dynamic request = url[name];
    dynamic newRequest = request is Map ? {} : '';
    bool isPost = false;
    Map post = {};
    switch (api) {
      case API.PROJECTS:
        newRequest = request
            .replaceAll('param1', isProjectsDev.toString())
            .replaceAll('param2', isProjectsCharter.toString());
        break;
      case API.PROJECT_DETAILS:
        newRequest = request.replaceAll('param1', tecProjectDetails.text);
        break;
      case API.TIMETABLE_FULL:
        newRequest = request.replaceAll('param1', tecTimetableFull.text);
        break;
      case API.TIMETABLE_PARTIAL:
        newRequest = request.replaceAll('param1', tecTimetablePartial.text);
        break;
      case API.TIMETABLE_BY_LINE:
        newRequest = request
            .replaceAll('param1', tecTimetableProjectId.text)
            .replaceAll('param2', tecTimetableRouteId.text);
        break;
      case API.TRIP:
        newRequest = request
            .replaceAll('param1', tecTripRouteId.text)
            .replaceAll('param2', tecTripTripId.text);
        break;
      case API.NEAR_STOPS:
        newRequest = MOBApiUtils.getStopsInViewUrl(bounds);
        break;
      case API.DEPARTURES:
        newRequest = request.replaceAll('param1', tecDepartureStopId.text);
        break;
      case API.POS:
        newRequest =
            request.replaceAll('param1', tecPointsOfSaleProjectId.text);
        break;
      case API.NEWS_ALERTS:
        for (final k in request.keys)
          newRequest[k] =
              request[k].replaceAll('param1', tecNewsAlertsProjectId.text);
        break;
      case API.REVERSE:
        String address = tecOtpAddress.text.replaceAll(' ', '+');
        newRequest = '$otp:$port/otp/geocode?address=$address';
        break;
      case API.OPEN_TRIPPLANER:
        urlOTP = Utils.getOtpParamsUrl(
          dateTime,
          otp,
          port,
          fromParam,
          toParam,
          timeParam,
          dateParam,
          sliderValue,
          arriveBy,
          wheelChair,
          modes,
        );
        newRequest = urlOTP;
        break;
      case API.CLEAR_CACHE:
        newRequest = request.replaceAll('param1', '$selectedClearProjectId');
        break;
      case API.AGENCIES:
        String id = tecAgencies.text;
        String companyOnly = isCompanyOnly.toString();
        newRequest =
            request.replaceAll('param1', id).replaceAll('param2', companyOnly);
        break;
      case API.LOGIN:
        String login = tecLoginNumber.text;
        String agencyId = tecAgencyId.text;
        isPost = true;
        post = {
          'matricula': login,
          'agency_id': agencyId,
        };
        break;
      case API.STOPS_LOGIN:
        String strLat = tecCharterStopsLoginLatitude.text;
        String strLng = tecCharterStopsLoginLongitude.text;

        double latitude = strLat.isEmpty ? 0.0 : double.parse(strLat);
        double longitude = strLng.isEmpty ? 0.0 : double.parse(strLng);
        newRequest = MOBApiUtils.getStopsInViewUrl(
            SphericalUtils.toBounds(latitude, longitude, radiusInMeters),
            routeIds);
        break;
    }
    if (isPost)
      await doPost(request, post);
    else if (newRequest is Map)
      await doGet2(newRequest);
    else
      await doGet(newRequest);
    setState(() => isLoading = false);
  }

  Future<void> doPost(dynamic request, Map post) async {
    try {
      String jsonPost = convert.json.encode(post);

      http.Response response = await http
          .post(
            request,
            body: jsonPost,
            headers: MOBApiUtils.headers,
          )
          .timeout(Utils.timeoutDuration);
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> result = convert.json.decode(jsonString);

      if (result.isEmpty) throw Exception();
      jsonViewer = JsonViewerWidget(result);
    } on Exception {
      setState(() => hasError = true);
    }
  }

  Future<void> doGet2(dynamic request) async {
    try {
      Map<String, dynamic> map = {};
      for (final k in request.keys) {
        http.Response response = await http
            .get(
              request[k],
              headers: MOBApiUtils.headers,
            )
            .timeout(Utils.timeoutDuration);
        String jsonString = convert.utf8.decode(response.bodyBytes);
        var con = convert.json.decode(jsonString);
        List<dynamic> l = con.toList();
        map[k] = l;
      }

      if (map.isEmpty) throw Exception();
      jsonViewer = JsonViewerWidget(map);
    } on Exception {
      setState(() => hasError = true);
    }
  }

  Future<void> doGet(dynamic request) async {
    try {
      http.Response response = await http
          .get(
            request,
            headers: MOBApiUtils.headers,
          )
          .timeout(Utils.timeoutDuration);
      String jsonString = convert.utf8.decode(response.bodyBytes);
      var con = convert.json.decode(jsonString);
      Map<String, dynamic> map = {};
      if (con is Iterable || con is List) {
        List<dynamic> l = con.toList();
        for (final m in l) {
          String key = (m as Map).values.toString();
          map[key] = m;
        }
      } else
        map[''] = con;
      if (map.isEmpty) throw Exception();
      jsonViewer = JsonViewerWidget(map);
    } on Exception {
      setState(() => hasError = true);
    }
  }

  @override
  Widget build(BuildContext context) {
    switch (api) {
      case API.PROJECTS:
        apiWidget = projectsWidget();
        break;
      case API.PROJECT_DETAILS:
        apiWidget = projectDetailsWidget();
        break;
      case API.TIMETABLE_FULL:
        apiWidget = timetableWidget();
        break;
      case API.TIMETABLE_PARTIAL:
        apiWidget = timetablePartialWidget();
        break;
      case API.TIMETABLE_BY_LINE:
        apiWidget = timetableLineWidget();
        break;
      case API.TRIP:
        apiWidget = tripWidget();
        break;
      case API.NEAR_STOPS:
        apiWidget = nearStopsWidget();
        break;
      case API.DEPARTURES:
        apiWidget = departureWidget();
        break;
      case API.POS:
        apiWidget = pointsOfSaleWidget();
        break;
      case API.NEWS_ALERTS:
        apiWidget = newsAlertsWidget();
        break;
      case API.REVERSE:
        apiWidget = reverseWidget();
        Map mapOtp = url.values.first;
        List<String> urls = mapOtp.values.toList();
        otp = urls[groupRadio];
        break;
      case API.OPEN_TRIPPLANER:
        apiWidget = otpWidget();
        Map mapOtp = url.values.first;
        List<String> urls = mapOtp.values.toList();
        otp = urls[groupRadio];
        break;
      case API.AGENCIES:
        apiWidget = charterAgenciesWidget();
        break;
      case API.CLEAR_CACHE:
        apiWidget = charterClearCacheWidget();
        break;
      case API.LOGIN:
        apiWidget = charterLoginWidget();
        break;
      case API.STOPS_LOGIN:
        apiWidget = charterStopsByLoginWidget();
        break;
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      key: scaffoldKey,
      appBar: AppBar(
        title: Text(name),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, () => setState(() {})),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.sync,
              size: 35,
            ),
            onPressed: () {
              setState(() {
                isLoading = true;
                hasError = false;
              });
              doApi();
            },
          )
        ],
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 50),
          child: Column(
            children: <Widget>[
              if (apiWidget != null)
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(5),
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    elevation: 5,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: apiWidget,
                    ),
                  ),
                ),
              if (isLoading)
                CircularProgressIndicator(
                    strokeWidth: 3,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Utils.isLightTheme(context)
                            ? Utils.getColorFromPrimary(context)
                            : Colors.black)),
              if (!hasError && jsonViewer != null) jsonViewer,
              if (hasError)
                Container(
                  padding: EdgeInsets.all(50),
                  child: Text('Inconsistência de dados.\n\n'
                      'Motivos:\n'
                      '- Dados desvinculados\n'
                      '- Resultado vazio\n'),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
