import 'dart:async';

import 'package:cpfcnpj/cpfcnpj.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromTransportCard.dart';

import 'customwidget/custom_text_form_field.dart';

class HolderDetails extends StatefulWidget {
  HolderDetails(
    this.transportCardUser,
    this.formMap,
    this.formKey,
  ) : super();
  final DatapromTransportCardUser transportCardUser;
  final Map<String, dynamic> formMap;
  final GlobalKey<FormState> formKey;
  _HolderDetails createState() => _HolderDetails(
        transportCardUser,
        formMap,
        formKey,
      );
}

class _HolderDetails extends State<HolderDetails> {
  _HolderDetails(
    this.transportCardUser,
    this.formMap,
    this.formKey,
  ) : super();
  final DatapromTransportCardUser transportCardUser;
  final Map<String, dynamic> formMap;
  final GlobalKey<FormState> formKey;

  TextEditingController tecName = TextEditingController();
  TextEditingController tecEmail = TextEditingController();
  TextEditingController tecCPF = TextEditingController();
  TextEditingController tecPhone = TextEditingController();
  TextEditingController tecBornDate = TextEditingController();

  Timer timer;

  @override
  void initState() {
    final bornDate =
        DateFormat('dd/MM/yyyy').format(transportCardUser.bornDate);
    tecBornDate.text = bornDate;
    super.initState();
    Future.delayed(
        Duration(seconds: 1),
        () => timer = Timer.periodic(
            Duration(milliseconds: 500), (timer) => setState(() {})));
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    formMap['name'] = tecName.text;
    formMap['email'] = tecEmail.text;
    formMap['cpf'] = tecCPF.text;
    formMap['phone'] = tecPhone.text;
    formMap['bornDate'] = tecBornDate.text;

    return Scrollbar(
      child: SingleChildScrollView(
        child: Container(
          child: Form(
            key: formKey,
            child: Column(
              children: [
                CustomTextFormField(
                  LengthLimitingTextInputFormatter(50),
                  'Nome Completo',
                  TextInputType.text,
                  tecName,
                  tecName.text.split(' ').length > 0,
                  'Nome incompleto',
                  formKey,
                ),
                CustomTextFormField(
                  LengthLimitingTextInputFormatter(50),
                  'E-Mail',
                  TextInputType.emailAddress,
                  tecEmail,
                  EmailValidator.validate(tecEmail.text),
                  'E-mail inválido',
                  formKey,
                  textCapitalization: TextCapitalization.none,
                ),
                Row(
                  children: <Widget>[
                    CustomTextFormField(
                      LengthLimitingTextInputFormatter(11),
                      'CPF',
                      TextInputType.number,
                      tecCPF,
                      CPF.isValid(tecCPF.text),
                      'CPF inválido',
                      formKey,
                      width: MediaQuery.of(context).size.width / 2,
                      textCapitalization: TextCapitalization.none,
                    ),
                    CustomTextFormField(
                      LengthLimitingTextInputFormatter(11),
                      'Celular',
                      TextInputType.number,
                      tecPhone,
                      tecPhone.text.length == 11,
                      'Formato: XX9XXXXXXXX',
                      formKey,
                      width: MediaQuery.of(context).size.width / 2,
                      textCapitalization: TextCapitalization.none,
                    ),
                  ],
                ),
                CustomTextFormField(
                  LengthLimitingTextInputFormatter(50),
                  'Data de Nascimento',
                  TextInputType.datetime,
                  tecBornDate,
                  tecBornDate.text.length == 10,
                  'Formato: XX/XX/XXXX',
                  formKey,
                  textCapitalization: TextCapitalization.none,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
