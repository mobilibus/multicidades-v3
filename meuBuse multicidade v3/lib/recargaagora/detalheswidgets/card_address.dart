import 'dart:async';
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/utils/Utils.dart';

import 'customwidget/custom_text_form_field.dart';

class CardAddress extends StatefulWidget {
  CardAddress(this.formMap, this.formKey) : super();
  final Map<String, dynamic> formMap;
  final GlobalKey<FormState> formKey;
  _CardAddress createState() => _CardAddress(formMap, formKey);
}

class _CardAddress extends State<CardAddress> {
  _CardAddress(this.formMap, this.formKey) : super();
  final Map<String, dynamic> formMap;
  final GlobalKey<FormState> formKey;

  TextEditingController tecZipCode = TextEditingController();
  TextEditingController tecPlace = TextEditingController();
  TextEditingController tecCountry = TextEditingController();
  TextEditingController tecState = TextEditingController();
  TextEditingController tecCity = TextEditingController();
  TextEditingController tecNeighborhood = TextEditingController();
  TextEditingController tecStreet = TextEditingController();
  TextEditingController tecPlaceNumber = TextEditingController();

  Timer timer;

  @override
  void initState() {
    super.initState();
    Future.delayed(
        Duration(seconds: 1),
        () => timer = Timer.periodic(
            Duration(milliseconds: 500), (timer) => setState(() {})));
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void zipCodeListener(String text) async {
    if (text.length == 8) {
      String cep = text;
      http.Response response = await http
          .get('https://viacep.com.br/ws/$cep/json/')
          .timeout(Duration(seconds: 30));
      String body = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> json = convert.json.decode(body);
      _ZipCode zipCode = _ZipCode.fromJson(json);
      tecState.text = zipCode.uf;
      tecCity.text = zipCode.localidade;
      tecStreet.text = zipCode.logradouro;
      tecNeighborhood.text = zipCode.bairro;
      tecCountry.text = 'BR';
      //tecPlace.text = zipCode.complemento;
      setState(() {});
    }
  }

  Widget bigScreenHolderDetails() => Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              CustomTextFormField(
                LengthLimitingTextInputFormatter(8),
                'CEP',
                TextInputType.number,
                tecZipCode,
                tecZipCode.text.length == 8,
                'Use 8 dígitos',
                formKey,
                width: MediaQuery.of(context).size.width / 2,
                onChanged: zipCodeListener,
                textCapitalization: TextCapitalization.none,
              ),
              CustomTextFormField(
                LengthLimitingTextInputFormatter(10),
                'Complemento',
                TextInputType.text,
                tecPlace,
                null, //tecPlace.text.length > 0,
                null,
                formKey,
                width: MediaQuery.of(context).size.width / 2,
                textCapitalization: TextCapitalization.none,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              CustomTextFormField(
                LengthLimitingTextInputFormatter(50),
                'Cidade',
                TextInputType.text,
                tecCity,
                tecCity.text.length > 1,
                'Informe a cidade',
                formKey,
                width: MediaQuery.of(context).size.width / 2,
                textCapitalization: TextCapitalization.none,
              ),
              CustomTextFormField(
                LengthLimitingTextInputFormatter(50),
                'Bairro',
                TextInputType.text,
                tecNeighborhood,
                tecNeighborhood.text.length > 1,
                'Informe o bairro',
                formKey,
                width: MediaQuery.of(context).size.width / 2,
                textCapitalization: TextCapitalization.none,
              ),
            ],
          ),
          Row(
            children: <Widget>[
              CustomTextFormField(
                LengthLimitingTextInputFormatter(50),
                'Rua',
                TextInputType.text,
                tecStreet,
                tecStreet.text.length > 1,
                'Informe a rua',
                formKey,
                width: MediaQuery.of(context).size.width / 2,
                textCapitalization: TextCapitalization.none,
              ),
              CustomTextFormField(
                LengthLimitingTextInputFormatter(5),
                'Número',
                TextInputType.number,
                tecPlaceNumber,
                tecPlaceNumber.text.length > 1,
                'Informe o número do complemento',
                formKey,
                width: MediaQuery.of(context).size.width / 2,
                textCapitalization: TextCapitalization.none,
              ),
            ],
          ),
        ],
      );

  Widget smallScreenHolderDetails(bool halfContainer) => Column(
        children: <Widget>[
          CustomTextFormField(
            LengthLimitingTextInputFormatter(8),
            'CEP',
            TextInputType.number,
            tecZipCode,
            tecZipCode.text.length == 8,
            'Use 8 dígitos',
            formKey,
            onChanged: zipCodeListener,
            textCapitalization: TextCapitalization.none,
          ),
          CustomTextFormField(
            LengthLimitingTextInputFormatter(50),
            'Rua',
            TextInputType.text,
            tecStreet,
            tecStreet.text.length > 1,
            'Informe a rua',
            formKey,
            textCapitalization: TextCapitalization.none,
          ),
          CustomTextFormField(
            LengthLimitingTextInputFormatter(5),
            'Número',
            TextInputType.number,
            tecPlaceNumber,
            tecPlaceNumber.text.length > 1,
            'Informe o número do complemento',
            formKey,
            textCapitalization: TextCapitalization.none,
          ),
          CustomTextFormField(
            LengthLimitingTextInputFormatter(10),
            'Complemento',
            TextInputType.text,
            tecPlace,
            null, //tecPlace.text.length > 0,
            null,
            formKey,
            textCapitalization: TextCapitalization.none,
          ),
          CustomTextFormField(
            LengthLimitingTextInputFormatter(50),
            'Cidade',
            TextInputType.text,
            tecCity,
            tecCity.text.length > 1,
            'Informe a cidade',
            formKey,
            textCapitalization: TextCapitalization.none,
          ),
          CustomTextFormField(
            LengthLimitingTextInputFormatter(50),
            'Bairro',
            TextInputType.text,
            tecNeighborhood,
            tecNeighborhood.text.length > 1,
            'Informe o bairro',
            formKey,
            textCapitalization: TextCapitalization.none,
          ),
        ],
      );
  @override
  Widget build(BuildContext context) {
    formMap['zipcode'] = tecZipCode.text;
    formMap['place'] = tecPlace.text;
    formMap['country'] = tecCountry.text;
    formMap['state'] = tecState.text;
    formMap['city'] = tecCity.text;
    formMap['neighborhood'] = tecNeighborhood.text;
    formMap['street'] = tecState.text;
    formMap['placeNumber'] = tecPlaceNumber.text;

    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    return Container(
      child: Scrollbar(
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              children: [
                devicePixelRatio < 2.5
                    ? smallScreenHolderDetails(false)
                    : bigScreenHolderDetails(),
                Row(
                  children: <Widget>[
                    CustomTextFormField(
                      LengthLimitingTextInputFormatter(2),
                      'País (Sigla)',
                      TextInputType.text,
                      tecCountry,
                      tecCountry.text.length == 2,
                      'Use 2 dígitos',
                      formKey,
                      width: MediaQuery.of(context).size.width / 2,
                      textCapitalization: TextCapitalization.characters,
                    ),
                    CustomTextFormField(
                      LengthLimitingTextInputFormatter(2),
                      'Estado (Sigla)',
                      TextInputType.text,
                      tecState,
                      tecState.text.length == 2,
                      'Use 2 dígitos',
                      formKey,
                      width: MediaQuery.of(context).size.width / 2,
                      textCapitalization: TextCapitalization.characters,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _ZipCode {
  final String logradouro;
  final String complemento;
  final String bairro;
  final String localidade;
  final String uf;

  _ZipCode(
      this.logradouro, this.complemento, this.bairro, this.localidade, this.uf);

  factory _ZipCode.fromJson(Map<String, dynamic> json) {
    String logradouro = json['logradouro'] ?? '';
    String complemento = json['complemento'] ?? '';
    String bairro = json['bairro'] ?? '';
    String localidade = json['localidade'] ?? '';
    String uf = json['uf'] ?? '';
    return _ZipCode(logradouro, complemento, bairro, localidade, uf);
  }
}
