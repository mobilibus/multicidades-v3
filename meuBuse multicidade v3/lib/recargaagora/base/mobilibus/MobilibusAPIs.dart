import 'dart:convert' as convert;

import 'package:http/http.dart' as http;
import 'package:mobilibus/recargaagora/base/gimave/Eucatur.dart';
import 'package:mobilibus/recargaagora/base/mobilibus/MobilibusTransaction.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class MobilibusAPIs {
  static String _mobDAO = 'https://editor.mobilibus.com/web/transaction';
  // {personalCard, cardType, amount}
  static String _mobTransactionCreate = '$_mobDAO/create';
  //personalCard
  static String _mobTransactions = '$_mobDAO/list';
  // {lastTransactionId, tokenDataprom, tokenEucard, STATUS}
  static String _mobTransactionUpdate = '$_mobDAO/update';

  static Map<String, String> _headers = {'Content-Type': 'application/json'};

  static Future<List<MobilibusTransaction>> getTransactions(
      String cardNumber) async {
    http.Response response = await http
        .get('$_mobTransactions/$cardNumber')
;    String body = convert.utf8.decode(response.bodyBytes);
    Iterable json = convert.json.decode(body);

    List<MobilibusTransaction> transactions =
        json.map((e) => MobilibusTransaction.fromJson(e)).toList();

    return transactions;
  }

  static Future<bool> isInvalidTransaction(
      String cardNumber, String identificadorRecarga) async {
    List<MobilibusTransaction> transactions = await getTransactions(cardNumber);
    MobilibusTransaction transaction = transactions.firstWhere(
        (transaction) => transaction.tokenDataprom == identificadorRecarga,
        orElse: null);

    STATUS status = transaction.status;

    //Validates if user cancelled or finished operation before paying
    return transaction == null ||
        status == STATUS.CANCELLED ||
        status == STATUS.CLOSED;
  }

  static Future<int> createTransaction(String cardNumber, String cardType,
      double amount, String identificadorRecarga) async {
    Map<String, dynamic> map = {
      'amount': amount,
      'personalCard': cardNumber,
      'cardType': cardType,
    };
    String post = convert.json.encode(map);
    http.Response response = await http
        .post(
          _mobTransactionCreate,
          body: post,
          headers: _headers,
        )
;    String body = convert.utf8.decode(response.bodyBytes);
    Map<String, dynamic> json = convert.json.decode(body);
    int transactionId = json['transactionId'];
    MobilibusAPIs.updateTransaction(
      STATUS.OPENED,
      transactionId,
      tokenDataprom: identificadorRecarga,
    );
    return transactionId;
  }

  static Future<void> updateTransaction(STATUS status, int transactionId,
      {String tokenDataprom, String tokenEucard}) async {
    Map<String, dynamic> map = {
      'transactionId': transactionId,
      if (tokenDataprom != null) 'tokenDataprom': tokenDataprom,
      if (tokenEucard != null) 'tokenEucard': tokenEucard,
      'status': status.toString().replaceAll('STATUS.', ''),
    };
    String post = convert.json.encode(map);
    return await http
        .post(
          _mobTransactionUpdate,
          body: post,
          headers: _headers,
        )
;  }

  static void updatePaymentsToken(Eucatur eucatur) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString('gimave_token', eucatur.creditCardToken);
    sp.setString('gimave_mask', eucatur.maskedCardNumber);
  }

  static Future<bool> doesAppContainsSavedToken() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    return sp.containsKey('gimave_token');
  }

  static Future<String> getCreditCardToken() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    return sp.getString('gimave_token');
  }

  static Future<String> getCreditCardMask() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    return sp.getString('gimave_mask');
  }

  static void clearTokens() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.remove('gimave_token');
    sp.remove('gimave_mask');
  }
}
