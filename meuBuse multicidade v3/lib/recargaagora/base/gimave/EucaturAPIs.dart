import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/recargaagora/base/dataprom/DatapromAPIs.dart';
import 'package:mobilibus/recargaagora/base/gimave/Eucatur.dart';
import 'package:mobilibus/recargaagora/base/mobilibus/MobilibusAPIs.dart';
import 'package:mobilibus/recargaagora/base/mobilibus/MobilibusTransaction.dart';

enum CARD {
  VISA,
  MASTERCARD,
  AMEX,
  CB,
  HIPERCARD,
  AURA,
  DINERS,
  ELO,
}

abstract class EucaturAPIs {
  static String _payUrlEucaturTest = 'https://testpayments.eucard.com.br/pay';
  static String _payUrlEucatur = 'https://payments.eucard.com.br/pay';

  static String _userTest = '58920167';
  static String _pswdTest = '2994640523578222';

  static String _user = '58920167';
  static String _pswd = '1877911285856697';

  static String _baseTest =
      convert.base64.encode(convert.utf8.encode('$_userTest:$_pswdTest'));
  static String _base =
      convert.base64.encode(convert.utf8.encode('$_user:$_pswd'));

  static Map<String, String> _headersTest = {
    "Content-Type": "application/json",
    'Authorization': 'Basic $_baseTest',
  };

  static Map<String, String> _headers = {
    "Content-Type": "application/json",
    'Authorization': 'Basic $_base',
  };

  static Map<CARD, RegExp> _cardPatterns = {};

  static Future<void> payWithToken(
    BuildContext context,
    int transactionId,
    String identificadorRecarga,
    String cardNumber,
    double amount,
    bool isDatapromProduction,
    bool isGimaveProduction,
  ) async {
    Map<String, dynamic> map = await EucaturAPIs._payWithToken(
        transactionId,
        identificadorRecarga,
        cardNumber,
        amount,
        isDatapromProduction,
        isGimaveProduction);

    TextStyle textStyle = TextStyle(color: Colors.white);
    showDialog(
      context: context,
      builder: (BuildContext context) =>  AlertDialog(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(map.isEmpty ? 'Sucesso!' : 'Erro', style: textStyle),
        content: Wrap(
          children: <Widget>[
            Text(
                map.isEmpty
                    ? 'Pagamento realizado com sucesso!\n'
                        'Aguarde 30 minutos e passe o cartão no validador para os créditos serem carregados\n'
                        'Você pode conferir no histórico!'
                    : 'Erro ao realizar pagamento\n\n',
                style: textStyle),
            Text(
              map.isEmpty ? '' : map.toString(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 12,
              ),
            ),
          ],
        ),
        actions: <Widget>[
          FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
              child: Text('Fechar', style: textStyle))
        ],
      ),
    );
  }

  static Future<Map<String, dynamic>> _payWithToken(
    int transactionId,
    String identificadorRecarga,
    String cardNumber,
    double amount,
    bool isDatapromProduction,
    bool isGimaveProduction,
  ) async {
    String token = await MobilibusAPIs.getCreditCardToken();
    Map<String, dynamic> post = {
      'amount': amount,
      'credit_card_token': token,
      'method': 'CREDIT_CARD_TOKEN',
      'shopping_cart': [
        {
          'amount': amount,
          'description': 'Recarga Sinetram',
          'quantity': 1,
        }
      ],
    };
    String json = convert.json.encode(post);

    String url = isGimaveProduction ? _payUrlEucatur : _payUrlEucaturTest;
    Map<String, String> headers = isGimaveProduction ? _headers : _headersTest;
    http.Response response = await http.post(url, body: json, headers: headers);
    String body = convert.utf8.decode(response.bodyBytes);
    Map<String, dynamic> map = convert.json.decode(body);

    map = await _processResponse(
      response.statusCode,
      map,
      cardNumber,
      identificadorRecarga,
      transactionId,
      isDatapromProduction,
      isGimaveProduction,
    );

    return map;
  }

  static Future<Map<String, dynamic>> pay(
    Map<String, dynamic> eucaturPay,
    int transactionId,
    String identificadorRecarga,
    String cardNumber,
    bool isDatapromProduction,
    bool isGimaveProduction,
  ) async {
    bool isInvalid = await MobilibusAPIs.isInvalidTransaction(
        cardNumber, identificadorRecarga);
    if (isInvalid)
      return {
        'causa': 'Usuário',
        'erro': 'Tentativa de pagamento com operação cancelada ou finalizada',
        'antifraud': '',
      };

    String json = convert.json.encode(eucaturPay);

    String url = isGimaveProduction ? _payUrlEucatur : _payUrlEucaturTest;
    Map<String, String> headers = isGimaveProduction ? _headers : _headersTest;
    http.Response response = await http.post(url, body: json, headers: headers);
    String body = convert.utf8.decode(response.bodyBytes);
    Map<String, dynamic> map = convert.json.decode(body);

    map = await _processResponse(
      response.statusCode,
      map,
      cardNumber,
      identificadorRecarga,
      transactionId,
      isDatapromProduction,
      isGimaveProduction,
    );

    return map;
  }

  static Future<Map<String, dynamic>> _processResponse(
    int statusCode,
    Map<String, dynamic> map,
    String cardNumber,
    String identificadorRecarga,
    int transactionId,
    bool isDatapromProduction,
    bool isGimaveProduction,
  ) async {
    if (statusCode == 201) {
      Eucatur eucatur = Eucatur.fromJson(map);
      await MobilibusAPIs.updateTransaction(
        STATUS.CLOSED,
        transactionId,
        tokenEucard: eucatur.uuid,
        tokenDataprom: identificadorRecarga,
      );
      MobilibusAPIs.updatePaymentsToken(eucatur);
      return await DatapromAPIs.confirmAuthorization(
        cardNumber,
        identificadorRecarga,
        isDatapromProduction,
      );
    } else {
      await MobilibusAPIs.updateTransaction(
        STATUS.DENIED,
        transactionId,
        tokenDataprom: identificadorRecarga,
      );
      map = {
        'erro': map['returns']['message']['text'],
        'causa': map['returns']['status'],
        'antifraud':
            map.containsKey('antifraud') ? map['antifraud']['error'] : '',
      };
    }
    return map;
  }

  static CARD getCardType(String cardNumber) {
    String mCardNumber = cardNumber.replaceAll(RegExp('/[^0-9]+/g'), '');

    if (_cardPatterns.isEmpty) _initCardRegEx();
    CARD card;
    _cardPatterns.forEach((key, value) {
      if (value.hasMatch(mCardNumber)) {
        card = key;
        return;
      }
    });

    return card;
  }

  static _initCardRegEx() => CARD.values.forEach((cardEnum) {
        switch (cardEnum) {
          case CARD.VISA:
            _cardPatterns[cardEnum] = RegExp(r'^4[0-9]{12}(?:[0-9]{3})');
            break;
          case CARD.MASTERCARD:
            _cardPatterns[cardEnum] = RegExp(r'^5[1-5][0-9]{14}');
            break;
          case CARD.AMEX:
            _cardPatterns[cardEnum] = RegExp(r'^3[47][0-9]{13}');
            break;
          case CARD.CB:
            _cardPatterns[cardEnum] = RegExp(r'^(?:2131|1800|35\d{3})\d{11}');
            break;
          case CARD.HIPERCARD:
            _cardPatterns[cardEnum] =
                RegExp(r'^(606282\d{10}(\d{3})?)|(3841\d{15})');
            break;
          case CARD.AURA:
            _cardPatterns[cardEnum] = RegExp(r'^(5078\d{2})(\d{2})(\d{11})$');
            break;
          case CARD.DINERS:
            _cardPatterns[cardEnum] =
                RegExp(r'^3(?:0[0-5]|[68][0-9])[0-9]{11}');
            break;
          case CARD.ELO:
            _cardPatterns[cardEnum] = RegExp(
                r'^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})');
            break;
        }
      });

  static String getAssetByCardType(CARD cardEnum) {
    switch (cardEnum) {
      case CARD.VISA:
        return 'assets/cards/visa.png';
      case CARD.MASTERCARD:
        return 'assets/cards/mastercard.png';
      case CARD.AMEX:
        return 'assets/cards/amex.png';
      case CARD.CB:
        return 'assets/cards/jcb.png';
      case CARD.HIPERCARD:
        return 'assets/cards/hipercard.png';
      case CARD.AURA:
        return 'assets/cards/aura.png';
      case CARD.DINERS:
        return 'assets/cards/diners.png';
      case CARD.ELO:
        return 'assets/cards/elo.png';
      default:
        return '';
    }
  }
}
