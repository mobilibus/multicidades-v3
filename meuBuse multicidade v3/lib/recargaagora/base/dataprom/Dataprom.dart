class Dataprom {
  final List<Bilhetagem> bilhetagens;

  Dataprom(this.bilhetagens);

  factory Dataprom.fromJson(Map<String, dynamic> json) {
    List<Bilhetagem> bilhetagens = json.containsKey('bilhetagens')
        ? (json['bilhetagens'] as Iterable)
            .map((e) => Bilhetagem.fromJson(e))
            .toList()
        : [];
    return Dataprom(bilhetagens);
  }
}

class Bilhetagem {
  final String codigo;
  final String nome;
  final Cidade cidade;
  final String regexNumeroCartao;
  final List<TipoCartaoTransporte> tiposCartaoTransporte;

  Bilhetagem(this.codigo, this.nome, this.cidade, this.regexNumeroCartao,
      this.tiposCartaoTransporte);

  factory Bilhetagem.fromJson(Map<String, dynamic> json) {
    String codigo = json['codigo'] ?? '-1';
    String nome = json['nome'] ?? '';
    Cidade cidade =
        json.containsKey('cidade') ? Cidade.fromJson(json['cidade']) : null;
    String regexNumeroCartao = json['regexNumeroCartao'] ?? '';
    List<TipoCartaoTransporte> tiposCartaoTransporte =
        json.containsKey('tiposCartaoTransporte')
            ? (json['tiposCartaoTransporte'] as Iterable)
                .map((e) => TipoCartaoTransporte.fromJson(e))
                .toList()
            : [];
    return Bilhetagem(
        codigo, nome, cidade, regexNumeroCartao, tiposCartaoTransporte);
  }
}

class Cidade {
  final String codigo;
  final String nome;
  final String siglaEstado;
  final String siglaPais;

  Cidade(this.codigo, this.nome, this.siglaEstado, this.siglaPais);

  factory Cidade.fromJson(Map<String, dynamic> json) {
    String codigo = json['codigo'] ?? '-1';
    String nome = json['nome'] ?? '';
    String siglaEstado = json['siglaEstado'] ?? '';
    String siglaPais = json['sigraPais'] ?? '';

    return Cidade(codigo, nome, siglaEstado, siglaPais);
  }
}

class TipoCartaoTransporte {
  final String codigo;
  final String nome;

  TipoCartaoTransporte(this.codigo, this.nome);

  factory TipoCartaoTransporte.fromJson(Map<String, dynamic> json) {
    String codigo = json['codigo'] ?? '-1';
    String nome = json['nome'] ?? '';
    return TipoCartaoTransporte(codigo, nome);
  }
}
