class CartaoTransporte {
  final bool podeReceberRecargas;
  final UsuarioCartaoTransporte usuarioCartaoTransporte;
  final List<Saldo> saldos;

  CartaoTransporte(
      this.podeReceberRecargas, this.usuarioCartaoTransporte, this.saldos);

  factory CartaoTransporte.fromJson(Map<String, dynamic> json) {
    bool podeReceberRecargas = json['podeReceberRecargas'] ?? false;
    UsuarioCartaoTransporte usuarioCartaoTransporte =
        json.containsKey('usuarioCartaoTransporte')
            ? UsuarioCartaoTransporte.fromJson(json['usuarioCartaoTransporte'])
            : null;
    List<Saldo> saldos = json.containsKey('saldos')
        ? (json['saldos'] as Iterable).map((e) => Saldo.fromJson(e)).toList()
        : [];
    return CartaoTransporte(
        podeReceberRecargas, usuarioCartaoTransporte, saldos);
  }
}

class UsuarioCartaoTransporte {
  final String nome;
  final String documento;
  final String dataNascimento;
  final String endereco;
  final String nomeCidade;
  final String siglaEstado;

  UsuarioCartaoTransporte(this.nome, this.documento, this.dataNascimento,
      this.endereco, this.nomeCidade, this.siglaEstado);

  factory UsuarioCartaoTransporte.fromJson(Map<String, dynamic> json) {
    String nome = json['nome'] ?? '';
    String documento = json['documento'] ?? '';
    String dataNascimento = json['dataNascimento'] ?? '1970-01-01 00:00:00';
    String endereco = json['endereco'] ?? '';
    String nomeCidade = json['nomeCidade'] ?? '';
    String siglaEstado = json['siglaEstado'] ?? '';
    return UsuarioCartaoTransporte(
        nome, documento, dataNascimento, endereco, nomeCidade, siglaEstado);
  }
}

class Saldo {
  final String codigoTipoCartaoTransporte;
  final double valorMaximoRecargas;
  final double quantidadeMaximaRecargas;
  final double saldo;
  final String dataSaldo;

  Saldo(this.codigoTipoCartaoTransporte, this.valorMaximoRecargas,
      this.quantidadeMaximaRecargas, this.saldo, this.dataSaldo);

  factory Saldo.fromJson(Map<String, dynamic> json) {
    String codigoTipoCartaoTransporte =
        json['codigoTipoCartaoTransporte'] ?? '-1';
    double valorMaximoRecargas =
        double.parse(json['valorMaximoRecargas'].toString()) ?? -1;
    double quantidadeMaximaRecargas =
        double.parse(json['quantidadeMaximaRecargas'].toString()) ?? -1;
    double saldo = double.parse(json['saldo'].toString()) ?? -1;
    String dataSaldo = json['dataSaldo'] ?? '1970-01-01 00:00:00';
    try {
      dataSaldo = dataSaldo.replaceAll('T', ' ').substring(0, 19);
      DateTime.parse(dataSaldo);
    } catch (e) {
      dataSaldo = '1970-01-01T00:00:00';
    }
    return Saldo(codigoTipoCartaoTransporte, valorMaximoRecargas,
        quantidadeMaximaRecargas, saldo, dataSaldo);
  }
}
