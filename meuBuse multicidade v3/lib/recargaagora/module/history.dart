import 'dart:async';

import 'package:clipboard_manager/clipboard_manager.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/recargaagora/module/recarga_detalhes.dart';
import 'package:mobilibus/recargaagora/module/token_request.dart';
import 'package:mobilibus/base/recargaagora/dataprom/Dataprom.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromTransportCard.dart';
import 'package:mobilibus/base/recargaagora/mobilibus/MobilibusAPIs.dart';
import 'package:mobilibus/base/recargaagora/mobilibus/MobilibusTransaction.dart';
import 'package:mobilibus/utils/Utils.dart';

class History extends StatefulWidget {
  History(this.usuarioCartaoTransporte, this.cardNumber, this.bilhetagem,
      this.isDatapromProduction, this.isGimaveProduction);
  final DatapromTransportCardUser usuarioCartaoTransporte;
  final String cardNumber;
  final DatapromTicketing bilhetagem;
  final bool isDatapromProduction;
  final bool isGimaveProduction;
  _History createState() => _History(usuarioCartaoTransporte, cardNumber,
      bilhetagem, isDatapromProduction, isGimaveProduction);
}

class _History extends State<History> with TickerProviderStateMixin {
  _History(this.usuarioCartaoTransporte, this.cardNumber, this.bilhetagem,
      this.isDatapromProduction, this.isGimaveProduction)
      : super();
  final DatapromTransportCardUser usuarioCartaoTransporte;
  final String cardNumber;
  final DatapromTicketing bilhetagem;
  final bool isDatapromProduction;
  final bool isGimaveProduction;

  TabController tabController;
  List<MobilibusTransaction> transactions = [];
  bool loading = true;
  int page = 1;
  Timer timer;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    tabController = TabController(length: 4, vsync: this);
    Future.delayed(Duration(), () async {
      loading = false;
      await request();
    });
    super.initState();
    timer = Timer.periodic(Duration(seconds: 1), (timer) async {
      bool isNotEmpty = await request();
      if (!isNotEmpty) timer.cancel();
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  Future<bool> request() async {
    List<MobilibusTransaction> mTransactions =
    await MobilibusAPIs.getTransactions(cardNumber, page);
    transactions.addAll(mTransactions);
    transactions.sort((a, b) => b.transactionId.compareTo(a.transactionId));
    page++;
    setState(() {});
    return mTransactions.isNotEmpty;
  }

  void done(
      int transactionId,
      String identificadorRecarga,
      double amount,
      String cardType,
      ) async {
    bool hasToken = await MobilibusAPIs.doesAppContainsSavedToken();
    if (hasToken) {
      String cardMask = await MobilibusAPIs.getCreditCardMask();
      String token = await MobilibusAPIs.getCreditCardToken();
      await Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => TokenRequest(
            cardMask,
            token,
            cardNumber,
            amount,
            usuarioCartaoTransporte,
            cardType,
            isDatapromProduction,
            isGimaveProduction,
          )));
    } else
      await Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => RecargaDetalhes(
            usuarioCartaoTransporte,
            cardNumber,
            cardType,
            amount,
            isDatapromProduction,
            isGimaveProduction,
          )));
  }
/*
  void cancel(MobilibusTransaction transaction) async {
    String cancelResult = await DatapromAPIs.cancelAuthorization(
      cardNumber,
      transaction.tokenDataprom,
      transaction,
      isDatapromProduction,
    );

    TextStyle textStyle = TextStyle(color: Colors.white);

    showDialog(
      context: context,
      child: AlertDialog(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text('Cancelar Autorização', style: textStyle),
        content: Text(cancelResult, style: textStyle),
        actions: <Widget>[
          RaisedButton(
              color: Theme.of(context).accentColor,
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Fechar', style: textStyle))
        ],
      ),
    );
  } */

  void showTokens(String tokenDataprom, String tokenEucard) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);

    Widget alertDialog = AlertDialog(
      backgroundColor: Theme.of(context).primaryColor,
      title: Text('Tokens', style: textStyle),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Clique no token que deseja copiar\n', style: textStyle),
          Divider(),
          InkWell(
            onTap: () => copyToken(tokenDataprom),
            child: Text('Dataprom:\n$tokenDataprom', style: textStyle),
          ),
          Divider(),
          InkWell(
            onTap: () => copyToken(tokenEucard),
            child: Text('Eucard:\n$tokenEucard', style: textStyle),
          ),
        ],
      ),
      actions: <Widget>[
        FlatButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text('FECHAR', style: textStyle)),
      ],
    );
    showDialog(context: context, builder: (BuildContext context) =>  alertDialog);
  }

  void copyToken(String token) async {
    await ClipboardManager.copyToClipBoard(token);
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text('Token copiado com sucesso!'),
      duration: Duration(seconds: 2),
    ));
  }

  Widget listHistory(List<MobilibusTransaction> transactions) => transactions
      .isEmpty
      ? Center(child: Text('Nada por aqui!'))
      : Scrollbar(
    child: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: transactions.length,
            itemBuilder: (context, index) {
              MobilibusTransaction transaction = transactions[index];

              STATUS status = transaction.status;

              Color statusColor;
              switch (status) {
                case STATUS.OPENED:
                  statusColor = Colors.blue;
                  break;
                case STATUS.DENIED:
                  statusColor = Colors.red;
                  break;
                case STATUS.CLOSED:
                  statusColor = Colors.green;
                  break;
                case STATUS.CANCELLED:
                  statusColor = Colors.grey[600];
                  break;
              }

              String datapromToken = transaction.tokenDataprom;
              String eucardToken = transaction.tokenEucard.isEmpty
                  ? 'Não criado'
                  : transaction.tokenEucard;

              double amount = transaction.amount;
              String value =
              amount.toStringAsFixed(2).replaceAll('.', ',');
              DateTime createdAt = DateTime.fromMillisecondsSinceEpoch(
                  transaction.createTime);
              DateTime updatedAt = DateTime.fromMillisecondsSinceEpoch(
                  transaction.lastUpdate);

              String cardType = transaction.cardType.toString();
              cardType = bilhetagem.transportCardTypes
                  .firstWhere((element) => element.id == cardType)
                  .name;

              int transactionId = transaction.transactionId;
              String transactionIdStr = transactionId.toString();

              DateFormat dateFormat = DateFormat('dd/MM/yyyy HH:mm:ss');

              String createdDate = dateFormat.format(createdAt);
              String updatedDate = dateFormat.format(updatedAt);

              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Card(
                      elevation: 5,
                      color: statusColor,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      cardType,
                                      style:
                                      TextStyle(color: Colors.white),
                                    ),
                                    IconButton(
                                      icon: Icon(
                                        Icons.info,
                                        color: Colors.white,
                                      ),
                                      onPressed: () => showTokens(
                                        datapromToken,
                                        eucardToken,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      'R\$ $value',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Chip(
                                        label: Text(
                                            'ID: $transactionIdStr')),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10),
                            color: Colors.white,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Data de criação: $createdDate'),
                                Text('Última alteração: $updatedDate'),
                              ],
                            ),
                          ),
                          if (status == STATUS.OPENED ||
                              status == STATUS.DENIED)
                            Container(
                              color: Theme.of(context).primaryColor,
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  FlatButton(
                                    onPressed: () => done(
                                      transactionId,
                                      datapromToken,
                                      amount,
                                      cardType,
                                    ),
                                    child: Container(
                                      child: Column(
                                        children: <Widget>[
                                          Icon(Icons.done,
                                              color: Colors.white),
                                          Text('FINALIZAR',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 10)),
                                        ],
                                      ),
                                    ),
                                  ),
                                  /* FlatButton(
                                          onPressed: () => cancel(transaction),
                                          child: Container(
                                            child: Column(
                                              children: <Widget>[
                                                Icon(Icons.cancel,
                                                    color: Colors.white),
                                                Text('CANCELAR',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 10)),
                                              ],
                                            ),
                                          ),
                                        ), */
                                ],
                              ),
                            ),
                        ],
                      ),
                    ),
                  )
                ],
              );
            },
          )
        ],
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);

    List<List> tabsData = [
      ['Finalizado', Icons.done],
      ['Pendente', Icons.timelapse_outlined],
      ['Negado', Icons.warning],
      ['Cancelado', Icons.close],
    ];

    List<MobilibusTransaction> openTransactions = [];
    List<MobilibusTransaction> deniedTransactions = [];
    List<MobilibusTransaction> closedTransactions = [];
    List<MobilibusTransaction> cancelledTransactions = [];

    transactions.forEach((element) {
      switch (element.status) {
        case STATUS.OPENED:
          openTransactions.add(element);
          break;
        case STATUS.DENIED:
          deniedTransactions.add(element);
          break;
        case STATUS.CLOSED:
          closedTransactions.add(element);
          break;
        case STATUS.CANCELLED:
          cancelledTransactions.add(element);
          break;
      }
    });

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      key: scaffoldKey,
      appBar: AppBar(
        title: Text('Histórico', style: textStyle),
        centerTitle: true,
        actions: <Widget>[],
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: themeColor,
          ),
          onPressed: () => Navigator.pop(context, null),
        ),
        bottom: TabBar(
          isScrollable: true,
          controller: tabController,
          tabs: tabsData
              .map((e) => Tab(
            text: e[0],
            //icon: Icon(e[1]),
          ))
              .toList(),
        ),
      ),
      body: loading
          ? Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('Aguarde...'),
            CircularProgressIndicator(
                strokeWidth: 3,
                valueColor: AlwaysStoppedAnimation<Color>(
                    Utils.isLightTheme(context)
                        ? Utils.getColorFromPrimary(context)
                        : Colors.black))
          ],
        ),
      )
          : transactions.isEmpty
          ? Center(
        child: Text('Nenhuma transação efetuada até agora.'),
      )
          : TabBarView(
        controller: tabController,
        children: [
          listHistory(closedTransactions),
          listHistory(openTransactions),
          listHistory(deniedTransactions),
          listHistory(cancelledTransactions),
        ],
      ),
    );
  }
}
