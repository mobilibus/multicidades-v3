import 'dart:async';
import 'dart:convert' as convert;
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:bubble_overlay/bubble_overlay.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_utils/google_maps_utils.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:mobilibus/app/urban/route/departures/urban_route_departures.dart';
import 'package:mobilibus/base/MOBDeparture.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/base/MOBTripDetails.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:share/share.dart';
import 'package:timeline_tile/timeline_tile.dart';
import 'package:vibration/vibration.dart';

class UrbanRouteViewer extends StatefulWidget {
  UrbanRouteViewer(this.stopId, this.routeId, this.tripId);

  final int stopId;
  final int routeId;
  final int tripId;

  _UrbanRouteViewer createState() => _UrbanRouteViewer(stopId, routeId, tripId);
}

class _UrbanRouteViewer extends State<UrbanRouteViewer> {
  _UrbanRouteViewer(this.stopId, this.routeId, this.tripId) : super();

  final int stopId;
  final int routeId;
  final int tripId;
  List<Marker> directionsMarker = [];
  List<Marker> directionsMarkerFar = [];
  LatLng center;
  LatLngBounds bounds;

  Map<int, Marker> stopMarkers = {};
  Map<String, Marker> vehiclesMarkers = {};

  GoogleMapController controller;
  BitmapDescriptor iconStop;
  BitmapDescriptor iconBus;
  BitmapDescriptor iconBus0;
  BitmapDescriptor iconBus45;
  BitmapDescriptor iconBus90;
  BitmapDescriptor iconBus135;
  BitmapDescriptor iconBus180;
  BitmapDescriptor iconBus225;
  BitmapDescriptor iconBus270;
  BitmapDescriptor iconBus315;
  Uint8List bus0bytes;
  double zoom = 0.0;
  CameraPosition cameraPosition;

  Timer timerUpdate;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final BubbleOverlay bubbleOverlay =
      Platform.isAndroid ? BubbleOverlay() : null;

  AdmobBanner admob = AdmobBanner(
    adUnitId: Utils.getBannerAdUnitId(),
    adSize: AdmobBannerSize.BANNER,
  );

  MOBTripDetails tripDetails;
  Polyline polyline;
  final List<MOBTripDetailsStop> stops = [];
  List<MOBTripDetailsVehicle> vehicles = [];

  MarkerId selectedVehicle;
  MOBTripDetailsVehicle selectedMobVehicle;
  String selectedVehicleId = '';
  String selectedLastCommunication = '';

  MOBLine line;
  ScrollController listDeparturesController = ScrollController();
  MOBTripDetailsStop selectedStop;
  final List<DateTime> departures = [];
  int timezoneOffset = 0;

  @override
  void dispose() {
    timerUpdate?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    line = Utils.lines
        .firstWhere((line) => line.routeId == routeId, orElse: () => null);
    super.initState();
    Future.delayed(Duration(), () async {
      timerUpdate =
          Timer.periodic(Duration(seconds: Utils.updateDelay), (timer) async {
        await getTripDetails();
        setState(() {});
      });

      Size size2_1 = Size(65, 72);
      Size size1_2 = Size(72, 65);
      Size size1_1 = Size(72, 72);

      int width = 100;

      bus0bytes = await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_0.svg', size2_1, width);
      iconBus0 = BitmapDescriptor.fromBytes(bus0bytes);
      iconBus180 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_180.svg', size2_1, width));

      iconBus270 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_270.svg', size1_2, width));
      iconBus90 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_90.svg', size1_2, width));

      iconBus135 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_135.svg', size1_1, width));
      iconBus45 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_45.svg', size1_1, width));
      iconBus225 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_225.svg', size1_1, width));
      iconBus315 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_315.svg', size1_1, width));

      String strAssetStop = Utils.globalPinBusStop;

      iconStop = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetStop, Size(64, 64), 64));

      await initTrip();

      Future.delayed(Duration(seconds: 2), () {
        SnackBar snackBar = SnackBar(
          duration: Duration(seconds: 2),
          content: Wrap(
            alignment: WrapAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text('Clique no'),
                  SvgPicture.asset('assets/route/arrow_0.svg',
                      width: 30, height: 30),
                  Text('para ver mais detalhes.'),
                ],
              ),
              Text('Ou aproxime para visualizar as paradas.'),
            ],
          ),
        );
        scaffoldKey.currentState.showSnackBar(snackBar);
        MOBApiUtils.mobEvent('route selected', {
          if (line != null) 'shortName': line.shortName,
          if (line != null) 'longName': line.longName,
          'routeId': routeId,
          'tripId': tripId,
        });
      });

      Future.delayed(Duration(seconds: 1), () {
        selectedStop = stops.firstWhere((element) => element.stopId == stopId,
            orElse: () => null);
        if (selectedStop != null) {
          double latitude = selectedStop.latitude;
          double longitude = selectedStop.longitude;
          LatLng latLng = LatLng(latitude, longitude);
          controller.animateCamera(CameraUpdate.newLatLngZoom(latLng, 15));
          scrollTimeline();
          setState(() {});
        }
      });
      if (stopId == null)
        await fitBounds();
      else
        controller.moveCamera(CameraUpdate.newLatLngZoom(
            LatLng(center.latitude, center.longitude), 15.0));
      setState(() {});
    });
  }

  Future<void> initTrip() async {
    tripDetails = await getTripDetails();
    stops.addAll(tripDetails.stops);

    for (final service in tripDetails.services) {
      int weekDay = DateTime.now().weekday;
      weekDay = weekDay == 7 ? 0 : weekDay;
      SERVICE_DAYS serviceDay = SERVICE_DAYS.values[weekDay];
      if (service.days.contains(serviceDay)) {
        List<DateTime> mDepartures = service.departures.map((d) {
          List<String> split = d.split(':');
          int h = int.parse(split[0]);
          int m = int.parse(split[1]);
          return DateTime(1970, 1, 1, h, m);
        }).toList();
        departures.addAll(mDepartures);
        break;
      }
    }
    setState(() {});
    String shape = tripDetails.shape;
    List<PointLatLng> shapeDecoded = PolylinePoints().decodePolyline(shape);

    int pointAreference = 0;
    int pointBreference = 10;

    String strAssetDirection = 'assets/images/direction_map.png';
    String strAssetDirectionInverted =
        'assets/images/direction_map_inverted.png';
    if (Theme.of(context).platform == TargetPlatform.iOS)
      strAssetDirection = 'assets/images/direction_map.png';
    strAssetDirectionInverted = 'assets/images/direction_map_inverted.png';

    BitmapDescriptor iconDirection = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(1, 1)), strAssetDirection);
    BitmapDescriptor iconDirectionInverted =
        await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(size: Size(1, 1)), strAssetDirectionInverted);

    while (pointAreference < shapeDecoded.length - 1 &&
        pointBreference < shapeDecoded.length - 1) {
      bool inverted = false;
      PointLatLng first = shapeDecoded[pointAreference];
      PointLatLng seconds = shapeDecoded[pointBreference];

      Point firstLatLng = Point(first.latitude, first.longitude);
      Point secondLatLng = Point(seconds.latitude, seconds.longitude);

      double distance =
          SphericalUtils.computeDistanceBetween(firstLatLng, secondLatLng);

      if (distance >= 400) {
        MarkerId markerId =
            MarkerId('${firstLatLng.hashCode - secondLatLng.hashCode}');
        LatLng latLng = LatLng(first.latitude, first.longitude);
        double bearing =
            SphericalUtils.computeHeading(firstLatLng, secondLatLng);
        Marker marker = Marker(
          markerId: markerId,
          flat: true,
          draggable: false,
          consumeTapEvents: false,
          position: latLng,
          icon: inverted ? iconDirectionInverted : iconDirection,
          rotation: bearing,
        );
        directionsMarker.add(marker);

        if (distance >= 1000) directionsMarkerFar.add(marker);

        inverted = !inverted;
      }

      pointAreference += 10;
      pointBreference += 10;
    }

    //PointLatLng -> LatLng
    List<LatLng> latLngs = [];
    for (final shapeObject in shapeDecoded) {
      double lat = shapeObject.latitude;
      double lng = shapeObject.longitude;
      LatLng latLng = LatLng(lat, lng);
      latLngs.add(latLng);
    }

    Color lineColor = Utils.getColorFromHex(line?.color ?? '#000000');
    //Create Polyline
    polyline = Polyline(
      polylineId: PolylineId(latLngs.hashCode.toString()),
      points: latLngs,
      width: 5,
      color: lineColor,
    );

    //Calculate bounds
    double x0, x1, y0, y1;
    for (final LatLng latLng in latLngs) {
      if (x0 == null) {
        x0 = x1 = latLng.latitude;
        y0 = y1 = latLng.longitude;
      } else {
        if (latLng.latitude > x1) x1 = latLng.latitude;
        if (latLng.latitude < x0) x0 = latLng.latitude;
        if (latLng.longitude > y1) y1 = latLng.longitude;
        if (latLng.longitude < y0) y0 = latLng.longitude;
      }
    }

    LatLng ne = LatLng(x1, y1);
    LatLng sw = LatLng(x0, y0);

    bounds = LatLngBounds(northeast: ne, southwest: sw);

    //Calculate center from bounds
    double centerLatBound = (ne.latitude + sw.latitude) / 2;
    double centerLngBound = (ne.longitude + sw.longitude) / 2;

    center = LatLng(centerLatBound, centerLngBound);

    setState(() {});
  }

  Future<MOBTripDetails> getTripDetails() async {
    http.Response response = await MOBApiUtils.getTripDetailsFuture(tripId);
    if (response != null) {
      String body = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> object = convert.json.decode(body);
      MOBTripDetails tripDetails = MOBTripDetails.fromJson(object);
      //timezoneOffset = tripDetails.tzOffset;
      vehicles = tripDetails.vehicles;

      for (final newVehicle in tripDetails.vehicles)
        createVehicleMarker(newVehicle);

      if (stops.isEmpty)
        for (final stop in tripDetails.stops) {
          double lat = stop.latitude;
          double lng = stop.longitude;
          LatLng position = LatLng(lat, lng);
          Marker m = Marker(
            markerId: MarkerId(position.hashCode.toString()),
            icon: iconStop,
            position: position,
            onTap: () => stopSelected(stop, position),
          );
          stopMarkers[stop.stopId] = m;
        }

      return tripDetails;
    }
    return null;
  }

  Future<void> fitBounds() async =>
      controller.animateCamera(CameraUpdate.newLatLngBounds(bounds, 20));

  void stopSelected(MOBTripDetailsStop stop, LatLng position) async {
    selectedVehicle = null;
    controller.animateCamera(CameraUpdate.newLatLngZoom(position, 15));
    selectedStop = stop;
    setState(() {});
    scrollTimeline();
  }

  void createVehicleMarker(MOBTripDetailsVehicle v) {
    MarkerId id = MarkerId(v.hashCode.toString());
    String date = v.positionTime.toString();
    DateTime dateTime = DateTime.parse(date);
    DateFormat dateFormat = DateFormat('dd/MM/yyyy HH:mm');
    String format = dateFormat.format(dateTime);

    String vehicleId = v.vehicleId;
    String lastComunication = 'Posição atualizada em $format';

    if (selectedVehicleId == vehicleId) {
      selectedVehicleId = vehicleId;
      selectedLastCommunication = lastComunication;
    }

    double lat = v.latitude;
    double lng = v.longitude;
    LatLng position = LatLng(lat, lng);

    BitmapDescriptor iconBus;
    int heading = v.heading;
    if (heading >= 23 && heading <= 67)
      iconBus = iconBus45;
    else if (heading >= 68 && heading <= 112)
      iconBus = iconBus90;
    else if (heading >= 113 && heading <= 157)
      iconBus = iconBus135;
    else if (heading >= 158 && heading <= 202)
      iconBus = iconBus180;
    else if (heading >= 203 && heading <= 247)
      iconBus = iconBus225;
    else if (heading >= 248 && heading <= 292)
      iconBus = iconBus270;
    else if (heading >= 293 && heading <= 337)
      iconBus = iconBus315;
    else //0-22, 338-359
      iconBus = iconBus0;

    Marker m = Marker(
        markerId: id,
        icon: iconBus,
        position: position,
        onTap: () {
          controller.animateCamera(CameraUpdate.newLatLngZoom(position, 15));
          selectedMobVehicle = v;
          selectedVehicle = id;
          selectedVehicleId = v.vehicleId;
          selectedLastCommunication = lastComunication;
        });

    vehiclesMarkers[v.vehicleId] = m;
  }

  Widget lineTripHeader() => Container(
        width: MediaQuery.of(context).size.width,
        child: Card(
          color:
              line != null ? Utils.getColorFromHex(line.color) : Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
              side: BorderSide(
                  width: 1,
                  color: line != null
                      ? Utils.getColorFromHex(line?.color ?? '#000')
                      : Colors.black)),
          child: Container(
            padding: EdgeInsets.all(5),
            child: line == null
                ? InkWell(
                    onTap: () => Navigator.of(context).pop(),
                    child: Icon(
                      Icons.arrow_back,
                      color: Utils.getColorFromHex(line?.color ?? '#000')
                                  .computeLuminance() >
                              0.5
                          ? Colors.black
                          : Colors.white,
                    ),
                  )
                : Row(
                    children: [
                      InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        child: Icon(
                          Icons.arrow_back,
                          color: Utils.getColorFromHex(line?.color ?? '#000')
                                      .computeLuminance() >
                                  0.5
                              ? Colors.black
                              : Colors.white,
                        ),
                      ),
                      SvgPicture.asset(
                        Utils.getStopTypeSvg(
                            line?.type ?? 3,
                            Utils.getColorFromHex(line?.color ?? '#000')
                                    .computeLuminance() <
                                0.5),
                        height: 30,
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 10),
                        width: MediaQuery.of(context).size.width / 1.5,
                        child: Text(
                          line?.shortName ?? '',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Utils.getColorFromHex(line?.color ?? '#000')
                                        .computeLuminance() >
                                    0.5
                                ? Colors.black
                                : Colors.white,
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: share,
                        child: Icon(
                          Icons.share,
                          size: 30,
                          color: Utils.getColorFromHex(line?.color ?? '#000')
                                      .computeLuminance() >
                                  0.5
                              ? Colors.black
                              : Colors.white,
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      );

  Widget tripHeader() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 1.2,
                    child: Text(
                      tripDetails != null
                          ? (line == null
                              ? tripDetails.routeName
                              : tripDetails.routeName
                                  .split(line.shortName + ' - ')[1])
                          : '',
                      textAlign: TextAlign.start,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 1.2,
                    child: Text(
                      'Destino: ${tripDetails?.tripName ?? ''}',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      );

  void share([String vehicleId]) {
    String webHash = Utils.project.hash;
    String hashRouteId = routeId.toRadixString(32);
    String base = 'https://editor.mobilibus.com/web/bus2you';
    String url = '$base/$webHash#$hashRouteId;$tripId';
    if (vehicleId != null) url += ';$vehicleId';
    Share.share(url, subject: 'Compartilhar viagem');
  }

  Widget selectedBusWidget() => Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(5),
                  child: Image.memory(bus0bytes, height: 40),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 1.4,
                          child: Text(
                            '$selectedVehicleId', //' - seq = ${selectedMobVehicle.seq}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        InkWell(
                          onTap: () => share(selectedVehicleId),
                          child: Icon(
                            Icons.share,
                            size: 30,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.3,
                      child: Text(
                        selectedLastCommunication,
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      );

  void onMapCreated(mapController) async {
    controller = mapController;
    Completer<GoogleMapController>().complete(controller);
    rootBundle
        .loadString('assets/maps_style_default.json')
        .then((style) => controller.setMapStyle(style));
  }

  void onCameraMove(CameraPosition cameraPosition) {
    zoom = cameraPosition.zoom;
    setState(() => this.cameraPosition = cameraPosition);
  }

  Widget showDepartures() {
    DateTime mNow = DateTime.now();
    DateTime now = DateTime(1970, 1, 1, mNow.hour, mNow.minute);

    Map<int, MOBTripDetailsVehicle> mapVehicles = {};
    //remove repeated seq and stay lowest departure
    for (final vehicle in vehicles) {
      MOBTripDetailsVehicle mVehicle = mapVehicles[vehicle.seq];
      if (mVehicle == null)
        mapVehicles[vehicle.seq] = vehicle;
      else {
        DateTime mVehicleStart = mVehicle.startTime;
        DateTime vehicleStart = vehicle.startTime;
        if (vehicleStart.isBefore(mVehicleStart))
          mapVehicles[vehicle.seq] = vehicle;
      }
    }

    List<MOBTripDetailsVehicle> mVehicles = mapVehicles.values.toList();

    int lowestSeq = stops.length;
    List<int> seqs = [];
    if (mVehicles.isNotEmpty)
      for (final vehicle in mVehicles) {
        int seq = vehicle.seq;
        seqs.add(seq);
        if (seq < lowestSeq) lowestSeq = seq;
      }
    seqs.sort((a, b) => a.compareTo(b));

    DateTime offlineDeparture;

    for (final departure in departures) {
      offlineDeparture = departure;
      if (departure.isAfter(now)) break;
    }

    DateTime realtimeDeparture;

    return ListView.builder(
      controller: listDeparturesController,
      itemCount: stops.length,
      //physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        MOBTripDetailsStop stop = stops[index];
        String name = stop.name;
        int stopId = stop.stopId;
        int previousStopSeconds = stop.previousStopSeconds;

        bool isRealtime = (index + 1 >= lowestSeq && mVehicles.isNotEmpty);

        int vehicleDelay = 0;

        // offline departures
        if (!isRealtime) {
        } else {
          // realtime departures
          if (seqs.contains(index)) lowestSeq = index;

          MOBTripDetailsVehicle vehicle = mVehicles
              .firstWhere((v) => v.seq == lowestSeq, orElse: () => null);
          if (vehicle != null) {
            realtimeDeparture = vehicle.startTime;
            vehicleDelay = vehicle.delay;
          }
        }

        DateTime dateTime = isRealtime ? realtimeDeparture : offlineDeparture;

        int diffSeconds = now.difference(dateTime).inSeconds;
        if (diffSeconds < 0) diffSeconds *= -1;
        int seconds = (isRealtime ? (-diffSeconds) : diffSeconds) +
            vehicleDelay +
            previousStopSeconds;
        int minutes = seconds ~/ 60;

        String time;
        if (minutes >= 0 && minutes < 2)
          time = 'Aprox.';
        else if (minutes >= 2 && minutes < 60)
          time = '$minutes min';
        else {
          Duration d = Duration(
            seconds: previousStopSeconds + vehicleDelay,
            milliseconds: dateTime.millisecondsSinceEpoch,
          );
          dateTime = DateTime.fromMillisecondsSinceEpoch(d.inMilliseconds);
          time = DateFormat('HH:mm').format(dateTime).replaceAll(':', 'h');
        }

        return Container(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                onTap: () {
                  selectedStop = stop;
                  controller.animateCamera(
                    CameraUpdate.newCameraPosition(
                      CameraPosition(
                        target: LatLng(stop.latitude, stop.longitude),
                        zoom: 18,
                      ),
                    ),
                  );
                  scrollTimeline();
                  setState(() {});
                },
                child: Container(
                  width: MediaQuery.of(context).size.width / 1.8,
                  child: TimelineTile(
                    lineXY: 0.1,
                    alignment: TimelineAlign.manual,
                    axis: TimelineAxis.vertical,
                    beforeLineStyle:
                        LineStyle(color: Theme.of(context).primaryColor),
                    afterLineStyle:
                        LineStyle(color: Theme.of(context).primaryColor),
                    indicatorStyle: IndicatorStyle(
                      color: line?.color != null
                          ? Utils.getColorFromHex(line.color)
                          : Theme.of(context).accentColor,
                      width: stop?.stopId != selectedStop?.stopId ? 10 : 20,
                      height: stop?.stopId != selectedStop?.stopId ? 10 : 20,
                      indicator: stop?.stopId != selectedStop?.stopId
                          ? null
                          : SvgPicture.asset(
                              Utils.globalPinBusStop,
                              height: 30,
                            ),
                    ),
                    endChild: Container(
                      padding:
                          EdgeInsets.only(left: stop != selectedStop ? 10 : 5),
                      child: Text('${Utils.isMobilibus ? '[$index]' : ''}$name',
                          style: TextStyle(fontSize: 12)),
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 5),
                width: MediaQuery.of(context).size.width / 2.5 - 5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: Utils.isMobilibus
                          ? () => Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => UrbanRouteDepartures(
                                  routeId, stopId, tripId)))
                          : null,
                      child: time == null
                          ? CircularProgressIndicator()
                          : Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(time),
                                if (isRealtime && stop != stops.first)
                                  Image.asset('assets/rt.gif', width: 20),
                              ],
                            ),
                    ),
                    //if (Platform.isAndroid)
                    //  IconButton(
                    //    icon: Icon(Icons.notifications_active, size: 20),
                    //    onPressed: () => openBubble(),
                    //  ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void openBubble() {
    String shortName;
    String departureText;
    String vehicleId;
    String textColor;
    String backgroundColor;
    Color color;
    bool isBlack = (color?.computeLuminance() ?? 0.0) > 0.5 ? true : false;

    if (bubbleOverlay.isOpen) bubbleOverlay.closeBubble();
    bubbleOverlay.openBubble(
      topText: shortName,
      middleText: departureText,
      bottomText: vehicleId,
      topTextColor: textColor,
      middleTextColor: textColor,
      bottomTextColor: textColor,
      backgroundColor: backgroundColor,
      topIconAsset: 'assets/rt.gif',
      bottomIconAsset: 'assets/images/bus${isBlack ? '_black' : ''}.svg',
    );
  }

  void updateBubbleOverlay(MOBDepartures mobDepartures) async {
    if (!Platform.isAndroid) return;
    for (final departureTrip in mobDepartures.departures) {
      int mRouteId = departureTrip.routeId;
      int mTripId = departureTrip.tripId;
      if (mRouteId == routeId && mTripId == tripId) {
        MOBDeparture departure = departureTrip?.departures?.firstWhere(
            (departure) => departure?.online ?? false,
            orElse: () => departureTrip?.departures?.first ?? null);
        if (departure != null) {
          int minutes = departure.minutes;

          String departureText = '';
          if (minutes < 2 && !departure.nextDay)
            departureText = 'Agora';
          else if (minutes < 60 && !departure.nextDay)
            departureText = '$minutes min';
          else
            departureText = departure.time.substring(0, 5).replaceAll(':', 'h');

          String vehicleId = departure.vehicleId ?? '';
          String shortName = departureTrip.shortName;

          bubbleOverlay.updateTopText(shortName);
          bubbleOverlay.updateMiddleText(departureText);
          bubbleOverlay.updateBottomText(vehicleId);

          if (minutes == 2 && await Vibration.hasVibrator())
            Vibration.vibrate(duration: 1000);
        }
        break;
      }
    }
  }

  void scrollTimeline() {
    int index = stops.indexOf(selectedStop);
    double offset = (index - 1) * 50.0;
    try {
      listDeparturesController.animateTo(
        offset,
        duration: Duration(seconds: 2),
        curve: Curves.ease,
      );
    } catch (e) {
      Future.delayed(Duration(milliseconds: 300), () => scrollTimeline());
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Marker> markers = vehiclesMarkers.values.toList();
    if (zoom >= 15) markers.addAll(stopMarkers.values);
    if (zoom >= 13)
      markers.addAll(directionsMarker);
    else
      markers.addAll(directionsMarkerFar);

    if (selectedVehicle != null) if (selectedMobVehicle.latitude ==
            cameraPosition.target.latitude &&
        selectedMobVehicle.longitude == cameraPosition.target.longitude)
      controller.animateCamera(CameraUpdate.newLatLng(
          LatLng(selectedMobVehicle.latitude, selectedMobVehicle.longitude)));

    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Stack(
            children: [
              Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height / 1.5,
                    child: GoogleMap(
                      rotateGesturesEnabled: false,
                      myLocationEnabled: true,
                      trafficEnabled: false,
                      zoomGesturesEnabled: true,
                      myLocationButtonEnabled: false,
                      mapToolbarEnabled: false,
                      zoomControlsEnabled: false,
                      minMaxZoomPreference: MinMaxZoomPreference(5, 20),
                      mapType: MapType.normal,
                      markers: Set<Marker>.of(markers),
                      padding: EdgeInsets.only(
                        top: kToolbarHeight * 3,
                        bottom: 40,
                      ),
                      polylines:
                          Set<Polyline>.of([if (polyline != null) polyline]),
                      onMapCreated: onMapCreated,
                      onCameraMove: onCameraMove,
                      initialCameraPosition: CameraPosition(
                        zoom: 15,
                        target: LatLng(0.0, 0.0),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          if (line != null)
                            FloatingActionButton(
                              mini: true,
                              heroTag: 'timetableTag',
                              child: Icon(Icons.departure_board),
                              foregroundColor: Utils.globalPrimaryColor,
                              onPressed: () async =>
                                  await Utils.selectedLineBehaviour(
                                      line, false, context, true),
                            ),
                          if (Utils.isMobilibus)
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Chip(label: Text('routeId=$routeId')),
                                Chip(label: Text('tripId=$tripId')),
                              ],
                            ),
                          FloatingActionButton(
                            mini: true,
                            heroTag: 'fitBoundsTag',
                            child: Icon(Icons.zoom_out_map),
                            onPressed: fitBounds,
                            foregroundColor: Utils.globalPrimaryColor,
                          ),
                        ],
                      ),
                      if (Utils.showAds) admob,
                    ],
                  ),
                ],
              ),
              SafeArea(
                child: Column(
                  children: [
                    lineTripHeader(),
                    Container(
                      child: Card(
                        child: Container(
                          padding: EdgeInsets.all(5),
                          child: Column(
                            children: [
                              stops.isEmpty
                                  ? Center(child: CircularProgressIndicator())
                                  : tripHeader(),
                              Divider(),
                              if (selectedVehicleId.isNotEmpty &&
                                  selectedVehicle != null)
                                selectedBusWidget(),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height / 3,
            child: departures.isEmpty
                ? Center(child: CircularProgressIndicator())
                : showDepartures(),
          ),
        ],
      ),
    );
  }
}
