import 'dart:async';
import 'dart:convert' as convert;

import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/base/MOBDeparture.dart';
import 'package:mobilibus/app/urban/route/departures/widgets/DepartureItemLineWidget.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class UrbanRouteDepartures extends StatefulWidget {
  UrbanRouteDepartures(this.routeId, this.stopId, this.tripId);

  final int routeId;
  final int stopId;
  final int tripId;
  _RouteDepartures createState() => _RouteDepartures(routeId, stopId, tripId);
}

class _RouteDepartures extends State<UrbanRouteDepartures>
    with WidgetsBindingObserver {
  _RouteDepartures(this.routeId, this.stopId, this.tripId) : super();
  final int routeId;
  final int stopId;
  final int tripId;

  MOBDepartures departureResult;
  Timer timerUpdate;
  AppLifecycleState state;

  AdmobBanner admob = AdmobBanner(
    adUnitId: Utils.getBannerAdUnitId(),
    adSize: AdmobBannerSize.BANNER,
  );

  MOBDepartureTrip departureLine;
  MOBDepartureTrip departureLineNextDay;
  String stop = '';
  String headsign = '';
  String routeShortName = '';
  String routeLongName = '';

  @override
  void initState() {
    timerUpdate = Timer.periodic(
        Duration(seconds: Utils.updateDelay), (timer) => getDepartures());
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    didChangeAppLifecycleState(AppLifecycleState.resumed);
    getDepartures();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    if (timerUpdate != null) timerUpdate.cancel();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) =>
      setState(() => this.state = state);

  void getDepartures() async {
    if (state == AppLifecycleState.resumed) {
      http.Response response = await MOBApiUtils.getDepartures(stopId);
      String body = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> object = convert.json.decode(body);
      departureResult = MOBDepartures.fromJson(object, routeId, tripId);

      stop = departureResult.stop;

      for (final departure in departureResult.departuresToday)
        if (departure.routeId == routeId) {
          departureLine = departure;
          break;
        }
      for (final departure in departureResult.departuresNextDay)
        if (departure.routeId == routeId) {
          departureLineNextDay = departure;
          break;
        }
      if (departureLine != null) {
        headsign = departureLine.headsign;
        routeShortName = departureLine.shortName;
        routeLongName = departureLine.longName;
      }

      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        title: Text('Partidas', style: textStyle),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: themeColor),
          onPressed: () {
            if (timerUpdate != null) timerUpdate.cancel();
            Navigator.of(context).pop();
          },
        ),
        actions: <Widget>[
          if (Utils.isMobilibus)
            IconButton(
              icon: Icon(
                Icons.refresh,
                size: 35,
              ),
              onPressed: () => getDepartures(),
            )
        ],
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              if (Utils.isMobilibus) Text('stopId=$stopId'),
              if (departureLine == null && departureLineNextDay == null)
                CircularProgressIndicator(
                    strokeWidth: 3,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Utils.isLightTheme(context)
                            ? Utils.getColorFromPrimary(context)
                            : Colors.black)),
              Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: ExactAssetImage(Utils.globalPinBusStop),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 50,
                      child: Text(
                        stop,
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          color: Utils.isLightTheme(context)
                              ? Colors.black
                              : Utils.getColorFromPrimary(context),
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              if (departureLine != null)
                Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(5),
                        width: MediaQuery.of(context).size.width,
                        child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              side: BorderSide(
                                  width: 1,
                                  color: Utils.getColorFromHex(
                                      departureLine.color ?? '#000000'))),
                          color: Utils.getColorFromHex(
                              departureLine.color ?? '#000000'),
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              '$routeShortName $routeLongName',
                              style: TextStyle(
                                color: Utils.getColorFromHex(
                                                departureLine.color ??
                                                    '#ffffff')
                                            .computeLuminance() >
                                        0.5
                                    ? Colors.black
                                    : Colors.white,
                                backgroundColor: Utils.getColorFromHex(
                                    departureLine.color ?? '#000000'),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: Text('Destino: $headsign'),
                      ),
                    ],
                  ),
                ),
              if (departureLine != null)
                DepartureItemLineWidget(departureLine.departures),
              if (departureLineNextDay != null)
                DepartureItemLineWidget(
                  departureLineNextDay.departures,
                  nextDay: true,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
