import 'dart:async';
import 'dart:convert' as convert;
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:mobilibus/app/urban/departure/widgets/DepartureWidget.dart';
import 'package:mobilibus/app/urban/map/default_map.dart' as gmap;
import 'package:mobilibus/base/MOBDeparture.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DeparturesView extends StatefulWidget {
  DeparturesView(this.stop);
  final MOBStop stop;

  _DeparturesView createState() => _DeparturesView(stop);
}

class _DeparturesView extends State<DeparturesView> {
  bool circularIt = false;
  List<dynamic> favoritesDepartures;

  _DeparturesView(this.stop);
  final MOBStop stop;

  MOBDepartures departureResult;

  List<MOBDepartureTrip> departures = [];
  Map<int, bool> favorites = {};
  IconData iconFav = Icons.favorite_border;
  bool isFavorited = false;
  bool isForward = true;
  bool showFavorites = false;
  Timer timerUpdate;

  Map<int, bool> favoritedMap = {};
  Map<int, bool> downloadedMap = {};
  SharedPreferences prefs;
  @override
  void dispose() {
    timerUpdate?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    //CircularProgressIndicator();

    Future.delayed(
        Duration(), () async => prefs = await SharedPreferences.getInstance());
    timerUpdate = Timer.periodic(
        Duration(seconds: Utils.updateDelay), (timer) => updateDepartures());

    updateDepartures();
    super.initState();
  }

  void setupFavorites() {
    int stopId = stop.stopId;
    String paramString = 'stop_favorite_$stopId';
    bool value = prefs.getBool(paramString) ?? false;
    if (value == null)
      prefs.setBool(paramString, value);
    else {
      isFavorited = value;
      iconFav = isFavorited ? Icons.favorite : Icons.favorite_border;
    }

    int projectId = Utils.project.projectId;

    String jsonObject = prefs.getString('timetable_favorites_$projectId');

    if (jsonObject != null) {
      Iterable favoritesIterable = convert.json.decode(jsonObject);
      favoritesDepartures = favoritesIterable.toList();

      for (final line in Utils.lines) {
        int routeId = line.routeId;
        bool value = favoritesDepartures.contains(routeId);

        bool isFavorited = value ?? false;
        favoritedMap[routeId] = isFavorited;

        String timetablePath = 'timetable_last_download_$projectId\_$routeId';
        downloadedMap[routeId] =
            prefs.containsKey(timetablePath) ? true : false;
      }
    }
  }

  void updateDepartures() async {
    http.Response response = await MOBApiUtils.getDepartures(stop.stopId);
    if (response != null) {
      String body = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> object = convert.json.decode(body);
      MOBDepartures dr = MOBDepartures.fromJson(object);
      departureResult = dr;
      if (dr.departuresToday.isNotEmpty || dr.departuresNextDay.isNotEmpty) {
        departures.clear();
        departures.addAll(departureResult.departuresToday);
        departures.addAll(departureResult.departuresNextDay);
        setState(() {});
      } else {
        circularIt = true;
        setState(() {});
      }

      DateFormat df = DateFormat('HH:mm:ss');
      DateTime now = DateTime.now();
      DateTime nowF = df.parse(df.format(now));

      List data = [];
      for (final departure in departures) {
        String shortName = departure.shortName;
        String longName = departure.longName;
        Map<String, dynamic> mData = {
          'shortName': shortName,
          'longName': longName,
          'stopId': stop.stopId,
        };
        List times = [];
        for (final mDeparture in departure.departures) {
          String time = mDeparture.time;
          DateTime dtDep = df.parse(time);
          Duration d = dtDep.difference(nowF);
          int seconds = d.inSeconds;
          if (seconds < 0) {
            Map<String, dynamic> mTime = {'seconds': seconds};
            times.add(mTime);
          }
        }
        if (times.isNotEmpty) {
          mData['times'] = times;
          data.add(mData);
        }
      }

      String date = now.toIso8601String();
      String key = 'negative_departures_seconds_$date';
      SharedPreferences sp = await SharedPreferences.getInstance();
      String json = convert.json.encode(data);
      print('$key');
      await sp.setString(key, json);
    }
  }

  void favoriteStop(MOBStop stop) async {
    int stopId = stop.stopId;
    int projectId = Utils.project.projectId;

    isFavorited = !isFavorited;
    iconFav = isFavorited
        ? iconFav = Icons.favorite
        : iconFav = Icons.favorite_border;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('stop_favorite_$stopId', isFavorited);

    String favStopsParam = 'stops_favorite_$projectId';

    String jsonString = prefs.getString(favStopsParam) ?? '[]';
    Iterable iterable = convert.json.decode(jsonString) ?? [];
    List<MOBStop> favoritedStops =
        iterable.map((e) => MOBStop.fromJson(e)).toList();

    if (isFavorited)
      favoritedStops.add(stop);
    else
      favoritedStops.removeWhere((element) => element.stopId == stopId);
    jsonString =
        convert.json.encode(favoritedStops.map((e) => e.toMap()).toList());
    prefs.setString(favStopsParam, jsonString);

    setState(() {});
    Navigator.of(context).pop();
  }

  void showInfo() {
    Color themeColor = Theme.of(context).primaryColor;
    Color textColor = Utils.getColorByLuminanceThemeReverse(context);

    TextStyle textStyle = TextStyle(color: textColor);

    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Text('Legenda', style: textStyle),
              content: Container(
                child: Scrollbar(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.map,
                              size: 40,
                              color: Utils.getColorByLuminanceThemeReverse(
                                  context),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 3,
                              child: Text(
                                  'Clique no ícone do mapa e acompanhe a localização dos veículos desta linha!',
                                  style: textStyle),
                            ),
                          ],
                        ),
                        if (Platform.isAndroid)
                          Container(
                            padding: EdgeInsets.all(20),
                            child: Divider(
                              thickness: 1,
                              height: 3,
                            ),
                          ),
                        if (Platform.isAndroid)
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image(
                                image: AssetImage("assets/rt.gif"),
                                height: 40,
                                width: 40,
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
                                child: Text(
                                    'Clique no icone de tempo real para acompanhar a previsão fora do app!',
                                    style: textStyle),
                              ),
                            ],
                          ),
                      ],
                    ),
                  ),
                ),
              ),
              backgroundColor: themeColor,
              actions: [
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text('Fechar', style: textStyle)),
              ],
            ));
  }

  List<Widget> popUpActions(Color color) => [
        FlatButton(
          onPressed: () => favoriteStop(stop),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Icon(
                  isFavorited ? Icons.favorite : Icons.favorite_border,
                  size: 20,
                  color: color,
                ),
              ),
              Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Text('FAVORITAR PONTO',
                      style: TextStyle(color: color),
                      textAlign: TextAlign.center)),
            ],
          ),
        ),
        if (Utils.project.otpUri != null)
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
              LatLng latLng = LatLng(stop.latitude, stop.longitude);
              gmap.updateTripFrom(latLng, stop.name, context);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Icon(
                    Icons.my_location,
                    size: 20,
                    color: color,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10),
                  child: Text('COMO ORIGEM',
                      style: TextStyle(color: color),
                      textAlign: TextAlign.center),
                ),
              ],
            ),
          ),
        if (Utils.project.otpUri != null)
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
              LatLng latLng = LatLng(stop.latitude, stop.longitude);
              gmap.updateTripTo(latLng, stop.name, context);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Icon(
                    Icons.directions_run,
                    size: 20,
                    color: color,
                  ),
                ),
                Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Text('COMO DESTINO',
                        style: TextStyle(color: color),
                        textAlign: TextAlign.center)),
              ],
            ),
          ),
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
            setState(() => showFavorites = !showFavorites);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 20,
                height: 20,
                child: Transform.scale(
                  scale: 1.0,
                  child: Checkbox(
                    activeColor: color,
                    onChanged: (checked) {
                      Navigator.of(context).pop();
                      showFavorites = !showFavorites;
                      setState(() {});
                    },
                    value: showFavorites,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 10),
                child: Text(
                  'SÓ FAVORITAS',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: color),
                ),
              ),
            ],
          ),
        ),
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
            showInfo();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(Icons.info, size: 20, color: color),
              Container(
                padding: EdgeInsets.only(left: 10),
                child: Text('LEGENDA',
                    textAlign: TextAlign.start, style: TextStyle(color: color)),
              ),
            ],
          ),
        ),
      ];

  @override
  Widget build(BuildContext context) {
    String strAsset = '';

    String strAssetBus = Utils.globalPinBusStop;
    String strAssetFerry = 'assets/images/pin_ferry_stop_gmaps.svg';
    String strAssetSubway = 'assets/images/pin_subway_stop_gmaps.svg';
    String strAssetTrain = 'assets/images/pin_train_stop_gmaps.svg';
    String strAssetTram = 'assets/images/pin_tram_stop_gmaps.svg';

    int stopType = stop.stopType;
    switch (stopType) {
      case 0:
        strAsset = strAssetTram;
        break;
      case 1:
        strAsset = strAssetSubway;
        break;
      case 2:
        strAsset = strAssetTrain;
        break;
      case 3:
        strAsset = strAssetBus;
        break;
      case 4:
        strAsset = strAssetFerry;
        break;
      case 5:
        break;
      case 6:
        break;
      case 7:
        break;
    }
    Color color = Theme.of(context).primaryColor.computeLuminance() > 0.5
        ? Colors.black
        : Theme.of(context).primaryColor;

    int stopId = stop.stopId, countFavDepartures;
    String code = stop.code;

    List<MOBDepartureTrip> departureTrips = [];
    if (departureResult != null) {
      if (prefs != null) setupFavorites();

      if (showFavorites) {
        for (int countDepartures = 0;
            countDepartures < departureResult.departures.length;
            countDepartures++) {
          try {
            countFavDepartures = favoritesDepartures.length;
          } catch (e) {
            countFavDepartures = 0;
          }

          for (int countFavorites = 0;
              countFavorites < countFavDepartures;
              countFavorites++) {
            if (departureResult.departures[countDepartures].routeId ==
                favoritesDepartures[countFavorites]) {
              //departureTrips[countDepartures] = departureResult.departures[countDepartures];
              departureTrips.add(departureResult.departures[countDepartures]);
            }
          }
        }
      } else {
        departureTrips = departureResult.departures;
      }

      //departureTrips = departureResult.departures;
      Map<int, MOBDepartureTrip> mGroupedTrips = {};
      for (final departureTrip in departureTrips) {
        int tripId = departureTrip.tripId;

        try {
          List<MOBDeparture> departures = departureTrip.departures;
          mGroupedTrips[tripId].departures.addAll(departures);
        } catch (e) {
          mGroupedTrips[tripId] = departureTrip;
        }
      }
    }

    return Scaffold(
      appBar: departureResult == null
          ? null
          : AppBar(
              backgroundColor: Utils.getColorFromPrimary(context),
              leading: IconButton(
                icon: Icon(
                  Icons.close,
                  color: Utils.getColorByLuminanceTheme(context),
                ),
                onPressed: () {
                  if (timerUpdate != null) timerUpdate.cancel();
                  Navigator.of(context).pop();
                },
              ),
              title: Text('Ponto de Parada',
                  style: TextStyle(
                    color: Utils.getColorByLuminanceTheme(context),
                  )),
              centerTitle: true,
              actions: <Widget>[
                /*
                if (Utils.isMobilibus)
                  IconButton(
                    icon: Icon(
                      Icons.refresh,
                      size: 35,
                    ),
                    onPressed: () => updateDepartures(),
                  ),
                */
                if (!Utils.isCharter)
                  PopupMenuButton(
                    itemBuilder: (_) => popUpActions(color)
                        .map((widget) => PopupMenuItem<dynamic>(child: widget))
                        .toList(),
                    child: Icon(
                      Icons.more_horiz,
                      size: 50,
                    ),
                  ),
              ],
            ),
      backgroundColor: Utils.isCharter ? Color(0xff25303E) : null,
      body: departureResult == null
          ? Center(child: CircularProgressIndicator())
          : departures.isEmpty
              ? Scrollbar(
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            SvgPicture.asset(
                              strAsset,
                              height: 30,
                              width: 30,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width - 80,
                              child: Text(
                                (code != null ? code + ' - ' : '') +
                                    departureResult.stop,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  color: Theme.of(context).brightness ==
                                          Brightness.dark
                                      ? Colors.white
                                      : Utils.isLightTheme(context)
                                          ? Colors.black
                                          : Utils.getColorFromPrimary(context),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          SvgPicture.asset(
                            'assets/images/exclamation-circle.svg',
                            height: 64,
                            width: 64,
                          ),
                          Center(
                            child: Container(
                                padding: EdgeInsets.all(20),
                                child: Text(
                                  'Desculpe! No momento não há previsões a serem informadas para este ponto de parada.',
                                  textAlign: TextAlign.center,
                                )),
                          )
                        ],
                      ),
                    ],
                  ),
                )

              /*Center(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    child: Text('Nenhuma partida disponível.\n'
                        'Tente novamente mais tarde'),
                  ),
                )
      */
              : Scrollbar(
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      /*
                      if (Utils.isMobilibus)
                        Container(
                          padding: EdgeInsets.all(5),
                          child: Card(
                            color: Colors.grey,
                            child: Container(
                              padding: EdgeInsets.all(5),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    children: [
                                      Text('stopType=$stopType'),
                                      Text('stopId=$stopId'),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      Text(
                                          'departures=${departureResult.departuresToday.length}'),
                                      Text(
                                          'departuresNextDay=${departureResult.departuresNextDay.length}'),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      */
                      Container(
                        padding: EdgeInsets.all(20),
                        child:
                            /*Utils.isCharter
                            ? Container(
                                padding: EdgeInsets.only(bottom: 35),
                                child: Text(
                                  'Clique na previsão para ver a rota',
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Theme.of(context).canvasColor,
                                  ),
                                ),
                              )
                            :
                            */
                            Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            SvgPicture.asset(
                              strAsset,
                              height: 30,
                              width: 30,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width - 80,
                              child: Text(
                                (code != null ? code + ' - ' : '') +
                                    departureResult.stop,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  color: Theme.of(context).brightness ==
                                          Brightness.dark
                                      ? Colors.white
                                      : Utils.isLightTheme(context)
                                          ? Colors.black
                                          : Utils.getColorFromPrimary(context),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      if (departureTrips.isEmpty && !showFavorites)
                        Center(
                          child: Container(
                              padding: EdgeInsets.all(20),
                              child: Text(
                                  'Não há partidas no monento, tente novamente mais tarde. :(')),
                        )
                      else if (departureTrips.isEmpty && showFavorites)
                        Center(
                          child: Container(
                              padding: EdgeInsets.all(20),
                              child: Text(
                                  'Não há partidas para as linhas favoritas no momento.')),
                        )
                      else if (departureTrips.isNotEmpty)
                        DepartureWidget(
                          context,
                          departureTrips,
                          stop,
                          favoritedMap,
                          downloadedMap,
                          () => setState(() {}),
                        )
                    ],
                  ),
                ),
    );
  }
}
