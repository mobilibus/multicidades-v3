import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavoritesTimetable extends StatelessWidget {
  FavoritesTimetable(this.linesFavorited, this.scaffoldKey, this.context);
  final List<MOBLine> linesFavorited;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final BuildContext context;

  void delete(MOBLine line) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int projectId = Utils.project.projectId;

    String jsonObject = prefs.getString('timetable_favorites_$projectId');

    Iterable favoritesIterable;
    List<dynamic> favorites = [];

    if (jsonObject != null) {
      favoritesIterable = convert.json.decode(jsonObject);
      favorites = favoritesIterable.toList();
    }
    favorites.remove(line.routeId);
    linesFavorited.remove(line);

    String encoded = convert.json.encode(favorites);
    prefs.setString('timetable_favorites_$projectId', encoded);

    scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text('Linha removida dos favoritos')));
    (context as Element)?.markNeedsBuild();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: linesFavorited.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        MOBLine line = linesFavorited[index];
        String textColorStr = line.textColor;
        String backgroundColorStr = line.color;

        Color textColor = Utils.getColorFromHex(textColorStr ?? '#ffffff');
        Color backgroundColor =
            Utils.getColorFromHex(backgroundColorStr ?? '#000000');

        bool darkColor = textColor.computeLuminance() > 0.5;
        String imageAsset = Utils.getStopTypeSvg(line.type, darkColor);

        Text textShortName = Text(line.shortName,
            style: TextStyle(
                backgroundColor: backgroundColor,
                fontWeight: FontWeight.bold,
                color: textColor));
        Text textLongName = Text(line.longName);
        return Container(
          padding: EdgeInsets.all(10),
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              side: BorderSide(width: 2, color: backgroundColor),
            ),
            child: Container(
              padding: EdgeInsets.all(5),
              child: InkWell(
                onTap: () async =>
                    await Utils.selectedLineBehaviour(line, false, context),
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Card(
                            color: backgroundColor,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            child: Container(
                              padding: EdgeInsets.all(5),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(5),
                                        child: SvgPicture.asset(
                                          imageAsset,
                                          height: 20,
                                        ),
                                      ),
                                      textShortName,
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width:
                                ((MediaQuery.of(context).size.width / 4) * 3) -
                                    20,
                            child: textLongName,
                          ),
                        ],
                      ),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () => delete(line),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
