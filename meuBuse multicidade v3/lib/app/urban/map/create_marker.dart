import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mobilibus/app/urban/map/default_map.dart';
import 'package:mobilibus/base/MOBMarker.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateMarker extends StatefulWidget {
  CreateMarker(this.name, this.latitude, this.longitude);
  final String name;
  final double latitude;
  final double longitude;
  _CreateMarker createState() => _CreateMarker(name, latitude, longitude);
}

class _CreateMarker extends State<CreateMarker> {
  _CreateMarker(this.name, this.latitude, this.longitude);
  final String name;
  final double latitude;
  final double longitude;
  BitmapDescriptor iconCustomMarker;

  int selectedRadio = 0;
  TextEditingController tecName = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    tecName.text = name;
    super.initState();
  }

  void save() async {
    if (formKey.currentState.validate())
      try {
        MARKER_TYPE markerType = MARKER_TYPE.values[selectedRadio];
        MOBMarker mobMarker = MOBMarker(
          tecName.text,
          latitude,
          longitude,
          markerType,
        );

        SharedPreferences sharedPreferences =
        await SharedPreferences.getInstance();
        String jsonStringCustomStops =
            sharedPreferences.getString('custom_stops') ?? '[]';
        Iterable iterable = convert.json.decode(jsonStringCustomStops);
        List list = iterable.toList();
        var map = mobMarker.asMap();
        list.add(map);
        jsonStringCustomStops = convert.json.encode(list);
        sharedPreferences.setString('custom_stops', jsonStringCustomStops);

        String pathCustomIcon = null;

        switch (markerType.index) {
          case 0:
            pathCustomIcon = 'assets/images/v3_pinHome.png';
            break;
          case 1:
            pathCustomIcon = 'assets/images/v3_pinWork.png';
            break;
          case 2:
            pathCustomIcon = 'assets/images/v3_pinSchool.png';
            break;
          case 3:
            pathCustomIcon = 'assets/images/v3_pinTown.png';
            break;
        }

        myMarker = Marker(
            markerId: MarkerId(tecName.text),
            position: LatLng(latitude, longitude),
            icon: await BitmapDescriptor.fromAssetImage(
                ImageConfiguration(), pathCustomIcon));

        Navigator.of(context).pop();
      } catch (e) {
        print(e);
      }
  }

  @override
  Widget build(BuildContext context) {
    List<String> icons = ['pinHome', 'pinWork', 'pinSchool', 'pinTown'];
    List<String> types = ['Casa', 'Trabalho', 'Escola', 'Local'];
    List<int> index = [0, 1, 2, 3];

    return Scaffold(
      appBar: AppBar(title: Text('Ponto personalizado')),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: save,
        label: Text('Salvar'),
        icon: Icon(Icons.save),
      ),
      body: Form(
        key: formKey,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 60, right: 60, top: 10),
              child: Text(
                'Seu ponto personalizado poderá ser acessado no favoritos, na aba Pontos',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 12,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(20),
              child: TextFormField(
                minLines: 1,
                maxLines: 3,
                autovalidateMode: AutovalidateMode.always,
                controller: tecName,
                validator: (value) =>
                value.isEmpty ? 'Nome não pode ser vazio' : null,
              ),
            ),
            Container(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Text(
                    'Icone para representar no mapa',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12),
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: index
                          .map(
                            (i) => Container(
                          width: MediaQuery.of(context).size.width / 5,
                          child: Column(
                            children: [
                              Radio(
                                  fillColor: MaterialStateColor.resolveWith((states) => Utils.getColorFromPrimary(context)),
                                  value: i,
                                  groupValue: selectedRadio,
                                  onChanged: (value) => setState(
                                          () => selectedRadio = value)),
                              SvgPicture.asset(
                                'assets/images/v3_${icons[i]}.svg',
                                height: 30,
                              ),
                              Text(types[i], textAlign: TextAlign.center),
                            ],
                          ),
                        ),
                      )
                          .toList())
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}