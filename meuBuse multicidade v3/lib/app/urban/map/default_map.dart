import 'dart:async';
import 'dart:convert' as convert;
import 'dart:math';
import 'dart:ui';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_utils/google_maps_utils.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:mobilibus/app/urban/departure/departures.dart';
import 'package:mobilibus/app/urban/map/create_marker.dart';
import 'package:mobilibus/app/urban/tripplaner/trip_planner.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/base/MOBMarker.dart';
import 'package:mobilibus/base/MOBProject.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/base/otp/OTPGeocode.dart';
import 'package:mobilibus/utils/Debouncer.dart';
import 'package:mobilibus/utils/DialogUtils.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

bool isTripMode = false;
bool isLoading = false;
OTPGeocodeResult geocodeResultFrom;
OTPGeocodeResult geocodeResultTo;
TextEditingController tecFrom;
TextEditingController tecTo;

GoogleMapController gMapsController;
Set<Marker> markersSet = {};
Marker myMarker;

class DefaultMap extends StatefulWidget {
  _DefaultMap createState() => _DefaultMap();
}

class _DefaultMap extends State<DefaultMap>
    with SingleTickerProviderStateMixin {
  TextEditingController tecHybrid = TextEditingController(text: '');
  SharedPreferences sharedPreferences;
  Map<LatLng, Marker> markersMap = {};
  Map<Marker, int> markersProjectIdMap = {};
  List<int> blackList = [];

  //BitmapDescriptor iconCustomMarker;

  GoogleMapController controller;
  double zoom = 15.0;

  BitmapDescriptor iconBus;
  BitmapDescriptor iconFerry;
  BitmapDescriptor iconSubway;
  BitmapDescriptor iconTrain;
  BitmapDescriptor iconTram;

  bool showHeader = true;
  bool isFocusInit = true;
  bool isFocusFrom = false;
  bool isFocusTo = false;
  bool isSameProject = false;

  Debouncer debouncer = Debouncer(milliseconds: 3000);

  AdmobBanner admob = AdmobBanner(
    adUnitId: Utils.getBannerAdUnitId(),
    adSize: AdmobBannerSize.BANNER,
  );

  TextFieldConfiguration tfcHybrid;
  TypeAheadField tafHybrid;

  TextFieldConfiguration tfcFrom;
  TypeAheadField tafFrom;

  TextFieldConfiguration tfcTo;
  TypeAheadField tafTo;

  CameraPosition mCameraPosition;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  String strMarker;
  List<dynamic> favorites = [];
  List<MOBMarker> customMarkers = [];

  @override
  void dispose() {
    markersMap.clear();
    tecHybrid?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    tecFrom = TextEditingController(text: '');
    tecTo = TextEditingController(text: '');

    super.initState();

    Future.delayed(Duration(), () async {
      sharedPreferences = await SharedPreferences.getInstance();
      initCustomMarkers();
      buildTextFieldConfiguration();
      buildTypeAheadField();

      String strAssetBus = Utils.globalPinBusStop;
      String strAssetFerry = 'assets/images/pin_ferry_stop_gmaps.svg';
      String strAssetSubway = 'assets/images/pin_subway_stop_gmaps.svg';
      String strAssetTrain = 'assets/images/pin_train_stop_gmaps.svg';
      String strAssetTram = 'assets/images/pin_tram_stop_gmaps.svg';

      iconBus = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetBus, Size(64, 64), 64));
      iconFerry = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetFerry, Size(64, 64), 64));
      iconSubway = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetSubway, Size(64, 64), 64));
      iconTrain = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetTrain, Size(64, 64), 64));
      iconTram = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetTram, Size(64, 64), 64));
      setState(() {});
    });
  }

  void initCustomMarkers() async {
    String jsonString = sharedPreferences.getString('custom_stops') ?? '[]';
    Iterable iterable = convert.json.decode(jsonString) ?? [];
    customMarkers = iterable.map((e) => MOBMarker.fromJson(e)).toList();

    for (int index = 0; index < iterable.length; index++) {
      String pathCustomIcon = null;

      switch (customMarkers[index].markerType.index) {
        case 0:
          pathCustomIcon = 'assets/images/v3_pinHome.png';
          break;
        case 1:
          pathCustomIcon = 'assets/images/v3_pinWork.png';
          break;
        case 2:
          pathCustomIcon = 'assets/images/v3_pinSchool.png';
          break;
        case 3:
          pathCustomIcon = 'assets/images/v3_pinTown.png';
          break;
      }

      myMarker = Marker(
          markerId: MarkerId(customMarkers[index].name),
          position: LatLng(
              customMarkers[index].latitude, customMarkers[index].longitude),
          icon: await BitmapDescriptor.fromAssetImage(
              ImageConfiguration(), pathCustomIcon));

      setState(() {
        //setCustomIconMarker();
        markersSet.add(myMarker);
      });
    }
  }

  void onMapCreated(mapController) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isDark = prefs.getBool('isDark') ?? false;
    controller = mapController;
    if (Utils.project != null)
      await controller.moveCamera(CameraUpdate.newLatLngZoom(
          LatLng(Utils.project.latitude, Utils.project.longitude), 15.0));
    Completer<GoogleMapController>().complete(controller);
    await rootBundle
        .loadString(isDark
            ? 'assets/maps_style_dark.json'
            : 'assets/maps_style_default.json')
        .then((style) => controller.setMapStyle(style));

    mapMoveEnded();
    goToMyLocation();
  }

  void onCameraMove(CameraPosition cameraPosition) {
    zoom = cameraPosition.zoom;
    mCameraPosition = cameraPosition;
    isLoading = isTripMode;

    setState(() {});
  }

  void onCameraIdle() {
    if (isTripMode) {
      updateFromOrTo();
    } else if (zoom >= 14.0) {
      mapMoveEnded();
    }

    setState(() {});
  }

  void goToMyLocation() async {
    try {
      LocationData currentLocation = Utils.locationData;
      if (currentLocation != null && await Location().serviceEnabled())
        controller.animateCamera(CameraUpdate.newLatLngZoom(
            LatLng(currentLocation.latitude, currentLocation.longitude), 18));
      else
        throw Exception('GPS disabled');
    } on Exception {
      if (Utils.project != null)
        controller.animateCamera(CameraUpdate.newLatLngZoom(
            LatLng(Utils.project.latitude, Utils.project.longitude), 18));
    }
  }

  void updateFromOrTo() {
    try {
      debouncer.run(() async {
        LatLng target = LatLng(
            mCameraPosition.target.latitude, mCameraPosition.target.longitude);
        double lat = target.latitude;
        double lng = target.longitude;
        String description =
            await MOBApiUtils.getReverseGeocodingDescrition(lat, lng);
        List<OTPGeocodeResult> results = await MOBApiUtils.suggestionCallback(
            description, Utils.project.otpUri);
        OTPGeocodeResult geocodeResult = results.first;

        if (isFocusFrom) {
          if (!isFocusInit) {
            geocodeResultFrom = geocodeResult;
            tecFrom.text = geocodeResult.description;
          } else {
            isFocusInit = false;
          }
        } else if (isFocusTo) {
          geocodeResultTo = geocodeResult;
          tecTo.text = geocodeResult.description;
        }

        isLoading = false;
        setState(() {});
      });
    } catch (e) {}
  }

  LatLngBounds easterEggBounds = LatLngBounds(
    northeast: LatLng(-29.16806,
        -51.17944), //LatLng(-26.90653532890502, -49.078466445207596),
    southwest: LatLng(-29.16806,
        -51.17944), //LatLng(-26.907728855217265, -49.079520888626575),
  );

  void mapMoveEnded() async {
    var visibleBounds = await controller.getVisibleRegion();
    GMULatLngBounds mBounds = GMULatLngBounds(
        Point(visibleBounds.northeast.latitude,
            visibleBounds.northeast.longitude),
        Point(visibleBounds.southwest.latitude,
            visibleBounds.southwest.longitude));
    if (mCameraPosition != null && Utils.isMobilibus) {
      Utils.lastLatitude =
          double.parse(mCameraPosition.target.latitude.toStringAsFixed(6));
      Utils.lastLongitude =
          double.parse(mCameraPosition.target.longitude.toStringAsFixed(6));
    }

    String url = MOBApiUtils.getStopsInViewUrl(mBounds);

    http.Response response = await MOBApiUtils.getStopsFuture(url);
    String body = convert.utf8.decode(response.bodyBytes);
    Iterable list = convert.json.decode(body);
    List<MOBStop> stops = list.map((model) => MOBStop.fromJson(model)).toList();

    stops.removeWhere((stop) => stop == null);
    stops.removeWhere((stop) => stop.stopType == 8);
    stops.removeWhere((stop) =>
        markersMap.containsKey(LatLng(stop.latitude, stop.longitude)));

    for (final stop in stops) {
      LatLng latLng = LatLng(stop.latitude, stop.longitude);

      if (stop.projectId == Utils.globalProjectID) {
        //FILTRA OS PONTOS PARA O PROJETO ESPECÍFICO
        Marker m = markerBuild(stop, latLng);
        markersMap[latLng] = m;
      }
      //markersProjectIdMap[m] = stop.projectId;
    }

    //if (Utils.isMultipleCities && stops.isNotEmpty)
    //debouncer.run(() => checkProjecStops(stops));

    setState(() {});
  }

  void checkProjecStops(List<MOBStop> stops) async {
    List<int> projectIds = [];
    for (final stop in stops) {
      int projectId = stop.projectId;
      if (!projectIds.contains(projectId)) projectIds.add(projectId);
    }
    if (Utils.project != null &&
        projectIds.length == 1 &&
        projectIds.first == Utils.project.projectId) return;

    bool contains = false;
    for (final projectId in projectIds)
      if (blackList.contains(projectId)) contains = true;

    if (!contains) {
      List<MOBProject> projects = [];
      for (final projectId in projectIds)
        projects.add(await Utils.selectedProject(projectId));

      Utils.project == null
          ? Utils.project = await Utils.selectedProject(projectIds.first)
          : null; //DialogUtils(context).showRequestProjectChange(projects,
      //  (projectId) async {
      //  Utils.project = await Utils.selectedProject(projectId);
      //  blackList = projectIds;
      //  setState(() {});
      //});
    }
  }

  void onLongPress(LatLng latLng) async {
    bool hasOtp = Utils.project.otpUri != null;
    bool isMarker = markersMap.containsKey(latLng);

    if (hasOtp && !isMarker) {
      scaffoldKey.currentState.showSnackBar(SnackBar(
        duration: Duration(seconds: 1),
        content: Text('Buscando...'),
      ));

      double lat = latLng.latitude;
      double lng = latLng.longitude;
      String address =
          await MOBApiUtils.getReverseGeocodingDescrition(lat, lng);
      if (address == null) {
        SnackBar snackbar = SnackBar(
            content: Text('Endereço desconhecido. Tente outro local.'));
        scaffoldKey.currentState.showSnackBar(snackbar);
        return;
      }
      showModalBottomSheet(
        context: context,
        builder: (context) {
          TextStyle style = TextStyle(
              color: Colors.black /*Utils.getColorByLuminanceTheme(context)*/);
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  padding: EdgeInsets.all(5),
                  child: Card(
                    elevation: 5,
                    color: Theme.of(context).primaryColor,
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child:
                          Text(address, style: TextStyle(color: Colors.white)),
                    ),
                  ),
                ),
                FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Definir como Origem', style: style),
                      SvgPicture.asset('assets/images/pin_from_gmaps.svg',
                          height: 30),
                    ],
                  ),
                  onPressed: () async {
                    Navigator.of(context).pop();
                    await updateTripFrom(latLng, address, context);
                    setState(() {});
                  },
                ),
                FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Definir como Destino', style: style),
                      SvgPicture.asset('assets/images/pin_end_gmaps.svg',
                          height: 30),
                    ],
                  ),
                  onPressed: () async {
                    Navigator.of(context).pop();
                    await updateTripTo(latLng, address, context);
                    setState(() {});
                  },
                ),
                FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Salvar', style: style),
                      Icon(Icons.save, size: 30),
                    ],
                  ),
                  onPressed: () async {
                    await Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => CreateMarker(address, lat, lng)));
                    Navigator.of(context).pop();

                    setState(() {
                      //setCustomIconMarker();
                      markersSet.add(myMarker);
                    });
                  },
                ),
                FlatButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Cancelar', style: style),
                      Icon(Icons.cancel, size: 30),
                    ],
                  ),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          );
        },
      );
    }
  }

  Marker markerBuild(MOBStop stop, LatLng latLng) {
    int stopType = stop.stopType;

    BitmapDescriptor icon;
    switch (stopType) {
      case 0:
        icon = iconTram;
        break;
      case 1:
        icon = iconSubway;
        break;
      case 2:
        icon = iconTrain;
        break;
      case 3:
        icon = iconBus;
        break;
      case 4:
        icon = iconFerry;
        break;
      case 5:
        break;
      case 6:
        break;
      case 7:
        break;
    }
    String idStopType = Utils.getIDByStopType(stopType);
    return Marker(
      markerId: MarkerId(stop.hashCode.toString() + idStopType),
      position: latLng,
      icon: icon,
      onTap: () async {
        if (stopType != 8) showDepartures(stop);
        await controller.moveCamera(CameraUpdate.newLatLngZoom(latLng, zoom));
      },
    );
  }

  void showDepartures(MOBStop stop) async {
    DeparturesView dpView = DeparturesView(stop);
    await showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      enableDrag: false,
      builder: (context) => Container(
        height: (MediaQuery.of(context).size.height / 8) * 7,
        child: dpView,
      ),
    ).then((_) => setState(() {}));
  }

  Widget suggestionTile(List<String> contents) => ListTile(
      title: Text(contents[0]),
      subtitle: contents.length > 3
          ? Text(contents.getRange(1, 3).toString())
          : Text(contents.toString()));

  void showFullHistory() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int projectId = Utils.project?.projectId ?? 0;
    String jsonString = prefs.getString('search_history_$projectId') ?? '[]';
    List<dynamic> history =
        (convert.json.decode(jsonString) as Iterable).toList();
    List<OTPGeocodeResult> geocodeResults =
        history.map((h) => OTPGeocodeResult.fromJson(h['data'])).toList();

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      enableDrag: false,
      builder: (context) => Container(
        height: (MediaQuery.of(context).size.height / 8) * 7,
        child: Column(
          children: <Widget>[
            AppBar(
              title: Text('Histórico'),
            ),
            Scrollbar(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: geocodeResults.length,
                itemBuilder: (context, index) {
                  OTPGeocodeResult geocodeResult = geocodeResults[index];
                  String desc = geocodeResult.description;
                  String provider = geocodeResult.provider;
                  return ListTile(
                    title: Text(desc),
                    subtitle: Utils.isMobilibus ? Text(provider) : null,
                    onTap: () async {
                      tecTo.text = geocodeResult.description;
                      geocodeResultTo = geocodeResult;
                      strMarker = 'assets/images/pin_from_gmaps.svg';
                      await controller.animateCamera(CameraUpdate.newLatLng(
                          LatLng(geocodeResult.latitude,
                              geocodeResult.longitude)));

                      Future.delayed(Duration(seconds: 1), () {
                        Navigator.of(context).pop();
                        tecHybrid.clear();
                        isTripMode = true;
                        isLoading = false;
                        isFocusTo = false;
                        isFocusFrom = true;
                        setState(() {});
                      });
                    },
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void buildTextFieldConfiguration() async {
    tfcHybrid = TextFieldConfiguration(
      controller: tecHybrid,
      autofocus: false,
      onChanged: (_) => setState(() => isLoading = true),
      style: DefaultTextStyle.of(context).style.copyWith(fontSize: 18),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0),
        hintText: 'Procure por linha, ponto ou local',
        fillColor: Theme.of(context).brightness == Brightness.dark
            ? Colors.black
            : Colors.white,
        filled: true,
        enabled: true,
        suffixIcon: Utils.isMobilibus
            ? IconButton(
                icon: Icon(Icons.history),
                onPressed: showFullHistory,
              )
            : null,
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
      ),
    );
    tfcFrom = TextFieldConfiguration(
      controller: tecFrom,
      autofocus: false,
      onChanged: (_) => setState(() => isLoading = true),
      style: DefaultTextStyle.of(context).style.copyWith(fontSize: 18),
      decoration: InputDecoration(
        prefixIcon: IconButton(
          icon: SvgPicture.asset(
            'assets/images/pin_from_gmaps.svg',
            width: 24,
          ),
          onPressed: null,
        ),
        contentPadding: EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0),
        hintText: 'Selecione o local de origem',
        fillColor: Theme.of(context).brightness == Brightness.dark
            ? Colors.black
            : Colors.white,
        filled: true,
        enabled: true,
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
      ),
    );
    tfcTo = TextFieldConfiguration(
      controller: tecTo,
      autofocus: false,
      onChanged: (_) => setState(() => isLoading = true),
      style: DefaultTextStyle.of(context).style.copyWith(fontSize: 18),
      decoration: InputDecoration(
        prefixIcon: IconButton(
          icon: SvgPicture.asset(
            'assets/images/pin_end_gmaps.svg',
            width: 24,
          ),
          onPressed: null,
        ),
        contentPadding: EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0),
        hintText: 'Selecione o local de destino',
        fillColor: Theme.of(context).brightness == Brightness.dark
            ? Colors.black
            : Colors.white,
        filled: true,
        enabled: true,
        border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
      ),
    );
  }

  void buildTypeAheadField() async {
    tafHybrid = TypeAheadField(
      debounceDuration: Duration(seconds: 2),
      noItemsFoundBuilder: (context) => null,
      errorBuilder: (context, error) => null,
      textFieldConfiguration: tfcHybrid,
      onSuggestionSelected: (value) async {
        int projectId = Utils.project?.projectId ?? 0;
        SharedPreferences prefs = await SharedPreferences.getInstance();

        if (value is OTPGeocodeResult) {
          if (value.description == 'Meu Local') {
            isLoading = true;
            setState(() {});
            tecFrom.text = await MOBApiUtils.getReverseGeocodingDescrition();
            geocodeResultFrom = OTPGeocodeResult(
                tecFrom.text, value.latitude, value.longitude, null);
            strMarker = 'assets/images/pin_end_gmaps.svg';

            tecHybrid.clear();
            isLoading = false;
            isTripMode = true;
            isFocusTo = true;
            isFocusFrom = false;
            setState(() {});
          } else {
            tecTo.text = value.description;
            geocodeResultTo = value;
            strMarker = 'assets/images/pin_from_gmaps.svg';
            await controller.animateCamera(CameraUpdate.newLatLng(
                LatLng(value.latitude, value.longitude)));

            String jsonString =
                prefs.getString('search_history_$projectId') ?? '[]';
            if (!jsonString.contains(value.description)) {
              List<dynamic> history =
                  (convert.json.decode(jsonString) as Iterable).toList();
              Map<String, dynamic> data = {
                'type': 'GEOCODE',
                'data': value.toMap(),
              };
              history.add(data);
              history =
                  history.sublist(history.length >= 3 ? history.length - 3 : 0);
              jsonString = convert.json.encode(history);
              prefs.setString('search_history_$projectId', jsonString);
            }

            Future.delayed(Duration(seconds: 1), () {
              tecHybrid.clear();
              isTripMode = true;
              isFocusTo = false;
              isFocusInit = true;
              isFocusFrom = true;
              isLoading = false;
              setState(() {});
            });
          }
        } else if (value is MOBLine) {
          await Utils.selectedLineBehaviour(value, false, context, false);

          int routeId = value.routeId;

          String timetablePath = 'timetable_last_download_$projectId\_$routeId';
          String timetableJson = prefs.getString(timetablePath);
          Map<String, dynamic> map = convert.json.decode(timetableJson);
          MOBTimetable timetable = MOBTimetable.fromJson(map);
          List<MOBTrip> trips = timetable.trips;

          String shortName = value.shortName;
          String imageAsset = Utils.getStopTypeSvg(value.type, false);

          Color lineColor = Utils.getColorFromHex(value.color ?? '#000000');
          DialogUtils(context).showRequestTripToView(
              shortName, trips, imageAsset, routeId, lineColor);
        }
      },
      suggestionsCallback: (value) async {
        List<dynamic> results = [
          if (Utils.locationData != null)
            OTPGeocodeResult(
              'Meu Local',
              Utils.locationData.latitude,
              Utils.locationData.longitude,
              null,
            ),
        ];
        debouncer.run(() => setState(() => isLoading = false));
        String text = value.toLowerCase();

        int projectId = Utils?.project?.projectId ?? 0;

        SharedPreferences prefs = await SharedPreferences.getInstance();
        String jsonString =
            prefs.getString('search_history_$projectId') ?? '[]';
        Iterable history = (convert.json.decode(jsonString) as Iterable);

        if (text.isEmpty || text.length < 2) {
          List<OTPGeocodeResult> historyItems = [];
          for (final h in history) {
            String item = h['type'];
            if (item == 'GEOCODE') {
              Map<String, dynamic> data = h['data'];
              OTPGeocodeResult geocodeResult =
                  OTPGeocodeResult.fromJson(data, true);
              historyItems.add(geocodeResult);
            }
          }
          results.addAll(historyItems.reversed);
          return Future<Iterable>.value(Iterable.castFrom(results));
        }

        String jsonObject = prefs.getString('timetable_favorites_$projectId');
        if (jsonObject != null) {
          Iterable favoritesIterable = convert.json.decode(jsonObject);
          favorites = favoritesIterable.toList();
        }

        MOBLine line = Utils.lines.firstWhere((line) {
          String shortName = line.shortName.toLowerCase();
          String longName = line.longName.toLowerCase();
          return shortName.contains(text) || longName.contains(text);
        }, orElse: () => null);
        if (line != null) results.add(line);

        List<OTPGeocodeResult> geocodeResults = [];

        int count = 2;
        try {
          geocodeResults =
              await MOBApiUtils.suggestionCallback(value, Utils.project.otpUri);

          for (final geocodeResult in geocodeResults) {
            results.add(geocodeResult);
            count--;
            if (count == 0) break;
          }
        } catch (e) {}

        count = 2;
        for (final h in history) {
          String item = h['type'];
          if (item == 'GEOCODE') {
            Map<String, dynamic> data = h['data'];
            OTPGeocodeResult geocodeResult =
                OTPGeocodeResult.fromJson(data, true);
            String desc = geocodeResult.description.toLowerCase();
            if (desc.contains(text))
              results.add(geocodeResult);
            else
              count++;
          }
          count--;
          if (count == 0) break;
        }

        for (final line in Utils.lines)
          if (!results.contains(line)) {
            String shortName = line.shortName.toLowerCase();
            String longName = line.longName.toLowerCase();
            if (shortName.contains(text) || longName.contains(text))
              results.add(line);
          }

        for (final geocodeResult in geocodeResults)
          if (!results.contains(geocodeResult)) results.add(geocodeResult);

        return Future<Iterable>.value(Iterable.castFrom(results));
      },
      itemBuilder: (context, value) {
        String text = '';
        String imageAsset;
        bool favorited = false;

        if (value is OTPGeocodeResult) {
          imageAsset = value.description == 'Meu Local'
              ? 'assets/images/pin_from_gmaps.svg'
              : value.provider == 'OpenTripPlanner GTFS'
                  ? Utils.globalPinBusStop
                  : 'assets/images/pin_end_gmaps.svg';
          String description = value.description;
          text = description;
        } else if (value is MOBLine) {
          imageAsset = Utils.getStopTypeSvg(value.type, false);

          favorited = favorites.contains(value.routeId);
        }

        return Container(
          padding: EdgeInsets.all(5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              text == 'Meu Local'
                  ? Icon(
                      Icons.near_me,
                      size: 20,
                    )
                  : (value is OTPGeocodeResult && value.isHistory)
                      ? Icon(
                          Icons.history,
                          size: 20,
                        )
                      : SvgPicture.asset(
                          imageAsset,
                          width: 20,
                        ),
              Container(
                width: (MediaQuery.of(context).size.width / 5) * 4 - 10,
                padding: EdgeInsets.all(5),
                child: value is MOBLine
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Wrap(
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: <Widget>[
                              Card(
                                color: Utils.getColorFromHex(
                                    value.color ?? '#000000'),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10.0))),
                                child: Container(
                                  padding: EdgeInsets.all(5),
                                  child: Text(value.shortName,
                                      style: TextStyle(
                                          color: Utils.getColorFromHex(
                                              value.textColor ?? '#ffffff'))),
                                ),
                              ),
                              if (favorited) Icon(Icons.favorite),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Text(value.longName),
                          ),
                        ],
                      )
                    : Text(text),
              ),
            ],
          ),
        );
      },
    );
    tafFrom = TypeAheadField(
      debounceDuration: Duration(seconds: 2),
      noItemsFoundBuilder: (context) => null,
      errorBuilder: (context, error) => null,
      textFieldConfiguration: tfcFrom,
      onSuggestionSelected: (value) async {
        if (value is OTPGeocodeResult) {
          isLoading = true;
          setState(() {});
          tecFrom.text = value.description == 'Meu Local'
              ? await MOBApiUtils.getReverseGeocodingDescrition()
              : value.description;
          geocodeResultFrom = OTPGeocodeResult(
              tecFrom.text, value.latitude, value.longitude, null);
          await checkTrip(context);
          isLoading = false;
          setState(() {});
        }
      },
      suggestionsCallback: (value) async {
        strMarker = 'assets/images/pin_from_gmaps.svg';

        isFocusFrom = true;
        isFocusTo = false;
        setState(() {});

        List<OTPGeocodeResult> geocodeResults = [
          if (Utils.locationData != null)
            OTPGeocodeResult(
              'Meu Local',
              Utils.locationData.latitude,
              Utils.locationData.longitude,
              null,
            ),
        ];

        debouncer.run(() => setState(() => isLoading = false));
        String text = value.toLowerCase();
        if (text.isEmpty || text.length < 2)
          return Future<Iterable>.value(Iterable.castFrom(geocodeResults));

        geocodeResults.addAll(
            await MOBApiUtils.suggestionCallback(value, Utils.project.otpUri));

        return Future<Iterable>.value(Iterable.castFrom(geocodeResults));
      },
      itemBuilder: (context, value) {
        String text = '';
        String imageAsset;
        bool favorited = false;

        if (value is OTPGeocodeResult) {
          imageAsset = value.description == 'Meu Local'
              ? 'assets/images/pin_from_gmaps.svg'
              : value.provider == 'OpenTripPlanner GTFS'
                  ? Utils.globalPinBusStop
                  : 'assets/images/pin_end_gmaps.svg';
          String description = value.description;
          text = description;
        } else if (value is MOBLine) {
          imageAsset = Utils.getStopTypeSvg(value.type, false);

          favorited = favorites.contains(value.routeId);
        }
/*
        if (value is OTPGeocodeResult) {
          String description = value.description;
          text = description;
        }
        */

        return Container(
          padding: EdgeInsets.all(5),
          child: Container(
            padding: EdgeInsets.all(5),
            child: Row(
              children: [
                text == 'Meu Local'
                    ? Icon(
                        Icons.near_me,
                        size: 20,
                      )
                    : (value is OTPGeocodeResult && value.isHistory)
                        ? Icon(
                            Icons.history,
                            size: 20,
                          )
                        : SvgPicture.asset(
                            imageAsset,
                            width: 20,
                          ),
                /*
                if (text == 'Meu Local')
                  Icon(
                    Icons.near_me,
                    size: 20,
                  ),
                */
                Container(
                  width: MediaQuery.of(context).size.width - 60,
                  child: Text('  ' + text),
                ),
              ],
            ),
          ),
        );
      },
    );
    tafTo = TypeAheadField(
      debounceDuration: Duration(seconds: 2),
      noItemsFoundBuilder: (context) => null,
      errorBuilder: (context, error) => null,
      textFieldConfiguration: tfcTo,
      onSuggestionSelected: (value) async {
        if (value is OTPGeocodeResult) {
          isLoading = true;
          setState(() {});
          tecTo.text = value.description == 'Meu Local'
              ? await MOBApiUtils.getReverseGeocodingDescrition()
              : value.description;

          geocodeResultTo = OTPGeocodeResult(
              tecTo.text, value.latitude, value.longitude, null);
          await checkTrip(context);
          isLoading = false;
          setState(() {});
        }
      },
      suggestionsCallback: (value) async {
        strMarker = 'assets/images/pin_end_gmaps.svg';

        isFocusFrom = false;
        isFocusTo = true;
        setState(() {});

        List<OTPGeocodeResult> geocodeResults = [
          if (Utils.locationData != null)
            OTPGeocodeResult(
              'Meu Local',
              Utils.locationData.latitude,
              Utils.locationData.longitude,
              null,
            ),
        ];
        debouncer.run(() => setState(() => isLoading = false));
        String text = value.toLowerCase();
        if (text.isEmpty || text.length < 2)
          return Future<Iterable>.value(Iterable.castFrom(geocodeResults));

        geocodeResults.addAll(
            await MOBApiUtils.suggestionCallback(value, Utils.project.otpUri));

        return Future<Iterable>.value(Iterable.castFrom(geocodeResults));
      },
      itemBuilder: (context, value) {
        String text = '';

        if (value is OTPGeocodeResult) {
          String description = value.description;
          text = description;
        }

        return Container(
          padding: EdgeInsets.all(5),
          child: Container(
            padding: EdgeInsets.all(5),
            child: Row(
              children: [
                if (text == 'Meu Local')
                  Icon(
                    Icons.near_me,
                    size: 20,
                  ),
                Container(
                  width: MediaQuery.of(context).size.width - 60,
                  child: Text(text),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget mapZoomControlls() => Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            heroTag: 'fabZoomIn',
            mini: true,
            backgroundColor: Colors.white,
            child: Icon(
              Icons.zoom_in,
              color: Utils.isLightTheme(context)
                  ? Colors.black
                  : Utils.getColorFromPrimary(context),
            ),
            onPressed: () => controller.animateCamera(CameraUpdate.zoomIn()),
          ),
          FloatingActionButton(
            heroTag: 'fabZoomOut',
            mini: true,
            backgroundColor: Colors.white,
            child: Icon(
              Icons.zoom_out,
              color: Utils.isLightTheme(context)
                  ? Colors.black
                  : Utils.getColorFromPrimary(context),
            ),
            onPressed: () => controller.animateCamera(CameraUpdate.zoomOut()),
          ),
          FloatingActionButton(
            heroTag: 'fabCenter',
            mini: true,
            backgroundColor: Theme.of(context).primaryColor,
            child: Icon(
              Icons.my_location,
              color: Utils.isLightTheme(context) ? Colors.black : Colors.white,
            ),
            onPressed: goToMyLocation,
          ),
        ],
      );

  Widget mapButtonsLayer() => Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width:
                    (MediaQuery.of(context).size.width / (isTripMode ? 4 : 2)) -
                        10,
                child: InkWell(
                  child: Container(
                    child: Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      color: Theme.of(context).primaryColor,
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Icon(
                          Icons.clear,
                          color: Utils.getColorByLuminanceTheme(context),
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    isTripMode = false;
                    isLoading = false;
                    isFocusFrom = false;
                    isFocusTo = false;
                    tecFrom.clear();
                    tecTo.clear();

                    geocodeResultFrom = null;
                    geocodeResultTo = null;
                    setState(() {});
                  },
                ),
              ),
              if (geocodeResultFrom != null && geocodeResultTo != null)
                Container(
                  width: (MediaQuery.of(context).size.width / 4) - 10,
                  child: InkWell(
                    onTap: () async {
                      await checkTrip(context);
                      setState(() {});
                    },
                    child: Container(
                      child: Card(
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0))),
                        color: Theme.of(context).primaryColor,
                        child: Container(
                          padding: EdgeInsets.all(5),
                          child: Icon(
                            Icons.check_circle_outline,
                            color: Utils.getColorByLuminanceTheme(context),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
            ],
          ),
        ],
      );

  void searchMyLocation() async {
    try {
      LocationData locationData = Utils.locationData;
      double lat = locationData.latitude;
      double lng = locationData.longitude;
      LatLng latLng = LatLng(lat, lng);

      String name = await MOBApiUtils.getReverseGeocodingDescrition();

      updateTripFrom(latLng, name, context);
    } catch (e) {}
  }

  void swapDirections() async {
    String from = tecFrom.text;
    String to = tecTo.text;

    tecFrom.text = to;
    tecTo.text = from;

    OTPGeocodeResult mGeocodeResultFrom = geocodeResultFrom == null
        ? null
        : OTPGeocodeResult.fromJson(geocodeResultFrom.toMap());

    OTPGeocodeResult mGeocodeResultTo = geocodeResultTo == null
        ? null
        : OTPGeocodeResult.fromJson(geocodeResultTo.toMap());

    geocodeResultFrom = mGeocodeResultTo;
    geocodeResultTo = mGeocodeResultFrom;

    FocusScope.of(context).unfocus();

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (tecFrom.text.isEmpty && tecTo.text.isEmpty) isTripMode = false;

    Widget controllersLayer = Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        if (Utils.project != null)
          Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Column(
              children: <Widget>[
                Stack(
                  alignment: AlignmentDirectional.center,
                  children: <Widget>[
                    Container(
                      child: Stack(
                        alignment: Alignment.centerRight,
                        children: <Widget>[
                          SafeArea(
                            child: Container(
                              padding: EdgeInsets.only(top: 10),
                              child: (tafHybrid != null &&
                                      tafFrom != null &&
                                      tafTo != null)
                                  ? SafeArea(
                                      child: isTripMode
                                          ? Stack(
                                              alignment: Alignment.centerRight,
                                              children: [
                                                Column(
                                                  children: [
                                                    tafFrom,
                                                    tafTo,
                                                  ],
                                                ),
                                                FloatingActionButton(
                                                  child: Icon(
                                                    Icons.swap_vert,
                                                    color: Utils.isLightTheme(
                                                            context)
                                                        ? Colors.black
                                                        : Colors.white,
                                                  ),
                                                  heroTag: 'fabSwap',
                                                  mini: true,
                                                  backgroundColor: Utils
                                                          .isLightTheme(context)
                                                      ? Utils
                                                          .getColorFromPrimary(
                                                              context)
                                                      : Colors.black,
                                                  onPressed: swapDirections,
                                                ),
                                              ],
                                            )
                                          : tafHybrid,
                                    )
                                  : null,
                            ),
                          ),
                        ],
                      ),
                    ),
                    if (isLoading)
                      CircularProgressIndicator(
                        strokeWidth: 3,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            Utils.isLightTheme(context)
                                ? Colors.black
                                : Utils.getColorFromPrimary(context)),
                      ),
                  ],
                ),
                if (isTripMode) mapButtonsLayer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    mapZoomControlls(),
                  ],
                )
              ],
            ),
          ),
        //if (Utils.showAds)
        // InkResponse(
        // onTap: () => Navigator.pushNamed(context, '/new-transport-card'),//Navigator.of(context).push(MaterialPageRoute(builder: (context) => SaoJoseWidget())),
        // child: Image.asset('assets/header/visate_banner.webp'),
        // ),
      ],
    );

    List<Marker> markers = [];
    if (isTripMode)
      markers = [];
    else if (zoom >= 15) markers = List.of(markersMap.values.toList());
    markers.removeWhere(
        (element) => element.markerId.value.contains('pointOfSale'));

    if (mCameraPosition != null) {
      List<Point<num>> polygon = markers
          .map((e) => Point(e.position.latitude, e.position.longitude))
          .toList();

      markers.removeWhere((marker) {
        LatLng latLng = marker.position;
        Point p = Point(latLng.latitude, latLng.longitude);

        return !PolyUtils.containsLocationPoly(p, polygon);
      });
    }

    if (isSameProject)
      markers.removeWhere((m) {
        int projectId = markersProjectIdMap[m];
        return Utils.project.projectId != projectId;
      });

    Set<Marker> cMarkersSet = Set<Marker>.of([
      ...markers,
      ...markersSet,
    ]);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: scaffoldKey,
      floatingActionButton: Utils.isMobilibus
          ? FloatingActionButton.extended(
              label: Text('Mesmo projeto: '),
              onPressed: () => setState(() => isSameProject = !isSameProject),
              icon: Icon(isSameProject ? Icons.done : Icons.cancel),
            )
          : null,
      body: Stack(
        children: <Widget>[
          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              GoogleMap(
                //minMaxZoomPreference: MinMaxZoomPreference(13,17),
                myLocationEnabled: true,
                trafficEnabled: false,
                zoomGesturesEnabled: true,
                myLocationButtonEnabled: false,
                mapToolbarEnabled: false,
                compassEnabled: false,
                zoomControlsEnabled: false,
                mapType: MapType.normal,
                markers: cMarkersSet,
                onMapCreated: onMapCreated,
                onCameraMove: onCameraMove,
                onCameraIdle: onCameraIdle,
                onLongPress: onLongPress,
                padding: Utils.showAds
                    ? EdgeInsets.only(bottom: 50, top: 150)
                    : EdgeInsets.only(
                        bottom: 5, left: 5), //65 Se existir banner
                initialCameraPosition: CameraPosition(
                  zoom: Utils.project != null ? zoom : 0,
                  target: Utils.project != null
                      ? LatLng(Utils.project.latitude, Utils.project.longitude)
                      : LatLng(0.0, 0.0),
                ),
              ),
              if (isTripMode && strMarker != null)
                Container(
                  padding: EdgeInsets.only(top: 100),
                  child: SvgPicture.asset(
                    strMarker,
                    height: 30,
                  ),
                ),
            ],
          ),
          controllersLayer,
        ],
      ),
    );
  }
}

Future<void> checkTrip(BuildContext context) async {
  if (geocodeResultFrom != null && geocodeResultTo != null) {
    await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => TripPlanner(
              geocodeResultFrom.description,
              geocodeResultTo.description,
              Point<num>(
                  geocodeResultFrom.latitude, geocodeResultFrom.longitude),
              Point<num>(geocodeResultTo.latitude, geocodeResultTo.longitude),
            )));

    isTripMode = false;
    tecFrom.clear();
    tecTo.clear();

    geocodeResultFrom = null;
    geocodeResultTo = null;
  }
  isLoading = false;
  try {
    (context as Element).markNeedsBuild();
  } catch (e) {}
}

Future<void> updateTripFrom(
    LatLng latLng, String name, BuildContext context) async {
  isTripMode = true;
  tecFrom.text = name;
  geocodeResultFrom =
      OTPGeocodeResult(name, latLng.latitude, latLng.longitude, null);
  checkTrip(context);
}

Future<void> updateTripTo(
    LatLng latLng, String name, BuildContext context) async {
  isTripMode = true;
  tecTo.text = name;
  geocodeResultTo =
      OTPGeocodeResult(name, latLng.latitude, latLng.longitude, null);
  checkTrip(context);
}
