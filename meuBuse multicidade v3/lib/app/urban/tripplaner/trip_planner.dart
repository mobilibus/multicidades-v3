import 'dart:convert' as convert;
import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/app/urban/tripplaner/viewer/trip_planner_viewer.dart';
import 'package:mobilibus/base/otp/OTPPlan.dart';
import 'package:mobilibus/base/otp/OTPTrip.dart';
import 'package:mobilibus/utils/DialogUtils.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/TripplanerUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

List<String> timeStartGB = [];
List<String> timeEndGB = [];
List<String> durationDistanceGB = [];

class TripPlanner extends StatefulWidget {
  TripPlanner(this.origin, this.destiny, this.from, this.to);

  final String origin;
  final String destiny;
  final Point from;
  final Point to;

  _TripPlanner createState() => _TripPlanner(origin, destiny, from, to);
}

class _TripPlanner extends State<TripPlanner> {
  _TripPlanner(this.origin, this.destiny, this.from, this.to) : super();

  final String origin;
  final String destiny;
  final Point from;
  final Point to;

  final GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();

  bool isDialogOpen = false;
  bool isArriveBy = false;
  bool isVehicle = true;
  DateTime currentDateTimeSelected = DateTime.now().add(Duration(minutes: 1));
  String departureSelectedDate = '';
  List<OTPItinerary> itineraries = [];
  List<Widget> listItineraries = [];
  List<List<Widget>> tripDetails = [];

  TextEditingController tecFrom;
  TextEditingController tecTo;

  Map<OTP_MODE, bool> modeMap = {
    OTP_MODE.WALK: true,
    OTP_MODE.TRANSIT: true,
  };
  int maxWalkDistance = 1000;
  OTPPlan plan;
  String detailsTP = '';

  @override
  void initState() {
    super.initState();
    tecFrom = TextEditingController(text: origin ?? '');
    tecTo = TextEditingController(text: destiny ?? '');
    updateTripPlanner();
    saveTrip();
  }

  void setTripDetailsValues(String sTripDetails){

    detailsTP = sTripDetails;

  }

  void saveTrip() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    int projectId = Utils.project.projectId;

    String favTripsParam = 'trips_favorite_$projectId';

    OTPTrip tripPlannerTrip =
        OTPTrip(origin, destiny, from.x, from.y, to.x, to.y);

    String jsonString = prefs.getString(favTripsParam) ?? '[]';
    Iterable iterable = convert.json.decode(jsonString) ?? [];

    List<OTPTrip> trips =
        iterable.map((trip) => OTPTrip.fromJson(trip)).toList();

    bool exist = false;
    var map = tripPlannerTrip.toMap();
    for (final trip in trips) {
      var mMap = trip.toMap();
      if (mapEquals(map, mMap)) exist = true;
    }
    if (!exist) {
      trips.add(tripPlannerTrip);
      jsonString = convert.json.encode(trips.map((e) => e.toMap()).toList());
      prefs.setString(favTripsParam, jsonString);
    }
  }

  void updateTripPlanner() {
    itineraries = [];
    listItineraries = [];
    tripDetails = [];
    setState(() {});

    DateTime dateTime = currentDateTimeSelected;

    String dateString = DateFormat('MM-dd-yyyy').format(dateTime);
    String timeString = DateFormat('HH:mm').format(dateTime);
    String dateTimeString = DateFormat('dd/MM/yyyy HH:mm').format(dateTime);

    //String dayOfWeek = Utils.getDayOfTheWeek(dateTime.weekday);

    Future.delayed(Duration(), () async {
      plan = await MOBApiUtils.getTripplanner(
        from,
        to,
        isArriveBy,
        dateString,
        timeString,
        maxWalkDistance: maxWalkDistance,
        modes: modeMap.keys.where((mode) => modeMap[mode]).toList(),
      );

      await TripplanerUtils.decodePlan(
          plan, tripDetails, listItineraries, itineraries, context);

      setState(() => departureSelectedDate = dateTimeString);
    });
  }

  void datePicker() async {
    DateTime dt = DateTime.now().add(Duration(minutes: 1));

    currentDateTimeSelected = dt;
    CupertinoDatePicker dateTimeWidget = CupertinoDatePicker(
      onDateTimeChanged: (dateTime) => dt = dateTime,
      initialDateTime: currentDateTimeSelected,
      maximumDate: DateTime.now().add(Duration(days: 30)),
      minimumDate: DateTime.now(),
      use24hFormat: true,
      mode: CupertinoDatePickerMode.dateAndTime,
    );

    DateTime dateTime = await showModalBottomSheet(
      context: context,
      builder: (context) => Container(
        height: 200,
        child: Column(
          children: [
            Container(
              height: 150,
              child: dateTimeWidget,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(dt),
                  child: Text(
                    'Selecionar',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child:
                      Text('Cancelar', style: TextStyle(color: Colors.black)),
                ),
              ],
            )
          ],
        ),
      ),
    );

    if (dateTime != null) {
      int dif = DateTime.now().difference(dateTime).inDays;
      if (DateTime.now().isAfter(dateTime))
      {
        DialogUtils(context).dialogMaxFuture();
      }
      else if (DateTime.now().isBefore(dateTime) && dif < 14)
      {
        currentDateTimeSelected = dateTime;
        updateTripPlanner();
      }
    }
  }

  Widget tripParamsHeadSign(Color color) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Card(
              elevation: 5,
              child: InkWell(
                onTap: () {
                  setState(() => isArriveBy = !isArriveBy);
                  updateTripPlanner();
                },
                child: Container(
                  width: MediaQuery.of(context).size.width / 4.2,
                  height: 55,
                  padding: EdgeInsets.all(5),
                  child: Column(
                    children: [
                      Icon(Icons.transfer_within_a_station, size: 18),
                      Text(isArriveBy ? 'Chegando em' : 'Partindo em',
                          style: TextStyle(fontSize: 12)),
                      Text(
                        !isArriveBy ? 'Chegando em' : 'Partindo em',
                        style: TextStyle(fontSize: 8, color: Colors.grey),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Card(
              elevation: 5,
              child: InkWell(
                onTap: () => datePicker(),
                child: Container(
                  width: MediaQuery.of(context).size.width / 3.4,
                  height: 55,
                  padding: EdgeInsets.all(5),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(Icons.access_time),
                      Text(departureSelectedDate,
                          style: TextStyle(fontSize: 12)),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Card(
              elevation: 5,
              child: InkWell(
                onTap: openModalSelector,
                child: Container(
                  child: Container(
                    height: 55,
                    padding: EdgeInsets.all(5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(isVehicle ? Icons.directions_bus : Icons.cancel),
                        Text('Modais', style: TextStyle(fontSize: 12)),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Card(
              elevation: 5,
              child: InkWell(
                onTap: openDistanceSlider,
                child: Container(
                  child: Container(
                    height: 55,
                    padding: EdgeInsets.all(5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Distância'),
                        Text(
                            maxWalkDistance >= 2000
                                ? '${maxWalkDistance ~/ 1000}' + ' km'
                                : '$maxWalkDistance m',
                            style: TextStyle(fontSize: 12)),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      );

  Widget tripHeadSign(String origin, String destiny, Color color) => Container(
        child: Column(
          children: <Widget>[
            Container(
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Container(
                            child: Icon(Icons.my_location, color: color),
                            width: 40),
                      ),
                      Container(
                          padding: EdgeInsets.all(5),
                          alignment: AlignmentDirectional.centerStart,
                          child: TextField(controller: tecFrom),
                          width: MediaQuery.of(context).size.width - 80),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Container(
                          child: Icon(Icons.directions_walk, color: color),
                          width: 40,
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.all(5),
                          alignment: AlignmentDirectional.centerStart,
                          child: TextField(controller: tecTo),
                          width: MediaQuery.of(context).size.width - 80),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );

  void openDistanceSlider() async {
    double mDistance = maxWalkDistance * 1.0;
    int distance = await showModalBottomSheet(
      context: context,
      builder: (context) => StatefulBuilder(
        builder: (context, setState) {
          String strDistance = '${mDistance.toStringAsFixed(2)} m';
          if (mDistance > 3000) strDistance = '${mDistance ~/ 1000}km';

          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Distância máxima percorrida a pé'),
              Text('$strDistance'),
              Slider(
                min: 1.0,
                max: 10000.0,
                value: mDistance,
                onChanged: (value) => setState(() => mDistance = value),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FlatButton(
                    child: Text('Cancelar'),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                  FlatButton(
                    child: Text('Confirmar'),
                    onPressed: () =>
                        Navigator.of(context).pop(mDistance.toInt()),
                  ),
                ],
              ),
            ],
          );
        },
      ),
    );
    if (distance != null) {
      maxWalkDistance = distance;
      setState(() {});
      updateTripPlanner();
    }
  }

  void openModalSelector() async {
    await showModalBottomSheet(
      context: context,
      builder: (context) => StatefulBuilder(
        builder: (context, setState) => ListView(
          shrinkWrap: true,
          children: OTP_MODE.values
              .where((mode) =>
                  mode != OTP_MODE.CAR &&
                  mode != OTP_MODE.TRAINISH &&
                  mode != OTP_MODE.BUSISH &&
                  mode != OTP_MODE.CABLE_CAR &&
                  mode != OTP_MODE.LEG_SWITCH &&
                  mode != OTP_MODE.CUSTOM_MOTOR_VEHICLE)
              .map((otpMode) {
            String modeName = Utils.getModeName(otpMode);
            IconData modeIcon = Utils.getModeIcon(otpMode);
            return InkWell(
              onTap: () => setState(
                  () => modeMap[otpMode] = !(modeMap[otpMode] ?? false)),
              child: Container(
                padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Icon(modeIcon),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          width: (MediaQuery.of(context).size.width / 6) * 4,
                          child: Text(modeName),
                        ),
                      ],
                    ),
                    Checkbox(
                      value: modeMap[otpMode] ?? false,
                      onChanged: (value) =>
                          setState(() => modeMap[otpMode] = value),
                    ),
                  ],
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
    updateTripPlanner();
  }

  @override
  Widget build(BuildContext context) {
    currentDateTimeSelected = DateTime.now().add(Duration(minutes: 1));

    return Scaffold(
      key: key,
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        leading: IconButton(
          icon: Icon(Icons.arrow_back,
              color: Utils.getColorByLuminanceTheme(context)),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Planejador de Viagem',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context))),
        centerTitle: true,
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              tripHeadSign(
                origin,
                destiny,
                Utils.isLightTheme(context)
                    ? Colors.black
                    : Utils.getColorFromPrimary(context),
              ),
              Divider(),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5),
                child: tripParamsHeadSign(Utils.isLightTheme(context)
                    ? Colors.black
                    : Utils.getColorFromPrimary(context)),
              ),
              listItineraries.isEmpty
                  ? Center(
                      child: plan == null
                          ? Container(
                              padding: EdgeInsets.all(20),
                              child: Text('Nenhuma viagem encontrada! :('),
                            )
                          : CircularProgressIndicator(
                              strokeWidth: 3,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Utils.isLightTheme(context)
                                      ? Utils.getColorFromPrimary(context)
                                      : Colors.black)))
                  : ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: listItineraries.length,
                      itemBuilder: (context, index) {
                        List<Widget> widgetTripDetails = tripDetails[index];
                        return Container(
                          child: Card(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(width: 1, color: Colors.black),
                            ),
                            child: InkWell(
                              onTap: () {
                                int ind = index;
                                OTPItinerary i = itineraries[ind];

                                //print (tsGB[1]);

                                TripPlannerViewer w = TripPlannerViewer(i, widgetTripDetails, true, timeStartGB[index], timeEndGB[index], durationDistanceGB[index]);
                                Navigator.push(context, MaterialPageRoute(builder: (context) => w));
                              },
                              child: Container(
                                padding: EdgeInsets.all(10),
                                child: listItineraries[index],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
