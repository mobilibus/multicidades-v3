import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/accessibility/main_accessibility.dart';
import 'package:mobilibus/app/select_city_urban.dart';
import 'package:mobilibus/app/urban/more/contact/contact_message.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:package_info/package_info.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mobilibus/utils/widgets/NotificationWebView.dart';
import 'more/information/information.dart';
import 'more/pos/points_of_sale.dart';

class More extends StatefulWidget {
  @override
  _More createState() => _More();
}

class _More extends State<More> with AutomaticKeepAliveClientMixin {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences sharedPreferences;
  int tapCount = 0;
  bool hasRedDot = false;
  String version = '';
  bool visible = false;
  Timer timer;
  int selectedAsset = 0;
  List<String> assetsName = [
    'assets/mobilibus/access.png',
    'assets/mobilibus/adrianavatar.png',
    'assets/mobilibus/man.png',
    'assets/mobilibus/woman.png',
    'assets/mobilibus/workman.png',
  ];

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      sharedPreferences = await SharedPreferences.getInstance();
      version = (await PackageInfo.fromPlatform()).version +
          '.' +
          (await PackageInfo.fromPlatform()).buildNumber;
      setState(() {});
    });
    timer = Timer.periodic(Duration(seconds: 5), (timer) {
      selectedAsset++;
      if (selectedAsset > assetsName.length - 1) selectedAsset = 0;
      visible = true;
      setState(() {});
      Future.delayed(Duration(seconds: 2, milliseconds: 500),
          () => setState(() => visible = false));
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  // void _openBalanceAndRecharge() {
  //   //  Descomentar quando o backend passar a retornar os cartões de transporte salvos
  //   // Navigator.pushNamed(context, '/transport-card-selection');
  //   //String s = "dhq";
  //   Navigator.pushNamed(context, '/new-transport-card');
  //   //Navigator.push(context, MaterialPageRoute(builder: (context) => PurchaseRepository()));
  // }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    hasRedDot = sharedPreferences?.containsKey('redDot') ?? false
        ? sharedPreferences.getBool('redDot')
        : false;

    String urbanProjectNameLabel = Utils.project == null
        ? ''
        : Utils.isMobilibus
            ? Utils.project.name
            : Utils.project.city;

    List<dynamic> mores = [
      {
        'icon': null,
        'asset': 'assets/more/v3_news.svg',
        'name': 'NOTÍCIAS E ALERTAS',
        'action': () => Navigator.push(
            context, MaterialPageRoute(builder: (context) => Information())),
      },
      {
        'icon': null,
        'asset': 'assets/more/v3_store.svg',
        'name': 'PONTOS DE VENDA',
        'action': () => Navigator.push(
            context, MaterialPageRoute(builder: (context) => POS())),
      },
      {
        'icon': Icons.card_giftcard,
        'asset': null,
        'name': 'PROGRAMA DE FIDELIDADE',
        'action': () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => NotificationWebView(
                'Como Utilizar', 'https://www.ecobonuz.com'))),
      },
      {
        'icon': null,
        'asset': 'assets/more/v3_comment.svg',
        'name': 'ENTRE EM CONTATO',
        'action': () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => ContactMessageUrban())),
      },
      {
        'icon': null,
        'asset': 'assets/more/v3_acessibilidade.svg',
        'name': 'ACESSIBILIDADE',
        'action': () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => MainPageAccessibility())),
      },
      {
        'icon': null,
        'asset': 'assets/more/v3_manual.svg',
        'name': 'COMO UTILIZAR',
        'action': () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => NotificationWebView(
                'Como Utilizar', 'https://blog.mobilibus.com/manual'))),
      },
      {
        'icon': null, //Icons.share,
        'asset': 'assets/more/v3_share.svg',
        'name': 'COMPARTILHE',
        'action': () => Share.share(
              'http://app.mobilibus.com/${Utils.globalLandingPage}',
              subject: 'Compartilhar aplicativo',
              sharePositionOrigin: Rect.fromCenter(
                center: Offset(100, 100),
                width: 100,
                height: 100,
              ),
            ),
      },
      {
        'icon': Icons.location_city,
        'asset': null,
        'name': 'Selecionar cidade'.toUpperCase(),
        'action': () => scaffoldKey.currentState.openDrawer(),
      },
    ];

    Color color = Colors.black;

    ThemeData themeData = ThemeData(
      fontFamily: GoogleFonts.openSans().fontFamily,
    );

    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage(Utils.globalStrHeader),
        ),
      ),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomInset: false,
          //resizeToAvoidBottomPadding: false,

          key: scaffoldKey,
          drawer: Drawer(child: SelectCityUrban()),
          body: Column(
            children: [
              Container(
                height: 160,
              ),
              Expanded(
                child: Scrollbar(
                  isAlwaysShown: true,
                  thickness: 5,
                  radius: Radius.circular(10),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        //Container(height: 160),
                        ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          padding: EdgeInsets.only(
                            left: 1,
                            right: 1,
                          ),
                          itemCount: mores.length,
                          itemBuilder: (context, index) {
                            var more = mores[index];
                            Color colorText = Color(0xff888888);
                            Color color = Utils.globalPrimaryColor;
                            IconData iconData = more['icon'];
                            String asset = more['asset'];
                            String name = more['name'];
                            Function action = more['action'];
                            return Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 0, vertical: 2),
                              child: ListTile(
                                onTap: action,
                                title: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 15),
                                    color: Colors.white,
                                    width: MediaQuery.of(context).size.width,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        asset == null
                                            ? Icon(iconData,
                                                color: color, size: 45)
                                            //: (index == 0 || index == 1)  ?
                                            : SvgPicture.asset(
                                                asset,
                                                color: color,
                                                height: 45,
                                                width: 45,
                                              ),
                                        //: SvgPicture.asset(asset, color: color, height: 24, width: 24,),
                                        Container(
                                          padding: EdgeInsets.only(left: 20),
                                          child: Text(
                                            name,
                                            style: TextStyle(
                                                color: colorText,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                        Container(
                          padding: EdgeInsets.all(15),
                        ),
                        Column(
                          children: [
                            Text(
                              'Versão: $version',
                              style: TextStyle(color: color),
                            ),
                            RichText(
                              text: TextSpan(
                                style: TextStyle(color: color),
                                children: [
                                  TextSpan(
                                    text: 'Política de Privacidade',
                                    style: TextStyle(
                                      color: color,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                          GoogleFonts.catamaran().fontFamily,
                                    ),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () => Navigator.of(context)
                                          .push(MaterialPageRoute(
                                              builder: (context) =>
                                                  NotificationWebView(
                                                      'Política de Privacidade',
                                                      'https://blog.mobilibus.com/privacy'))), //launch('http://app.mobilibus.com/politica-de-privacidade.html'),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
