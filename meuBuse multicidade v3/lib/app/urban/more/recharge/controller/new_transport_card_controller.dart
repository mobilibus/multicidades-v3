import 'package:flutter/material.dart';
import 'package:mobilibus/transdata/data/purchase_repository.dart';

class NewTransportCardController {
  final PurchaseRepository _repository;
  final cpfController = TextEditingController();
  final transportCardController = TextEditingController();

  NewTransportCardController(this._repository);

  String validateCPF(String value) {
    if (value.isEmpty) {
      return "O CPF é obrigatório";
    }

    // TODO Melhorar a validação do CPF

    return null;
  }

  String validateCardNumber(String value) {
    if (value.isEmpty) {
      return "O número do cartão é obrigatório";
    }

    if (int.tryParse(value) == null) {
      return "Número do cartão inválido";
    }

    return null;
  }

  Future saveTransportCardData() async {
    await _repository.saveCurrentTransportCardData(
        cpfController.text, int.parse(transportCardController.text));
  }

  Future getLastUsedTransportCard() async {
    final lastTransportCard = await _repository.getSavedTransportCardData();
    if (lastTransportCard != null) {
      cpfController.value = TextEditingValue(text: lastTransportCard.userCPF);
      transportCardController.value = TextEditingValue(
        text: lastTransportCard.transportCardNumber.toString(),
      );
    }
  }
}
