import 'package:flutter/foundation.dart';
import 'package:mobilibus/app/urban/more/recharge/data/balance_extract_model.dart';
import 'package:mobilibus/app/urban/more/recharge/data/recharge_data_singleton.dart';
import 'package:mobilibus/transdata/data/purchase_repository.dart';

class TransportCardStatementController extends ChangeNotifier {
  final PurchaseRepository _repository;
  RechargeDataSingleton _singleton;
  BalanceExtractModel data;
  bool _loading = false;
  bool get isLoading => _loading;

  TransportCardStatementController(this._repository) {
    // Reset nos dados para um novo pedido de recarga;
    RechargeDataSingleton.clearInstance();
    _singleton = RechargeDataSingleton.getInstance();
  }

  void setLoadingStatus(bool newStatus) {
    _loading = newStatus;
    notifyListeners();
  }

  Future getBalanceFromTransportCard(String cpf, int cardNumber) async {
    setLoadingStatus(true);
    _setUserDataOnSingleton(cpf, cardNumber);
    data = await _repository.getBalanceFromUser(cpf, cardNumber);
    setLoadingStatus(false);
  }

  void _setUserDataOnSingleton(String cpf, int cardNumber) {
    _singleton.cpf = cpf;
    _singleton.transportCardNumber = cardNumber;
  }
}
