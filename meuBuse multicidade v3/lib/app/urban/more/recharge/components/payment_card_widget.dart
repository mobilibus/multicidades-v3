import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PaymentCardWidget extends StatelessWidget {
  final String cardBrand;
  final String maskedCardNumber;
  final VoidCallback onTap;

  const PaymentCardWidget({
    Key key,
    @required this.onTap,
    @required this.cardBrand,
    @required this.maskedCardNumber,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final labelTextStyle = GoogleFonts.nunito(
      color: Color(0xff212121),
      fontSize: 16,
      fontWeight: FontWeight.w700,
    );

    final valueTextStyle =
        GoogleFonts.nunito(fontSize: 16, fontWeight: FontWeight.w400, color: Colors.grey[900]);

    return GestureDetector(
      child: Container(
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Color(0x212121).withOpacity(0.15)),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 9,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Cartão de crédito', style: labelTextStyle),
                  SizedBox(height: 8),
                  Text(this.maskedCardNumber, style: valueTextStyle),
                ],
              ),
            ),
            Image.asset(
              _getCardBrandAssetPath(),
              width: MediaQuery.of(context).size.width * 0.08,
              height: MediaQuery.of(context).size.height * 0.08,
            ),
          ],
        ),
      ),
      onTap: onTap,
    );
  }

  String _getCardBrandAssetPath() {
    String path;
    switch (this.cardBrand) {
      case 'VISA':
        path = 'assets/cards/visa.png';
        break;
      case 'MASTERCARD':
        path = 'assets/cards/mastercard.png';
        break;
    }

    return path;
  }
}
