import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/transdata/data/model/transdata_products.dart';
import 'package:mobilibus/utils/date_number_utils.dart';

class RechargeProductWidget extends StatelessWidget {
  final TransdataProduct product;
  final VoidCallback onTap;

  const RechargeProductWidget({
    Key key,
    @required this.product,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final labelTextStyle = GoogleFonts.nunito(
      color: Color(0xff212121).withOpacity(0.6),
      fontSize: 14,
      fontWeight: FontWeight.w400,
    );
    final valueTextStyle = GoogleFonts.nunito(
      fontSize: 16,
      fontWeight: FontWeight.w400,
    );

    return GestureDetector(
      child: Container(
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Color(0x212121).withOpacity(0.15)),
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                spreadRadius: 2,
                blurRadius: 5,
                offset: Offset(0, 3),
              )
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Nome', style: labelTextStyle),
            Text(product.nome, style: valueTextStyle),
            SizedBox(height: 24),
            Text('Valor mínimo', style: labelTextStyle),
            Text(NumberUtils.formatAmountAsCurrency(product.valorMin), style: valueTextStyle),
            SizedBox(height: 24),
            Text('Valor máximo', style: labelTextStyle),
            Text(NumberUtils.formatAmountAsCurrency(product.valorMax), style: valueTextStyle),
          ],
        ),
      ),
      onTap: onTap,
    );
  }
}
