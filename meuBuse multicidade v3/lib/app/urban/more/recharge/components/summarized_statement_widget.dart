import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/transport_card_detailed_statement_page.dart';
import 'package:mobilibus/app/urban/more/recharge/components/card_statement_register_widget.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_flat_button.dart';
import 'package:mobilibus/app/urban/more/recharge/data/user_transport_card_model.dart';
import 'package:mobilibus/transdata/data/model/transdata_extract.dart';

class SummarizedStatementWidget extends StatelessWidget {
  final String statementDate;
  final List<TransdataTransaction> transactions;
  final UserTransportCardModel transportCard;

  const SummarizedStatementWidget({
    Key key,
    @required this.statementDate,
    @required this.transactions,
    @required this.transportCard,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int maxCount = 5;
    if (transactions != null && transactions.length <= 5) {
      maxCount = transactions.length;
    }

    final TextStyle tableHeaderStyle = GoogleFonts.nunito(
      fontSize: 12,
      fontWeight: FontWeight.w600,
      color: Color(0xff212121).withOpacity(0.6),
    );

    return Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Histórico',
            style: GoogleFonts.nunito(
              fontSize: 18,
              fontWeight: FontWeight.w700,
              color: Color(0xff212121),
            ),
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Expanded(flex: 2, child: Text('Data', style: tableHeaderStyle)),
              Expanded(flex: 2, child: Text('Detalhes', style: tableHeaderStyle)),
              Expanded(flex: 1, child: Text('Valor (R\$)', style: tableHeaderStyle)),
            ],
          ),
          transactions == null || transactions.isEmpty
              ? Padding(
            padding: const EdgeInsets.symmetric(vertical: 32.0),
            child: Text(
              'Sem histórico recente',
              textAlign: TextAlign.center,
              style: GoogleFonts.nunito(
                fontSize: 20,
                fontWeight: FontWeight.w400,
                color: Color(0xff212121).withOpacity(0.8),
              ),
            ),
          )
              : ListView.separated(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: maxCount,
            separatorBuilder: (_, __) => Divider(),
            itemBuilder: (_, i) => CardStatementRegisterWidget(transaction: transactions[i]),
          ),
          Divider(),

          Center(
            child: CustomFlatButton(
              label: 'Ver mais',
              onTap: () => _openDetailedStatement(context),
            ),
          ),
        ],
      ),
    );
  }

  void _openDetailedStatement(BuildContext ctx) {
    Navigator.push(
      ctx,
      MaterialPageRoute(
        builder: (_) => TransportCardDetailedStatementPage(
          transportCard: this.transportCard,
          statementDate: this.statementDate,
          transactions: this.transactions,
        ),
      ),
    );
  }
}