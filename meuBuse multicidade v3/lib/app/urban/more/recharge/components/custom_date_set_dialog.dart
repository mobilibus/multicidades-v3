import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:mobilibus/utils/formatting_utils.dart';

class CustomDateSetDialog extends StatefulWidget {
  @override
  _CustomDateSetDialogState createState() => _CustomDateSetDialogState();
}

class _CustomDateSetDialogState extends State<CustomDateSetDialog> {
  final _textController = TextEditingController();
  ValueNotifier<String> _errorMessage = ValueNotifier(null);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Digite uma data:'),
      content: ValueListenableBuilder(
        valueListenable: _errorMessage,
        builder: (_, value, __) => TextField(
          controller: _textController,
          keyboardType: TextInputType.number,
          inputFormatters: [MaskTextInputFormatter(mask: FormattingUtils.dateMask)],
          decoration: InputDecoration(hintText: 'dd/mm/aaaa', errorText: value),
        ),
      ),
      actions: [
        FlatButton(
          child: Text('OK'),
          onPressed: _validateDate,
        ),
      ],
    );
  }

  void _validateDate() {
    // TODO Melhorar a validação dessa data
    _errorMessage.value = null;
    try {
      DateTime dt = DateFormat('dd/MM/yyyy').parse(_textController.text);
      Navigator.pop(context, dt);
    } catch (e) {
      _errorMessage.value = 'Data inválida';
      return;
    }
  }
}
