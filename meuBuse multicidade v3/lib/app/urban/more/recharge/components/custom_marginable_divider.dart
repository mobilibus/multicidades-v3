import 'package:flutter/material.dart';

class CustomMarginableDivider extends StatelessWidget {
  final double verticalMargin;

  const CustomMarginableDivider({Key key, this.verticalMargin}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: verticalMargin),
        Divider(),
        SizedBox(height: verticalMargin),
      ],
    );
  }
}
