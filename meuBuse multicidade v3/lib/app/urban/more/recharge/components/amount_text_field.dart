import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AmountTextField extends StatefulWidget {
  final String initialValue;
  final TextEditingController editingController;
  final ValueChanged<String> onChanged;
  final TextStyle valueTextStyle;

  const AmountTextField({
    Key key,
    this.initialValue,
    this.editingController,
    this.onChanged,
    this.valueTextStyle,
  }) : super(key: key);

  @override
  _AmountTextFieldState createState() => _AmountTextFieldState();
}

class _AmountTextFieldState extends State<AmountTextField> {
  final defaultTextStyle = GoogleFonts.nunito(
    color: Color(0xff212121),
    fontSize: 16,
    fontWeight: FontWeight.w400,
  );

  @override
  Widget build(BuildContext context) {
    return TextField(
      style: widget.valueTextStyle ?? defaultTextStyle,
      keyboardType: TextInputType.number,
      textAlign: TextAlign.center,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xffe0e0e0))),
      ),
      controller: widget.editingController,
      onChanged: widget.onChanged,
      onTap: () {
        widget.editingController.selection =
            TextSelection.fromPosition(TextPosition(offset: widget.editingController.text.length));
      },
    );
  }
}
