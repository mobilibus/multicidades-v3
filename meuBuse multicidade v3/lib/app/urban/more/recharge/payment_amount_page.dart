import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/components/amount_text_field.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_circular_progress_indicator.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_info_dialog.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_raised_button.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_rounded_amount_button.dart';
import 'package:mobilibus/app/urban/more/recharge/controller/payment_amount_controller.dart';
import 'package:mobilibus/app/urban/more/recharge/new_payment_card_page.dart';
import 'package:mobilibus/main.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:mobilibus/utils/date_number_utils.dart';

class PaymentAmountPage extends StatefulWidget {
  @override
  _PaymentAmountPageState createState() => _PaymentAmountPageState();
}

class _PaymentAmountPageState extends State<PaymentAmountPage> {
  PaymentAmountController _controller;

  @override
  void initState() {
    super.initState();
    _controller = PaymentAmountController(getPurchaseRepositoryInstance());
  }

  @override
  Widget build(BuildContext context) {
    final roundedButtonDecoration = BoxDecoration(
      color: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(50),
    );

    final minAmount = NumberUtils.formatAmountAsCurrency(_controller.singleton.product.valorMin);
    final maxAmount = NumberUtils.formatAmountAsCurrency(_controller.singleton.product.valorMax);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        centerTitle: true,
        elevation: 0,
        title: Text(
          'Valor',
          style: GoogleFonts.nunito(fontSize: 20, fontWeight: FontWeight.w700),
        ),
      ),
      body: SafeArea(
        child: AnimatedBuilder(
          animation: _controller,
          builder: (_, __) {
            if (_controller.isLoading) {
              return Center(child: CustomCircularProgressIndicator());
            }

            return Column(
              children: [
                /*
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                  width: double.infinity,
                  child: Row(
                    children: [
                      IconButton(
                        padding: const EdgeInsets.all(0),
                        iconSize: 28,
                        icon: Icon(Icons.west),
                        onPressed: () => Navigator.pop(context),
                      ),
                      Spacer()
                    ],
                  ),
                ),
                */
                Expanded(
                  child: Container(
                    width: double.infinity,
                    child: SingleChildScrollView(
                      padding: const EdgeInsets.symmetric(horizontal: 32),
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          /*
                          Text(
                            'Valor',
                            style: GoogleFonts.nunito(
                              fontSize: 32,
                              fontWeight: FontWeight.w700,
                              color: Colors.grey[800],
                            ),
                          ),
                          */
                          SizedBox(height: 44),
                          Text(
                            'Selecione o valor da recarga. O valor mínimo é $minAmount e o máximo é $maxAmount',
                            style: GoogleFonts.nunito(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Colors.grey[700],
                              height: 1.8,
                            ),
                          ),
                          SizedBox(height: 24),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: roundedButtonDecoration,
                                child: IconButton(
                                  icon: Icon(Icons.remove, color: Theme.of(context).accentColor),
                                  onPressed: _controller.removeAmount,
                                ),
                              ),
                              SizedBox(width: 14),
                              Expanded(
                                child: Center(
                                  child: AnimatedBuilder(
                                    animation: _controller,
                                    builder: (_, __) => AmountTextField(
                                      editingController: _controller.amountController,
                                      valueTextStyle: GoogleFonts.nunito(
                                        fontSize: 32,
                                        fontWeight: FontWeight.w800,
                                        color: Color(0xff212121),
                                      ),
                                      onChanged: _controller.onChangedAmountManually,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 14),
                              Container(
                                decoration: roundedButtonDecoration,
                                child: IconButton(
                                  icon: Icon(Icons.add, color: Theme.of(context).accentColor),
                                  onPressed: _controller.addAmount,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 24),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CustomRoundedAmountButton(
                                label: 'R\$ 20',
                                onPressed: () => _controller.setFixedAmount(2000),
                              ),
                              CustomRoundedAmountButton(
                                label: 'R\$ 30',
                                onPressed: () => _controller.setFixedAmount(3000),
                              ),
                              CustomRoundedAmountButton(
                                label: 'R\$ 50',
                                onPressed: () => _controller.setFixedAmount(5000),
                              ),
                              CustomRoundedAmountButton(
                                label: 'R\$ 80',
                                onPressed: () => _controller.setFixedAmount(8000),
                              ),
                            ],
                          ),
                          SizedBox(height: 32),
                          AnimatedBuilder(
                            animation: _controller,
                            builder: (_, __) {
                              final amount = NumberUtils.formatAmountAsCurrency(
                                _controller.singleton.productTaxAmount ?? 0,
                              );

                              return RichText(
                                text: TextSpan(
                                  style: GoogleFonts.nunito(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.grey[700],
                                    height: 1.8,
                                  ),
                                  children: [
                                    TextSpan(text: 'Será adicionado o valor de '),
                                    TextSpan(
                                      text: '$amount',
                                      style: GoogleFonts.nunito(fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text:
                                      ' ao valor selecionado, referente a Taxa de Conveniência.',
                                    ),
                                  ],
                                ),
                              );
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(24),
                  child: CustomRaisedButton(
                    label: 'Continuar',
                    onPressed: _openPaymentCard,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  void _openPaymentCard() {
    final errorMessage = _controller.validateAmount();
    if (errorMessage != null) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) => CustomInfoDialog(
          title: 'Valor inválido',
          message: errorMessage,
          dismissButtonLabel: 'CORRIGIR',
        ),
      );
    } else {
      // TODO Descomentar quando o backend passar a retornar os cartões de pagamento salvos
      // Navigator.push(context, MaterialPageRoute(builder: (_) => PaymentCardPage()));

      Navigator.push(context, MaterialPageRoute(builder: (_) => NewPaymentCardPage()));
    }
  }
}
