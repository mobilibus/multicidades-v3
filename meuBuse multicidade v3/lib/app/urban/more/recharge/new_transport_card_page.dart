import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_marginable_divider.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_raised_button.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_text_form_field.dart';
import 'package:mobilibus/app/urban/more/recharge/controller/new_transport_card_controller.dart';
import 'package:mobilibus/app/urban/more/recharge/transport_card_statement_page.dart';
import 'package:mobilibus/main.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:mobilibus/utils/formatting_utils.dart';

class NewTransportCardPage extends StatefulWidget {
  @override
  _NewTransportCardPageState createState() => _NewTransportCardPageState();
}

class _NewTransportCardPageState extends State<NewTransportCardPage> {
  final _formKey = GlobalKey<FormState>();
  NewTransportCardController _controller;
  final cpfFN = FocusNode();
  final transportCardFN = FocusNode();

  final labelTextStyle = GoogleFonts.nunito(
    color: Color(0xff616161),
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );

  @override
  void initState() {
    super.initState();
    _controller = NewTransportCardController(getPurchaseRepositoryInstance());
    _controller.getLastUsedTransportCard().then((_) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        centerTitle: true,
        elevation: 0,
        title: Text(
          'Adicionar cliente',
          style: GoogleFonts.nunito(fontSize: 20, fontWeight: FontWeight.w700),
        ),
      ),
      backgroundColor: Theme.of(context).accentColor,
      body: Form(
        key: _formKey,
        child: ListView(
          padding: const EdgeInsets.all(32),
          physics: BouncingScrollPhysics(),
          children: [
            Text(
              'Dados do passageiro',
              style: GoogleFonts.nunito(
                fontSize: 22,
                fontWeight: FontWeight.w700,
                color: Color(0xff424242),
              ),
            ),
            SizedBox(height: 16),
            Text('CPF', style: labelTextStyle),
            CustomTextFormField(
              inputFormatters: [MaskTextInputFormatter(mask: FormattingUtils.cpfMask)],
              inputType: TextInputType.number,
              editingController: _controller.cpfController,
              validator: _controller.validateCPF,
              focusNode: cpfFN,
              onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(transportCardFN),
            ),
            CustomMarginableDivider(verticalMargin: 32),
            Text(
              'Cartão',
              style: GoogleFonts.nunito(
                fontSize: 22,
                fontWeight: FontWeight.w700,
                color: Color(0xff424242),
              ),
            ),
            SizedBox(height: 16),
            Text('Número', style: labelTextStyle),
            CustomTextFormField(
              inputType: TextInputType.number,
              editingController: _controller.transportCardController,
              validator: _controller.validateCardNumber,
              focusNode: transportCardFN,
              onFieldSubmitted: (_) => _openNextScreen(),
            ),
            SizedBox(height: 32),
            CustomRaisedButton(
              label: 'Entrar',
              onPressed: _openNextScreen,
            ),
            SizedBox(height: 20,),
            SizedBox(
              child: Text('Atenção! Seu crédito estará disponível em até 30 minutos '+
                  'mediante apresentação do cartão no validador.',
                style: GoogleFonts.nunito(
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                  color: Color(0xff424242),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future _openNextScreen() async {
    if (_formKey.currentState.validate()) {
      await _controller.saveTransportCardData();
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => TransportCardStatementPage(
            cpf: _controller.cpfController.text,
            cardNumber: int.parse(_controller.transportCardController.text),
          ),
        ),
      );
    }
  }
}
