import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_circular_progress_indicator.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_date_set_dialog.dart';
import 'package:mobilibus/app/urban/more/recharge/components/detailed_statement_widget.dart';
import 'package:mobilibus/app/urban/more/recharge/controller/card_detailed_statement_controller.dart';
import 'package:mobilibus/app/urban/more/recharge/data/user_transport_card_model.dart';
import 'package:mobilibus/main.dart';
import 'package:mobilibus/transdata/data/model/transdata_extract.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:mobilibus/utils/date_number_utils.dart';

class TransportCardDetailedStatementPage extends StatefulWidget {
  final String statementDate;
  final List<TransdataTransaction> transactions;
  final UserTransportCardModel transportCard;

  const TransportCardDetailedStatementPage({
    Key key,
    @required this.statementDate,
    @required this.transactions,
    @required this.transportCard,
  }) : super(key: key);

  @override
  _TransportCardDetailedStatementPageState createState() =>
      _TransportCardDetailedStatementPageState();
}

class _TransportCardDetailedStatementPageState extends State<TransportCardDetailedStatementPage> {
  CardDetailedStatementController _controller;

  @override
  void initState() {
    super.initState();
    _controller = CardDetailedStatementController(getPurchaseRepositoryInstance());
    _controller.initTransportCard(widget.transportCard);
    _controller.initStatementDate(widget.statementDate);
    _controller.initTransactions(widget.transactions);
  }

  @override
  Widget build(BuildContext context) {
    final buttonDecoration = BoxDecoration(
      border: Border.all(color: Color(0xff9e9e9e)),
      borderRadius: BorderRadius.circular(15),
    );

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        centerTitle: true,
        elevation: 0,
        title: Text(
          'Histórico',
          style: GoogleFonts.nunito(fontSize: 20, fontWeight: FontWeight.w700),
        ),
      ),
      backgroundColor: Theme.of(context).accentColor,
      body: ListView(
        padding: const EdgeInsets.all(32),
        physics: BouncingScrollPhysics(),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                decoration: buttonDecoration,
                child: IconButton(
                  icon: Icon(Icons.west, color: buttonDecoration.color),
                  onPressed: _controller.moveToPreviousDate,
                ),
              ),
              SizedBox(width: 14),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(12),
                  decoration: buttonDecoration,
                  child: Center(
                    child: ValueListenableBuilder<String>(
                      valueListenable: _controller.statementDate,
                      builder: (_, value, __) => GestureDetector(
                        child: Text(
                          DateTime.parse(value).day.toString().padLeft(2,'0') + '/' +
                              DateTime.parse(value).month.toString().padLeft(2,'0') + '/' +
                              DateTime.parse(value).year.toString()
                        ,//DateMOBUtils.getShortDate(value),
                          style: GoogleFonts.nunito(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            color: Color(0xff212121).withOpacity(0.8),
                          ),
                        ),
                        onTap: _openDatePicker,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 14),
              Container(
                decoration: buttonDecoration,
                child: IconButton(
                  icon: Icon(Icons.east, color: buttonDecoration.color),
                  onPressed: _controller.moveToNextDate,
                ),
              ),
            ],
          ),
          SizedBox(height: 32),
          AnimatedBuilder(
              animation: _controller,
              builder: (_, __) {
                if (_controller.isLoading) {
                  return CustomCircularProgressIndicator();
                }

                if (_controller.transactions.value == null ||
                    _controller.transactions.value.isEmpty) {
                  return Center(
                    child: Text(
                      'Nenhum registro para esta data',
                      style: GoogleFonts.nunito(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                        color: Color(0xff212121),
                      ),
                    ),
                  );
                }

                return DetailedStatementWidget(transactions: _controller.transactions.value);
              })
        ],
      ),
    );
  }

  void _openDatePicker() async {
    var pickedDateTime = await showDialog<DateTime>(
        context: this.context, barrierDismissible: false, builder: (_) => CustomDateSetDialog());

    if (pickedDateTime != null) {
      _controller.moveToPickedDate(pickedDateTime);
    }
  }
}