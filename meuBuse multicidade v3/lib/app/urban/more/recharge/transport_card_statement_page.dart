import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/components/balance_board_widget.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_circular_progress_indicator.dart';
import 'package:mobilibus/app/urban/more/recharge/components/summarized_statement_widget.dart';
import 'package:mobilibus/app/urban/more/recharge/controller/transport_card_statement_controller.dart';
import 'package:mobilibus/main.dart';
import 'package:mobilibus/utils/Utils.dart';

class TransportCardStatementPage extends StatefulWidget {
  final String cpf;
  final int cardNumber;

  const TransportCardStatementPage({
    Key key,
    @required this.cpf,
    @required this.cardNumber,
  }) : super(key: key);
  @override
  _TransportCardStatementPageState createState() => _TransportCardStatementPageState();
}

class _TransportCardStatementPageState extends State<TransportCardStatementPage> {
  TransportCardStatementController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TransportCardStatementController(getPurchaseRepositoryInstance());
    _controller.getBalanceFromTransportCard(widget.cpf, widget.cardNumber);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        centerTitle: true,
        elevation: 0,
        title: Text(
          'Saldo e recarga',
          style: GoogleFonts.nunito(fontSize: 20, fontWeight: FontWeight.w700),
        ),
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: AnimatedBuilder(
          animation: _controller,
          builder: (context, _) {
            if (_controller.isLoading) {
              return CustomCircularProgressIndicator();
            }

            if (_controller.data.balanceValue.hasError && _controller.data.cardExtract.hasError) {
              return _drawInvalidData();
            }

            return _drawBalanceAndExtract();
          }),
    );
  }

  Widget _drawBalanceAndExtract() {
    int balanceAmount;
    String balanceDate;

    if (_controller.data.balanceValue.data != null &&
        _controller.data.balanceValue.data.isNotEmpty) {
      balanceAmount = _controller.data.balanceValue.data.first.balance;
      balanceDate = _controller.data.balanceValue.data.first.balanceDate;
    }

    return ListView(
      physics: BouncingScrollPhysics(),
      padding: const EdgeInsets.all(32),
      children: [

        BalanceBoardWidget(balanceAmount: balanceAmount, updatedAt: balanceDate),
        SizedBox(height: 32),
        SummarizedStatementWidget(
          transportCard: _controller.data.transportCard,
          statementDate: _controller.data.balanceValue.data.first.balanceDate,
          transactions: _controller.data.cardExtract.data,
        ),

      ],
    );
  }

  Widget _drawInvalidData() {
    return Container(
      color: Colors.white,
      child: Center(
        child: Text(
          'Dados incorretos. Por favor revise e tente novamente.',
          textAlign: TextAlign.center,
          style: GoogleFonts.nunito(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}