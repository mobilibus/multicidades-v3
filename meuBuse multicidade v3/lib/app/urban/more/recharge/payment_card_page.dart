import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_flat_button.dart';
import 'package:mobilibus/app/urban/more/recharge/new_payment_card_page.dart';

class PaymentCardPage extends StatefulWidget {
  @override
  _PaymentCardPageState createState() => _PaymentCardPageState();
}

class _PaymentCardPageState extends State<PaymentCardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
              width: double.infinity,
              child: Row(
                children: [
                  IconButton(
                    padding: const EdgeInsets.all(0),
                    iconSize: 28,
                    icon: Icon(Icons.west),
                    onPressed: () => Navigator.pop(context),
                  ),
                  Spacer()
                ],
              ),
            ),
            Container(
              width: double.infinity,
              child: SingleChildScrollView(
                padding: const EdgeInsets.symmetric(horizontal: 32),
                physics: BouncingScrollPhysics(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Pagamento',
                      style: GoogleFonts.nunito(
                        fontSize: 32,
                        fontWeight: FontWeight.w700,
                        color: Colors.grey[800],
                      ),
                    ),
                    SizedBox(height: 24),
                    Text(
                      'Selecione a forma de pagamento',
                      style: GoogleFonts.nunito(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.grey[700],
                        height: 1.8,
                      ),
                    ),
                    SizedBox(height: 24),
                    // TODO Implementar listagem dos cartões cadastrados vindos do backend
                    SizedBox(height: 32),
                    Center(
                      child: CustomFlatButton(
                        label: '+ Adicionar novo',
                        onTap: _openNewPaymentCard,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _openNewPaymentCard() {
    Navigator.push(context, MaterialPageRoute(builder: (_) => NewPaymentCardPage()));
  }
}
