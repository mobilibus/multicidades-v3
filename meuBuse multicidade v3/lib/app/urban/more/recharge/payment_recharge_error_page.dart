import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/data/transdata_payment_recharge_result.dart';
import 'package:mobilibus/utils/Utils.dart';

class PaymentRechargeErrorPage extends StatefulWidget {
  final TransdataPaymentRechargeResult result;

  const PaymentRechargeErrorPage({
    Key key,
    @required this.result,
  }) : super(key: key);

  @override
  _PaymentRechargeErrorPageState createState() => _PaymentRechargeErrorPageState();
}

class _PaymentRechargeErrorPageState extends State<PaymentRechargeErrorPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Utils.getColorFromPrimary(context),
          centerTitle: true,
          elevation: 0,
          title: Text(
            'Pagamento',
            style: GoogleFonts.nunito(fontSize: 20, fontWeight: FontWeight.w700),
          ),
        ),
        backgroundColor: Colors.red[900],
        body: Container(
          padding: const EdgeInsets.all(32),
          width: double.infinity,
          height: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                _getErrorMessage(),
                style: GoogleFonts.nunito(
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  color: Theme.of(context).accentColor,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 32),
              Icon(Icons.dangerous, size: 100, color: Colors.white),
            ],
          ),
        ),
      ),
      onWillPop: () async {
        Navigator.pop(context);
        return true;
      },
    );
  }

  String _getErrorMessage() {
    switch (widget.result.status) {
      case RechargeResultStatus.PAYMENT_ERROR:
        return 'Desculpe! Sua transação não foi concluída.\nVerifique os dados preenchidos, e se você possui saldo disponível, e tente novamente!';
        break;
      case RechargeResultStatus.RECHARGE_ERROR:
        return 'Desculpe! Não foi possível concluir o processo de recarga.\nA cobrança em seu cartão, caso realizada, será estornada dentro de instantes.\nTente novamente!';
        break;
      default:
        return 'Ocorreu um erro no processamento. Por favor, tente novamente mais tarde!';
    }
  }
}
