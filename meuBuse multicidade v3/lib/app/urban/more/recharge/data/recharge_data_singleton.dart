import 'package:mobilibus/transdata/data/model/transdata_products.dart';

class RechargeDataSingleton {
  static RechargeDataSingleton _instance;

  String cpf;
  int transportCardNumber;
  TransdataProduct product;
  int productTaxAmount;
  int rechargeAmount;
  String paymentCardNumber;
  String paymentCardExpiration;
  String paymentCardCVV;
  String paymentCardOwner;

  RechargeDataSingleton._();

  static RechargeDataSingleton getInstance() {
    if (_instance == null) {
      _instance = RechargeDataSingleton._();
    }

    return _instance;
  }

  static void clearInstance() => _instance = RechargeDataSingleton._();

  String get paymentCardNumberLast4Digits =>
      '***${this.paymentCardNumber.substring(this.paymentCardNumber.length - 4)}';
}
