class UserTransportCardModel {
  final String cpf;
  final int cardNumber;

  UserTransportCardModel({this.cpf, this.cardNumber});
}
