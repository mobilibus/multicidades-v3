import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_flat_button.dart';
import 'package:mobilibus/app/urban/more/recharge/new_transport_card_page.dart';

class TransportCardSelectionPage extends StatefulWidget {
  @override
  _TransportCardSelectionPageState createState() => _TransportCardSelectionPageState();
}

class _TransportCardSelectionPageState extends State<TransportCardSelectionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          'Selecionar cartão',
          style: GoogleFonts.nunito(fontSize: 20, fontWeight: FontWeight.w700),
        ),
      ),
      backgroundColor: Theme.of(context).accentColor,
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(32),
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            Text(
              'Selecione o cartão o qual deseja ver o saldo ou fazer a recarga de créditos:',
              style: GoogleFonts.nunito(fontSize: 16, fontWeight: FontWeight.w400, height: 1.8),
            ),
            SizedBox(height: 20),
            // TODO Implementar aqui a exibição dos cartões retornados pelo backend
            // ListView.separated(
            //   itemCount: 2,
            //   physics: NeverScrollableScrollPhysics(),
            //   shrinkWrap: true,
            //   separatorBuilder: (_, __) => SizedBox(height: 16),
            //   itemBuilder: (_, i) => TransportCardWidget(
            //     onTap: () => _openCardStatement(context),
            //   ),
            // ),
            SizedBox(height: 32),
            CustomFlatButton(
              label: '+ Adicionar novo',
              onTap: _openAddTransportCard,
            ),
          ],
        ),
      ),
    );
  }

  void _openAddTransportCard() {
    Navigator.push(context, MaterialPageRoute(builder: (_) => NewTransportCardPage()));
  }

  // void _openCardStatement() {
  //   Navigator.push(
  //       context, MaterialPageRoute(builder: (_) => CardStatementPage()));
  // }
}
