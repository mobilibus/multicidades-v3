import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/app/urban/departure/departures.dart';
import 'package:mobilibus/base/MOBInformation.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:url_launcher/url_launcher.dart';

class FullInformation extends StatefulWidget {
  FullInformation(this.information);
  final MOBInformation information;

  _FullInformation createState() => _FullInformation(information);
}

class _FullInformation extends State<FullInformation> {
  _FullInformation(this.information) : super();
  final MOBInformation information;

  Future<void> getDepartures(MOBAlertStop stop) async {
    int stopId = stop.stopId;
    String stopName = stop.name;
    String stopCode = stop.code;
    MOBStop mobStop = MOBStop(stopId, null, null, null, stopName, 3, stopCode);
    DeparturesView dpView = DeparturesView(mobStop);
    await showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      enableDrag: false,
      builder: (context) => Container(
        height: (MediaQuery.of(context).size.height / 8) * 7,
        child: dpView,
      ),
    ).then((_) => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    int activeFrom = information.activeFrom;
    int activeTo = information.activeTo;
    int timezoneOffset = information.timezoneOffset;

    DateTime dtFrom =
        DateTime.fromMillisecondsSinceEpoch(activeFrom, isUtc: true);
    DateTime dtTo = DateTime.fromMillisecondsSinceEpoch(activeTo, isUtc: true);

    if (timezoneOffset > 0) {
      dtFrom = dtFrom.add(Duration(hours: timezoneOffset));
      dtTo = dtTo.add(Duration(hours: timezoneOffset));
    } else {
      dtFrom = dtFrom.subtract(Duration(hours: timezoneOffset));
      dtTo = dtTo.subtract(Duration(hours: timezoneOffset));
    }

    DateFormat dateFormat = DateFormat('dd/MM/yyyy');

    String from = dateFormat.format(dtFrom);
    String to = dateFormat.format(dtTo);

    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);

    String effect;
    String title;
    String content;

    List<String> sName = [];
    List<MOBLine> lines = [];
    List<MOBAlertStop> stops = [];
    List<int> agencyIds = [];

    if (information is MOBAlert) {
      MOBAlert alert = information;

      MOBAlertContent alertContent = alert.alertContent;
      //MOBAlertContentLanguage alertContentLanguage = alertContent.alertContentLanguages;
      //.firstWhere((a) => a.language == 'pt');
      List<MOBAlertContentLanguage> alertContentLanguages =
          alertContent.alertContentLanguages;

      title = alertContentLanguages[0].title;
      content = alertContentLanguages[0].content;
      String url = alertContentLanguages[0].url;

      content = '$content\n\n$url';

      MOBAlertAffectedEntities affectedEntities = alert.alertAffectedEntities;
      List<int> routeIds = affectedEntities.routeIds;
      stops = affectedEntities.stops;
      agencyIds = affectedEntities.agencyIds;

      List<MOBLine> mLines = Utils.lines;
      int indexC = 0;
      for (final line in mLines) {
        int routeId = line.routeId;
        if (routeIds.contains(routeId)) {
          String sNameR = mLines[indexC].shortName;
          lines.add(line);
          sName.add(sNameR);
        }
        indexC++;
      }

      effect = alert.effect;
    } else if (information is MOBNews) {
      MOBNews news = information;

      title = news.title;
      content = news.content;
    }

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        centerTitle: true,
        title: Text('Informação', style: textStyle),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: themeColor),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            //crossAxisAlignment: CrossAxisAlignment.start,
            //mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              if (effect != null && effect != 'INFORMAÇÃO')
                Container(
                  color: Theme.of(context).accentColor,
                  child: Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Container(
                      width: MediaQuery.of(context).size.width - 20,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.info,
                            size: 40,
                            color: Utils.getColorByLuminanceTheme(context),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width - 80,
                            child: Container(
                              width: MediaQuery.of(context).size.width - 10,
                              child: Text(
                                effect,
                                style: TextStyle(
                                    color:
                                        Utils.getColorByLuminanceTheme(context),
                                    fontSize: 24),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(bottom: 5),
                      child: Text(title.toUpperCase(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 24)),
                    ),
                    Linkify(
                      text: content,
                      onOpen: (link) async {
                        if (await canLaunch(link.url)) await launch(link.url);
                      },
                    ),
                    Container(
                      //padding: EdgeInsets.only(top: 20),

                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          if (lines.isNotEmpty)
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Divider(),
                                Text('LINHAS AFETADAS',
                                    textAlign: TextAlign.start),
                                Container(
                                  //width: MediaQuery.of(context).size.width,
                                  child: GridView.builder(
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 6,
                                      crossAxisSpacing: 18,
                                      //mainAxisSpacing: 10
                                      //childAspectRatio: 2
                                    ),
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount: lines.length,
                                    itemBuilder: (context, index) {
                                      MOBLine line = lines[index];
                                      String shortName = line.shortName;
                                      String color = line.color;
                                      color = 'ff' + color.replaceAll('#', '');
                                      int iColor = int.parse(color, radix: 16);

                                      return InkWell(
                                        onTap: () async =>
                                            await Utils.selectedLineBehaviour(
                                                line, false, context, true),
                                        child: Container(
                                          //padding: EdgeInsets.all(5),
                                          child: Row(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                  left: 10,
                                                  right: 10,
                                                  top: 8,
                                                  bottom: 8,
                                                ),
                                                child: Text('$shortName'),
                                                color: Color(iColor),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          if (stops.isNotEmpty)
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Divider(),
                                Row(
                                  children: [
                                    SvgPicture.asset(
                                      Utils.globalPinBusStop,
                                      height: 30,
                                    ),
                                    Text(' PONTOS DE PARADA AFETADOS'),
                                  ],
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: stops.length,
                                    shrinkWrap: true,
                                    itemBuilder: (context, index) {
                                      MOBAlertStop stop = stops[index];
                                      String name = stop.name;
                                      String code = stop.code;
                                      double width =
                                          MediaQuery.of(context).size.width;
                                      double widthPart = (width / 4) * 3;
                                      String codeName =
                                          '${code == null ? '' : '$code - '}$name';
                                      return InkWell(
                                        onTap: () => getDepartures(stop),
                                        child: Container(
                                          padding: EdgeInsets.all(5),
                                          child: Row(
                                            children: [
                                              Container(
                                                //padding: EdgeInsets.only(
                                                //  left: 10,
                                                //  right: 10,
                                                //),
                                                width: widthPart,
                                                child: Text(codeName),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                          if (false) //(agencyIds.isNotEmpty)
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Divider(),
                                Text('Agências afetadas'),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: agencyIds.length,
                                    shrinkWrap: true,
                                    itemBuilder: (context, index) {
                                      int agencyId = agencyIds[index];
                                      String agencyName = Utils.project.agencies
                                          .firstWhere((agency) =>
                                              agency.agencyId == agencyId)
                                          .name;
                                      return Container(
                                        padding: EdgeInsets.all(5),
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.business,
                                              size: 30,
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                left: 10,
                                                right: 10,
                                              ),
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  120,
                                              child: Text(agencyName),
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
