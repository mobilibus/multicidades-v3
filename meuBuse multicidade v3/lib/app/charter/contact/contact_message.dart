import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class ContactMessageCharter extends StatefulWidget {
  _ContactMessageCharter createState() => _ContactMessageCharter();
}

class _ContactMessageCharter extends State<ContactMessageCharter> {
  TextEditingController tffName = TextEditingController();
  TextEditingController tffMail = TextEditingController();
  TextEditingController tffPhone = TextEditingController();
  TextEditingController tffMsg = TextEditingController();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool isLoading = false;
  void send() async {
    if (formKey.currentState.validate()) {
      isLoading = true;
      setState(() {});

      String name = tffName.value.text;
      String mail = tffMail.value.text;
      String phone = tffPhone.value.text;
      String msg = tffMsg.value.text;

      name = name.isEmpty ? null : name;
      mail = mail.isEmpty ? null : mail;
      phone = phone.isEmpty ? null : phone;

      Map<String, dynamic> jsonPost = {
        'projectId': Utils.isMobilibus ? -1 : Utils.project.projectId,
        'name': name,
        'email': mail,
        'phone': phone,
        'message': msg,
        'attachment': null,
      };
      String bodyParams = convert.json.encode(jsonPost);
      http.Response response = await MOBApiUtils.sendMessage(
          Utils.project.projectId, bodyParams, context);

      String body = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(body);
      bool success = map.containsKey('success') ? map['success'] : false;
      if (success) {
        Navigator.of(context).pop();
        dialogSuccess();
      } else
        dialogError();

      isLoading = false;
      setState(() {});
    }
  }

  void dialogSuccess() => showDialog(
        context: context,
        useRootNavigator: false,
        barrierDismissible: false,
        builder: (dialogContext) => AlertDialog(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            'Sucesso!',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          content: Text(
            'Recebemos sua mensagem, obrigado!',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Fechar',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              ),
              onPressed: () {
                Navigator.of(dialogContext).pop();
              },
            )
          ],
        ),
      );

  void dialogError() => showDialog(
        context: context,
        builder: (dialogContext) => AlertDialog(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text(
            'Erro!',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          content: Text(
            'Não foi possível enviar sua mensagem, tente novamente mais tarde.',
            style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Fechar',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              ),
              onPressed: () => Navigator.of(dialogContext).pop(),
            )
          ],
        ),
      );

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Entre em contato', style: textStyle),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: themeColor),
          onPressed: () => Navigator.pop(context, () => setState(() {})),
        ),
      ),
      floatingActionButton: isLoading
          ? null
          : FloatingActionButton(
              onPressed: () => send(),
              child: Icon(Icons.send),
            ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(
                  strokeWidth: 3,
                  valueColor: AlwaysStoppedAnimation<Color>(
                      Utils.isLightTheme(context)
                          ? Utils.getColorFromPrimary(context)
                          : Colors.black)),
            )
          : ListView(
              children: <Widget>[
                Form(
                  key: formKey,
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          validator: (text) =>
                              text.length == 0 ? 'Campo obrigatório' : null,
                          controller: tffName,
                          maxLines: 1,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            labelText: 'Nome (Obrigatório)',
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              ),
                            ),
                            filled: true,
                          ),
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          validator: (text) {
                            Pattern pattern =
                                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
                            RegExp regex = RegExp(pattern);
                            if (!regex.hasMatch(text))
                              return 'E-mail inválido';
                            else
                              return null;
                          },
                          maxLines: 1,
                          controller: tffMail,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            labelText: 'E-mail (Obrigatório)',
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              ),
                            ),
                            filled: true,
                          ),
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          maxLines: 1,
                          controller: tffPhone,
                          keyboardType: TextInputType.phone,
                          inputFormatters: <TextInputFormatter>[
                            LengthLimitingTextInputFormatter(14),
                            FilteringTextInputFormatter.digitsOnly,
                            FilteringTextInputFormatter.singleLineFormatter,
                          ],
                          decoration: InputDecoration(
                            labelText: 'Telefone (Opcional)',
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              ),
                            ),
                            filled: true,
                          ),
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          validator: (text) =>
                              text.length == 0 ? 'Campo obrigatório' : null,
                          controller: tffMsg,
                          keyboardType: TextInputType.multiline,
                          decoration: InputDecoration(
                            labelText: 'Mensagem (Obrigatório)',
                            border: OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              ),
                            ),
                            filled: true,
                          ),
                          maxLines: 5,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 100),
                ),
              ],
            ),
    );
  }
}
