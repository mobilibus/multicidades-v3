import 'dart:async';
import 'dart:convert' as convert;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_boom_menu/flutter_boom_menu.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as gm;
import 'package:google_maps_utils/spherical_utils.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:marquee/marquee.dart';
import 'package:mobilibus/base/MOBCharterCompany.dart';
import 'package:mobilibus/base/MOBDeparture.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/app/charter/builder/CharterRouteBuilder.dart';
import 'package:mobilibus/app/charter/charter_help.dart';
import 'package:mobilibus/app/charter/charter_login.dart';
import 'package:mobilibus/app/charter/contact/contact_message.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'information_central.dart';

class CharterView extends StatefulWidget {
  CharterView(this.charterType, this.isLogin, this.routes);
  final CHARTER_TYPE charterType;
  final bool isLogin;
  final Iterable routes;

  _CharterView createState() => _CharterView(charterType, isLogin, routes);
}

class _CharterView extends State<CharterView> with TickerProviderStateMixin {
  _CharterView(this.charterType, this.isLogin, this.routes) : super();
  final CHARTER_TYPE charterType;
  final bool isLogin;
  final Iterable routes;

  Timer timer;
  Point<double> myLocation;
  final Map<MOBStop, List<MOBDepartureTrip>> departuresByStop = {};
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final PageController carouselController =
      PageController(initialPage: 0, viewportFraction: 0.7);
  TextEditingController tecRadius;
  String company = '';
  String login = '';
  int noDeparturesCounter = 0;
  double radius = 1000;

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  void initState() {
    if (Utils.isMobilibus) tecRadius = TextEditingController(text: '1000.0');
    super.initState();
    Timer.periodic(Duration(seconds: 1), (timer) {
      int departures = 0;
      departuresByStop.values.forEach(
          (dt) => dt.forEach((dt) => departures += dt.departures.length));

      if (departures == 0)
        noDeparturesCounter++;
      else
        noDeparturesCounter = 0;
    });
    timer = Timer.periodic(
        Duration(seconds: Utils.updateDelay), (t) => loadNextDepartures());
    loadNextDepartures();
    Future.delayed(Duration(), () async {
      try {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        String jsonString = sharedPreferences
            .getString('charter_login_${Utils.project.projectId}');
        Map<String, dynamic> map = convert.json.decode(jsonString);
        company = map['agency'];
        login = map['login'];
        setState(() {});
      } catch (e) {}
    });
  }

  void loadNextDepartures() async {
    Location location = Location();
    bool hasPermission =
        (await location.hasPermission() == PermissionStatus.granted);
    hasPermission =
        (await location.requestPermission() == PermissionStatus.granted);
    if (Utils.isMobilibus || hasPermission) {
      double lat = myLocation?.x ?? 0.0;
      double lng = myLocation?.y ?? 0.0;

      if (hasPermission) {
        LocationData locationData = Utils.locationData;
        lat = locationData.latitude;
        lng = locationData.longitude;
      }
      myLocation = Point<double>(lat, lng);

      GMULatLngBounds bounds = SphericalUtils.toBounds(lat, lng, radius);

      List<int> tripIds = [];
      if (routes != null)
        tripIds.addAll(routes.map((route) {
          Iterable mTripIds = route['tripIds'];
          List<int> mnTripIds = mTripIds.map<int>((tripId) => tripId).toList();
          tripIds.addAll(mnTripIds);
        }).toList());
      tripIds.removeWhere((element) => element == null);
      String url = MOBApiUtils.getStopsInViewUrl(bounds, tripIds);

      http.Response response = await MOBApiUtils.getStopsFuture(url);
      if (response != null) {
        String body = convert.utf8.decode(response.bodyBytes);
        Iterable list = convert.json.decode(body);
        List<MOBStop> stops = list
            .map((model) => MOBStop.fromJson(model, [Utils.project.projectId]))
            .toList();

        stops.removeWhere((stop) => stop == null || stop.stopType == 8);

        for (final stop in stops) {
          int stopType = stop.stopType;
          if (stopType < 8) {
            Point stopLatLng = Point(stop.latitude, stop.longitude);
            Point myPos = Point(lat, lng);

            double distance =
                SphericalUtils.computeDistanceBetween(myPos, stopLatLng);
            double degrees = SphericalUtils.computeHeading(myPos, stopLatLng);
            stop.distance = distance;
            stop.degrees = degrees;
          }
        }

        stops.sort((a, b) => a.distance.compareTo(b.distance));

        departuresByStop.clear();

        if (Utils.isMobilibus) {
          DateTime start = DateTime.now();

          for (final stop in stops) await updateDeparture(stop, tripIds);

          DateTime end = DateTime.now();

          Duration diff = end.difference(start);

          String strDiff = diff.inSeconds.toString();

          scaffoldKey.currentState.showSnackBar(SnackBar(
              content: Text(
                  'Raio: ${radius ~/ 1000}km\nProcessamento de todos pontos: $strDiff segundos')));
        } else
          for (final stop in stops) await updateDeparture(stop, tripIds);

        setState(() {});
      }
    }
  }

  Future<void> updateDeparture(MOBStop stop, List<int> tripIds) async {
    http.Response response = await MOBApiUtils.getDepartures(stop.stopId);
    if (response != null) {
      try {
        String body = convert.utf8.decode(response.bodyBytes);

        Map<String, dynamic> object = convert.json.decode(body);
        MOBDepartures departureResult = MOBDepartures.fromJson(object);
        List<MOBDepartureTrip> mDepartures = [];
        mDepartures.addAll(departureResult.departuresToday);
        mDepartures.addAll(departureResult.departuresNextDay);

        if (tripIds.isNotEmpty)
          mDepartures
              .removeWhere((departure) => !tripIds.contains(departure.tripId));

        departuresByStop[stop] = mDepartures;
      } catch (e) {}
    }
  }

  void goToMap(int routeId, int tripId, MOBStop stop) => CharterRouteBuilder(
        stop,
        myLocation,
        routeId,
        tripId,
        Theme.of(context).primaryColor,
        context,
        false,
      ).init();

  void goToLogin() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    int projectId = Utils.project.projectId;
    await sp.remove('charter_login_$projectId');
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => CharterLogin()));
  }

  void goToContact() => Navigator.of(context)
      .push(MaterialPageRoute(builder: (context) => ContactMessageCharter()));

  void goToInformation() {
    int routeId;
    if (charterType == CHARTER_TYPE.COMPANY_SELECT ||
        routes != null && routes.isNotEmpty) {
      Map mRoute = routes.first;
      routeId = mRoute['routeId'] ?? null;
    }
    List<int> tripIds = [];
    if (routeId != null) {
      Map<String, dynamic> route = routes.first;
      tripIds = route['tripIds'].map<int>((tripId) => tripId as int).toList();
    }
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => InformationCentral(routeId, tripIds)));
  }

  Widget boomMenu() => BoomMenu(
        animatedIcon: AnimatedIcons.menu_close,
        backgroundColor: Color(0xff229fd5),
        animatedIconTheme: IconThemeData(size: 22.0),
        overlayColor: Color(0xff25303E),
        overlayOpacity: 0.9,
        children: [
          if (Utils.isMobilibus && charterType == CHARTER_TYPE.LOGIN)
            MenuItem(
              title: 'Usuário Logado',
              titleColor: Colors.black,
              subtitle: '$company - $login',
              subTitleColor: Colors.black,
              backgroundColor: Colors.grey,
              child: Icon(Icons.verified_user,
                  color: Theme.of(context).canvasColor),
            ),
          MenuItem(
            title: 'Central do Cliente',
            titleColor: Colors.white,
            subtitle: 'Notícias e Informações sobre sua rota',
            subTitleColor: Colors.white,
            backgroundColor: Color(0xff229fd5),
            child: Icon(Icons.info, color: Theme.of(context).canvasColor),
            onTap: goToInformation,
          ),
          MenuItem(
            title: 'Entre em Contato',
            titleColor: Colors.white,
            subtitle: 'Envie sua sugestão ou reclamação',
            subTitleColor: Colors.white,
            backgroundColor: Color(0xff229fd5),
            child:
                Icon(Icons.contact_mail, color: Theme.of(context).canvasColor),
            onTap: goToContact,
          ),
          if (Utils.isMobilibus)
            MenuItem(
              title: 'Controle de Acesso',
              titleColor: Colors.white,
              subtitle: 'Seu cartão de embarque no veículo',
              subTitleColor: Colors.white,
              backgroundColor: Color(0xff229fd5),
              child: Icon(Icons.perm_identity,
                  color: Theme.of(context).canvasColor),
            ),
          MenuItem(
            title: 'Ajuda',
            titleColor: Colors.white,
            subtitle: 'Como usar o app',
            subTitleColor: Colors.white,
            backgroundColor: Color(0xff229fd5),
            child: Icon(Icons.help, color: Theme.of(context).canvasColor),
            onTap: () => Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => CharterHelp())),
          ),
          MenuItem(
            title: 'Sair',
            titleColor: Colors.white,
            subtitle: '',
            subTitleColor: Colors.white,
            backgroundColor: Color(0xff229fd5),
            child: Icon(
              Icons.exit_to_app,
              color: Theme.of(context).canvasColor,
            ),
            onTap: isLogin
                ? goToLogin
                : () => SystemChannels.platform
                    .invokeMethod<void>('SystemNavigator.pop'),
          ),
        ],
      );

  Widget drawer(List<MOBStop> stops) => Theme(
        data: Theme.of(context).copyWith(canvasColor: Colors.transparent),
        child: Drawer(
          child: ListView.builder(
            padding: EdgeInsets.only(
              top: kToolbarHeight,
              bottom: kToolbarHeight,
            ),
            itemCount: stops.length,
            itemBuilder: (context, index) {
              MOBStop stop = stops[index];
              String name = stop.name;
              int stopId = stop.stopId;
              double distance = stop.distance;
              return Container(
                child: Card(
                  elevation: 5,
                  child: Container(
                    child: ListTile(
                      onTap: () {},
                      title: Text('(stopId: $stopId)\n$name'),
                      subtitle:
                          Text('Distância: ${distance.toStringAsFixed(2)}m'),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      );

  Widget listDepartures(List<MOBDepartureTrip> departureTrips, MOBStop stop) =>
      ListView.separated(
        shrinkWrap: true,
        separatorBuilder: (context, index) => Container(
          padding: EdgeInsets.only(left: 50, right: 50),
          child: Divider(
            color: Theme.of(context).canvasColor,
          ),
        ),
        padding: EdgeInsets.only(bottom: 5, top: 5),
        itemCount: departureTrips.length,
        itemBuilder: (context, index) {
          MOBDepartureTrip departureTrip = departureTrips[index];
          MOBDeparture departure = departureTrip.departures.first;

          String time = departure.time.substring(0, 5);
          String vehicleId = departureTrip.departures.first.vehicleId;
          String headsign = departureTrip.headsign;
          int tripId = departureTrip.tripId;
          int routeId = departureTrip.routeId;

          int minutes = departureTrip.departures.first.minutes;
          if (minutes > 2 && minutes < 60)
            time = '$minutes min.';
          else if (minutes < 2) time = 'Aprox.';

          bool isNextDay = departure.nextDay;
          bool isOnline = departure.online;

          double width = MediaQuery.of(context).size.width / 3;
          return InkWell(
            onTap: () => goToMap(routeId, tripId, stop),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '$headsign${Utils.isMobilibus ? ' [$tripId]' : ''}',
                    style: TextStyle(fontSize: 12, color: Colors.white),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: width,
                        child: Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              if (isNextDay)
                                Container(
                                  child: Text(
                                    'Amanhã, ',
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              Container(
                                child: Text(
                                  time,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      if (isOnline)
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                'assets/images/bus.svg',
                                height: 20,
                              ),
                              Container(
                                width: width - 40,
                                padding: EdgeInsets.only(left: 5),
                                child: Text(
                                  vehicleId ?? '',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      );

  Widget departuresCorousel(List<MOBStop> stops) => Container(
        height: 200,
        child: PageView(
          controller: carouselController,
          children: stops.map(
            (stop) {
              List<MOBDepartureTrip> departureTrips = departuresByStop[stop];

              String stopName = stop.name;
              double distance = stop.distance;
              String strDistance = (distance > 1000
                      ? '>${distance ~/ 1000}k'
                      : distance.toStringAsFixed(0)) +
                  'm';
              double width = MediaQuery.of(context).size.width * 0.01;
              return Container(
                padding: EdgeInsets.only(left: width, right: width),
                child: Column(
                  children: <Widget>[
                    Card(
                      elevation: 0,
                      color: Color(0xff30394D),
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.white, width: 0.3),
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 15,
                              child: Marquee(
                                text: stopName,
                                blankSpace: 200.0,
                                velocity: 50.0,
                                numberOfRounds: 1,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                scrollAxis: Axis.horizontal,
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            Text(
                              'Distância: $strDistance' +
                                  (Utils.isMobilibus
                                      ? ' | ${distance.toStringAsFixed(2)}\m'
                                      : ''),
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                              ),
                            ),
                            Divider(
                              height: 1.0,
                              color: Theme.of(context).primaryColor,
                            ),
                            Container(
                              height: 130,
                              child: Scrollbar(
                                child: listDepartures(departureTrips, stop),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          ).toList(),
        ),
      );

  Widget debugWidgets(List<MOBStop> mStopsWithDepartures) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              if (departuresByStop.keys.isNotEmpty)
                FloatingActionButton.extended(
                  onPressed: () => scaffoldKey.currentState.openDrawer(),
                  label: Text(
                    'Pontos\nSem partidas',
                    textAlign: TextAlign.center,
                  ),
                  icon: SvgPicture.asset(
                    Utils.globalPinBusStop,
                    height: 20,
                  ),
                ),
              Container(
                child: Card(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: [
                        Text('${mStopsWithDepartures.length}'),
                        Icon(Icons.credit_card),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      );

  void pickPosition() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isDark = prefs.getBool('isDark') ?? false;
    gm.GoogleMapController gmc;
    gm.CameraPosition cameraPosition;
    double height = MediaQuery.of(context).size.height - kToolbarHeight;
    Point<double> location = await showModalBottomSheet(
      context: context,
      enableDrag: false,
      isScrollControlled: true,
      builder: (context) => StatefulBuilder(
        builder: (context, setState) => Container(
          height: height,
          child: Scaffold(
            floatingActionButton: FloatingActionButton(
              child: Icon(Icons.done),
              onPressed: () async {
                gm.LatLng latLng =
                    cameraPosition?.target ?? gm.LatLng(0.0, 0.0);
                double lat = latLng.latitude;
                double lng = latLng.longitude;
                Point<double> location = Point<double>(lat, lng);
                Navigator.of(context).pop(location);
              },
            ),
            body: Stack(
              alignment: Alignment.center,
              children: [
                gm.GoogleMap(
                  onMapCreated: (onGmc) {
                    gmc = onGmc;
                    Completer<gm.GoogleMapController>().complete(gmc);
                    rootBundle
                        .loadString(isDark
                            ? 'assets/maps_style_dark.json'
                            : 'assets/maps_style_default.json')
                        .then((style) => gmc.setMapStyle(style));

                    gmc.animateCamera(
                      gm.CameraUpdate.newLatLngZoom(
                        myLocation != null
                            ? gm.LatLng(myLocation.x, myLocation.y)
                            : gm.LatLng(0.0, 0.0),
                        1,
                      ),
                    );
                  },
                  onCameraMove: (onCameraMove) =>
                      setState(() => cameraPosition = onCameraMove),
                  circles: cameraPosition == null
                      ? null
                      : Set.of([
                          gm.Circle(
                            strokeColor: Colors.red,
                            strokeWidth: 2,
                            radius: radius,
                            circleId: gm.CircleId('CircleRadiusId'),
                            center: gm.LatLng(cameraPosition.target.latitude,
                                cameraPosition.target.longitude),
                          )
                        ]),
                  myLocationButtonEnabled: false,
                  myLocationEnabled: false,
                  initialCameraPosition: gm.CameraPosition(
                    zoom: 0.0,
                    target: myLocation != null
                        ? gm.LatLng(myLocation.x, myLocation.y)
                        : gm.LatLng(0.0, 0.0),
                  ),
                ),
                Icon(Icons.pin_drop),
                if (cameraPosition != null)
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('Latitude: ${cameraPosition.target.latitude}\n'
                          'Longitude: ${cameraPosition.target.longitude}'),
                    ],
                  ),
              ],
            ),
          ),
        ),
      ),
    );
    if (location != null) {
      myLocation = location;
      loadNextDepartures();
    }
  }

  Widget positionRadiusPicker() => SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              child: Card(
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 100,
                            child: TextField(
                              controller: tecRadius,
                              onChanged: (text) {
                                try {
                                  double newValue = double.parse(text);
                                  if (newValue >= 1000.0 && newValue <= 10000.0)
                                    setState(() => radius = newValue);
                                } catch (e) {}
                              },
                              maxLines: 1,
                              decoration: InputDecoration(hintMaxLines: 1),
                            ),
                          ),
                          Text('metros'),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text('1km'),
                          Container(
                            width: MediaQuery.of(context).size.width - 100,
                            child: Slider(
                              min: 1000.0,
                              max: 10000.0,
                              label: 'Raio: $radius',
                              value: radius,
                              onChanged: (value) => setState(() {
                                radius = value;
                                tecRadius.text = radius.toStringAsFixed(1);
                              }),
                            ),
                          ),
                          Text('10km'),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Card(
                    child: InkWell(
                      onTap: pickPosition,
                      child: Container(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          children: [
                            Icon(Icons.map),
                            if (myLocation != null)
                              Text('Latitude: ${myLocation.x}\n'
                                  'Longitude: ${myLocation.y}'),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: Card(
                      child: InkWell(
                        onTap: () => loadNextDepartures(),
                        child: Container(
                          padding: EdgeInsets.all(20),
                          child: Icon(Icons.refresh),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  List<MOBStop> getStopsFiltered() {
    List<MOBStop> stops = departuresByStop.keys
        .where((stop) => stop != null && departuresByStop[stop].isNotEmpty)
        .toList();
    stops.sort((a, b) => a.distance.compareTo(b.distance));

    Set<int> tripIds = <int>{};

    for (final stop in stops) {
      List<MOBDepartureTrip> departureTrips = departuresByStop[stop];
      departureTrips.removeWhere((departureTrip) {
        int tripId = departureTrip.tripId;
        bool contains = tripIds.contains(tripId);
        return false; // contains;
      });
      for (final departureTrip in departureTrips) {
        int tripId = departureTrip.tripId;
        tripIds.add(tripId);
      }
      departuresByStop[stop] = departureTrips;
    }

    stops = departuresByStop.keys
        .where((stop) =>
            departuresByStop[stop] != null && departuresByStop[stop].isNotEmpty)
        .toList();

    stops.sort((a, b) => a.distance.compareTo(b.distance));

    return stops;
  }

  @override
  Widget build(BuildContext context) {
    List<MOBStop> stops = getStopsFiltered();

    List<MOBStop> noDeparturesStop = departuresByStop.keys
        .where((stop) => departuresByStop[stop].isEmpty)
        .toList();

    noDeparturesStop.sort((a, b) => a.distance.compareTo(b.distance));

    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      backgroundColor: Color(0xff25303E),
      floatingActionButton: boomMenu(),
      drawer: Utils.isMobilibus ? drawer(noDeparturesStop) : null,
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          Image.asset('assets/header/charter_main.png', fit: BoxFit.fitWidth),
          stops.isEmpty
              ? Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.all(20),
                        child: SvgPicture.asset(
                          'assets/images/bus.svg',
                          height: 40,
                        ),
                      ),
                      Text(
                        'Aguarde...',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Theme.of(context).canvasColor,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            left: 40,
                            bottom: noDeparturesCounter >= 15
                                ? 40
                                : MediaQuery.of(context).size.height / 8,
                            right: 40),
                        child: Text(
                          noDeparturesCounter >= 15
                              ? 'Ainda não encontramos pontos de embarque nas proximidades. '
                                  'Pode ser que não existam mais previsões para o dia de hoje, '
                                  'ou que você esteja distante dos pontos de embarque.'
                              : 'Estamos procurando\npontos de embarque\npróximos a você.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 12,
                            color: Theme.of(context).canvasColor,
                          ),
                        ),
                      ),
                      if (noDeparturesCounter >= 15)
                        Container(
                          padding:
                              EdgeInsets.only(bottom: 30, right: 80, left: 80),
                          child: Text(
                            'Na Central do Cliente, confira as rotas disponíveis, inclusive em tempo real quando há viagens sendo realizadas',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 12,
                              color: Theme.of(context).canvasColor,
                            ),
                          ),
                        ),
                    ],
                  ),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    if (Utils.isMobilibus) debugWidgets(stops),
                    departuresCorousel(stops),
                    Container(
                      padding: EdgeInsets.only(bottom: 35),
                      child: Text(
                        'Clique na previsão para ver a rota',
                        textAlign: TextAlign.end,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).canvasColor,
                        ),
                      ),
                    ),
                  ],
                ),
          if (Utils.isMobilibus) positionRadiusPicker(),
        ],
      ),
    );
  }
}
