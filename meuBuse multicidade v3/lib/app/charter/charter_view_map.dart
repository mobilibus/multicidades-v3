import 'dart:async';
import 'dart:convert' as convert;
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_boom_menu/flutter_boom_menu.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_utils/google_maps_utils.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:mobilibus/app/charter/contact/contact_message.dart';
import 'package:mobilibus/app/charter/information_central.dart';
import 'package:mobilibus/app/urban/departure/departures.dart';
import 'package:mobilibus/base/MOBCharterCompany.dart';
import 'package:mobilibus/base/MOBProject.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CharterViewMap extends StatefulWidget {
  CharterViewMap(this.charterType, this.isLogin, this.routes);
  final CHARTER_TYPE charterType;
  final bool isLogin;
  final Iterable routes;

  _CharterViewMap createState() =>
      _CharterViewMap(charterType, isLogin, routes);
}

class _CharterViewMap extends State<CharterViewMap> {
  _CharterViewMap(this.charterType, this.isLogin, this.routes);
  final CHARTER_TYPE charterType;
  final bool isLogin;
  final Iterable routes;

  CameraPosition mCameraPosition;
  GoogleMapController controller;
  double zoom = 15.0;
  Map<LatLng, Marker> markersMap = {};

  BitmapDescriptor iconBus;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(), () async {
      String strAssetBus = Utils.globalPinBusStop;

      iconBus = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetBus, Size(64, 64), 64));
      setState(() {});
    });
  }

  void goToContact() => Navigator.of(context)
      .push(MaterialPageRoute(builder: (context) => ContactMessageCharter()));

  void goToInformation() {
    int routeId;
    if (charterType == CHARTER_TYPE.COMPANY_SELECT ||
        routes != null && routes.isNotEmpty) {
      Map mRoute = routes.first;
      routeId = mRoute['routeId'] ?? null;
    }
    List<int> tripIds = [];
    if (routeId != null) {
      Map<String, dynamic> route = routes.first;
      tripIds = route['tripIds'].map<int>((tripId) => tripId as int).toList();
    }
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => InformationCentral(routeId, tripIds)));
  }

  Widget boomMenu() => BoomMenu(
        animatedIcon: AnimatedIcons.menu_close,
        backgroundColor: Color(0xff229fd5),
        animatedIconTheme: IconThemeData(size: 22.0),
        overlayColor: Color(0xff25303E),
        overlayOpacity: 0.9,
        children: [
          MenuItem(
            title: 'Central do Cliente',
            titleColor: Colors.white,
            subtitle: 'Notícias e Informações sobre sua rota',
            subTitleColor: Colors.white,
            backgroundColor: Color(0xff229fd5),
            child: Icon(Icons.info, color: Theme.of(context).canvasColor),
            onTap: goToInformation,
          ),
          MenuItem(
            title: 'Entre em Contato',
            titleColor: Colors.white,
            subtitle: 'Envie sua sugestão ou reclamação',
            subTitleColor: Colors.white,
            backgroundColor: Color(0xff229fd5),
            child:
                Icon(Icons.contact_mail, color: Theme.of(context).canvasColor),
            onTap: goToContact,
          ),
          if (Utils.isMobilibus)
            MenuItem(
              title: 'Controle de Acesso',
              titleColor: Colors.white,
              subtitle: 'Seu cartão de embarque no veículo',
              subTitleColor: Colors.white,
              backgroundColor: Color(0xff229fd5),
              child: Icon(Icons.perm_identity,
                  color: Theme.of(context).canvasColor),
            ),
        ],
      );

  void goToMyLocation() async {
    try {
      LocationData currentLocation = Utils.locationData;
      if (currentLocation != null)
        controller.animateCamera(CameraUpdate.newLatLngZoom(
            LatLng(currentLocation.latitude, currentLocation.longitude), 18));
    } on Exception {}
  }

  void onMapCreated(mapController) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isDark = prefs.getBool('isDark') ?? false;
    controller = mapController;
    if (Utils.project != null)
      await controller.moveCamera(CameraUpdate.newLatLngZoom(
          LatLng(Utils.project.latitude, Utils.project.longitude), 15.0));
    Completer<GoogleMapController>().complete(controller);
    await rootBundle
        .loadString(isDark
            ? 'assets/maps_style_dark.json'
            : 'assets/maps_style_default.json')
        .then((style) => controller.setMapStyle(style));

    mapMoveEnded();
    goToMyLocation();
  }

  void onCameraMove(CameraPosition cameraPosition) {
    zoom = cameraPosition.zoom;
    mCameraPosition = cameraPosition;

    setState(() {});
  }

  void onCameraIdle() {
    if (zoom >= 15.0)
      mapMoveEnded();
    else
      markersMap.clear();
    setState(() {});
  }

  void mapMoveEnded() async {
    var visibleBounds = await controller.getVisibleRegion();
    GMULatLngBounds mBounds = GMULatLngBounds(
        Point(visibleBounds.northeast.latitude,
            visibleBounds.northeast.longitude),
        Point(visibleBounds.southwest.latitude,
            visibleBounds.southwest.longitude));
    if (mCameraPosition != null && Utils.isMobilibus) {
      Utils.lastLatitude =
          double.parse(mCameraPosition.target.latitude.toStringAsFixed(6));
      Utils.lastLongitude =
          double.parse(mCameraPosition.target.longitude.toStringAsFixed(6));
    }

    String url = MOBApiUtils.getStopsInViewUrl(mBounds);

    http.Response response = await MOBApiUtils.getStopsFuture(url);
    String body = convert.utf8.decode(response.bodyBytes);
    Iterable list = convert.json.decode(body);
    List<MOBStop> stops = list
        .map((model) => MOBStop.fromJson(model, [Utils.project.projectId]))
        .toList();

    stops.removeWhere((stop) => stop == null);
    stops.removeWhere((stop) => stop.stopType == 8);
    stops.removeWhere((stop) =>
        markersMap.containsKey(LatLng(stop.latitude, stop.longitude)));

    for (final stop in stops) {
      LatLng latLng = LatLng(stop.latitude, stop.longitude);
      Marker m = markerBuild(stop, latLng);
      markersMap[latLng] = m;
    }

    setState(() {});
  }

  Marker markerBuild(MOBStop stop, LatLng latLng) {
    int stopType = stop.stopType;

    BitmapDescriptor icon;
    switch (stopType) {
      case 3:
        icon = iconBus;
        break;
      case 5:
        break;
      case 6:
        break;
      case 7:
        break;
    }
    String idStopType = Utils.getIDByStopType(stopType);
    return Marker(
      markerId: MarkerId(stop.hashCode.toString() + idStopType),
      position: latLng,
      icon: icon,
      onTap: () async {
        if (stopType != 8) showDepartures(stop);
        await controller.moveCamera(CameraUpdate.newLatLngZoom(latLng, zoom));
      },
    );
  }

  void showDepartures(MOBStop stop) async {
    DeparturesView dpView = DeparturesView(stop);
    await showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      enableDrag: false,
      builder: (context) => Container(
        height: (MediaQuery.of(context).size.height / 8) * 7,
        child: dpView,
      ),
    ).then((_) => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    LocationData locationData = Utils.locationData;
    MOBProject project = Utils.project;

    double lat = project.latitude;
    double lng = project.longitude;

    List<Marker> markers = [];
    if (zoom >= 15) markers = List.of(markersMap.values.toList());
    markers.removeWhere(
        (element) => element.markerId.value.contains('pointOfSale'));

    if (mCameraPosition != null) {
      List<Point<num>> polygon = markers
          .map((e) => Point(e.position.latitude, e.position.longitude))
          .toList();
      if (polygon.length >= 3)
        markers.removeWhere((marker) {
          LatLng latLng = marker.position;
          Point p = Point(latLng.latitude, latLng.longitude);

          return !PolyUtils.containsLocationPoly(p, polygon);
        });
    }

    Set<Marker> markersSet = Set<Marker>.of(markers);

    return Scaffold(
      floatingActionButton: boomMenu(),
      body: Stack(
        children: [
          GoogleMap(
            onCameraMove: onCameraMove,
            onMapCreated: onMapCreated,
            onCameraIdle: onCameraIdle,
            zoomControlsEnabled: false,
            markers: markersSet,
            compassEnabled: false,
            myLocationEnabled: true,
            mapToolbarEnabled: false,
            tiltGesturesEnabled: false,
            myLocationButtonEnabled: false,
            initialCameraPosition: CameraPosition(
              zoom: 12,
              target: locationData != null
                  ? LatLng(locationData.latitude, locationData.longitude)
                  : LatLng(lat ?? 0, lng ?? 0),
            ),
          ),
          SafeArea(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  FloatingActionButton(
                    heroTag: 'fabZoomIn',
                    mini: true,
                    backgroundColor: Colors.white,
                    child: Icon(
                      Icons.zoom_in,
                      color: Utils.isLightTheme(context)
                          ? Colors.black
                          : Utils.getColorFromPrimary(context),
                    ),
                    onPressed: () =>
                        controller.animateCamera(CameraUpdate.zoomIn()),
                  ),
                  FloatingActionButton(
                    heroTag: 'fabZoomOut',
                    mini: true,
                    backgroundColor: Colors.white,
                    child: Icon(
                      Icons.zoom_out,
                      color: Utils.isLightTheme(context)
                          ? Colors.black
                          : Utils.getColorFromPrimary(context),
                    ),
                    onPressed: () =>
                        controller.animateCamera(CameraUpdate.zoomOut()),
                  ),
                  FloatingActionButton(
                    heroTag: 'fabCenter',
                    mini: true,
                    backgroundColor: Color(0xff229fd5),
                    onPressed: goToMyLocation,
                    child: Icon(
                      Icons.my_location,
                      color: Utils.isLightTheme(context)
                          ? Colors.black
                          : Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
