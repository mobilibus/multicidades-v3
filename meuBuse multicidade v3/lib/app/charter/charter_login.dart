import 'dart:convert' as convert;

import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/base/MOBCharterCompany.dart';
import 'package:mobilibus/app/charter/charter_view.dart';
import 'package:mobilibus/app/charter/charter_view_map.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CharterLogin extends StatefulWidget {
  _CharterLogin createState() => _CharterLogin();
}

class _CharterLogin extends State<CharterLogin> {
  final CHARTER_TYPE charterType = Utils.charterType;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController tecLogin = TextEditingController();
  List<MOBCharterCompany> companies = [];
  String selectedItem;
  bool isLoading = true;
  bool isLogin = true;
  bool visible = false;

  void dropDownOnChanged(dynamic value) {
    selectedItem = value;
    setState(() {});
  }

  @override
  void initState() {
    MOBApiUtils.headers['x-mob-project-id'] =
        Utils.project.projectId.toString();
    super.initState();
    Future.delayed(Duration(seconds: 1), () async {
      await loadCharter();
      if (!isLogin) {
        Widget w = charterType == CHARTER_TYPE.NO_LOGIN
            ? CharterView(charterType, isLogin, null)
            : CharterViewMap(charterType, isLogin, null);

        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => w));
      } else {
        int projectId = Utils.project.projectId;
        SharedPreferences sp = await SharedPreferences.getInstance();
        String json = sp.getString('charter_login_$projectId') ?? null;
        if (json != null) {
          isLoading = true;
          Map<String, dynamic> map = convert.json.decode(json);
          String login = map['login'];
          String agency = map['agency'];

          tecLogin.text = login;
          selectedItem = agency;

          await Future.delayed(Duration(seconds: 1), done);
        } else {
          isLoading = false;
          setState(() {});
          Future.delayed(Duration(milliseconds: 500),
              () => setState(() => visible = true));
        }
      }
    });
  }

  Future<void> loadCharter() async {
    isLoading = true;
    setState(() {});
    http.Response response = await MOBApiUtils.getAgencies(
        Utils.project.projectId, charterType == CHARTER_TYPE.COMPANY_SELECT);
    String body = convert.utf8.decode(response.bodyBytes);
    Iterable list = convert.json.decode(body);
    companies =
        list.map((e) => MOBCharterCompany.fromJson(e, charterType)).toList();
    selectedItem = companies[0].name;
    if (charterType == CHARTER_TYPE.NO_LOGIN ||
        charterType == CHARTER_TYPE.NO_LOGIN_MAP) isLogin = false;
    isLoading = false;
    setState(() {});
  }

  void done() async {
    Iterable routes = [];
    if (!isLogin && charterType == CHARTER_TYPE.NO_LOGIN)
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => CharterView(charterType, isLogin, routes)));
    else if (charterType == CHARTER_TYPE.COMPANY_SELECT ||
        isLogin &&
            (formKey?.currentState?.validate() ??
                false || tecLogin.text.isNotEmpty)) {
      if (!isLoading) {
        isLoading = true;
        setState(() {});
      }

      try {
        String login = tecLogin.text;
        String agency = selectedItem;
        int agencyId =
            companies.firstWhere((company) => company.name == agency).agencyId;

        if (charterType == CHARTER_TYPE.COMPANY_SELECT) {
          routes = [
            {
              'routeId': companies
                  .firstWhere((element) => element.name == selectedItem)
                  .routeIds
                  .first,
              'tripIds': [],
            }
          ];
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => CharterView(charterType, isLogin, routes)));
        } else if (charterType == CHARTER_TYPE.LOGIN) {
          http.Response response =
              await MOBApiUtils.loginCharter(login, agencyId);
          String body = convert.utf8.decode(response.bodyBytes);
          Map<String, dynamic> map = convert.json.decode(body);
          routes = map['routes'] ?? [];

          if (routes.isNotEmpty) {
            SharedPreferences sp = await SharedPreferences.getInstance();
            String json = convert.json.encode({
              'login': login,
              'agency': agency,
            });
            sp.setString('charter_login_${Utils.project.projectId}', json);

            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) =>
                    CharterView(charterType, isLogin, routes)));
          } else
            throw Exception;
        }
      } catch (e) {
        await showDialog(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              backgroundColor: Color(0xff25303E),
              title: Text('Erro', style: TextStyle(color: Colors.white)),
              content: Text(
                  'A matrícula não está vinculada a empresa selecionada.',
                  style: TextStyle(color: Colors.white)),
              actions: [
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child:
                        Text('Fechar', style: TextStyle(color: Colors.white)))
              ],
            ));
      }
    }
    isLoading = false;
    setState(() {});
  }

  void agencyInfo() {
    String info = '';
    companies.forEach((c) {
      int agencyId = c.agencyId;
      String name = c.name;
      info += '$agencyId:$name\n';
    });
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(title: Text('Info'), content: Text(info)));
  }

  @override
  Widget build(BuildContext context) {
    companies.sort((a, b) {
      String first = removeDiacritics(a.name);
      String second = removeDiacritics(b.name);
      return first.compareTo(second);
    });

    return Scaffold(
      backgroundColor: Color(0xff25303E),
      floatingActionButton: isLoading
          ? null
          : FloatingActionButton(
              onPressed: done,
              backgroundColor: Color(0xff229fd5),
              child: Icon(Icons.done),
            ),
      body: isLoading || !isLogin
          ? Center(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircularProgressIndicator(
                    strokeWidth: 3,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Utils.isLightTheme(context)
                            ? Utils.getColorFromPrimary(context)
                            : Colors.black))
              ],
            ))
          : Stack(
              children: <Widget>[
                AnimatedOpacity(
                  opacity: visible ? 1.0 : 0.0,
                  duration: Duration(seconds: 3),
                  child: Image.asset(
                    'assets/charter/login.png',
                    color: Color.fromRGBO(255, 255, 255, 0.5),
                    fit: BoxFit.contain,
                    colorBlendMode: BlendMode.modulate,
                  ),
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SafeArea(
                        child: Image.asset(
                          'assets/header/header.png',
                          width: MediaQuery.of(context).size.width / 2,
                        ),
                      ),
                      Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                                top: 20,
                                right: 50,
                                bottom:
                                    charterType == CHARTER_TYPE.COMPANY_SELECT
                                        ? kToolbarHeight * 2
                                        : 0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text('Selecione a empresa',
                                      style: TextStyle(
                                          color:
                                              Theme.of(context).canvasColor)),
                                ),
                                Card(
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(10.0),
                                    bottomRight: Radius.circular(10.0),
                                  )),
                                  child: Container(
                                    padding: EdgeInsets.all(5),
                                    child: DropdownButton(
                                      underline: Container(),
                                      isExpanded: true,
                                      value: selectedItem,
                                      onChanged: dropDownOnChanged,
                                      items: companies
                                          .map((e) => DropdownMenuItem(
                                                value: e.name,
                                                child: Text(e.name),
                                              ))
                                          .toList(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          if (charterType != CHARTER_TYPE.COMPANY_SELECT)
                            Form(
                              key: formKey,
                              child: Container(
                                padding: EdgeInsets.only(top: 20, right: 50),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Text(
                                        'Chave de acesso',
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).canvasColor),
                                      ),
                                    ),
                                    Card(
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(10.0),
                                        bottomRight: Radius.circular(10.0),
                                      )),
                                      child: Container(
                                        padding: EdgeInsets.all(5),
                                        child: TextFormField(
                                          validator: (text) => text.length == 0
                                              ? 'Campo obrigatório'
                                              : null,
                                          controller: tecLogin,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                        ],
                      ),
                      if (charterType != CHARTER_TYPE.COMPANY_SELECT)
                        Container(
                          padding: EdgeInsets.only(
                              left: 5, top: 5, right: 5, bottom: 50),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
                                child: Image.asset(
                                    'assets/header/charter_login.png'),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Text(
                                  'Se você não consegue acessar este recurso, entre em contato com o RH',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: Theme.of(context).canvasColor,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
    );
  }
}
