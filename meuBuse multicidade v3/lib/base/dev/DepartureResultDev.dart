import 'package:instant/instant.dart' as i;

class DepartureResultDev {
  final String stop;
  final int tzOffset;
  final List<DepartureTripDev> departures;
  final List<DepartureTripDev> departuresNextDay;

  DepartureResultDev(
      this.stop, this.tzOffset, this.departures, this.departuresNextDay);

  factory DepartureResultDev.fromJson(Map<String, dynamic> json,
      [int routeId = -1, int tripId = -1]) {
    int tzOffset = json['tzOffset'] ?? 0;
    String stopName = json['stopName'] ?? null;

    List<DepartureTripDev> trips = json.containsKey('trips')
        ? (json['trips'] as Iterable)
            .map((model) => DepartureTripDev.fromJson(model, tzOffset))
            .toList()
        : [];

    List<DepartureTripDev> departures = trips
        .map((trip) => DepartureTripDev.fromJson(trip.toMap(), tzOffset))
        .toList();
    List<DepartureTripDev> departuresNextDay = trips
        .map((trip) => DepartureTripDev.fromJson(trip.toMap(), tzOffset))
        .toList();

    if (trips.isNotEmpty) {
      departures.forEach((departure) => departure.departures
          .removeWhere((lineDeparture) => lineDeparture.nextDay == true));

      departuresNextDay.forEach((departure) => departure.departures
          .removeWhere((lineDeparture) => lineDeparture.nextDay == false));

      _filterByRouteIdTripId(departures, departuresNextDay, routeId, tripId);

      departures.removeWhere((departure) => departure.departures.isEmpty);
      departuresNextDay
          .removeWhere((departure) => departure.departures.isEmpty);

      departures.sort((dep1, dep2) => dep1.departures.first.secondsDelay
          .compareTo(dep2.departures.first.secondsDelay));
      departuresNextDay.sort((dep1, dep2) => dep1.departures.first.secondsDelay
          .compareTo(dep2.departures.first.secondsDelay));
    }

    return DepartureResultDev(
        stopName, tzOffset, departures, departuresNextDay);
  }

  static void _filterByRouteIdTripId(List<DepartureTripDev> departures,
      List<DepartureTripDev> departuresNextDay,
      [int routeId = -1, tripId = -1]) {
    if (tripId > -1) {
      departures.removeWhere((dl) => dl.tripId != tripId);
      departuresNextDay.removeWhere((dl) => dl.tripId != tripId);
    }
    if (routeId > -1) {
      departures.removeWhere((dl) => dl.routeId != routeId);
      departuresNextDay.removeWhere((dl) => dl.routeId != routeId);
    }
  }
}

class DepartureTripDev {
  final int routeId;
  final int tripId;
  final String shortName;
  final String longName;
  final String color;
  final String headsign;
  final List<DepartureDev> departures;
  final List<VehicleDev> vehicles;

  DepartureTripDev(
    this.routeId,
    this.tripId,
    this.shortName,
    this.longName,
    this.color,
    this.headsign,
    this.departures,
    this.vehicles,
  ) {
    departures.removeWhere((departure) => departure.seconds <= 20);
  }

  factory DepartureTripDev.fromJson(Map<String, dynamic> json, int tzOffset) {
    int routeId = json['routeId'] ?? 0;
    int tripId = json['tripId'] ?? 0;
    String shortName = json['shortName'] ?? null;
    String longName = json['longName'] ?? null;
    String color = json['color'] ?? null;
    String headsign = json['headsign'] ?? null;
    List<DepartureDev> departures = json.containsKey('departures')
        ? (json['departures'] as Iterable)
            .map((model) => DepartureDev.fromJson(model, tzOffset))
            .toList()
        : [];
    List<VehicleDev> vehicles = [];
    for (final departure in departures) {
      String vehicleId = departure.vehicleId;
      if (vehicleId != null) {
        String positionTime = departure.gpsTime;
        String tripFeedId = departure.tripFeedId;
        int delay = departure.delay;
        int bearing = departure.bearing;
        VehicleDev vehicleDev = VehicleDev(vehicleId, positionTime, tripId,
            tripFeedId, delay, bearing.toDouble());
        vehicles.add(vehicleDev);
      }
    }
    return DepartureTripDev(routeId, tripId, shortName, longName, color,
        headsign, departures, vehicles);
  }

  Map<String, dynamic> toMap() => {
        'routeId': routeId,
        'tripId': tripId,
        'shortName': shortName,
        'longName': longName,
        'color': color,
        'headsign': headsign,
        'departures': departures.map((departure) => departure.toMap()),
        'vehicles': vehicles.map((vehicle) => vehicle.toMap()),
      };
}

class DepartureDev {
  final String time;
  final bool wheelchairAccessible;
  final bool nextDay;
  final int seconds;
  final int secondsDelay;
  final int minutes;
  final bool online;
  final String vehicleId;
  final String gpsTime;
  final int bearing;
  final int delay;
  final String tripFeedId;

  DepartureDev(
    this.time,
    this.wheelchairAccessible,
    this.nextDay,
    this.seconds,
    this.secondsDelay,
    this.minutes,
    this.online,
    this.vehicleId,
    this.gpsTime,
    this.bearing,
    this.delay,
    this.tripFeedId,
  );

  factory DepartureDev.fromJson(Map<String, dynamic> json, int tzOffset) {
    int minutes = 0;
    int seconds = 0;
    int secondsDelay = 0;
    bool nextDay = json['nextDay'] ?? false;
    String time = json['time'] ?? null;
    if (time.isNotEmpty) {
      List<String> split = time.split(':');
      int h = int.parse(split[0]);
      int m = int.parse(split[1]);
      int s = int.parse(split[2]);

      DateTime now = i.curDateTimeByUtcOffset(offset: tzOffset.toDouble());

      Duration dNow =
          Duration(hours: now.hour, minutes: now.minute, seconds: now.second);
      Duration busTime = Duration(hours: h, minutes: m, seconds: s);

      int secondsNow = dNow.inSeconds;
      int secondsTime = busTime.inSeconds;

      seconds = secondsTime - secondsNow;
      secondsDelay = secondsTime - secondsNow;
      if (secondsDelay < 0) secondsDelay *= -1;
      if (seconds < 0) {
        if (nextDay)
          seconds *= -1;
        else
          seconds = 0;
      }

      minutes = seconds ~/ 60;
    }

    bool wheelchairAccessible = json['wa'] ?? false;
    bool online = json.containsKey('vehicleId')
        ? json['vehicleId'] != null && (json['vehicleId'] as String).isNotEmpty
        : false;
    String vehicleId = json['vehicleId'] ?? null;
    String gpsTime = json['gpsTime'] ?? null;
    int bearing = json['bearing'] ?? 0;
    int delay = json['delay'] ?? 0;
    String tripFeedId = json['tripFeedId'] ?? null;
    return DepartureDev(
      time,
      wheelchairAccessible,
      nextDay,
      seconds,
      secondsDelay,
      minutes,
      online,
      vehicleId,
      gpsTime,
      bearing,
      delay,
      tripFeedId,
    );
  }

  Map<String, dynamic> toMap() => {
        'time': time,
        'wa': wheelchairAccessible,
        'nextDay': nextDay,
        'online': online,
        'vehicleId': vehicleId,
        'gpsTime': gpsTime,
        'bearing': bearing,
        'delay': delay,
        'tripFeedId': tripFeedId,
      };
}

class VehicleDev {
  final String vehicleId;
  final String positionTime;
  final int tripId;
  final String tripFeedId;
  final int delay;
  final double bearing;

  VehicleDev(
    this.vehicleId,
    this.positionTime,
    this.tripId,
    this.tripFeedId,
    this.delay,
    this.bearing,
  );

  factory VehicleDev.fromJson(Map<String, dynamic> json) {
    String vehicleId = json['vehicleId'] ?? null;
    String positionTime = json['positionTime'] ?? null;
    int tripId = json['tripId'] ?? 0;
    String tripFeedId = json['tripFeedId'] ?? null;
    int delay = json['delay'] ?? 0;
    double bearing = (json['bearing'] ?? 0.0).toDouble();
    return VehicleDev(
        vehicleId, positionTime, tripId, tripFeedId, delay, bearing);
  }

  Map<String, dynamic> toMap() => {
        'vehicleId': vehicleId,
        'positionTime': positionTime,
        'tripId': tripId,
        'tripFeedId': tripFeedId,
        'delay': delay,
        'bearing': bearing,
      };
}
