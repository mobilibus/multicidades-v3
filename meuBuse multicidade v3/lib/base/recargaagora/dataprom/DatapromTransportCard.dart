class DatapromTransportCard {
  final bool canRecharge;
  final DatapromTransportCardUser cardTransportUser;
  final List<DatapromBalance> balances;

  DatapromTransportCard(
      this.canRecharge, this.cardTransportUser, this.balances);

  factory DatapromTransportCard.fromJson(Map<String, dynamic> json) {
    bool canRecharge = json['podeReceberRecargas'] ?? false;
    DatapromTransportCardUser cardTransportUser = json
            .containsKey('usuarioCartaoTransporte')
        ? DatapromTransportCardUser.fromJson(json['usuarioCartaoTransporte'])
        : null;
    List<DatapromBalance> balances = ((json['saldos'] ?? []) as Iterable)
        .map((e) => DatapromBalance.fromJson(e))
        .toList();
    return DatapromTransportCard(canRecharge, cardTransportUser, balances);
  }
}

class DatapromTransportCardUser {
  final String name;
  final String document;
  final DateTime bornDate;
  final String address;
  final String cityName;
  final String stateInitials;

  DatapromTransportCardUser(this.name, this.document, this.bornDate,
      this.address, this.cityName, this.stateInitials);

  factory DatapromTransportCardUser.fromJson(Map<String, dynamic> json) {
    String name = json['nome'] ?? '';
    String document = json['documento'] ?? '';
    DateTime bornDate = DateTime.parse(
        (json['dataNascimento'] ?? '1970-01-01 00:00:00').replaceAll('T', ' '));
    String address = json['endereco'] ?? '';
    String cityName = json['nomeCidade'] ?? '';
    String stateInitials = json['siglaEstado'] ?? '';
    return DatapromTransportCardUser(
        name, document, bornDate, address, cityName, stateInitials);
  }
}

class DatapromBalance {
  final String transportCardTypeCode;
  final double maxRechargeValue;
  final double maxRechargeTimes;
  final double balance;
  final DateTime balanceDate;

  DatapromBalance(this.transportCardTypeCode, this.maxRechargeValue,
      this.maxRechargeTimes, this.balance, this.balanceDate);

  factory DatapromBalance.fromJson(Map<String, dynamic> json) {
    String transportCardTypeCode = json['codigoTipoCartaoTransporte'] ?? '-1';
    double maxRechargeValue = json['valorMaximoRecargas']?.toDouble() ?? 0.0;
    double maxRechargeTimes =
        json['quantidadeMaximaRecargas']?.toDouble() ?? 0.0;
    double balance = json['saldo']?.toDouble() ?? 0.0;
    String strBalanceDate = json['dataSaldo'] ?? '1970-01-01 00:00:00';

    DateTime balanceDate;
    try {
      strBalanceDate = strBalanceDate.replaceAll('T', ' ');
      balanceDate = DateTime.parse(strBalanceDate);
    } catch (e) {
      balanceDate = DateTime.parse('1970-01-01 00:00:00');
    }
    return DatapromBalance(transportCardTypeCode, maxRechargeValue,
        maxRechargeTimes, balance, balanceDate);
  }
}
