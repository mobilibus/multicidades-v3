import 'dart:convert' as convert;

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromTransportCard.dart';
import 'package:mobilibus/base/recargaagora/dataprom/DatapromChekingAccount.dart';
import 'package:mobilibus/base/recargaagora/dataprom/Dataprom.dart';
import 'package:mobilibus/base/recargaagora/mobilibus/MobilibusAPIs.dart';
import 'package:mobilibus/base/recargaagora/mobilibus/MobilibusTransaction.dart';
import 'package:mobilibus/utils/Utils.dart';

abstract class DatapromAPIs {
  static String _datapromBaseURL =
      'https://passafacil.sinetram.com.br/sbe-ws/rest/recargaAgora/v1';
  static String _datapromBaseTestURL =
      'https://sbe-feira.dataprom.com/sbe-ws-feira/rest/recargaAgora/v1';

  static String _datapromSystemURL = 'sistema';
  static String _datapromCardQueryURL = 'cartao/consultar';
  static String _datapromCardQueryAccountURL = 'cartao/consultarContaCorrente';
  static String _datapromRechargeAuthorizeURL = 'recarga/autorizar';
  static String _datapromRechargeConfirmURL = 'recarga/confirmar';
  static String _datapromRechargeCancelURL = 'recarga/cancelar';

  static Map<String, String> _headers = {
    'Content-Type': 'application/json',
    'API-Key': '078c48a4-2940-42da-83d1-86744e05f1aa'
  };
  static Map<String, String> _headersTest = {
    'Content-Type': 'application/json',
    'API-Key': 'f14ab6f5-aace-4af5-a1fc-41e783875284'
  };

  static Future<Dataprom> getSystem(bool isDatapromProduction) async {
    Map<String, String> headers =
        isDatapromProduction ? _headers : _headersTest;
    String url = isDatapromProduction ? _datapromBaseURL : _datapromBaseTestURL;
    http.Response response = await http
        .get('$url/$_datapromSystemURL', headers: headers)
        .timeout(Utils.timeoutDuration);

    String body = convert.utf8.decode(response.bodyBytes);
    Map<String, dynamic> json = {};
    try {
      json = convert.json.decode(body) as Map<String, dynamic>;
    } on Exception {
      json['erro'] = {'codigo': '', 'mensagem': '$body'};
    }
    if (json.containsKey('erro')) _erro(json);

    Dataprom dataprom = Dataprom.fromJson(json);
    return dataprom;
  }

  static Future<DatapromTransportCard> getDatapromCard(
    String cardNumber,
    bool isDatapromProduction,
  ) async {
    Map<String, String> map = {'numeroCartaoTransporte': cardNumber};
    String post = convert.json.encode(map);

    Map<String, String> headers =
        isDatapromProduction ? _headers : _headersTest;
    String url = isDatapromProduction ? _datapromBaseURL : _datapromBaseTestURL;

    http.Response response = await http
        .post('$url/$_datapromCardQueryURL', body: post, headers: headers)
        .timeout(Utils.timeoutDuration);

    String body = convert.utf8.decode(response.bodyBytes);
    Map<String, dynamic> json = convert.json.decode(body);
    if (json.containsKey('erro')) _erro(json);

    DatapromTransportCard cartaoTransporte =
        DatapromTransportCard.fromJson(json);
    return cartaoTransporte;
  }

  static Future<DatapromChekingAccount> getDatapromCardDetails(
    String cardNumber,
    bool isDatapromProduction,
  ) async {
    DateTime dateTime = DateTime.now();

    DateFormat dateFormat = DateFormat('yyyy-MM-dd');

    String endFormat = dateFormat.format(dateTime);
    dateTime = dateTime.subtract(Duration(days: 5));
    String startFormat = dateFormat.format(dateTime);

    String dateEnd = endFormat + 'T23:59:59.999-0300';
    String dateInit = startFormat + 'T00:00:00.000-0300';

    Map<String, String> map = {
      'numeroCartaoTransporte': cardNumber,
      'dataInicial': dateInit,
      'dataFinal': dateEnd,
    };
    String post = convert.json.encode(map);

    Map<String, String> headers =
        isDatapromProduction ? _headers : _headersTest;
    String url = isDatapromProduction ? _datapromBaseURL : _datapromBaseTestURL;

    http.Response response = await http
        .post('$url/$_datapromCardQueryAccountURL',
            body: post, headers: headers)
        .timeout(Utils.timeoutDuration);

    String body = convert.utf8.decode(response.bodyBytes);
    Map<String, dynamic> json = convert.json.decode(body);
    if (json.containsKey('erro')) _erro(json);

    DatapromChekingAccount contaCorrente =
        DatapromChekingAccount.fromJson(json);
    return contaCorrente;
  }

  static Future<Map<String, dynamic>> authorize(
    String cardNumber,
    String ticketCode,
    String cardType,
    double amount,
    bool isDatapromProduction,
  ) async {
    Map<String, dynamic> map = {
      'numeroCartaoTransporte': cardNumber,
      'codigoBilhetagem': ticketCode,
      'codigoTipoCartao': cardType,
      'valorRecarga': amount,
    };
    String post = convert.json.encode(map);

    Map<String, String> headers =
        isDatapromProduction ? _headers : _headersTest;
    String url = isDatapromProduction ? _datapromBaseURL : _datapromBaseTestURL;

    http.Response response = await http
        .post('$url/$_datapromRechargeAuthorizeURL',
            body: post, headers: headers)
        .timeout(Utils.timeoutDuration);

    String body = convert.utf8.decode(response.bodyBytes);
    Map<String, dynamic> json = convert.json.decode(body);
    if (json.containsKey('erro')) _erro(json);

    String identificadorRecarga = json['identificadorRecarga'];

    int transactionId = await MobilibusAPIs.createTransaction(
        cardNumber, cardType, amount, identificadorRecarga);

    return {
      'identificadorRecarga': identificadorRecarga,
      'transactionId': transactionId,
    };
  }

  static Future<String> cancelAuthorization(
    String cardNumber,
    String datapromToken,
    MobilibusTransaction transaction,
    bool isDatapromProduction,
  ) async {
    Map<String, String> map = {
      'numeroCartaoTransporte': cardNumber,
      'identificadorRecarga': datapromToken,
    };
    String post = convert.json.encode(map);

    Map<String, String> headers =
        isDatapromProduction ? _headers : _headersTest;
    String url = isDatapromProduction ? _datapromBaseURL : _datapromBaseTestURL;

    http.Response response = await http
        .post('$url/$_datapromRechargeCancelURL', body: post, headers: headers)
        .timeout(Utils.timeoutDuration);

    String body = convert.utf8.decode(response.bodyBytes);
    if (body.isEmpty) {
      int transactionId = transaction.transactionId;
      String tokenDataprom = transaction.tokenDataprom;
      String tokenEucard = transaction.tokenEucard;
      if (response.statusCode == 204)
        await MobilibusAPIs.updateTransaction(
          STATUS.CANCELLED,
          transactionId,
          tokenDataprom: tokenDataprom,
          tokenEucard: tokenEucard,
        );
      return 'Cancelado com sucesso!';
    } else
      return convert.json.decode(body);
  }

  static Future<Map<String, dynamic>> confirmAuthorization(
    String cardNumber,
    String identificadorRecarga,
    bool isDatapromProduction,
  ) async {
    Map<String, String> map = {
      'numeroCartaoTransporte': cardNumber,
      'identificadorRecarga': identificadorRecarga,
    };
    String post = convert.json.encode(map);

    Map<String, String> headers =
        isDatapromProduction ? _headers : _headersTest;
    String url = isDatapromProduction ? _datapromBaseURL : _datapromBaseTestURL;

    http.Response response = await http
        .post('$url/$_datapromRechargeConfirmURL', body: post, headers: headers)
        .timeout(Utils.timeoutDuration);

    if (response.statusCode == 204) {
      return {};
    } else {
      String body = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> json = convert.json.decode(body);
      return json;
    }
  }

  static void _erro(Map<String, dynamic> json) {
    Map<String, dynamic> erro = json['erro'];
    String cod = erro['codigo'];
    String mensagem = erro['mensagem'];
    throw '$cod: $mensagem';
  }
}
