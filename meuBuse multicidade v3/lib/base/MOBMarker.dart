enum MARKER_TYPE {
  HOME,
  WORK,
  SCHOOL,
  GENERIC,
}

class MOBMarker {
  String name;
  final double latitude;
  final double longitude;
  final MARKER_TYPE markerType;

  MOBMarker(this.name, this.latitude, this.longitude, this.markerType);

  factory MOBMarker.fromJson(Map<String, dynamic> json) {
    String name = json['name'];
    double latitude = json['latitude'];
    double longitude = json['longitude'];
    String mMarkerType = json['marker_type'];

    MARKER_TYPE markerType;

    switch (mMarkerType) {
      case 'HOME':
        markerType = MARKER_TYPE.HOME;
        break;
      case 'WORK':
        markerType = MARKER_TYPE.WORK;
        break;
      case 'SCHOOL':
        markerType = MARKER_TYPE.SCHOOL;
        break;
      case 'GENERIC':
        markerType = MARKER_TYPE.GENERIC;
        break;
      default:
        markerType = MARKER_TYPE.HOME;
        break;
    }

    return MOBMarker(name, latitude, longitude, markerType);
  }

  Map<String, dynamic> asMap() => {
        'name': name,
        'latitude': latitude,
        'longitude': longitude,
        'marker_type': markerType.toString().replaceAll('MARKER_TYPE.', ''),
      };
}
