enum CHARTER_TYPE {
  LOGIN,
  NO_LOGIN,
  NO_LOGIN_MAP,
  COMPANY_SELECT,
}

class MOBCharterCompany {
  final String name;
  final int agencyId;
  final List<int> routeIds;

  MOBCharterCompany(this.name, this.agencyId, this.routeIds);

  factory MOBCharterCompany.fromJson(Map<String, dynamic> json,
      [CHARTER_TYPE charterType]) {
    String name = json['name'] ?? '';
    int agencyId = json['agencyId'] ?? -1;
    List<int> routeIds = [];
    if (charterType != null && charterType == CHARTER_TYPE.COMPANY_SELECT) {
      Iterable iterable = json['routeIds'];
      routeIds = iterable.map((e) => e as int).toList();
    }
    return MOBCharterCompany(name, agencyId, routeIds);
  }
}
