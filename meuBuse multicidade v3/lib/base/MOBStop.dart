class MOBStop {
  final int stopId;
  final int projectId;
  final double latitude;
  final double longitude;
  final int stopType;
  final String code;
  String name;
  double distance = 0.0;
  double degrees = 0.0;

  MOBStop(
    this.stopId,
    this.projectId,
    this.latitude,
    this.longitude,
    this.name,
    this.stopType,
    this.code,
  );

  factory MOBStop.fromJson(Map<String, dynamic> json, [List<int> projectId]) {
    int mProjectId = json['projectId'] ?? 0;
    if (projectId != null && !projectId.contains(mProjectId)) return null;
    int stopId = json['stopId'] ?? 0;
    double lat = json['lat'] ?? 0.0;
    double lng = json['lng'] ?? 0.0;
    String name = json['name'] ?? null;
    int stopType = json['stopType'] ?? 3;
    String code = json['code'] ?? null;
    return MOBStop(stopId, mProjectId, lat, lng, name, stopType, code);
  }

  Map toMap() => {
        'stopId': stopId,
        'projectId': projectId,
        'lat': latitude,
        'lng': longitude,
        'name': name,
        'stopType': stopType,
        'code': code,
      };
}
