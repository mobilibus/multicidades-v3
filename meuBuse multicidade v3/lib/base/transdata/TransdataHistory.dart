import 'package:intl/intl.dart';
import 'package:mobilibus/base/transdata/TransdataRef.dart';

class TransdataHistory extends TransdataRef {
  final List<TransdataHistoryItem> items;

  TransdataHistory(int errorCode, String errorMessage, this.items)
      : super(errorCode, errorMessage);

  factory TransdataHistory.fromJson(Map<String, dynamic> map) {
    int errorCode = map['errorCode'];
    String errorMessage = map['errorMessage'];
    List<TransdataHistoryItem> items = ((map['Valor'] ?? []) as Iterable)
        .map((e) => TransdataHistoryItem.fromJson(e))
        .toList();
    return TransdataHistory(errorCode, errorMessage, items);
  }
}

class TransdataHistoryItem {
  final String id;
  final DateTime requested;
  final DateTime status;
  final String description;
  final int totalInCents;
  final String cpf;
  final int numSerie;

  TransdataHistoryItem(this.id, this.requested, this.status, this.description,
      this.totalInCents, this.cpf, this.numSerie);

  factory TransdataHistoryItem.fromJson(Map<String, dynamic> map) {
    DateFormat df = DateFormat('yyyy-MM-dd HH:mm:ss');
    String id = map['IdPedido'];
    DateTime requested = df.parse(map['DataPedido'] ?? '1970-01-01 00:00:00');
    DateTime status = df.parse(map['DataStatus'] ?? '1970-01-01 00:00:00');
    String description = map['DescricaoStatus'];
    int totalInCents = map['ValorTotalPedidoEmCentavos'];
    String cpf = map['Cpf'];
    int numSerie = map['NumSerie'];
    return TransdataHistoryItem(
        id, requested, status, description, totalInCents, cpf, numSerie);
  }
}
