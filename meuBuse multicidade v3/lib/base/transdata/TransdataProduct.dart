import 'package:intl/intl.dart';
import 'package:mobilibus/base/transdata/TransdataRef.dart';

class TransdataProduct extends TransdataRef {
  final List<TransdataProductItem> ret;

  TransdataProduct(int errorCode, String errorMessage, this.ret)
      : super(errorCode, errorMessage);

  factory TransdataProduct.fromJson(Map<String, dynamic> map) {
    int errorCode = map['errorCode'];
    String errorMessage = map['errorMessage'];
    List<TransdataProductItem> ret = ((map['ret'] ?? []) as Iterable)
        .map((e) => TransdataProductItem.fromJson(e))
        .toList();
    return TransdataProduct(errorCode, errorMessage, ret);
  }
}

class TransdataProductItem {
  final int daysToEnableBuy;
  final String message;
  final String name;
  final int productId;
  final int maxCardBalance;
  final int tax;
  final int maxValue;
  final int minValue;
  final int unitValue;

  TransdataProductItem(
    this.daysToEnableBuy,
    this.message,
    this.name,
    this.productId,
    this.maxCardBalance,
    this.tax,
    this.maxValue,
    this.minValue,
    this.unitValue,
  );

  factory TransdataProductItem.fromJson(Map<String, dynamic> map) {
    int daysToEnableBuy = map['DiasParaHabilitarCompra'];
    String message = map['Mensagem'];
    String name = map['Nome'];
    int productId = map['ProdutoId'];
    int maxCardBalance = map['SaldoMaximoCartao'];
    int tax = map['Tarifa'];
    int maxValue = map['ValorMax'];
    int minValue = map['ValorMin'];
    int unitValue = map['ValorUnitario'];
    return TransdataProductItem(daysToEnableBuy, message, name, productId,
        maxCardBalance, tax, maxValue, minValue, unitValue);
  }
}

class TransdataProductItemCreated extends TransdataRef {
  final String id;
  final DateTime date;

  TransdataProductItemCreated(
      int errorCode, String errorMessage, this.id, this.date)
      : super(errorCode, errorMessage);

  factory TransdataProductItemCreated.fromJson(Map<String, dynamic> map) {
    final int errorCode = map['errorCode'];
    final String errorMessage = map['errorMessage'];
    final String id = map['IdPedido'];
    final DateTime date = DateFormat('yyy-MM-dd HH:mm:ss')
        .parse((map['DataRegistro'] ?? '1970-01-01 00:00:00'));
    return TransdataProductItemCreated(errorCode, errorMessage, id, date);
  }
}

class TransdataProductItemEnded extends TransdataRef {
  final bool value;

  TransdataProductItemEnded(int errorCode, String errorMessage, this.value)
      : super(errorCode, errorMessage);

  factory TransdataProductItemEnded.fromJson(Map<String, dynamic> map) {
    final int errorCode = map['errorCode'];
    final String errorMessage = map['errorMessage'];
    final bool value = map['Valor'];
    return TransdataProductItemEnded(errorCode, errorMessage, value);
  }
}
