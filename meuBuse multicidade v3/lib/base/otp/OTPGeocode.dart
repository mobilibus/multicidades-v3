class OTPGeocode {
  final List<OTPGeocodeResult> results;

  OTPGeocode(this.results);

  factory OTPGeocode.fromJson(Map<String, dynamic> json) {
    List<OTPGeocodeResult> results = ((json['results'] ?? []) as Iterable)
        .map((model) => OTPGeocodeResult?.fromJson(model))
        .toList();

    return OTPGeocode(results);
  }
}

class OTPGeocodeResult {
  final String description;
  final double latitude;
  final double longitude;
  final String provider;
  bool isHistory = false;

  OTPGeocodeResult(
      this.description, this.latitude, this.longitude, this.provider);

  factory OTPGeocodeResult.fromJson(Map<String, dynamic> json,
      [bool isHistory = false]) {
    String description = json['description'] ?? null;
    double latitude = json['lat'] ?? 0.0;
    double longitude = json['lng'] ?? 0.0;
    String provider = json['provider'] ?? null;

    OTPGeocodeResult geocodeResult =
        OTPGeocodeResult(description, latitude, longitude, provider);
    geocodeResult.isHistory = isHistory;

    return geocodeResult;
  }

  Map<String, dynamic> toMap() => {
        'description': description,
        'lat': latitude,
        'lng': longitude,
        'provider': provider,
      };
}
