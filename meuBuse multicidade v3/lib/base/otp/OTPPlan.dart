import 'package:mobilibus/utils/Utils.dart';

enum OTP_MODE {
  TRANSIT,
  WALK,
  BICYCLE,
  CAR,
  TRAM,
  SUBWAY,
  RAIL,
  BUS,
  FERRY,
  CABLE_CAR,
  GONDOLA,
  FUNICULAR,
  TRAINISH,
  BUSISH,
  LEG_SWITCH,
  CUSTOM_MOTOR_VEHICLE,
}

class OTPPlan {
  final int date;
  final OTPDirection from;
  final OTPDirection to;
  final List<OTPItinerary> itineraries;
  final String destination;
  final String jsonResult;

  OTPPlan(
    this.date,
    this.from,
    this.to,
    this.itineraries,
    this.destination,
    this.jsonResult,
  );

  factory OTPPlan.fromJson(
          Map<String, dynamic> json, String destination, String jsonResult) =>
      OTPPlan(
        json['date'] ?? 0,
        OTPDirection.fromJson(json['from']) ?? null,
        OTPDirection.fromJson(json['to']) ?? null,
        (json['itineraries'] as Iterable)
                ?.map((model) => OTPItinerary?.fromJson(model))
                ?.toList() ??
            [],
        destination,
        Utils.isMobilibus ? jsonResult : '',
      );
}

class OTPDirection {
  final String name;
  final double latitude;
  final double longitude;
  final int departure;
  final int arrival;
  final String vertexType;

  OTPDirection(
    this.name,
    this.latitude,
    this.longitude,
    this.departure,
    this.arrival,
    this.vertexType,
  );

  factory OTPDirection.fromJson(Map<String, dynamic> json) => OTPDirection(
        json['name'] ?? null,
        json['lat'] ?? 0.0,
        json['lon'] ?? 0.0,
        json['departure'] ?? 0,
        json['arrival'] ?? 0,
        json['vertexType'] ?? null,
      );
}

class OTPItinerary {
  final int duration;
  final int startTime;
  final int endTime;
  final int walkTime;
  final int transitTime;
  final int waitingTime;
  final double walkDistance;
  final List<OTPLeg> legs;

  OTPItinerary(
    this.duration,
    this.startTime,
    this.endTime,
    this.walkTime,
    this.transitTime,
    this.waitingTime,
    this.walkDistance,
    this.legs,
  );

  factory OTPItinerary.fromJson(Map<String, dynamic> json) => OTPItinerary(
        json['duration'] ?? 0,
        json['startTime'] ?? 0,
        json['endTime'] ?? 0,
        json['walkTime'] ?? 0,
        json['transitTime'] ?? 0,
        json['waitingTime'] ?? 0,
        json['walkDistance'] ?? 0.0,
        (json['legs'] as Iterable)
                ?.map((model) => OTPLeg?.fromJson(model))
                ?.toList() ??
            [],
      );
}

class OTPLeg {
  final int startTime;
  final int endTime;
  final int departureDelay;
  final int arrivalDelay;
  final bool realTime;
  final double distance;
  final bool pathway;
  final OTP_MODE mode;
  final String route;
  final int agencyTimeZoneOffset;
  final bool interlineWithPreviousLeg;
  final OTPDirection from;
  final OTPDirection to;
  final LegGeometry legGeometry;
  final bool rentedBike;
  final bool intermediatePlace;
  final String realTimeState;
  final double duration;
  final bool transitLeg;
  final String routeColor;
  final String routeTextColor;
  final String headsign;
  final String routeShortName;
  final String routeLongName;
  final String vehicleId;
  final String appRouteId;
  final String appTripId;
  final List<OTPStep> steps;

  OTPLeg(
    this.startTime,
    this.endTime,
    this.departureDelay,
    this.arrivalDelay,
    this.realTime,
    this.distance,
    this.pathway,
    this.mode,
    this.route,
    this.agencyTimeZoneOffset,
    this.interlineWithPreviousLeg,
    this.from,
    this.to,
    this.legGeometry,
    this.rentedBike,
    this.intermediatePlace,
    this.realTimeState,
    this.duration,
    this.transitLeg,
    this.routeColor,
    this.routeTextColor,
    this.headsign,
    this.routeShortName,
    this.routeLongName,
    this.vehicleId,
    this.appRouteId,
    this.appTripId,
    this.steps,
  );

  factory OTPLeg.fromJson(Map<String, dynamic> json) {
    int startTime = json['startTime'] ?? 0;
    int endTime = json['endTime'] ?? 0;
    int departureDelay = json['departureDelay'] ?? 0;
    int arrivalDelay = json['arrivalDelay'] ?? 0;
    bool realTime = json['realTime'] ?? false;
    double distance = json['distance'] ?? 0.0;
    bool pathway = json['pathway'] ?? false;
    String mode = json['mode'] ?? null;
    String route = json['route'] ?? null;
    int agencyTimeZoneOffset = json['agencyTimeZoneOffset'] ?? 0;
    bool interlineWithPreviousLeg = json['interlineWithPreviousLeg'] ?? false;
    OTPDirection from = OTPDirection?.fromJson(json['from']) ?? null;
    OTPDirection to = OTPDirection?.fromJson(json['to']) ?? null;
    LegGeometry legGeometry =
        LegGeometry?.fromJson(json['legGeometry']) ?? null;
    bool rentedBike = json['rentedBike'] ?? false;
    bool intermediatePlace = json['intermediatePlace'] ?? false;
    String realTimeState = json['realTimeState'] ?? null;
    double duration = json['duration'] ?? 0.0;
    bool transitLeg = json['transitLeg'] ?? false;
    String routeColor = json['routeColor'] ?? '#000000000';
    String routeTextColor = json['routeTextColor'] ?? '#fffffffff';
    String headsign = json['headsign'] ?? null;
    String routeShortName = json['routeShortName'] ?? null;
    String routeLongName = json['routeLongName'] ?? null;
    String vehicleId = json['vehicleId'] ?? null;
    String appRouteId = json['appRouteId'] ?? null;
    String appTripId = json['appTripId'] ?? null;
    List<OTPStep> steps = ((json['steps'] ?? []) as Iterable)
        .map((model) => OTPStep?.fromJson(model))
        .toList();

    OTP_MODE modalMode;
    switch (mode) {
      case 'WALK':
        modalMode = OTP_MODE.WALK;
        break;
      case 'BUS':
        modalMode = OTP_MODE.BUS;
        break;
      case 'SUBWAY':
        modalMode = OTP_MODE.SUBWAY;
        break;
      case 'TRAINISH':
        modalMode = OTP_MODE.TRAINISH;
        break;
      case 'BICYCLE':
        modalMode = OTP_MODE.BICYCLE;
        break;
      case 'RAIl':
        modalMode = OTP_MODE.RAIL;
        break;
    }

    return OTPLeg(
      startTime,
      endTime,
      departureDelay,
      arrivalDelay,
      realTime,
      distance,
      pathway,
      modalMode,
      route,
      agencyTimeZoneOffset,
      interlineWithPreviousLeg,
      from,
      to,
      legGeometry,
      rentedBike,
      intermediatePlace,
      realTimeState,
      duration,
      transitLeg,
      routeColor,
      routeTextColor,
      headsign,
      routeShortName,
      routeLongName,
      vehicleId,
      appRouteId,
      appTripId,
      steps,
    );
  }
}

class OTPStep {
  final double distance;
  final String relativeDirection;
  final String streetName;
  final String absoluteDirection;
  final bool stayOn;
  final bool area;
  final bool bogusName;
  final double latitude;
  final double longitude;

  OTPStep(
    this.distance,
    this.relativeDirection,
    this.streetName,
    this.absoluteDirection,
    this.stayOn,
    this.area,
    this.bogusName,
    this.latitude,
    this.longitude,
  );

  factory OTPStep.fromJson(Map<String, dynamic> json) {
    double distance = json['distance'] ?? 0.0;
    String relativeDirection = json['relativeDirection'] ?? null;
    String streetName = json['streetName'] ?? null;
    String absoluteDirection = json['absoluteDirection'] ?? null;
    bool stayOn = json['stayOn'] ?? false;
    bool area = json['area'] ?? 0;
    bool bogusName = json['bogusName'] ?? 0;
    double latitude = json['lat'] ?? 0.0;
    double longitude = json['lon'] ?? 0.0;
    return OTPStep(
      distance,
      relativeDirection,
      streetName,
      absoluteDirection,
      stayOn,
      area,
      bogusName,
      latitude,
      longitude,
    );
  }
}

class LegGeometry {
  final String points;
  final int length;

  LegGeometry(this.points, this.length);

  factory LegGeometry.fromJson(Map<String, dynamic> json) {
    String points = json['points'] ?? null;
    int length = json['length'] ?? 0;

    return LegGeometry(points, length);
  }
}
