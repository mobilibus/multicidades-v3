class OTPReverseGeocode {
  final int count;
  final List<ReverseGeocodeResult> results;

  OTPReverseGeocode(this.count, this.results);

  factory OTPReverseGeocode.fromJson(Map<String, dynamic> json) {
    int count = json['count'] ?? 0;
    List<ReverseGeocodeResult> results = json.containsKey('results')
        ? (json['results'] as Iterable)
            .map((e) => ReverseGeocodeResult.fromJson(e))
            .toList()
        : [];
    return OTPReverseGeocode(count, results);
  }
}

class ReverseGeocodeResult {
  final double latitude;
  final double longitude;
  final String description;
  final String place;
  final String street;
  final String number;
  final String neighborhood;
  final String provider;
  final String postalCode;
  final String city;
  final String state;

  ReverseGeocodeResult(
    this.latitude,
    this.longitude,
    this.description,
    this.place,
    this.street,
    this.number,
    this.neighborhood,
    this.provider,
    this.postalCode,
    this.city,
    this.state,
  );

  factory ReverseGeocodeResult.fromJson(Map<String, dynamic> json) {
    double latitude = json['lat'] ?? 0.0;
    double longitude = json['lng'] ?? 0.0;
    String description = json['description'] ?? null;
    String place = json['place'] ?? null;
    String street = json['street'] ?? null;
    String number = json['number'] ?? null;
    String neighborhood = json['neighborhood'] ?? null;
    String provider = json['provider'] ?? null;
    String postalCode = json['postalcode'] ?? null;
    String city = json['city'] ?? null;
    String state = json['state'] ?? null;
    return ReverseGeocodeResult(latitude, longitude, description, place, street,
        number, neighborhood, provider, postalCode, city, state);
  }
}
