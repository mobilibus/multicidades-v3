import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../Utils.dart';

class NotificationWebView extends StatefulWidget {
  NotificationWebView(this.urlName, this.url) : super();
  final String urlName;
  final String url;
  _NotificationWebView createState() => _NotificationWebView(urlName, url);
}

class _NotificationWebView extends State<NotificationWebView> {
  _NotificationWebView(this.urlName, this.url) : super();
  final String urlName;
  final String url;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Utils.getColorFromPrimary(context),
        title: Text(urlName),
      ),
      body: WebView(
        initialUrl: url,
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}
