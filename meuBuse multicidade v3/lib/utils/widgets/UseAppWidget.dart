import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobilibus/intro/CharterIntro.dart';
import 'package:mobilibus/intro/UrbanIntro.dart';
import 'package:mobilibus/mobilibus_main.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class UseAppWidget extends StatefulWidget {
  _UseAppWidget createState() => _UseAppWidget();
}

class _UseAppWidget extends State<UseAppWidget> {
  final String privacyPolicy = 'https://www.mobilibus.com.br/privacy.html';
  final String termsOfUse = 'https://www.mobilibus.com.br/tos.html';

  void cancel() => SystemChannels.platform.invokeMethod('SystemNavigator.pop');

  void accept() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool('acceptedApp', true);
    Widget widget = Utils.isMobilibus
        ? SelectProjectBehaviour()
        : Utils.isCharter
            ? CharterIntro()
            : UrbanIntro();
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => widget));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              'assets/mobilibus/workman.png',
              height: (MediaQuery.of(context).size.height / 5) * 3 - 20,
            ),
            Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'ATENÇÃO!',
                        style: TextStyle(color: Colors.black, fontSize: 20),
                      ),
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  'Para que sua experiência com nosso aplicativo seja completa, '
                                  'permissões de uso de recursos de seu telefone serão solicitadas, e o uso do aplicativo está condicionado ao aceitamento da ',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 14),
                            ),
                            TextSpan(
                              text: 'Política de Privacidade',
                              style:
                                  TextStyle(color: Colors.blue, fontSize: 14),
                              recognizer: TapGestureRecognizer()
                                ..onTapDown = (_) => launch(privacyPolicy),
                            ),
                            TextSpan(
                              text: ' e dos ',
                              style:
                                  TextStyle(color: Colors.black, fontSize: 14),
                            ),
                            TextSpan(
                              text: 'Termos de Uso do aplicativo',
                              style:
                                  TextStyle(color: Colors.blue, fontSize: 14),
                              recognizer: TapGestureRecognizer()
                                ..onTapDown = (_) => launch(termsOfUse),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Card(
                        color: Theme.of(context).primaryColor,
                        child: InkWell(
                          onTap: accept,
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Row(
                              children: [
                                Text('Aceitar'),
                                Icon(Icons.done),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
