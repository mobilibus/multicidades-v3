import 'dart:math';

import 'package:http/http.dart' as http;
import 'package:mobilibus/utils/Utils.dart';

class OSRMUtils {
  static Future<http.Response> getOSRM(
      Point<double> origin, Point<double> destiny) async {
    try {
      double originX = origin.x;
      double originY = origin.y;

      double destinyX = destiny.x;
      double destinyY = destiny.y;

      String osrmBase = 'http://router.project-osrm.org/route/v1/foot';
      String url = '$osrmBase/$originY,$originX;$destinyY,$destinyX';
      url = '$url?overview=full&alternatives=false&steps=true';
      return http.get(url).timeout(Utils.timeoutDuration);
    } catch (e) {
      return null;
    }
  }
}

class OSRM {
  final List<OSRMWaypoint> waypoints;
  final List<OSRMRoute> routes;

  OSRM(this.waypoints, this.routes);

  factory OSRM.fromJson(Map<String, dynamic> json) {
    List<OSRMWaypoint> waypoints = json.containsKey('waypoints')
        ? (json['waypoints'] as Iterable)
            .map((e) => OSRMWaypoint.fromJson(e))
            .toList()
        : [];
    List<OSRMRoute> routes = json.containsKey('routes')
        ? (json['routes'] as Iterable)
            .map((e) => OSRMRoute.fromJson(e))
            .toList()
        : [];

    return OSRM(waypoints, routes);
  }
}

class OSRMWaypoint {
  final String hint;
  final List<double> location;
  final String name;

  OSRMWaypoint(this.hint, this.location, this.name);
  factory OSRMWaypoint.fromJson(Map<String, dynamic> json) {
    String hint = json['hint'] ?? null;
    List<double> location =
        (json['location'] as Iterable).map((e) => e as double).toList();
    String name = json['name'] ?? null;
    return OSRMWaypoint(hint, location, name);
  }
}

class OSRMRoute {
  final List<OSRMLeg> legs;
  final String weightName;
  final String geometry;
  final double weight;
  final double distance;
  final double duration;

  OSRMRoute(
    this.legs,
    this.weightName,
    this.geometry,
    this.weight,
    this.distance,
    this.duration,
  );
  factory OSRMRoute.fromJson(Map<String, dynamic> json) {
    List<OSRMLeg> legs = json.containsKey('legs')
        ? (json['legs'] as Iterable).map((e) => OSRMLeg.fromJson(e)).toList()
        : [];
    String weightName = json['weight_name'];
    String geometry = json['geometry'] ?? null;
    double weight = json['weight'].toDouble() ?? 0.0;
    double distance = json['distance'].toDouble() ?? 0.0;
    double duration = json['duration'].toDouble() ?? 0.0;
    return OSRMRoute(legs, weightName, geometry, weight, distance, duration);
  }
}

class OSRMLeg {
  final List<OSRMStep> steps;
  final String summary;

  OSRMLeg(
    this.steps,
    this.summary,
  );

  factory OSRMLeg.fromJson(Map<String, dynamic> json) {
    List<OSRMStep> steps = json.containsKey('steps')
        ? (json['steps'] as Iterable).map((e) => OSRMStep.fromJson(e)).toList()
        : [];
    String summary = json['summary'] ?? null;
    return OSRMLeg(steps, summary);
  }
}

class OSRMStep {
  final List<OSRMIntersection> intersection;
  final String drivingSide;
  final String geometry;
  final double duration;
  final double distance;
  final String name;
  final double weight;
  final String mode;
  final OSRMManeuver maneuver;

  OSRMStep(
    this.intersection,
    this.drivingSide,
    this.geometry,
    this.duration,
    this.distance,
    this.name,
    this.weight,
    this.mode,
    this.maneuver,
  );
  factory OSRMStep.fromJson(Map<String, dynamic> json) {
    List<OSRMIntersection> intersection = json.containsKey('intersections')
        ? (json['intersections'] as Iterable)
            .map((e) => OSRMIntersection.fromJson(e))
            .toList()
        : [];
    String drivingSide = json['driving_side'] ?? null;
    String geometry = json['geometry'] ?? null;
    double duration = json['duration'].toDouble() ?? 0.0;
    double distance = json['distance'].toDouble() ?? 0.0;
    String name = json['name'] ?? null;
    double weight = json['weigth'] ?? 0.0;
    String mode = json['mode'] ?? null;
    OSRMManeuver maneuver = json.containsKey('maneuver')
        ? OSRMManeuver.fromJson(json['maneuver'])
        : null;
    return OSRMStep(intersection, drivingSide, geometry, duration, distance,
        name, weight, mode, maneuver);
  }
}

class OSRMIntersection {
  final int out;
  final List<bool> entries;
  final List<double> location;
  final List<int> bearings;
  OSRMIntersection(
    this.out,
    this.entries,
    this.location,
    this.bearings,
  );
  factory OSRMIntersection.fromJson(Map<String, dynamic> json) {
    int out = json['out'] ?? 0;
    List<bool> entries = json.containsKey('entry')
        ? (json['entry'] as Iterable).map((e) => e as bool).toList()
        : [];
    List<double> location = json.containsKey('location')
        ? (json['location'] as Iterable).map((e) => e as double).toList()
        : [];
    List<int> bearings = json.containsKey('bearings')
        ? (json['bearings'] as Iterable).map((e) => e as int).toList()
        : [];
    return OSRMIntersection(out, entries, location, bearings);
  }
}

class OSRMManeuver {
  final int bearingAfter;
  final int bearingBefore;
  final String type;
  final List<double> location;

  OSRMManeuver(
    this.bearingAfter,
    this.bearingBefore,
    this.type,
    this.location,
  );
  factory OSRMManeuver.fromJson(Map<String, dynamic> json) {
    int bearingAfter = json['bearing_after'] ?? 0;
    int bearingBefore = json['bearing_before'] ?? 0;
    String type = json['type'] ?? null;
    List<double> location = json.containsKey('location')
        ? (json['location'] as Iterable).map((e) => e as double).toList()
        : [];
    return OSRMManeuver(bearingAfter, bearingBefore, type, location);
  }
}
