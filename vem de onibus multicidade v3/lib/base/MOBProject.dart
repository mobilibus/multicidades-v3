class MOBProject {
  final int projectId;
  final String name;
  final String city;
  final String country;
  final double latitude;
  final double longitude;
  final String hash;
  final bool supportAlerts;
  final bool supportContactMessages;
  final bool isCharter;
  final String otpUri;
  final String apiUri;
  final int realtimeUpdateFreqSec;
  final int realtimeDeparturesOffset;
  final List<MOBAgency> agencies;

  MOBProject(
    this.projectId,
    this.name,
    this.city,
    this.country,
    this.latitude,
    this.longitude,
    this.hash,
    this.supportAlerts,
    this.supportContactMessages,
    this.isCharter,
    this.otpUri,
    this.apiUri,
    this.realtimeUpdateFreqSec,
    this.realtimeDeparturesOffset,
    this.agencies,
  );

  factory MOBProject.fromJson(Map<String, dynamic> json) {
    int projectId = json['projectId'] ?? 0;
    String name = json['name'] ?? null;
    String city = json['city'] ?? null;
    String country = json['country'] ?? null;
    double latitude = json['lat'] ?? 0.0;
    double longitude = json['lon'] ?? 0.0;
    String hash = json['hash'] ?? null;
    bool supportAlerts = json['supportAlerts'] ?? false;
    bool supportContactMessages = json['supportContactMessages'] ?? false;
    bool isCharter = json['charter'] ?? false;
    String otpUri = json['otpUri'] ?? null;
    String apiUri = json['apiUri'] ?? null;
    int realtimeUpdateFreqSec = json['realtimeUpdateFreqSec'] ?? 30;
    int realtimeDeparturesOffset = json['realtimeDeparturesOffset'] ?? 20;
    List<MOBAgency> agencies = [];
    if (json.containsKey('agencies'))
      agencies = (json['agencies'] as Iterable)
          .map((e) => MOBAgency.fromJson(e))
          .toList();

    return MOBProject(
      projectId,
      name,
      city,
      country,
      latitude,
      longitude,
      hash,
      supportAlerts,
      supportContactMessages,
      isCharter,
      otpUri,
      apiUri,
      realtimeUpdateFreqSec,
      realtimeDeparturesOffset,
      agencies,
    );
  }

  Map<String, dynamic> toMap() => {
        'projectId': projectId,
        'name': name,
        'city': city,
        'country': country,
        'lat': latitude,
        'lon': longitude,
        'hash': hash,
        'supportAlerts': supportAlerts,
        'supportContactMessages': supportContactMessages,
        'charter': isCharter,
        'otpUri': otpUri,
        'apiUri': apiUri,
        'realtimeUpdateFreqSec': realtimeUpdateFreqSec,
        'realtimeDeparturesOffset': realtimeDeparturesOffset,
        'agencies': agencies.map((a) => a.toMap()),
      };
}

class MOBAgency {
  final int agencyId;
  final String name;
  final double latitude;
  final double longitude;

  MOBAgency(this.agencyId, this.name, this.latitude, this.longitude);

  factory MOBAgency.fromJson(Map<String, dynamic> json) {
    int agencyId = json['agencyId'] ?? 0;
    String name = json['name'] ?? null;
    double latitude = json['lat'] ?? 0.0;
    double longitude = json['lon'] ?? 0.0;
    return MOBAgency(agencyId, name, latitude, longitude);
  }

  Map<String, dynamic> toMap() => {
        'agencyId': agencyId,
        'name': name,
        'lat': latitude,
        'lon': longitude,
      };
}

class SelectableProject {
  final int projectId;
  final String name;
  final String city;
  final String country;

  SelectableProject(this.projectId, this.name, this.city, this.country);

  factory SelectableProject.fromJson(Map<String, dynamic> json) {
    int projectId = json['projectId'] ?? 0;
    String name = json['name'] ?? null;
    String city = json['city'] ?? null;
    String country = json['country'] ?? null;
    return SelectableProject(projectId, name, city, country);
  }
}
