class OTPTrip {
  final String from;
  final String to;
  final double fromLat;
  final double fromLng;
  final double toLat;
  final double toLng;

  OTPTrip(
    this.from,
    this.to,
    this.fromLat,
    this.fromLng,
    this.toLat,
    this.toLng,
  );

  factory OTPTrip.fromJson(Map<String, dynamic> json) {
    String from = json['from'];
    String to = json['to'];
    double fromLat = json['fromLat'];
    double fromLng = json['fromLng'];
    double toLat = json['toLat'];
    double toLng = json['toLng'];
    return OTPTrip(from, to, fromLat, fromLng, toLat, toLng);
  }

  Map toMap() => {
        'from': from,
        'to': to,
        'fromLat': fromLat,
        'fromLng': fromLng,
        'toLat': toLat,
        'toLng': toLng,
      };
}
