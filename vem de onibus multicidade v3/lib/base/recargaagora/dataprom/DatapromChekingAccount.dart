class DatapromChekingAccount {
  final List<DatapromRecord> records;
  DatapromChekingAccount(this.records);

  factory DatapromChekingAccount.fromJson(Map<String, dynamic> json) {
    List<DatapromRecord> records = json.containsKey('registros')
        ? (json['registros'] as Iterable)
            .map((e) => DatapromRecord.fromJson(e))
            .toList()
        : [];
    records = records.reversed.toList();
    return DatapromChekingAccount(records);
  }
}

class DatapromRecord {
  final DateTime operationDate;
  final int cardNumber;
  final String operation;
  final String lineCode;
  final String line;
  final String path;
  final String company;
  final String externalVehicleCode;
  final String ticketCollectorRegistration;
  final String ticketCollectorName;
  final double value;
  final double cardBalance;
  final double scheduledBalance;

  DatapromRecord(
    this.operationDate,
    this.cardNumber,
    this.operation,
    this.lineCode,
    this.line,
    this.path,
    this.company,
    this.externalVehicleCode,
    this.ticketCollectorRegistration,
    this.ticketCollectorName,
    this.value,
    this.cardBalance,
    this.scheduledBalance,
  );

  factory DatapromRecord.fromJson(Map<String, dynamic> json) {
    String strOperationDate = json['dataOperacao'] ?? '-';
    int cardNumber = json['numeroCarteira'] ?? -1;
    String operation = json['operacao'] ?? '-';
    String lineCode = json['codigoLinha'] ?? '-';
    String line = json['linha'] ?? '-';
    String path = json['trecho'] ?? '-';
    String company = json['empresa'] ?? '-';
    String externalVehicleCode = json['codigoExternoVeiculo'] ?? '-';
    String ticketCollectorRegistration = json['matriculaCobrador'] ?? '-';
    String ticketCollectorName = json['nomeCobrador'] ?? '-';
    double value = double.parse(json['valor'].toString()) ?? 0;
    double cardBalance = double.parse(json['saldoCartao'].toString()) ?? 0;
    double scheduledBalance =
        double.parse(json['saldoAgendado'].toString()) ?? 0;

    DateTime operationDate;
    try {
      operationDate = DateTime.parse(strOperationDate);
    } catch (e) {
      operationDate = DateTime.parse('1970-01-01T00:00:00');
    }

    return DatapromRecord(
      operationDate,
      cardNumber,
      operation,
      lineCode,
      line,
      path,
      company,
      externalVehicleCode,
      ticketCollectorRegistration,
      ticketCollectorName,
      value,
      cardBalance,
      scheduledBalance,
    );
  }
}
