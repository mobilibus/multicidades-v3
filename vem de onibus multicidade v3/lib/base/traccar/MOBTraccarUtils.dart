import 'dart:convert' as convert;

import 'package:http/http.dart' as http;
import 'package:mobilibus/base/traccar/MOBTraccarDevice.dart';
import 'package:mobilibus/base/traccar/MOBTraccarPosition.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class MOBTraccarUtils {
  static String mobTraccar = 'http://traccar.mobilibus.com';

  static String _traccarUsername = 'app';
  static String _traccarPassword = 'Y@wZAQj%NTndkP*oUU';
  static String _traccarToken = 'KhSs0ZYlKX9nytv1lLGqFIDV7aR1HmsU';

  static Future<void> sendTraccar(
      String id, double lat, double lng, double speed, double bearing) async {
    String base = '$mobTraccar:5055';
    String url = '$base/?id=$id&'
        'lat=$lat&'
        'lon=$lng&'
        'speed=$speed&'
        'bearing=$bearing';
    Map<String, String> headers = MOBApiUtils.headers;
    Map<String, String> customHeader = Map.of(headers);
    customHeader['token'] = _traccarToken;

    await http.get(url, headers: customHeader).timeout(Utils.timeoutDuration);
  }

  static Future<List<MOBTraccarDevice>> getTraccarDevices() async {
    String username = _traccarUsername;
    String password = _traccarPassword;
    String basicAuth = 'Basic ' +
        convert.base64Encode(convert.utf8.encode('$username:$password'));

    Map<String, String> headers = MOBApiUtils.headers;
    String url = '$mobTraccar:8082/api/devices';
    Map<String, String> customHeader = Map.of(headers);
    customHeader['token'] = _traccarToken;
    customHeader['Authorization'] = basicAuth;

    List<MOBTraccarDevice> devices = [];

    http.Response response = await http
        .get(url, headers: customHeader)
        .timeout(Utils.timeoutDuration);
    if (response != null) {
      String json = convert.utf8.decode(response.bodyBytes);
      Iterable iterable = convert.json.decode(json);
      devices = iterable.map((map) => MOBTraccarDevice.fromJson(map)).toList();
    }

    return devices;
  }

  static Future<List<MOBTraccarPosition>> getTraccarPositions(
      List<MOBTraccarDevice> traccarDevices) async {
    String username = _traccarUsername;
    String password = _traccarPassword;
    String basicAuth = 'Basic ' +
        convert.base64Encode(convert.utf8.encode('$username:$password'));

    List<int> positionIds =
        traccarDevices.map((device) => device.positionId).toList();

    String ids = '';
    for (final positionId in positionIds) {
      ids += '$positionId';
      if (positionId != positionIds.last) ids += '&id=';
    }

    Map<String, String> headers = MOBApiUtils.headers;
    String url = '$mobTraccar:8082/api/positions?id=$ids';
    Map<String, String> customHeader = Map.of(headers);
    customHeader['token'] = _traccarToken;
    customHeader['Authorization'] = basicAuth;

    List<MOBTraccarPosition> positions = [];

    http.Response response = await http
        .get(url, headers: customHeader)
        .timeout(Utils.timeoutDuration);
    if (response != null) {
      String json = convert.utf8.decode(response.bodyBytes);
      Iterable iterable = convert.json.decode(json);
      positions =
          iterable.map((map) => MOBTraccarPosition.fromJson(map)).toList();
    }

    return positions;
  }
}
