import 'package:intl/intl.dart';
import 'package:mobilibus/base/transdata/TransdataRef.dart';

class TransdataUser extends TransdataRef {
  final int type;
  final int subtype;
  final String typeName;
  final String subtypeName;
  final String name;
  final int id;
  final bool isCardBlocked;
  final int logicalSerial;
  final DateTime date;
  final int physicalSerial;
  final String cpf;

  TransdataUser(
    int errorCode,
    String errorMessage,
    this.type,
    this.subtype,
    this.typeName,
    this.subtypeName,
    this.name,
    this.id,
    this.isCardBlocked,
    this.logicalSerial,
    this.date,
    this.physicalSerial,
    this.cpf,
  ) : super(errorCode, errorMessage);

  factory TransdataUser.fromJson(
      Map<String, dynamic> json, String cpf, String numSerie) {
    int errorCode = json['errorCode'] ?? null;
    String errorMessage = json['errorMessage'] ?? null;
    int type = json['Tipo'] ?? null;
    int subtype = json['SubTipo'] ?? null;
    String typeName = json['TipoStr'] ?? null;
    String subtypeName = json['SubTipoStr'] ?? null;
    String name = json['Nome'] ?? null;
    int id = json['CadastroId'] ?? null;
    bool isCardBlocked = json['CartaoBloqueado'] ?? null;
    int logicalSerial = json['SerialLogico'] ?? null;
    DateTime date = DateTime.parse(
            ((json['DataCadastro'] ?? '1970-01-01 00:00:00') as String)
                .replaceAll('T', ' ')) ??
        null;
    int physicalSerial =
        ((json['SerialFisico'] ?? 0) == 0 ? int.parse(numSerie) : null);
    return TransdataUser(
      errorCode,
      errorMessage,
      type,
      subtype,
      typeName,
      subtypeName,
      name,
      id,
      isCardBlocked,
      logicalSerial,
      date,
      physicalSerial,
      cpf,
    );
  }
}

class TransdataUserBalances extends TransdataRef {
  final List<TransdataUserBalanceItem> balances;

  TransdataUserBalances(
    int errorCode,
    String errorMessage,
    this.balances,
  ) : super(errorCode, errorMessage);

  factory TransdataUserBalances.fromJson(Map<String, dynamic> json) {
    int errorCode = json['errorCode'] ?? null;
    String errorMessage = json['errorMessage'] ?? null;

    Iterable values = json['Valor'] ?? [];

    List<TransdataUserBalanceItem> balances =
        values.map((e) => TransdataUserBalanceItem.fromJson(e)).toList();

    return TransdataUserBalances(errorCode, errorMessage, balances);
  }
}

class TransdataUserBalanceItem {
  final int numSerie;
  final String cpf;
  final DateTime date;
  final int balanceInCents;

  TransdataUserBalanceItem(
    this.numSerie,
    this.cpf,
    this.date,
    this.balanceInCents,
  );

  factory TransdataUserBalanceItem.fromJson(Map<String, dynamic> json) {
    int numSerie = json['NumSerie'];
    String cpf = json['CPF'];
    DateTime date = DateFormat('yyyy-MM-dd HH:mm:ss')
        .parse((json['DataSaldo'] ?? '1970-01-01 00:00:00'));
    int balanceInCents = json['SaldoEmCentavos'];
    return TransdataUserBalanceItem(numSerie, cpf, date, balanceInCents);
  }
}
