import 'dart:convert' as convert;

import 'package:encrypt/encrypt.dart';

class Transdata {
  final String key;
  int _seq;
  final String sid;
  final String version;

  Transdata(this.key, this._seq, this.sid, this.version);

  factory Transdata.fromJson(Map<String, dynamic> json, Encrypter encrypter, String version) {
    String data = json['data'] ?? null;

    String decrypted = encrypter.decrypt64(data);
    Map<String, dynamic> mapDecrypted = convert.json.decode(decrypted);
    String key = mapDecrypted['Key'];
    int seq = mapDecrypted['Seq'];
    String sid = mapDecrypted['Sid'];

    return Transdata(key, seq, sid, version);
  }

  int get currentSeq => _seq;

  int nextSeq() => _seq += 1;
}
