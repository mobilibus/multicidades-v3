import 'dart:convert' as convert;
import 'dart:typed_data';

import 'package:dart_des/dart_des.dart';
import 'package:encrypt/encrypt.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/base/transdata/Transdata.dart';
import 'package:mobilibus/base/transdata/TransdataHistory.dart';
import 'package:mobilibus/base/transdata/TransdataProduct.dart';
import 'package:mobilibus/base/transdata/TransdataProductDetail.dart';
import 'package:mobilibus/base/transdata/TransdataUser.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

enum LOGIN {
  DEFAULT,
  CPF,
  ID,
}

enum PRODUCT_STATUS {
  WAITING_PAYMENT, //0
  PAYD, //1
  EXPIRED, //2
  DENIED, //3
  CANCELLED_BY_INTEGRATOR, //4
  OTHER, //9
}

enum PRODUCT_CANCEL {
  EXPIRED, //2
  DENIED, //3
  CANCELLED_BY_INTEGRATOR, //4
  OTHER, // 99
}

class TransdataAPIs {
  final Transdata transdata;
  final Encrypter encrypter;
  static String _url = 'http://rdv.teste.itstransdata.com:1900';
  static String _handShake = 'http://rdv.teste.itstransdata.com:9090';
  static final _headers = {
    'Content-Type': 'application/json',
  };

  TransdataAPIs(this.transdata, this.encrypter, bool isDev) {
    _url = isDev
        ? 'http://rdv.teste.itstransdata.com:1900'
        : 'https://rdv.itstransdata.com:1901';
    _handShake = isDev
        ? 'http://rdv.teste.itstransdata.com:9090'
        : 'https://rdv.itstransdata.com';
  }

  int getCancelIdByReason(PRODUCT_CANCEL reason) {
    switch (reason) {
      case PRODUCT_CANCEL.EXPIRED:
        return 2;
        break;
      case PRODUCT_CANCEL.DENIED:
        return 3;
        break;
      case PRODUCT_CANCEL.CANCELLED_BY_INTEGRATOR:
        return 4;
        break;
      case PRODUCT_CANCEL.OTHER:
        return 99;
        break;
    }
    return 99;
  }

  Future<Transdata> handShake(bool isDev) async {
    ///
    /// parceiro = ID 88  --> Mobilibus desenvolvimento e consultoria de sistemas ltda
    /// cliente = ID 427  --> Mobicard Gestão de Créditos Inteligentes - RIO GRANDE/RS
    ///
    Map<String, dynamic> map = {
      'Cid': isDev ? 9999 : 427,
      'Pid': isDev ? 1 : 88,
      'Uid': 'Mobilibus',
      'App': MOBApiUtils.headers['x-mob-package'],
    };

    String json = convert.json.encode(map);

    Encrypted encrypted = encrypter.encrypt(json);
    String base64 = encrypted.base64;

    String encoded = Uri.encodeComponent(base64);

    String endPoint = 'commerce/api/Authentication/HandShake';
    String url = '$_handShake/$endPoint?clienteInfo=$encoded';

    String version;

    http.Response versionResponse = await http.get('$_url/versao/str');
    if (versionResponse != null && versionResponse.statusCode == 200) {
      String jsonString = convert.utf8.decode(versionResponse.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(jsonString);
      String mVersion = map['Valor'];
      if (mVersion == null) return null;
      version = mVersion;
    } else
      return null;

    http.Response response =
        await http.get(url, headers: _headers).timeout(Utils.timeoutDuration);
    if (response != null) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(jsonString);
      if (map['sucesso'] ?? false)
        return Transdata.fromJson(map, encrypter, version);
      else
        return Transdata(null, null, null, null);
    } else
      return null;
  }

  String _getSeq() {
    //Next sequence
    int seq = transdata.nextSeq(); //INC
    List<int> bytes = convert.utf8.encode(seq.toString());
    bytes = [bytes[0], 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0];

    //Decode base64 24 bytes length
    String key = transdata.key;
    Uint8List data = convert.base64Decode(key);

    DES3 desECB = DES3(key: data, mode: DESMode.ECB, iv: DES.IV_ZEROS);
    List<int> encryptedSeq = desECB.encrypt(bytes);

    String value = convert.base64Encode(encryptedSeq);
    return value;
  }

  ///
  /// LOGIN USER
  ///

  Future<TransdataUser> readUserDefault(String cardSerial, String cpf) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'numserie': cardSerial,
      'cpf': cpf,
    });

    String url = '$_url/info/LerUsuario';

    http.Response response = await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
    if (response != null && response.statusCode == 200) {
      String jsonString = convert.utf8.decode(response.bodyBytes);

      Map<String, dynamic> map = convert.json.decode(jsonString);
      TransdataUser user = TransdataUser.fromJson(map, cpf, cardSerial);
      return user;
    }
    return null;
  }
/* 
  Future<TransdataUser> readUserByCPF(String cpf, int userId) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'cpf': cpf,
      'cadastroid': userId,
    });

    String url = '$_testUrl/info/LerUsuarioPorCadastroCpf';
    http.Response response = await http.post(url, body: body, headers: _headers).timeout(Utils.timeoutDuration);
    if (response != null && response.statusCode == 200) {
      String jsonString = convert.utf8.decode(response.bodyBytes);

      Map<String, dynamic> map = convert.json.decode(jsonString);
      TransdataUser user = TransdataUser.fromJson(map, cpf);
      return user;
    }
    return null;
  }
   */
/* 
  Future<TransdataUser> readUserByID(int cardSerial, int userId) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'numserie': cardSerial,
      'cadastroid': userId,
    });

    String url = '$_testUrl/info/LerUsuarioPorCadastro';
    http.Response response = await http.post(url, body: body, headers: _headers).timeout(Utils.timeoutDuration);
    if (response != null && response.statusCode == 200) {
      String jsonString = convert.utf8.decode(response.bodyBytes);

      Map<String, dynamic> map = convert.json.decode(jsonString);
      TransdataUser user = TransdataUser.fromJson(map, cpf);
      return user;
    }
    return null;
  } */

  Future<http.Response> userCardExtract(
      int cardSerial, String cpf, String startDate, String endDate) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'numserie': cardSerial,
      'cpf': cpf,
      'inicio': startDate,
      'fim': endDate,
    });

    String url = '$_url/info/GerarExtrato';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  Future<http.Response> getBalances(
    List<String> cpfs,
    List<int> cardSerials,
  ) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'cpf': cpfs,
      'numserie': cardSerials,
    });

    String url = '$_url/info/SaldoPorCartao';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  ///
  /// History
  ///

  Future<TransdataHistory> getHistory(
      int cardSerial, String cpf, String startDate, String endDate) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'dataini': startDate,
      'datafim': endDate,
      'NumeroSerie': cardSerial,
      'cpf': cpf,
    });

    String url = '$_url/pedido/historico';
    http.Response response = await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
    if (response != null && response.statusCode == 200) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(jsonString);
      return TransdataHistory.fromJson(map);
    }
    return null;
  }

  Future<TransdataProductDetail> getHistoryDetail(String id) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'IdPedido': id,
    });

    String url = '$_url/pedido/detalhe';

    http.Response response = await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
    if (response != null && response.statusCode == 200) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(jsonString);
      return TransdataProductDetail.fromJson(map);
    }

    return null;
  }

  ///
  /// BUY
  ///

  Future<http.Response> getBuyRequestsByCard(String idBuyer) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'IdComprador': idBuyer,
    });

    String url = '$_url/pedido/PorCartao';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  Future<http.Response> getTaxes(int cardSerial, String cpf,
      List<int> productIds, List<double> values) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'numserie': cardSerial,
      'cpf': cpf,
      'produto': productIds,
      'valores': values,
    });

    String url = '$_url/pedido/PorCartao';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  Future<http.Response> getReceipt(String id) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'IdPedido': id,
    });

    String url = '$_url/pedido/nota';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  Future<http.Response> newPurchase(
    int cardSerial,
    String cpf,
    int taxInCents,
    List<TransdataProductItem> items,
  ) async {
    String seq = _getSeq();
    String token = transdata.sid;

    var headers = MOBApiUtils.headers;
    var package = headers['x-mob-package'];
    var version = headers['x-mob-version'];
    String appName = '$package-$version';

    List mItems = items
        .map((e) => {
              'Produto': e.productId,
              'ValorUnitarioEmCentavos': e.unitValue,
              'ValorCompraEmCentavos': e.unitValue,
            })
        .toList();

    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'pedido': {
        'NumeroSerie': cardSerial,
        'TaxasEmCentavos': taxInCents,
        'CPF': cpf,
        'IDCompraExterno': 'app_$appName',
        'IDComprar': 'transdata_api_mobilibus',
        'AppNome_Versao': transdata.version,
        'Itens': mItems,
      }
    });

    String url = '$_url/pedido/novo';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  Future<http.Response> confirmPurchase(String id) async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'IdPedido': id,
    });

    String url = '$_url/pedido/aprovar';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  Future<http.Response> cancelPurchase(String id, int reasonNumber) async {
    if (reasonNumber != 2 &&
        reasonNumber != 3 &&
        reasonNumber != 4 &&
        reasonNumber != 99) return null;
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'IdPedido': id,
      'motivo': reasonNumber,
    });

    String url = '$_url/pedido/fechar';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  ///
  /// EMBEDED CHARGE
  ///

  Future<http.Response> getCpfProducts(int cardSerial, String cpf) async {
    String seq = _getSeq();
    String token = transdata.sid;

    String params = 'tk=$token';
    params += '&seq=$seq';
    params += '&cpf=$cpf';
    params += '&ns=$cardSerial';

    String url = '$_url/ce/getProdutosCpf?$params';
    return await http
        .get(url, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  Future<TransdataProduct> getRegisteredProducts(
      int userId, int cardSerial) async {
    String seq = _getSeq();
    String token = transdata.sid;

    String params = 'tk=$token';
    params += '&seq=$seq';
    params += '&cad=$userId';
    params += '&ns=$cardSerial';

    String url = '$_url/ce/getProdutosCadastro?$params';
    http.Response response =
        await http.get(url, headers: _headers).timeout(Utils.timeoutDuration);
    if (response != null && response.statusCode == 200) {
      String json = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(json);
      return TransdataProduct.fromJson(map);
    }
    return null;
  }

  Future<http.Response> createSubscription() async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'info': {},
    });

    String url = '$_url/ce/venda/assinatura';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  Future<http.Response> confirmSubscription() async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'status': {},
    });

    String url = '$_url/ce/venda/confirmar';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }

  Future<http.Response> cancelSubscription() async {
    String seq = _getSeq();
    String token = transdata.sid;
    String body = convert.json.encode({
      'token': token,
      'seq': seq,
      'status': {},
    });

    String url = '$_url/ce/venda/cancelar';
    return await http
        .post(url, body: body, headers: _headers)
        .timeout(Utils.timeoutDuration);
  }
}
