abstract class TransdataRef {
  final int errorCode;
  final String errorMessage;

  TransdataRef(this.errorCode, this.errorMessage);
}
