import 'package:flutter/material.dart';
import 'package:mobilibus/app/select_city_urban.dart';

class ChooseCity extends StatelessWidget {
  const ChooseCity({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SelectCityUrban();
  }
}
