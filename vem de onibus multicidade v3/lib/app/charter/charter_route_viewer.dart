import 'dart:async';
import 'dart:convert' as convert;

import 'package:admob_flutter/admob_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:mobilibus/app/charter/charter_route_departures.dart';
import 'package:mobilibus/base/MOBCharterCompany.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/base/MOBTripDetails.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CharterRouteViewer extends StatefulWidget {
  CharterRouteViewer(
      this.stop,
      this.routeId,
      this.tripId,
      this.tripDetails,
      this.polyline,
      this.polylineOSRM,
      this.stops,
      this.vehicles,
      this.directionsMarker,
      this.directionsMarkerFar,
      this.center,
      this.bounds,
      this.showAllStops);

  final MOBStop stop;
  final int routeId;
  final int tripId;
  final MOBTripDetails tripDetails;
  final Polyline polyline;
  final Polyline polylineOSRM;
  final List<MOBTripDetailsStop> stops;
  final List<MOBTripDetailsVehicle> vehicles;
  final List<Marker> directionsMarker;
  final List<Marker> directionsMarkerFar;
  final LatLng center;
  final LatLngBounds bounds;
  final bool showAllStops;

  _CharterRouteViewer createState() => _CharterRouteViewer(
        stop,
        routeId,
        tripId,
        tripDetails,
        polyline,
        polylineOSRM,
        stops,
        vehicles,
        directionsMarker,
        directionsMarkerFar,
        center,
        bounds,
        showAllStops,
      );
}

class _CharterRouteViewer extends State<CharterRouteViewer> {
  _CharterRouteViewer(
      this.stop,
      this.routeId,
      this.tripId,
      this.tripDetails,
      this.polyline,
      this.polylineOSRM,
      this.stops,
      this.vehicles,
      this.directionsMarker,
      this.directionsMarkerFar,
      this.center,
      this.bounds,
      this.showAllStops)
      : super();

  final MOBStop stop;
  final int routeId;
  final int tripId;
  final MOBTripDetails tripDetails;
  final Polyline polyline;
  final Polyline polylineOSRM;
  final List<MOBTripDetailsStop> stops;
  final List<MOBTripDetailsVehicle> vehicles;
  final List<Marker> directionsMarker;
  final List<Marker> directionsMarkerFar;
  final LatLng center;
  final LatLngBounds bounds;
  final bool showAllStops;

  Map<int, Marker> stopMarkers = {};
  Map<String, Marker> vehiclesMarkers = {};

  GoogleMapController controller;
  BitmapDescriptor iconStop;
  BitmapDescriptor iconBus;
  BitmapDescriptor iconBus0;
  BitmapDescriptor iconBus45;
  BitmapDescriptor iconBus90;
  BitmapDescriptor iconBus135;
  BitmapDescriptor iconBus180;
  BitmapDescriptor iconBus225;
  BitmapDescriptor iconBus270;
  BitmapDescriptor iconBus315;
  double zoom = 0.0;
  CameraPosition cameraPosition;

  Timer timerUpdate;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  AdmobBanner admob = AdmobBanner(
    adUnitId: Utils.getBannerAdUnitId(),
    adSize: AdmobBannerSize.BANNER,
  );

  MarkerId selectedVehicle;
  String selectedVehicleId = '';
  String selectedLastCommunication = '';

  @override
  void dispose() {
    if (timerUpdate != null) timerUpdate.cancel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(), () async {
      Size size2_1 = Size(65, 72);
      Size size1_2 = Size(72, 65);
      Size size1_1 = Size(72, 72);

      int width = 100;

      iconBus0 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_0.svg', size2_1, width));
      iconBus180 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_180.svg', size2_1, width));

      iconBus270 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_270.svg', size1_2, width));
      iconBus90 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_90.svg', size1_2, width));

      iconBus135 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_135.svg', size1_1, width));
      iconBus45 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_45.svg', size1_1, width));
      iconBus225 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_225.svg', size1_1, width));
      iconBus315 = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, 'assets/route/arrow_315.svg', size1_1, width));

      String strAssetBusStop = 'assets/images/pin_bus_stop_gmaps.svg';

      iconStop = BitmapDescriptor.fromBytes(await Utils.bytesFromSvgAsset(
          context, strAssetBusStop, Size(64, 64), 64));

      TextStyle textStyle = TextStyle();

      SnackBar snackBar = SnackBar(
        duration: Duration(seconds: 5),
        content: Wrap(
          alignment: WrapAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text('Clique no', style: textStyle),
                SvgPicture.asset('assets/route/arrow_0.svg',
                    width: 30, height: 30),
                Text('para ver mais detalhes.', style: textStyle),
              ],
            ),
            Text('Ou aproxime para visualizar as paradas.', style: textStyle),
          ],
        ),
      );
      Future.delayed(Duration(seconds: 2),
          () => Scaffold.of(scaffoldKey.currentContext).showSnackBar(snackBar));

      timerUpdate = Timer.periodic(
          Duration(seconds: 3), (timer) async => await getTripDetails());
      await getTripDetails();

      buildStopsMarkers();
      for (final vehicle in vehicles) createVehicleMarkerAndContainer(vehicle);
    });
  }

  Future<void> getTripDetails() async {
    http.Response response = await MOBApiUtils.getTripDetailsFuture(tripId);
    if (response != null) {
      String body = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> object = convert.json.decode(body);
      MOBTripDetails tripDetails = MOBTripDetails.fromJson(object);
      for (final newVehicle in tripDetails.vehicles)
        createVehicleMarkerAndContainer(newVehicle);
      setState(() {});
    }
  }

  Future<void> fitBounds() async =>
      controller.animateCamera(CameraUpdate.newLatLngBounds(bounds, 100));

  void buildStopsMarkers() async {
    for (int i = 0; i < stops.length; i++) {
      MOBTripDetailsStop s = stops[i];
      double lat = s.latitude;
      double lng = s.longitude;
      LatLng position = LatLng(lat, lng);

      Marker m = Marker(
          markerId: MarkerId(position.hashCode.toString()),
          icon: iconStop,
          position: position,
          onTap: () {
            selectedVehicle = null;
            setState(() {});
            controller.animateCamera(CameraUpdate.newLatLngZoom(position, 15));
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        CharterRouteDepartures(routeId, s.stopId, tripId)));
          });
      stopMarkers[s.stopId] = m;
    }
  }

  void createVehicleMarkerAndContainer(MOBTripDetailsVehicle v) {
    MarkerId id = MarkerId(v.hashCode.toString());
    String date = v.positionTime.toString();
    DateTime dateTime = DateTime.parse(date);
    DateFormat dateFormat = DateFormat('dd/MM/yyyy HH:mm');
    String formated = dateFormat.format(dateTime);

    String vehicleId = v.vehicleId;
    String lastComunication = 'Última comunicação: $formated';

    if (selectedVehicleId == vehicleId) {
      selectedVehicleId = vehicleId;
      selectedLastCommunication = lastComunication;
    }

    double lat = v.latitude;
    double lng = v.longitude;
    LatLng position = LatLng(lat, lng);

    BitmapDescriptor iconBus;
    int heading = v.heading;
    if (heading >= 23 && heading <= 67)
      iconBus = iconBus45;
    else if (heading >= 68 && heading <= 112)
      iconBus = iconBus90;
    else if (heading >= 113 && heading <= 157)
      iconBus = iconBus135;
    else if (heading >= 158 && heading <= 202)
      iconBus = iconBus180;
    else if (heading >= 203 && heading <= 247)
      iconBus = iconBus225;
    else if (heading >= 248 && heading <= 292)
      iconBus = iconBus270;
    else if (heading >= 293 && heading <= 337)
      iconBus = iconBus315;
    else //0-22, 338-359
      iconBus = iconBus0;

    Marker m = Marker(
        markerId: id,
        icon: iconBus,
        position: position,
        onTap: () {
          controller.animateCamera(CameraUpdate.newLatLngZoom(position, 15));
          selectedVehicle = id;
          selectedVehicleId = v.vehicleId;
          selectedLastCommunication = lastComunication;
        });
    vehiclesMarkers[v.vehicleId] = m;
  }

  Widget tripHeader() => Container(
        color: Color(0xff25303E),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            if (Utils.isMobilibus)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Chip(label: Text('routeId=$routeId')),
                  Chip(label: Text('tripId=$tripId')),
                ],
              ),
            Text(
              tripDetails.tripName,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 14,
                color: Colors.white,
              ),
            ),
            if (selectedVehicleId.isNotEmpty && selectedVehicle != null)
              Container(
                padding: EdgeInsets.all(10),
                color: Color(0xff25303E),
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SvgPicture.asset(
                        'assets/images/bus.svg',
                        height: 50,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(selectedVehicleId,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 20,
                              )),
                          Text(
                            selectedLastCommunication,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.close,
                          color: Colors.white,
                        ),
                        onPressed: () => setState(
                          () {
                            selectedVehicle = null;
                            selectedVehicleId = '';
                            selectedLastCommunication = '';
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            if (stop != null && Utils.charterType != CHARTER_TYPE.NO_LOGIN_MAP)
              Container(
                color: Colors.white,
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 20),
                        child: Icon(Icons.people),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(stop.name),
                            Text(stop.distance.toStringAsFixed(0) +
                                'm ' +
                                Utils.directionByDegrees(stop.degrees) +
                                ' - ${stop.distance ~/ 60} min a pé'),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
          ],
        ),
      );

  void onMapCreated(mapController) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isDark = prefs.getBool('isDark') ?? false;
    controller = mapController;
    Completer<GoogleMapController>().complete(controller);
    rootBundle
        .loadString(isDark
            ? 'assets/maps_style_dark.json'
            : 'assets/maps_style_default.json')
        .then((style) => controller.setMapStyle(style));
    controller.moveCamera(CameraUpdate.newLatLngZoom(
        LatLng(center.latitude, center.longitude), 15.0));
    Future.delayed(Duration(seconds: 2), () async {
      await fitBounds();
      //Navigator.of(context).pop();
    });
  }

  void onCameraMove(CameraPosition cameraPosition) {
    zoom = cameraPosition.zoom;
    setState(() => this.cameraPosition = cameraPosition);
  }

  @override
  Widget build(BuildContext context) {
    List<Marker> markers = vehiclesMarkers.values.toList();
    if (showAllStops && zoom >= 15) //&& Utils.charterType != CHARTER_TYPE.LOGIN
      markers.addAll(stopMarkers.values);
    if (zoom >= 13)
      markers.addAll(directionsMarker);
    else
      markers.addAll(directionsMarkerFar);
    if (stop != null)
      markers.add(Marker(
          markerId: MarkerId('nearestStopMarker'),
          icon: iconStop,
          position: LatLng(stop.latitude, stop.longitude),
          onTap: () {
            selectedVehicle = null;
            setState(() {});
            LatLng position = LatLng(stop.latitude, stop.longitude);
            controller.animateCamera(CameraUpdate.newLatLngZoom(position, 15));
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        CharterRouteDepartures(routeId, stop.stopId, tripId)));
          }));

    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        key: scaffoldKey,
        backgroundColor: Color(0xff229fd5),
        title: Text(
          'Acompanhe seu veículo',
          style: textStyle,
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: themeColor,
          ),
          onPressed: () {
            if (timerUpdate != null) timerUpdate.cancel();
            Navigator.of(context).pop();
          },
        ),
        actions: <Widget>[
          if (Utils.isMobilibus)
            IconButton(
              icon: Icon(
                Icons.refresh,
                size: 35,
              ),
              onPressed: () => getTripDetails(),
            ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'fitBoundsTag',
        backgroundColor: Color(0xff229fd5),
        child: Icon(Icons.zoom_out_map, size: 40),
        onPressed: () => fitBounds(),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            myLocationEnabled: true,
            trafficEnabled: false,
            zoomGesturesEnabled: true,
            myLocationButtonEnabled: false,
            mapToolbarEnabled: false,
            zoomControlsEnabled: false,
            minMaxZoomPreference: MinMaxZoomPreference(5, 20),
            mapType: MapType.normal,
            markers: Set<Marker>.of(markers),
            polylines: [
              polyline,
              if (Utils.isMobilibus && polylineOSRM != null) polylineOSRM
            ].toSet(),
            onMapCreated: onMapCreated,
            onCameraMove: onCameraMove,
            initialCameraPosition: CameraPosition(
              zoom: 15,
              target: LatLng(center.latitude, center.longitude),
            ),
          ),
          Column(
            children: <Widget>[
              tripHeader(),
            ],
          )
        ],
      ),
    );
  }
}
