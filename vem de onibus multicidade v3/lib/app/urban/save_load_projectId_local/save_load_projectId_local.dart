import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class ILocalStorage {
  Future get(String key);
  Future delete(String key);
  Future put(String key, dynamic value);
}

class SharedLocalStorageServices implements ILocalStorage {
  @override
  Future delete(String key) async {
    var shared = await SharedPreferences.getInstance();
    return shared.remove(key);
  }

  @override
  Future get(String key) async {
    var shared = await SharedPreferences.getInstance();
    return shared.get(key);
  }

  @override
  Future put(String key, value) async {
    var shared = await SharedPreferences.getInstance();
    if (value is bool) {
      return shared.setBool(key, value);
    } else if (value is double) {
      return shared.setDouble(key, value);
    } else if (value is int) {
      return shared.setInt(key, value);
    } else if (value is String) {
      return shared.setString(key, value);
    } else if (value is List<String>) {
      return shared.setStringList(key, value);
    }
  }
}

class SaveLoadSharedLocalStorageViewModel {

  SharedLocalStorageServices storage = SharedLocalStorageServices();

  setProjectId(int projectID,) {
    storage.put('projectIDLocal', projectID);
    Utils.globalProjectID  = projectID;
  }

  getProjectId() async {
    Utils.globalProjectID = await storage.get('projectIDLocal') ?? Utils.globalProjectID;
  }

}