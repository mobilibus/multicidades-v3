import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/app/urban/tripplaner/viewer/trip_planner_viewer.dart';
import 'package:mobilibus/base/otp/OTPPlan.dart';
import 'package:mobilibus/utils/Utils.dart';

class TripPlannerViewerCardBuilder {
  TripPlannerViewerCardBuilder(this.context, {this.mapController}) : super();
  final BuildContext context;
  GoogleMapController mapController;
  int index = -1;
  Map<int, List> legByIndex = {};

  void goTo(int index)
  {

    List values = legByIndex[index];

    if (values.length == 1)
    {
      LatLng latLng = values.first;

      mapController.animateCamera(CameraUpdate.newLatLngZoom(latLng, 19));
    }
  else
      {

      LatLng first = values.first;
      LatLng last = values.last;

      LatLngBounds bounds;

      try {
        bounds = LatLngBounds(southwest: first, northeast: last);
      } catch (e) {
        bounds = LatLngBounds(southwest: last, northeast: first);
      }

      //mapController.animateCamera(CameraUpdate.newLatLngBounds(bounds, 70));


      LatLng centerBounds = LatLng
        (
          (bounds.northeast.latitude + bounds.southwest.latitude)/2,
          (bounds.northeast.longitude + bounds.southwest.longitude)/2
        );

// setting map position to centre to start with
        mapController.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: centerBounds,
          zoom: 13,
        )));

        zoomToFit(mapController, bounds, centerBounds);
    }

  }

  bool fits(LatLngBounds fitBounds, LatLngBounds screenBounds) {
    final bool northEastLatitudeCheck = screenBounds.northeast.latitude >= fitBounds.northeast.latitude;
    final bool northEastLongitudeCheck = screenBounds.northeast.longitude >= fitBounds.northeast.longitude;

    final bool southWestLatitudeCheck = screenBounds.southwest.latitude <= fitBounds.southwest.latitude;
    final bool southWestLongitudeCheck = screenBounds.southwest.longitude <= fitBounds.southwest.longitude;

    return northEastLatitudeCheck && northEastLongitudeCheck && southWestLatitudeCheck && southWestLongitudeCheck;
  }

  Future<void> zoomToFit(GoogleMapController controller, LatLngBounds bounds, LatLng centerBounds) async {
    bool keepZoomingOut = true;

    while(keepZoomingOut) {
      final LatLngBounds screenBounds = await controller.getVisibleRegion();
      if(fits(bounds, screenBounds)){
        keepZoomingOut = false;
        final double zoomLevel = await controller.getZoomLevel() - 0.05;
        controller.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: centerBounds,
          zoom: zoomLevel,
        )));
        break;
      }
      else {
        // Zooming out by 0.1 zoom level per iteration
        final double zoomLevel = await controller.getZoomLevel() - 0.02;
        controller.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: centerBounds,
          zoom: zoomLevel,
        )));
      }
    }
  }

  void buildItineraryLegWidget(
      OTPLeg leg, List<Widget> widgets, bool accessibility) {
    DateTime start = DateTime.fromMillisecondsSinceEpoch(leg.from.departure);

    String duration = DateFormat('HH:mm').format(start).replaceAll(':', 'h');

    Color color = Utils.isLightTheme(context)
        ? Colors.black
        : Utils.getColorFromPrimary(context);
    OTP_MODE mode = leg.mode;

    switch (mode) {
      case OTP_MODE.WALK:
        index++;
        //String steps = buildWalkSteps(leg.steps);
        //int meters = leg.distance.toInt();
        String steps = buildWalkStepsDirection(leg.steps);
        //String steps = 'Caminhe por $meters metros $compass';
        LatLng start = LatLng(leg.from.latitude, leg.from.longitude);
        LatLng end = LatLng(leg.to.latitude, leg.to.longitude);
        legByIndex[index] = [start, end];
        widgets.add(cardWalkWidget(steps, duration, color));
        break;
      case OTP_MODE.CAR:
      case OTP_MODE.CABLE_CAR:
      case OTP_MODE.GONDOLA:
      case OTP_MODE.FUNICULAR:
      case OTP_MODE.BUSISH:
      case OTP_MODE.CUSTOM_MOTOR_VEHICLE:
      case OTP_MODE.BUS:
      case OTP_MODE.SUBWAY:
      case OTP_MODE.TRAINISH:
      case OTP_MODE.BICYCLE:
      case OTP_MODE.RAIL:
      case OTP_MODE.TRAM:
      case OTP_MODE.FERRY:
      case OTP_MODE.TRANSIT:
      case OTP_MODE.LEG_SWITCH:
        OTPDirection directionFrom = leg.from;
        OTPDirection directionTo = leg.to;

        DateFormat dateFormat = DateFormat('HH:mm');

        DateTime dtArrival =
            DateTime.fromMillisecondsSinceEpoch(directionTo.arrival);
        String arrival = dateFormat.format(dtArrival).replaceAll(':', 'h');

        DateTime dtDeparture =
            DateTime.fromMillisecondsSinceEpoch(directionFrom.departure);
        String departure = dateFormat.format(dtDeparture).replaceAll(':', 'h');

        String durationRealTime = duration.toString();

        String nameFrom = directionFrom.name;
        String nameTo = directionTo.name;

        if (leg.realTime) {
          int depDelay = leg.departureDelay ~/ 60;
          if (depDelay < 0) depDelay *= -1;
          if (depDelay < 1)
            durationRealTime = '<1min';
          else
            durationRealTime = (depDelay).toString() + 'min';
        }

        LatLng startVehicle = LatLng(leg.from.latitude, leg.from.longitude);
        LatLng endVehicle = LatLng(leg.to.latitude, leg.to.longitude);

        String asset = 'assets/images';
        IconData vehicleIcon = Icons.directions_bus;
        if (mode == OTP_MODE.BUS)
          asset += '/pin_bus_stop_gmaps.svg';
        else if (mode == OTP_MODE.SUBWAY) {
          vehicleIcon = Icons.directions_subway;
          asset += '/pin_subway_stop_gmaps.svg';
        } else if (mode == OTP_MODE.RAIL) {
          vehicleIcon = Icons.directions_railway;
          asset += '/pin_train_stop_gmaps.svg';
        }

        String headSign = leg.headsign ?? '';
        String shortName = leg.routeShortName ?? '';
        String longName = leg.routeLongName ?? '';
        String routeColor = leg.routeColor ?? '';
        String routeTextColor = leg.routeTextColor ?? '';
        String vehicleId = leg.vehicleId ?? '';

        if (accessibility)
          widgets.add(
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                cardVehicleEnterWidget(nameFrom, departure, asset, color),
                cardVehicleWidget(
                  durationRealTime,
                  headSign,
                  shortName,
                  longName,
                  color,
                  routeColor,
                  routeTextColor,
                  vehicleId,
                  vehicleIcon,
                  accessibility,
                ),
                cardVehicleExitWidget(nameTo, arrival, asset, color),
              ],
            ),
          );
        else {
          /// Enter vehicle
          index++;
          legByIndex[index] = [startVehicle];
          widgets
              .add(cardVehicleEnterWidget(nameFrom, departure, asset, color));

          /// Vehicle full trip
          index++;
          legByIndex[index] = [startVehicle, endVehicle];
          widgets.add(cardVehicleWidget(
            durationRealTime,
            headSign,
            shortName,
            longName,
            color,
            routeColor,
            routeTextColor,
            vehicleId,
            vehicleIcon,
            accessibility,
          ));

          /// Exit vehicle
          index++;
          legByIndex[index] = [endVehicle];
          Widget vehicleEnd =
              cardVehicleExitWidget(nameTo, arrival, asset, color);
          widgets.add(vehicleEnd);
        }
        break;
    }
  }

  String buildWalkStepsDirection(List<OTPStep> steps) {
    String stepByStep = '';
    for (final step in steps) {
      String distance = step.distance.round().toString();
      String relativeDirection = step.relativeDirection.toLowerCase();
      //String streetName = stestreetName;

      if (relativeDirection == 'DEPART')
        stepByStep += 'Siga por $distance\m\n';
      else {
        String direction = 'para';
        if (relativeDirection.contains('left'))
          direction = '$direction esquerda';
        else if (relativeDirection.contains('right'))
          direction = '$direction direita';
        else if (relativeDirection.contains('continue'))
          direction = '$direction frente';
        else if (relativeDirection.contains('circle_clockwise'))
          direction = 'no sentido horário';
        else if (relativeDirection.contains('circle_counterclockwise'))
          direction = 'no sentido anti-horário';
        else
          direction = 'em frente';

        int meters = step.distance.toInt();
        stepByStep += 'Caminhe por $meters metros $direction\n';
        //stepByStep += 'Vire a $direction em $streetName\n';
      }
    }
    return stepByStep;
  }

  String buildWalkStepsCompass(List<OTPStep> steps) {
    String stepByStep = '';
    for (final step in steps) {
      String absoluteDirection = step.absoluteDirection;
      String desc = 'para';
      if (absoluteDirection == 'NORTH')
        desc = '$desc Norte';
      else if (absoluteDirection == 'SOUTH')
        desc = '$desc Sul';
      else if (absoluteDirection == 'WEST')
        desc = '$desc Oeste';
      else if (absoluteDirection == 'EAST')
        desc = '$desc Leste';
      else if (absoluteDirection == 'NORTHWEAST')
        desc = '$desc Noroeste';
      else if (absoluteDirection == 'NORTHEAST')
        desc = '$desc Nordeste';
      else if (absoluteDirection == 'SOUTHEAST')
        desc = '$desc Sudeste';
      else if (absoluteDirection == 'SOUTHWEAST') desc = '$desc Sudoeste';
      int meters = step.distance.toInt();
      stepByStep += 'Caminhe por $meters metros $desc\n';
    }
    return stepByStep;
  }

  Widget cardVehicleEnterWidget(
          String name, String arrival, String asset, Color color) =>
      Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              child: Text(
                arrival,
                style: TextStyle(color: color),
              ),
            ),
            Container(
              padding: EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 25,
                    width: 25,
                    child: SvgPicture.asset(asset),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    width: MediaQuery.of(context).size.width - 140,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Embarque',
                            style: TextStyle(
                                color: color, fontWeight: FontWeight.bold)),
                        Text(name, style: TextStyle(color: color)),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );

  Widget cardVehicleExitWidget(
          String name, String arrival, String asset, Color color) =>
      Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(5),
              child: Text(
                arrival,
                style: TextStyle(color: color),
              ),
            ),
            Container(
              padding: EdgeInsets.all(5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 25,
                    width: 25,
                    child: SvgPicture.asset(asset),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 5),
                    width: MediaQuery.of(context).size.width - 140,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Desembarque',
                            style: TextStyle(
                                color: color, fontWeight: FontWeight.bold)),
                        Text(name, style: TextStyle(color: color)),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );

  Widget cardWalkWidget(String steps, String departureTime, Color color) =>
      Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          side: BorderSide(width: 2, color: Colors.blue),
        ),
        child: Container(
          padding: EdgeInsets.all(5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(5),
                child: Text(departureTime, style: TextStyle(color: color)),
              ),
              Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Icon(Icons.directions_walk, color: color),
                    Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width - 120,
                      child: SingleChildScrollView(
                        child: Text(steps, style: TextStyle(color: color)),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );

  Widget cardVehicleWidget(
    String departureTime,
    String headsign,
    String routeShortName,
    String routeLongName,
    Color color,
    String routeColor,
    String routeTextColor,
    String vehicleId,
    IconData vehicleIcon,
    bool accessibility,
  ) =>
      Card(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          side: BorderSide(width: 2, color: Colors.red),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  if (!accessibility)
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: SvgPicture.asset(
                            'assets/emoji_people.svg',
                            color: color,
                          ),
                        ),
                        if (vehicleId != null && vehicleId.isNotEmpty)
                          Container(
                            child: Image(
                              image: AssetImage("assets/rt.gif"),
                              height: 20,
                              width: 20,
                            ),
                          ),
                        Icon(vehicleIcon, color: color),
                      ],
                    ),
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width - 110,
                          child: Container(
                            child: Card(
                              color: Utils.getColorFromHex(routeColor),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0))),
                              child: Container(
                                padding: EdgeInsets.all(5),
                                child: Text('$routeShortName $routeLongName',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Utils.getColorFromHex(
                                            routeTextColor))),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width - 110,
                          child: Text(
                            'Destino: $headsign',
                            style: TextStyle(color: color),
                          ),
                        ),
                        if (vehicleId != null && vehicleId.isNotEmpty)
                          Container(
                            padding: EdgeInsets.only(top: 20),
                            width: MediaQuery.of(context).size.width - 110,
                            child: Text(
                              'Veículo chegando: $vehicleId',
                              style: TextStyle(color: color),
                            ),
                          )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
}
