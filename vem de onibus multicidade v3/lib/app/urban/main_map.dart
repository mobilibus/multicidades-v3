import 'package:flutter/material.dart';

import 'map/default_map.dart';

class MyMap extends StatefulWidget {
  @override
  _MyMap createState() => _MyMap();
}

class _MyMap extends State<MyMap> with AutomaticKeepAliveClientMixin {
  
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return DefaultMap();
  }
}
