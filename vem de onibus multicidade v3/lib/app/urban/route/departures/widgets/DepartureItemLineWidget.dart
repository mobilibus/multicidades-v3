import 'package:flutter/material.dart';
import 'package:mobilibus/base/MOBDeparture.dart';
import 'package:mobilibus/utils/Utils.dart';

class DepartureItemLineWidget extends StatelessWidget {
  DepartureItemLineWidget(this.lineDepartures, {Key key, this.nextDay = false});
  final List<MOBDeparture> lineDepartures;
  final bool nextDay;
  @override
  Widget build(BuildContext context) => ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: lineDepartures.length,
        itemBuilder: (context, index) {
          MOBDeparture lineDeparture = lineDepartures[index];
          int minutes = lineDeparture.minutes;
          String departureRepresentatation = '';
          if (nextDay)
            departureRepresentatation =
                lineDeparture.time.substring(0, 5).replaceAll(':', 'h');
          else if (minutes > 1 && minutes < 60)
            departureRepresentatation = '$minutes\min';
          else if (minutes >= 60)
            departureRepresentatation =
                lineDeparture.time.substring(0, 5).replaceAll(':', 'h');
          else
            departureRepresentatation = 'Agora';
          return FlatButton(
            onPressed: () {},
            child: Container(
              padding: EdgeInsets.all(20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      if (lineDeparture.online)
                        Text(
                          departureRepresentatation,
                          style: TextStyle(
                              fontSize: 20,
                              color: Utils.isLightTheme(context)
                                  ? Colors.black
                                  : Utils.getColorFromPrimary(context),
                              fontWeight: FontWeight.bold),
                        )
                      else
                        Text(departureRepresentatation,
                            style: TextStyle(fontSize: 20)),
                      if (lineDeparture.wheelchairAccessible)
                        Icon(
                          Icons.accessible_forward,
                          color: Utils.isLightTheme(context)
                              ? Colors.black
                              : Utils.getColorFromPrimary(context),
                        )
                      else
                        Text(' '),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      if (lineDeparture.online)
                        Image(
                          image: AssetImage("assets/rt.gif"),
                          height: 20,
                          width: 20,
                        ),
                      if (lineDeparture.online)
                        Icon(
                          Icons.directions_bus,
                          color: Utils.isLightTheme(context)
                              ? Colors.black
                              : Utils.getColorFromPrimary(context),
                        )
                      else
                        Text(' '),
                      if (lineDeparture.online)
                        Text(
                          lineDeparture.vehicleId,
                          style: TextStyle(
                              fontSize: 20,
                              color: Utils.isLightTheme(context)
                                  ? Colors.black
                                  : Utils.getColorFromPrimary(context),
                              fontWeight: FontWeight.bold),
                        )
                      else
                        Text(' '),
                    ],
                  ),
                  if (nextDay)
                    Text('Amanhã',
                        style: TextStyle(
                          fontSize: 20,
                          color: Utils.isLightTheme(context)
                              ? Colors.black
                              : Utils.getColorFromPrimary(context),
                        )),
                ],
              ),
            ),
          );
        },
      );
}
