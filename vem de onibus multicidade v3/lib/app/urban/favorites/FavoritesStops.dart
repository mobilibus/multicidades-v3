import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:mobilibus/app/urban/departure/departures.dart';
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavoritesStops extends StatelessWidget {
  FavoritesStops(this.stopsFavorited, this.scaffoldKey, this.context);
  final List<MOBStop> stopsFavorited;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final BuildContext context;

  Future<void> openDepartures(MOBStop stop) async {
    DeparturesView dpView = DeparturesView(stop);

    await showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      enableDrag: false,
      builder: (context) => Container(
        height: (MediaQuery.of(context).size.height / 8) * 7,
        child: dpView,
      ),
    );
  }

  void delete(MOBStop stop) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int projectId = Utils.project.projectId;
    String favStopsParam = 'stops_favorite_$projectId';
    int stopId = stop.stopId;

    prefs.setBool('stop_favorite_$stopId', false);

    stopsFavorited.remove(stop);
    String stopsString =
    convert.json.encode(List.of(stopsFavorited.map((e) => e.toMap())));
    prefs.setString(favStopsParam, stopsString);


    scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text('Ponto removido dos favoritos')));
    (context as Element)?.markNeedsBuild();
  }

  void edit(MOBStop stop) async {
    TextEditingController teController = TextEditingController();
    await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Utils.getColorFromPrimary(context),
            title: Text(
              'Alterar nome do ponto',
              style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
            ),
            content: TextField(
              controller: teController,
              style: TextStyle(color: Utils.getColorByLuminanceTheme(context)),
              decoration: InputDecoration(hintText: stop.name),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  int projectId = Utils.project.projectId;
                  String newDescription = teController.value.text;
                  SharedPreferences.getInstance().then((prefs) {
                    String favStopsParam = 'stops_favorite_$projectId';

                    var value = prefs.getString(favStopsParam);
                    if (value == null) value = '';

                    String stopString = value;

                    stopString =
                        stopString.replaceAll(stop.name, newDescription);
                    prefs.setString(favStopsParam, stopString);

                    stopsFavorited[stopsFavorited.indexOf(stop)].name =
                        newDescription;
                    Navigator.of(context).pop();
                  });

                  String content = 'Descrição do ponto alterada';
                  scaffoldKey.currentState.showSnackBar(SnackBar(
                    content: Text(content),
                    duration: Duration(seconds: 1),
                  ));
                  Navigator.of(context).pop();
                },
                child: Text('Salvar',
                    style: TextStyle(
                        color: Utils.getColorByLuminanceTheme(context))),
              ),
              FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text('Cancelar',
                    style: TextStyle(
                        color: Utils.getColorByLuminanceTheme(context))),
              ),
            ],
          );
        });

    (context as Element)?.markNeedsBuild();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: stopsFavorited.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        MOBStop stop = stopsFavorited[index];
        return Container(
          padding: EdgeInsets.all(10),
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            child: InkWell(
              onTap: () => openDepartures(stop),
              child: Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(10),
                          width: MediaQuery.of(context).size.width - 180,
                          child: Text(stop.name),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            IconButton(
                              onPressed: () => edit(stop),
                              icon: Icon(Icons.edit),
                            ),
                            IconButton(
                              onPressed: () => delete(stop),
                              icon: Icon(Icons.delete),
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
