
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mobilibus/app/urban/map/default_map.dart';
import 'package:mobilibus/base/MOBMarker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavoritesCustomMarkers extends StatefulWidget{
  FavoritesCustomMarkers(this.customMarkers, this.scaffoldKey, this.context);

  final List<MOBMarker> customMarkers;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final BuildContext context;

  _FavoritesCustomMarkers createState() => _FavoritesCustomMarkers(customMarkers, scaffoldKey, context);
}

class _FavoritesCustomMarkers extends State<FavoritesCustomMarkers> {
  _FavoritesCustomMarkers(this.customMarkers, this.scaffoldKey, this.context);
  final List<MOBMarker> customMarkers;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final BuildContext context;
  TextEditingController nameCustomStop = TextEditingController();
  bool editCustomNameStop = false;
  int indexEdit;
  FocusNode myFocusNode;

  @override
  void initState() {
    super.initState();

    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();

    super.dispose();
  }

  void edit(MOBMarker customMarker, int index, String textEdit) async {

    indexEdit = index;

    editCustomNameStop = !editCustomNameStop;

    if(editCustomNameStop){ //SE FOR EDIÇÃO DE TEXTO

      nameCustomStop.text = customMarker.name;

      (context as Element)?.markNeedsBuild();

      setState(() {
        myFocusNode.requestFocus();
      });
    }
    else{ //SE FOR PARA SALVAR A EDIÇÃO

      customMarkers[index].name = textEdit;

      List list = customMarkers.map((e) => e.asMap()).toList();
      String json = convert.json.encode(list);

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString('custom_stops', json);

      (context as Element)?.markNeedsBuild();
    }

  }

  void delete(MOBMarker customMarker) async {
    markersSet.clear();

    customMarkers.remove(customMarker);
    List list = customMarkers.map((e) => e.asMap()).toList();
    String json = convert.json.encode(list);

    for(int index = 0; index < list.length; index++){

      String pathCustomIcon = null;

      switch(customMarkers[index].markerType.index)
      {
        case 0: pathCustomIcon = 'assets/images/v3_pinHome.png'; break;
        case 1: pathCustomIcon = 'assets/images/v3_pinWork.png'; break;
        case 2: pathCustomIcon = 'assets/images/v3_pinSchool.png'; break;
        case 3: pathCustomIcon = 'assets/images/v3_pinTown.png'; break;
      }

      myMarker = Marker(
          markerId: MarkerId(customMarkers[index].name),
          position: LatLng(customMarkers[index].latitude, customMarkers[index].longitude),
          icon: await BitmapDescriptor.fromAssetImage(ImageConfiguration(), pathCustomIcon));

      markersSet.add(myMarker);
    }

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString('custom_stops', json);

    scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text('Ponto removido dos favoritos')));

    (context as Element)?.markNeedsBuild();
  }

  void trip(MOBMarker customMarker) async {}

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: customMarkers.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        MOBMarker customMarker = customMarkers[index];
        String name = customMarker.name;
        MARKER_TYPE markerType = customMarker.markerType;
        IconData iconData;
        switch (markerType) {
          case MARKER_TYPE.HOME:
            iconData = Icons.home;
            break;
          case MARKER_TYPE.WORK:
            iconData = Icons.work;
            break;
          case MARKER_TYPE.SCHOOL:
            iconData = Icons.school;
            break;
          case MARKER_TYPE.GENERIC:
            iconData = Icons.map;
            break;
        }
        return Stack(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              child: Card(
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                child: InkWell(
                  onTap: () => trip(customMarker),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      children: [
                        editCustomNameStop && index == indexEdit ?
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          width: MediaQuery.of(context).size.width / 1.6 - 5,
                          child: TextFormField(
                            focusNode: myFocusNode,
                            autofocus: true,
                            minLines: 1,
                            maxLines: 3,
                            autovalidateMode: AutovalidateMode.always,
                            controller: nameCustomStop,
                            validator: (value) =>
                            value.isEmpty ? 'Nome não pode ser vazio' : null,
                          ),
                        ):
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          width: MediaQuery.of(context).size.width / 1.6 - 5,
                          child: Text(name),
                        ),

                        Row(
                          children: [
                            editCustomNameStop && index == indexEdit?
                            IconButton(
                              icon: Icon(Icons.save),
                              onPressed: () => edit(customMarker, index, nameCustomStop.text),
                            ) :
                            IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () => edit(customMarker, index, ''),
                            ),
                            IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () => delete(customMarker),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              child: Card(
                color: Theme.of(context).accentColor,
                elevation: 5,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(15.0))),
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Icon(iconData),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}