import 'package:flutter/material.dart';
import 'package:mobilibus/utils/Utils.dart';

class SubtitleInfoWidget extends StatelessWidget {
  SubtitleInfoWidget(this.fontSize) : super();
  final double fontSize;
  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        child: Wrap(
          spacing: 10.0,
          runSpacing: 10.0,
          children: <Widget>[
            Text(
                'Ao clicar no horário, você confere a descrição do itinerário realizado naquela viagem.\n'
                'Você também pode conferir essa informação na opção "Legenda", pelo número acima do horário.',
                style:
                    TextStyle(color: Utils.getColorByLuminanceTheme(context))),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Card(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Icon(
                                Icons.accessible_forward,
                                color: Colors.black,
                                size: fontSize - 3,
                              ),
                              Text('12',
                                  style: TextStyle(fontSize: fontSize - 3),
                                  textAlign: TextAlign.end),
                            ],
                          ),
                        ),
                        Text('12:34', style: TextStyle(fontSize: fontSize)),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 230,
                  child: Text('Horário de saída previstos para o dia seguinte',
                      style: TextStyle(
                          color: Utils.getColorByLuminanceTheme(context))),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Card(
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Icon(
                                Icons.accessible_forward,
                                color: Colors.black,
                                size: fontSize - 3,
                              ),
                              Text('12',
                                  style: TextStyle(fontSize: fontSize - 3),
                                  textAlign: TextAlign.end),
                            ],
                          ),
                        ),
                        Text('12:34',
                            style: TextStyle(
                              fontSize: fontSize,
                              fontWeight: FontWeight.bold,
                            )),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 230,
                  child: Text('Horário de saída previstos para o dia de hoje',
                      style: TextStyle(
                          color: Utils.getColorByLuminanceTheme(context))),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Card(
                  color: Colors.yellow,
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Icon(
                                Icons.accessible_forward,
                                color: Colors.black,
                                size: fontSize - 3,
                              ),
                              Text('12',
                                  style: TextStyle(fontSize: fontSize - 3),
                                  textAlign: TextAlign.end),
                            ],
                          ),
                        ),
                        Text('12:34',
                            style: TextStyle(
                              fontSize: fontSize,
                              fontWeight: FontWeight.bold,
                            )),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 230,
                  child: Text('Próximo horário de saída previsto',
                      style: TextStyle(
                          color: Utils.getColorByLuminanceTheme(context))),
                ),
              ],
            ),
          ],
        ),
      );
}
