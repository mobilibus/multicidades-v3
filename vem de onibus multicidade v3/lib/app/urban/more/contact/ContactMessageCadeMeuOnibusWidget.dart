import 'package:flutter/material.dart';

class ContactMessageCadeMeuOnibusWidget extends StatelessWidget {
  static final List<String> subjectType = [
    'Ônibus',
    'Cadê Meu Ônibus',
    'Cartão Passa Fácil',
    'Integração Temporal',
    'Postos Sinetram',
    'Recarga Online',
    'Pontos de Recarga',
    'Outros',
  ];
  static final List<String> sexType = [
    'Masculino',
    'Feminino',
  ];
  static final List<String> messageType = [
    'Sugestão',
    'Informação',
    'Elogio',
    'Reclamação',
  ];

  final Map<String, List<String>> data = {
    'Tipo de Relato': messageType,
    'Sexo': sexType,
    'Assunto': subjectType,
  };
  final Map<String, String> dataSelected = {
    'Tipo de Relato': null,
    'Sexo': null,
    'Assunto': null,
  };

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.only(bottom: 10, top: 10),
        child: Column(
          children: data.keys.map(
            (key) {
              List<String> dataValueKeys = data[key];
              String dataValue = dataSelected[key];
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('$key:'),
                  Container(
                    width: (MediaQuery.of(context).size.width / 2),
                    child: Card(
                      elevation: 5,
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: DropdownButton(
                          underline: Container(),
                          isExpanded: true,
                          value: dataValue ?? dataValueKeys.first,
                          onChanged: (value) {
                            dataSelected[key] = value;
                            (context as Element).markNeedsBuild();
                          },
                          items: dataValueKeys
                              .map((dataValue) => DropdownMenuItem(
                                  child: Text(dataValue), value: dataValue))
                              .toList(),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
          ).toList(),
        ),
      );
}
