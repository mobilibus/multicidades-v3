import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/transdata/data/model/transdata_extract.dart';
import 'package:mobilibus/utils/date_number_utils.dart';

class CardStatementRegisterWidget extends StatelessWidget {
  final TransdataTransaction transaction;

  const CardStatementRegisterWidget({
    Key key,
    @required this.transaction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextStyle registerStyle = GoogleFonts.nunito(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      color: Color(0xff212121),
    );

    final TextStyle amountStyle = GoogleFonts.nunito(
      fontSize: 15,
      fontWeight: FontWeight.w700,
      color: transaction.isCredit() ? Color(0xff212121) : Color(0xffb24434),
    );

    DateTime dtime = DateTime.parse(transaction.date);
    String dateTransaction = dtime.day.toString().padLeft(2,'0') +"/"+ dtime.month.toString().padLeft(2, '0') +"/"+ dtime.year.toString() +" "+ dtime.hour.toString().padLeft(2, '0') +":"+ dtime.minute.toString().padLeft(2, '0');

    String sTransactionValue = NumberUtils.formatAmountAsCurrency(transaction.value);

    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Text(
            '$dateTransaction', //DateUtils.getShortDateAndTime(transaction.date),
            style: registerStyle,
          ),
        ),
        Expanded(
          flex: 2,
          child: Text(
            transaction.description,
            style: registerStyle,
          ),
        ),
        Expanded(
          flex: 1,
          child: Text(
            '$sTransactionValue', //transaction.formattedValue,
            style: amountStyle,
            textAlign: TextAlign.end,
          ),
        )
      ],
    );
  }
}
