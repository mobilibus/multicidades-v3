import 'package:flutter/material.dart';

class WidgetToImageExportation extends StatefulWidget {
  final Function(GlobalKey) builder;

  const WidgetToImageExportation({
    Key key,
    @required this.builder,
  }) : super(key: key);

  @override
  _WidgetToImageExportationState createState() => _WidgetToImageExportationState();
}

class _WidgetToImageExportationState extends State<WidgetToImageExportation> {
  final globalKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      key: globalKey,
      child: widget.builder(globalKey),
    );
  }
}
