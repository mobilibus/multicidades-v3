import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_raised_button.dart';
import 'package:mobilibus/app/urban/more/recharge/product_selection_page.dart';
import 'package:mobilibus/utils/date_number_utils.dart';

class BalanceBoardWidget extends StatelessWidget {
  final int balanceAmount;
  final String updatedAt;

  const BalanceBoardWidget({
    Key key,
    @required this.balanceAmount,
    @required this.updatedAt,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    DateTime dtime = DateTime.parse(updatedAt);

    String  lastUpdate = dtime.day.toString();
    lastUpdate = lastUpdate + " de ";

    String dMonth;

    switch(dtime.month)
    {
      case  1: dMonth = "jan"; break;
      case  2: dMonth = "fev"; break;
      case  3: dMonth = "mar"; break;
      case  4: dMonth = "abr"; break;
      case  5: dMonth = "maio"; break;
      case  6: dMonth = "jun"; break;
      case  7: dMonth = "jul"; break;
      case  8: dMonth = "ago"; break;
      case  9: dMonth = "set"; break;
      case 10: dMonth = "out"; break;
      case 11: dMonth = "nov"; break;
      case 12: dMonth = "dez";
    }

    lastUpdate = lastUpdate + dMonth;
    lastUpdate = lastUpdate + " de " + dtime.year.toString();
    lastUpdate = lastUpdate + " às " + dtime.hour.toString().padLeft(2,'0') + ":" + dtime.minute.toString().padLeft(2,'0');

    return Container(
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Crédito comum',
            style: GoogleFonts.nunito(
              fontSize: 18,
              fontWeight: FontWeight.w700,
              color: Color(0xff212121),
            ),
          ),
          SizedBox(height: 20),
          balanceAmount == null
              ? Text(
            'Saldo indisponível',
            textAlign: TextAlign.center,
            style: GoogleFonts.nunito(
              fontSize: 20,
              fontWeight: FontWeight.w400,
              color: Color(0xff212121).withOpacity(0.8),
            ),
          )
              : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Saldo disponível é de',
                style: GoogleFonts.nunito(
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff212121).withOpacity(0.8),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Text(
                  NumberUtils.formatAmountAsCurrency(this.balanceAmount),
                  style: GoogleFonts.nunito(
                    fontSize: 32,
                    fontWeight: FontWeight.w800,
                    color: Color(0xff212121),
                  ),
                ),
              ),
              Text(
                "Última atualização em $lastUpdate",
                style: GoogleFonts.nunito(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff212121).withOpacity(0.6),
                ),
              ),
              SizedBox(height: 16),
              CustomRaisedButton(
                label: '\$ Recarregar',
                onPressed: () => _openProductSelection(context),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _openProductSelection(BuildContext ctx) {
    Navigator.push(ctx, MaterialPageRoute(builder: (_) => ProductSelectionPage()));
  }
}
