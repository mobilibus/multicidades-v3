import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomTextFormField extends StatefulWidget {
  final String initialValue;
  final String hint;
  final Widget suffixIcon;
  final TextEditingController editingController;
  final FormFieldValidator<String> validator;
  final TextInputType inputType;
  final bool obscureText;
  final List<TextInputFormatter> inputFormatters;
  final FocusNode focusNode;
  final ValueChanged<String> onFieldSubmitted;
  final double valueFontSize;
  final Function(String) onChanged;

  const CustomTextFormField({
    Key key,
    this.initialValue,
    this.hint,
    this.suffixIcon,
    this.editingController,
    this.validator,
    this.inputType,
    this.inputFormatters,
    this.focusNode,
    this.onFieldSubmitted,
    this.obscureText = false,
    this.valueFontSize,
    this.onChanged,
  }) : super(key: key);

  @override
  _CustomTextFormFieldState createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {

  @override
  Widget build(BuildContext context) {
    return TextFormField(

      validator: widget.validator,
      onChanged: widget.onChanged,
      style: TextStyle(color: Color(0xff212121), fontSize: widget.valueFontSize, fontWeight: FontWeight.w400),//valueTextStyle,
      initialValue: widget.initialValue,
      keyboardType: widget.inputType,
      obscureText: widget.obscureText,
      focusNode: widget.focusNode,
      onFieldSubmitted: widget.onFieldSubmitted,
      decoration: InputDecoration(
        hintText: widget.hint,
        labelText: widget.hint,
        border: OutlineInputBorder(),
        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[700]/*Color(0xffe0e0e0)*/)),
        //suffixIcon: Padding(
        //  padding: const EdgeInsets.symmetric(horizontal: 0),
        //  child: widget.suffixIcon,
        //),
      ),
      controller: widget.editingController,
      inputFormatters: widget.inputFormatters,

    );
  }
}
