import 'package:flutter/material.dart';

class CustomInfoDialog extends StatelessWidget {
  final String title;
  final String message;
  final String dismissButtonLabel;
  final VoidCallback onAfterDismiss;

  const CustomInfoDialog({
    Key key,
    @required this.title,
    @required this.message,
    this.dismissButtonLabel = 'OK',
    this.onAfterDismiss,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        FlatButton(
            child: Text(dismissButtonLabel),
            onPressed: () {
              Navigator.pop(context);
              if (onAfterDismiss != null) {
                onAfterDismiss();
              }
            }),
      ],
    );
  }
}
