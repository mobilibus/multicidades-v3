import 'package:mobilibus/app/urban/more/recharge/data/data_response.dart';
import 'package:mobilibus/app/urban/more/recharge/data/user_transport_card_model.dart';
import 'package:mobilibus/transdata/data/model/transdata_balance.dart';
import 'package:mobilibus/transdata/data/model/transdata_extract.dart';

class BalanceExtractModel {
  final UserTransportCardModel transportCard;
  final DataResponse<List<TransdataBalanceValue>> balanceValue;
  final DataResponse<List<TransdataTransaction>> cardExtract;

  BalanceExtractModel({
    this.transportCard,
    this.balanceValue,
    this.cardExtract,
  });
}
