import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_circular_progress_indicator.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_marginable_divider.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_raised_button.dart';
import 'package:mobilibus/app/urban/more/recharge/components/custom_text_form_field.dart';
import 'package:mobilibus/app/urban/more/recharge/controller/new_payment_card_controller.dart';
import 'package:mobilibus/app/urban/more/recharge/data/transdata_payment_recharge_result.dart';
import 'package:mobilibus/app/urban/more/recharge/payment_recharge_error_page.dart';
import 'package:mobilibus/app/urban/more/recharge/payment_recharge_success_page.dart';
import 'package:mobilibus/main.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:mobilibus/utils/formatting_utils.dart';

String cardOwnerGb = '';
String cardNumberGb = '';
String cardExpirationGb = '';
String cvvGb = '';

class NewPaymentCardPage extends StatefulWidget {
  @override
  _NewPaymentCardPageState createState() => _NewPaymentCardPageState();
}

class _NewPaymentCardPageState extends State<NewPaymentCardPage> {
  final _formKey = GlobalKey<FormState>();
  NewPaymentCardController _controller;
  final cardOwnerFN = FocusNode();
  final cardNumberFN = FocusNode();
  final cardExpirationFN = FocusNode();
  final cvvFN = FocusNode();

  @override
  void initState() {
    super.initState();
    _controller = NewPaymentCardController(getPurchaseRepositoryInstance());

    _controller.cardOwnerController.text = cardOwnerGb;
    _controller.cardNumberController.text = cardNumberGb;
    _controller.cardExpirationController.text = cardExpirationGb;
    _controller.cvvController.text = cvvGb;
  }

  final labelTextStyle = GoogleFonts.nunito(
    color: Color(0xff616161),
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );

  Future<bool> _onBackPressed() {

    cardOwnerGb = _controller.cardOwnerController.text;
    cardNumberGb = _controller.cardNumberController.text;
    cardExpirationGb = _controller.cardExpirationController.text;
    cvvGb = _controller.cvvController.text;

    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Utils.getColorFromPrimary(context),
            centerTitle: true,
            elevation: 0,
            title: Text(
              'Pagamento',
              style: GoogleFonts.nunito(fontSize: 20, fontWeight: FontWeight.w700),
            ),
          ),
          body: SafeArea(
            child: Form(
              key: _formKey,
              child: ListView(
                physics: BouncingScrollPhysics(),
                children: [
                  /*
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                width: double.infinity,
                child: Row(
                  children: [
                    IconButton(
                      padding: const EdgeInsets.all(0),
                      iconSize: 28,
                      icon: Icon(Icons.west),
                      onPressed: () => Navigator.pop(context),
                    ),
                    Spacer()
                  ],
                ),
              ),
              */
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        /*
                    Text(
                      'Adicionar cartão',
                      style: GoogleFonts.nunito(
                        fontSize: 32,
                        fontWeight: FontWeight.w700,
                        color: Colors.grey[800],
                      ),
                    ),
                    */
                        SizedBox(height: 44),
                        Text(
                          'Dados do titular',
                          style: GoogleFonts.nunito(
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[800],
                          ),
                        ),
                        SizedBox(height: 16),
                        Text('Nome Completo', style: labelTextStyle),
                        CustomTextFormField(
                          editingController: _controller.cardOwnerController,
                          validator: _controller.validateCardOwner,
                          focusNode: cardOwnerFN,
                          onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(cardNumberFN),
                        ),
                        SizedBox(height: 16),
                        CustomMarginableDivider(verticalMargin: 24),
                        Text(
                          'Cartão de Crédito',
                          style: GoogleFonts.nunito(
                            fontSize: 24,
                            fontWeight: FontWeight.w700,
                            color: Colors.grey[800],
                          ),
                        ),
                        SizedBox(height: 16),
                        Text('Número', style: labelTextStyle),
                        CustomTextFormField(
                          // TODO Implementar validação da bandeira do cartão para poder incluir a logo
                          editingController: _controller.cardNumberController,
                          inputFormatters: [
                            MaskTextInputFormatter(mask: FormattingUtils.paymentCardMask)
                          ],
                          inputType: TextInputType.number,
                          validator: _controller.validateCardNumber,
                          focusNode: cardNumberFN,
                          onFieldSubmitted: (_) =>
                              FocusScope.of(context).requestFocus(cardExpirationFN),
                        ),
                        SizedBox(height: 16),
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Validade', style: labelTextStyle),
                                  CustomTextFormField(
                                    editingController: _controller.cardExpirationController,
                                    inputFormatters: [
                                      MaskTextInputFormatter(mask: FormattingUtils.cardExpirationMask)
                                    ],
                                    inputType: TextInputType.number,
                                    validator: _controller.validateCardExpiration,
                                    focusNode: cardExpirationFN,
                                    onFieldSubmitted: (_) => FocusScope.of(context).requestFocus(cvvFN),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(width: 16),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('CVV', style: labelTextStyle),
                                  CustomTextFormField(
                                    editingController: _controller.cvvController,
                                    inputFormatters: [
                                      MaskTextInputFormatter(mask: FormattingUtils.cvvMask)
                                    ],
                                    inputType: TextInputType.number,
                                    obscureText: true,
                                    validator: _controller.validateCVV,
                                    focusNode: cvvFN,
                                    onFieldSubmitted: (_) => _openRechargeResult(),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 40),
                        AnimatedBuilder(
                          animation: _controller,
                          builder: (_, __) => _controller.isLoading
                              ? CustomCircularProgressIndicator()
                              : CustomRaisedButton(
                            label: 'Finalizar',
                            onPressed: _openRechargeResult,
                          ),
                        ),
                        SizedBox(height: 40),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  Future _openRechargeResult() async {
    if (_formKey.currentState.validate()) {
      _controller.setLoadingStatus(true);
      final result = await _controller.processRechargePayment();
      _controller.setLoadingStatus(false);
      // TODO Implementar chamada ao backend para salvar o cartão.

      switch (result.status) {
        case RechargeResultStatus.ALL_SUCCESS:
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => PaymentRechargeSuccessPage(paymentRechargeResult: result),
            ),
          );
          break;
        default:
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => PaymentRechargeErrorPage(result: result)));
      }
    }
  }
}