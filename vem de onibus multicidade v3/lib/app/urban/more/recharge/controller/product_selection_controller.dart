import 'package:flutter/foundation.dart';
import 'package:mobilibus/app/urban/more/recharge/data/data_response.dart';
import 'package:mobilibus/app/urban/more/recharge/data/recharge_data_singleton.dart';
import 'package:mobilibus/app/urban/more/recharge/data/user_transport_card_model.dart';
import 'package:mobilibus/transdata/data/model/transdata_products.dart';
import 'package:mobilibus/transdata/data/purchase_repository.dart';

import '../product_selection_page.dart';

class ProductSelectionController extends ChangeNotifier {
  final singleton = RechargeDataSingleton.getInstance();
  final PurchaseRepository _repository;
  UserTransportCardModel transportCardData;
  ValueNotifier<DataResponse<List<TransdataProduct>>> products = ValueNotifier(null);
  bool _loading = false;
  bool get isLoading => _loading;

  ProductSelectionController(this._repository);

  void setLoadingStatus(bool newStatus) {
    _loading = newStatus;
    notifyListeners();
  }

  Future getAvailableProducts(String cpf, int cardNumber) async {
    bool resultHasError = false;
    setLoadingStatus(true);

    int count = 0;
    countGB = 0;

    do{

      var result = await _repository.getAvailableProducts(cpf, cardNumber);
      products.value = result;

      resultHasError = result.hasError;
      count++;

      countGB = count ~/ 6;
      setLoadingStatus(true);
    }while(resultHasError && count < 22);

    setLoadingStatus(false);
  }

  void setChosenProduct(TransdataProduct product) {
    singleton.product = product;
  }
}