import 'package:flutter/foundation.dart';
import 'package:mobilibus/app/urban/more/recharge/data/user_transport_card_model.dart';
import 'package:mobilibus/transdata/data/model/transdata_extract.dart';
import 'package:mobilibus/transdata/data/purchase_repository.dart';
import 'package:mobilibus/utils/date_number_utils.dart';

class CardDetailedStatementController extends ChangeNotifier {
  final PurchaseRepository _repository;
  UserTransportCardModel transportCardData;
  ValueNotifier<String> statementDate = ValueNotifier('');
  ValueNotifier<List<TransdataTransaction>> transactions = ValueNotifier(null);
  bool _loading = false;
  bool get isLoading => _loading;

  CardDetailedStatementController(this._repository);

  void initTransportCard(UserTransportCardModel data) {
    this.transportCardData = data;
  }

  void initStatementDate(String date) {
    statementDate.value = date;
    statementDate.addListener(_updateStatement);
  }

  void initTransactions(List<TransdataTransaction> list) {
    transactions.value = list;
  }

  void setLoadingStatus(bool newStatus) {
    _loading = newStatus;
    notifyListeners();
  }

  void moveToNextDate() {
    if (isLoading) return;
    statementDate.value = DateMOBUtils.getNextDate(statementDate.value);
  }

  void moveToPreviousDate() {
    if (isLoading) return;
    statementDate.value = DateMOBUtils.getPreviousDate(statementDate.value);
  }

  void moveToPickedDate(DateTime newDate) {
    var result = newDate.toString();
    statementDate.value = result;
  }

  Future _updateStatement() async {
    setLoadingStatus(true);
    TransdataExtract extract = await _repository.getCardExtractFromDate(
        transportCardData.cpf, transportCardData.cardNumber, statementDate.value);

    transactions.value = extract.items;
    setLoadingStatus(false);
  }
}
