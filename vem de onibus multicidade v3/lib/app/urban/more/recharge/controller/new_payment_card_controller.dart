import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/app/urban/more/recharge/data/recharge_data_singleton.dart';
import 'package:mobilibus/app/urban/more/recharge/data/transdata_payment_recharge_result.dart';
import 'package:mobilibus/transdata/data/purchase_repository.dart';
import 'package:mobilibus/utils/date_number_utils.dart';

class NewPaymentCardController extends ChangeNotifier {
  final PurchaseRepository _repository;
  final singleton = RechargeDataSingleton.getInstance();
  bool _loading = false;
  bool get isLoading => _loading;
  final cardOwnerController = TextEditingController();
  final cardNumberController = TextEditingController();
  final cardExpirationController = TextEditingController();
  final cvvController = TextEditingController();

  NewPaymentCardController(this._repository);

  void setLoadingStatus(bool newStatus) {
    _loading = newStatus;
    notifyListeners();
  }

  String validateCardOwner(String value) {
    if (value.isEmpty) {
      return "O nome é obrigatório";
    }

    return null;
  }

  String validateCardNumber(String value) {
    if (value.isEmpty) {
      return "O número do cartão é obrigatório";
    }

    // TODO Melhorar a validação do número do cartão

    return null;
  }

  String validateCardExpiration(String value) {
    if (value.isEmpty) {
      return "A validade do cartão é obrigatória";
    }

    if (!DateMOBUtils.isValidCardExpiration(value)) {
      return "A data de validade do cartão está inválida";
    }

    return null;
  }

  String validateCVV(String value) {
    if (value.isEmpty) {
      return "O CVV é obrigatório";
    }

    return null;
  }

  Future<TransdataPaymentRechargeResult> processRechargePayment() async {
    _setPaymentDataOnSingleton();
    try {
      final newPurchaseData = await _repository.startNewPurchase(singleton);

      var paymentResponse =
      await _repository.sendTransactionPayment(newPurchaseData.transactionId, singleton);

      bool isApproved = paymentResponse.payment.status.toLowerCase() == 'success';
      bool isCharged = paymentResponse.chargeOnCard.status.toLowerCase() == 'success';

      if (isApproved && !isCharged) {
        paymentResponse = await _repository.checkPurchaseManually(newPurchaseData.transactionId);
        isApproved = paymentResponse.payment.status.toLowerCase() == 'success';
        isCharged = paymentResponse.chargeOnCard.status.toLowerCase() == 'success';
      }

      RechargeResultStatus status;
      if (isApproved && isCharged) {
        status = RechargeResultStatus.ALL_SUCCESS;
      } else if (isApproved && !isCharged) {
        status = RechargeResultStatus.RECHARGE_ERROR;
      } else {
        status = RechargeResultStatus.PAYMENT_ERROR;
      }

      DateTime now = DateTime.now();
      String transactionDT = DateFormat('dd/MM/yyyy kk:mm').format(now);

      return TransdataPaymentRechargeResult(
        status: status,
        paymentHash: paymentResponse.payment.info,
        transactionDateTime: transactionDT,//DateTime.now(),
        rechargeAmount: singleton.rechargeAmount,
        feeAmount: paymentResponse.payment.fee,
        maskedPaymentCardNumber: '${singleton.paymentCardNumberLast4Digits}',
        transactionId: newPurchaseData.transactionId,
      );
    } catch (e) {
      return TransdataPaymentRechargeResult(status: RechargeResultStatus.UNKNOWN_ERROR);
    }
  }

  void _setPaymentDataOnSingleton() {
    singleton.paymentCardOwner = cardOwnerController.text;
    singleton.paymentCardNumber = cardNumberController.text.replaceAll(' ', '');
    singleton.paymentCardExpiration = cardExpirationController.text;
    singleton.paymentCardCVV = cvvController.text;
  }
}
