import 'package:flutter/widgets.dart';
import 'package:mobilibus/app/urban/more/recharge/data/recharge_data_singleton.dart';
import 'package:mobilibus/transdata/data/purchase_repository.dart';
import 'package:mobilibus/utils/date_number_utils.dart';

class PaymentAmountController extends ChangeNotifier {
  RechargeDataSingleton singleton = RechargeDataSingleton.getInstance();
  final PurchaseRepository _repository;
  bool _loading = false;
  bool get isLoading => _loading;
  final amountController = TextEditingController();

  int rechargeAmount;
  String get formattedRechargeAmount => NumberUtils.formatAmountAsCurrency(rechargeAmount);

  PaymentAmountController(this._repository) {
    setFixedAmount(singleton.product.valorMin);
    _getProductTax();
  }

  void setLoadingStatus(bool newStatus) {
    _loading = newStatus;
    notifyListeners();
  }

  Future _getProductTax() async {
    setLoadingStatus(true);
    final response = await _repository.getProductTax(
      singleton.cpf,
      singleton.transportCardNumber,
      singleton.product.produtoId,
      singleton.product.tarifa,
    );
    singleton.productTaxAmount = response.valorTotalEmCentavos;
    setLoadingStatus(false);
  }

  void addAmount() {
    setFixedAmount(rechargeAmount + 100);
  }

  void removeAmount() {
    int newAmount = rechargeAmount - 100;
    if (newAmount < 0) {
      newAmount = 0;
    }

    setFixedAmount(newAmount);
  }

  void setFixedAmount(int newAmount) {
    rechargeAmount = newAmount;
    _setOrderRechargeAmount();
    amountController.value = TextEditingValue(
      text: formattedRechargeAmount,
      selection: TextSelection.fromPosition(TextPosition(offset: formattedRechargeAmount.length)),
    );
    notifyListeners();
  }

  void onChangedAmountManually(String newAmount) {
    String digits = newAmount.replaceAll(RegExp(r'[^0-9]'), '');
    setFixedAmount(int.parse(digits));
  }

  String validateAmount() {
    if (rechargeAmount < singleton.product.valorMin) {
      return 'Valor abaixo do mínimo permitido.';
    } else if (rechargeAmount > singleton.product.valorMax) {
      return 'Valor acima do máximo permitido.';
    }

    return null;
  }

  void _setOrderRechargeAmount() {
    singleton.rechargeAmount = this.rechargeAmount;
  }
}
