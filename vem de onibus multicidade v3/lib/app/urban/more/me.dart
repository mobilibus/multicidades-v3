import 'dart:convert' as convert;

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
/*
class Me extends StatefulWidget {
  _Me createState() => _Me();
}

class _Me extends State<Me> {
  bool loading = false;
  SharedPreferences sharedPreferences;
  int lineFavorites = 0;
  int stopsFavorites = 0;
  int trips = 0;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 5), () async {
      sharedPreferences = await SharedPreferences.getInstance();
      setState(() {});
    });
  }

  void signIn() async {
    setState(() => loading = true);
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    bool firebaseLogin = sharedPreferences.containsKey('firebase_login')
        ? sharedPreferences.getBool('firebase_login')
        : false;

    User user;
    if (firebaseLogin) user = FirebaseAuth.instance.currentUser;

    if (user == null) {
      user = await Utils.handleSignIn(context).catchError((e) {
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  content: Text(e.toString()),
                ));
      });
      Utils.firebaseUser = user;
      if (user != null) {
        sharedPreferences.setBool('firebase_login', true);
        String uid = user.uid;
        var sr = FirebaseStorage.instance.ref();
        String fileName = 'shared_preferences.json';
        sr = sr.child('shared_preferences').child(uid).child(fileName);

        try {
          await Utils.downloadFirebasePreferences(sr);
        } catch (e) {
          if (Utils.isMobilibus)
            showDialog(
                context: context,
                builder: (context) => AlertDialog(
                      content: Text(e.toString()),
                    ));
        }
        try {
          await Utils.uploadFirebasePreferences(sr, fileName);
        } catch (e) {
          if (Utils.isMobilibus)
            showDialog(
                context: context,
                builder: (context) => AlertDialog(
                      content: Text(e.toString()),
                    ));
        }
      }
    }

    setState(() => loading = false);
  }

  Widget cloudWidget() => Container(
        padding: EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Icon(Icons.phone_android),
            Icon(Icons.arrow_forward),
            Icon(Icons.cloud_upload),
            Icon(Icons.arrow_forward),
            Icon(Icons.cloud_done),
          ],
        ),
      );

  void updateNumbers() {
    int projectId = Utils.project.projectId;
    String jsonObjectTimetable =
        sharedPreferences.getString('timetable_favorites_$projectId');

    Iterable favoritesTimetableIterable;
    List<dynamic> favoritesTimetable = [];
    if (jsonObjectTimetable != null) {
      favoritesTimetableIterable = convert.json.decode(jsonObjectTimetable);
      favoritesTimetable = favoritesTimetableIterable.toList();
      lineFavorites = favoritesTimetable.length;
    }

    String favStopsParam = 'stops_favorite_$projectId';

    String jsonString = sharedPreferences.getString(favStopsParam) ?? '[]';
    stopsFavorites = (convert.json.decode(jsonString) ?? []).length;

    String favTripsParam = 'trips_favorite_$projectId';

    jsonString = sharedPreferences.getString(favTripsParam) ?? '[]';
    trips = (convert.json.decode(jsonString) ?? []).length;
  }

  @override
  Widget build(BuildContext context) {
    User user = Utils.firebaseUser;
    if (sharedPreferences != null) updateNumbers();
    return Scaffold(
      appBar: AppBar(title: Text('Perfil')),
      floatingActionButton: user != null && !loading
          ? FloatingActionButton.extended(
              heroTag: 'fabExitFAUTH',
              onPressed: () async {
                setState(() => loading = true);
                SharedPreferences sharedPreferences =
                    await SharedPreferences.getInstance();
                sharedPreferences.setBool('firebase_login', false);
                await FirebaseAuth.instance.signOut();
                Utils.firebaseUser = null;

                setState(() => loading = false);
              },
              label: Text('Sair'),
              icon: Icon(Icons.exit_to_app),
            )
          : null,
      body: loading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SafeArea(
              child: user == null
                  ? Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0))),
                              child: InkWell(
                                onTap: signIn,
                                child: Container(
                                  padding: EdgeInsets.all(20),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      cloudWidget(),
                                      Container(
                                        padding: EdgeInsets.all(10),
                                        child: Text('Efetuar Login'),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.all(5),
                          child: Card(
                            elevation: 5,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.all(5),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    'Seja bem-vindo ' + user.displayName,
                                    textAlign: TextAlign.center,
                                  ),
                                  cloudWidget(),
                                  Text(
                                    'Seus dados de favoritos e viagens\nserão salvos na nuvem :)',
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        RaisedButton.icon(
                          color: Theme.of(context).primaryColor,
                          onPressed: () async {
                            setState(() => loading = true);
                            String uid = user.uid;
                            var sr = FirebaseStorage.instance.ref();
                            String fileName = 'shared_preferences.json';
                            sr = sr
                                .child('shared_preferences')
                                .child(uid)
                                .child(fileName);
                            try {
                              await Utils.uploadFirebasePreferences(
                                  sr, fileName);
                            } catch (e) {
                              print(e);
                            }

                            setState(() => loading = false);
                          },
                          icon: Icon(Icons.sync_sharp),
                          label: Text('Sincronizar Dados'),
                        )
                      ],
                    ),
            ),
    );
  }
}
*/