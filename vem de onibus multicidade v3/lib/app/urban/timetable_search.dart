import 'dart:async';
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobilibus/base/MOBLine.dart';
import 'package:mobilibus/utils/DialogUtils.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TimetableSearch extends StatefulWidget {
  @override
  _TimetableSearch createState() => _TimetableSearch();
}

class _TimetableSearch extends State<TimetableSearch> {
  Map<int, bool> downloadedMap = {};
  Map<int, bool> favoritedMap = {};
  bool loading = true;
  bool downloadingTimetable = false;

  int listLength = 0;
  Icon iconSearch = Icon(Icons.search);
  bool isSearch = false;

  final _controller = ScrollController();

  static TextEditingController searchController = TextEditingController();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences prefs;
  @override

  void initState() {

    searchController.addListener(() {
      super.initState();
      setState(() {});
    });

    Future.delayed(
        Duration(), () async => prefs = await SharedPreferences.getInstance());
  }

  void initTimetable() {
    downloadingTimetable = true;
    loading = true;
    setState(() {});
    Future.delayed(Duration(), () async {
      await Utils.downloadTimetableList(Utils.project);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int projectId = Utils.project.projectId;
      String json = prefs.getString('timetable_$projectId') ?? '[]';
      Iterable iterable = convert.json.decode(json);
      if (Utils.lines.isEmpty)
        Utils.lines = iterable.map((e) => MOBLine.fromJson(e)).toList();

      setState(() => loading = false);

      downloadingTimetable = false;
    });
  }

  void setupFavorites() {
    int projectId = Utils.project.projectId;

    String jsonObject = prefs.getString('timetable_favorites_$projectId');

    if (jsonObject != null) {
      Iterable favoritesIterable = convert.json.decode(jsonObject);
      List<dynamic> favorites = favoritesIterable.toList();

      for (final line in Utils.lines) {
        int routeId = line.routeId;
        var value = favorites.contains(routeId);

        bool isFavorited = value ?? false;
        favoritedMap[routeId] = isFavorited;

        String timetablePath = 'timetable_last_download_$projectId\_$routeId';
        downloadedMap[routeId] =
            prefs.containsKey(timetablePath) ? true : false;

        searchController.addListener(() {
          super.initState();
          setState(() {});
        });
      }
    }
  }

  void favoriteAction(MOBLine line) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int projectId = Utils.project.projectId;
    int routeId = line.routeId;

    String jsonObject = prefs.getString('timetable_favorites_$projectId');

    Iterable favoritesIterable;
    List<dynamic> favorites = [];
    if (jsonObject != null) {
      favoritesIterable = convert.json.decode(jsonObject);
      favorites = favoritesIterable.toList();
    }

    bool value = favorites.contains(line.routeId);
    bool isFavorited = value ?? false;
    isFavorited = !isFavorited;
    if (isFavorited) {
      favorites.add(line.routeId);
      Future.delayed(Duration(seconds: 2), () {
        MOBApiUtils.mobEvent('line favorited', {
          'shortName': line.shortName,
          'longName': line.longName,
          'routeId': line.routeId,
        });
      });
    } else
      favorites.remove(line.routeId);

    String encoded = convert.json.encode(favorites);
    prefs.setString('timetable_favorites_$projectId', encoded);
    favoritedMap[routeId] = isFavorited;
    scaffoldKey.currentState.showSnackBar(SnackBar(
      content: isFavorited
          ? Text(
              'Linha adicionada nos favoritos',
              style: TextStyle(),
            )
          : Text(
              'Linha removida dos favoritos',
              style: TextStyle(),
            ),
      duration: Duration(seconds: 2),
    ));
    setState(() {});
    await Utils.selectedLineBehaviour(line, false, context, false);
  }

  Widget tfFilter(Color themeColor) => Container(

        padding: EdgeInsets.all(5),
        child: Card(

          color: Theme.of(context).primaryColor,
          elevation: 5,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          child: Container(
            padding: EdgeInsets.all(5),
            child: TextFormField(
              onFieldSubmitted: (_) async { //Click ok - ajusta lista de horários abaixo de campo filtrar
                _controller.jumpTo(0);
              },
              controller: searchController,
              maxLines: 1,
              style: TextStyle(color: themeColor),
              cursorColor: themeColor,
              decoration: InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                prefixIcon: Icon(Icons.search, color: themeColor),
                fillColor: themeColor,
                focusColor: themeColor,
                hintText: 'Filtrar',
                hintStyle: TextStyle(color: Colors.white),
                contentPadding:
                    EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                labelStyle: TextStyle(color: themeColor),
              ),
            ),
          ),
        ),
      );

  Widget timetableList(List<MOBLine> lines) => ListView.builder(
        controller: _controller,
        padding: EdgeInsets.only(top: kToolbarHeight * 2),
        itemCount: lines.length,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          MOBLine line = lines[index];
          int nLines = lines.length;


          String textColorStr = line.textColor;
          String backgroundColorStr = line.color;

          Color textColor = Utils.getColorFromHex(textColorStr ?? '#ffffff');
          Color backgroundColor =
              Utils.getColorFromHex(backgroundColorStr ?? '#000000');

          String shortName = line.shortName;

          Text textShortName = Text(
            shortName,
            textAlign: TextAlign.center,
            style: TextStyle(
              backgroundColor: backgroundColor,
              color: textColor,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          );
          Text textLongName = Text(
            line.longName,
            style: TextStyle(color: Colors.black),
          );

          int routeId = line.routeId;

          bool darkColor = textColor.computeLuminance() > 0.5;
          String imageAsset = Utils.getStopTypeSvg(line.type, darkColor);

          return Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              side: BorderSide(width: 2, color: backgroundColor),
            ),
            child: Container(
              padding: EdgeInsets.all(5),
              child: InkWell(
                onTap: () async => await Utils.selectedLineBehaviour(
                    line, false, context, true),
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[

                      Container(
                        width: MediaQuery.of(context).size.width - 125,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[

                            Row(
                              children: <Widget>[
                                Card(
                                  color: backgroundColor,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.0))),
                                  child: Container(
                                    padding: EdgeInsets.all(5),
                                    child: Column(
                                      children: <Widget>[

                                        Row(
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(5),
                                              child: SvgPicture.asset(
                                                imageAsset,
                                                height: 20,
                                              ),
                                            ),
                                            textShortName,
                                          ],
                                        ),

                                        if (false)//(Utils.isMobilibus)
                                          Chip(label: Text('routeId=$routeId'))
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            Container(
                              padding: EdgeInsets.only(left: 5),
                              child: textLongName,
                              width: MediaQuery.of(context).size.width - 100,
                            ),
                          ],
                        ),
                      ),
                      lineActions(line),

                    ],
                  ),
                ),
              ),
            ),
          );

        },
      );

  Widget lineActions(MOBLine line) {
    int routeId = line.routeId;
    return Row(
      children: [
        IconButton(
          onPressed: () => openMap(line),
          icon: Icon(Icons.map),
        ),
        IconButton(
          onPressed: () => favoriteAction(line),
          icon: favoritedMap[routeId] ?? false
              ? Icon(Icons.favorite)
              : Icon(Icons.favorite_border),
        ),
      ],
    );
  }

  void openMap(MOBLine line) async {
    await Utils.selectedLineBehaviour(line, false, context, false);

    int projectId = Utils.project.projectId;

    int routeId = line.routeId;
    String shortName = line.shortName;
    String backgroundColorStr = line.color;
    Color backgroundColor =
        Utils.getColorFromHex(backgroundColorStr ?? '#000000');

    String timetablePath = 'timetable_last_download_$projectId\_$routeId';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String timetableJson = prefs.getString(timetablePath);
    Map<String, dynamic> map = convert.json.decode(timetableJson);
    MOBTimetable timetable = MOBTimetable.fromJson(map);
    List<MOBTrip> trips = timetable.trips;

    String imageAsset = Utils.getStopTypeSvg(line.type, false);

    DialogUtils(context).showRequestTripToView(
        shortName, trips, imageAsset, routeId, backgroundColor);
  }

  @override
  Widget build(BuildContext context) {
    if (Utils.lines.isEmpty && !downloadingTimetable) initTimetable();

    Color themeColor = Utils.getColorByLuminanceTheme(context);

    List<MOBLine> mLines;
    String newText = searchController.text;
    if (newText.isEmpty)
      mLines = Utils.lines;
    else {
      mLines = [];
      final String str = newText.toLowerCase();
      Utils.lines.forEach((line) {
        String short = line.shortName.toLowerCase();
        String long = line.longName.toLowerCase();
        if (short.contains(str) || long.contains(str)) mLines.add(line);
      });
    }

    if (prefs != null) setupFavorites();

    return Scaffold(
      key: scaffoldKey,
      body: Utils.lines.isNotEmpty
          ? Stack(
              children: <Widget>[
                Scrollbar(child: timetableList(mLines)),
                SafeArea(child: tfFilter(themeColor)),
              ],
            )
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                if (loading)
                  CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Utils.isLightTheme(context)
                              ? Utils.getColorFromPrimary(context)
                              : Colors.black)),
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(50),
                  child: Text(
                    loading
                        ? 'Baixando linhas...'
                        : 'Não foi possível baixar a tabela de horários, tente novamente mais tarde',
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
    );
  }
}
