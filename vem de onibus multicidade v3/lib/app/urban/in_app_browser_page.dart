import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:package_info/package_info.dart';

import '../../utils/Utils.dart';

class InAppBrowserPage extends StatefulWidget{
  const InAppBrowserPage({Key key}) : super(key: key);

  @override
  _InAppBrowserPage createState() => _InAppBrowserPage();
}

class _InAppBrowserPage extends State<InAppBrowserPage>{
  double nProgress = 0;
  InAppWebViewController webView;
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    Color themeColor = Utils.getColorByLuminanceTheme(context);
    TextStyle textStyle = TextStyle(color: themeColor);

    @override
    void initState(){
      //while(!Utils.getPhoneInfo);
    }
    String uuid = Utils.uuid;
    String url = 'https://share.botstar.com/?id=s5c455670-2606-11ec-a988-257e7a1ff63a';

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Utils.getColorFromPrimary(context),
        centerTitle: true,
        title: Text('Assédio', style: textStyle),
      ),
      body: Stack(
        children: [
          InAppWebView(
            androidOnGeolocationPermissionsShowPrompt:
                (InAppWebViewController controller,
                String origin) async {
              return GeolocationPermissionShowPromptResponse(
                  origin: origin, allow: true, retain: true);
            },
            initialUrlRequest: URLRequest(
              url: Uri.parse(url)),
            onWebViewCreated: (InAppWebViewController controller){
              webView = controller;
            },
            onProgressChanged: (InAppWebViewController controller, int progress){
              setState(() {
                nProgress = progress / 100;
              });
            },
          ),
          nProgress < 1 ? SizedBox(
            height: 3,
            child: LinearProgressIndicator(
              value: nProgress,
            ),
          ) : SizedBox(),
        ],
      ),
    );
  }
}