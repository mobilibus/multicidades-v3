import 'dart:convert' as convert;
import 'dart:developer';

import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_json_widget/flutter_json_widget.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/accessibility/main_accessibility.dart';
import 'package:mobilibus/app/urban/app.dart';
import 'package:mobilibus/base/MOBProject.dart';
import 'package:mobilibus/main.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'urban/save_load_projectId_local/save_load_projectId_local.dart';

class SelectCityUrban extends StatefulWidget {
  const SelectCityUrban({Key key}) : super(key: key);

  @override
  _SelectCityUrban createState() => _SelectCityUrban();
}

class _SelectCityUrban extends State<SelectCityUrban> {
  TextEditingController searchController = TextEditingController();

  List<SelectableProject> urbans = [];

  Text txTimetable;
  Widget appBarTitle;
  Icon iconSearch = Icon(Icons.search);
  TextField tfFilter;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    debugPaintSizeEnabled = false;

    Future.delayed(Duration(), () async {
      http.Response response = await MOBApiUtils.getUrbanProjects();
      if (response != null) {
        String jsonString = convert.utf8.decode(response.bodyBytes);
        Iterable iterable = convert.json.decode(jsonString);
        urbans = iterable.map((i) => SelectableProject.fromJson(i)).toList();
      }
      loading = false;
      setState(() {});
    });
    SharedPreferences.getInstance().then((prefs) async {
      Color themeColor = Utils.getColorByLuminanceTheme(context);

      iconSearch = Icon(Icons.search, color: themeColor);

      txTimetable = Text('Cidades', style: TextStyle(color: themeColor));

      appBarTitle = txTimetable;

      tfFilter = TextField(
        controller: searchController,
        maxLines: 1,
        autofocus: true,
        style: TextStyle(color: themeColor),
        cursorColor: themeColor,
        onChanged: (text) => setState(() {}),
        decoration: InputDecoration(
          fillColor: themeColor,
          focusColor: themeColor,
          labelStyle: TextStyle(color: themeColor),
          labelText: 'Filtrar',
        ),
      );
      setState(() {});
    });
  }

  void lsoadUrban() {
    //MOBApiUtils.headers['x-mob-project-id'] = Utils.project.projectId.toString();

    if (MediaQuery.of(context).accessibleNavigation) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => MainPageAccessibility(),
          ),
          (_) => false);
    } else {
      MOBApiUtils.headers['x-mob-project-id'] =
          Utils.project.projectId.toString();
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => MainPage(
                    selectTab: 0,
                  )));
      // MainPage(0)));
      // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => MainPage(0)), (_) => false);
    }
  }

  // AQUI
  void selectedProject(SelectableProject urban) async {
    setState(() => loading = true);

    Utils.globalProjectID = urban.projectId;

    await Utils.selectedProject(urban.projectId).then((value) {
      setState(() {
        Utils.project = value;
      });
    }).catchError((onError) {
      log("OXI, Deu Erro: $onError");
    });

    // MOBApiUtils.headers['x-mob-project-id'] = urban.projectId.toString();

    MOBApiUtils.headers['x-mob-project-id'] =
        Utils.project.projectId.toString();

    SaveLoadSharedLocalStorageViewModel().setProjectId(urban.projectId);
    setState(() => loading = false);
    // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Splash()));
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (context) => MainPage(selectTab: 0)));

    // if (Utils.project != null) loadUrban();
    
  }

  void projectDetails(int projectId) async {
    http.Response response =
        await MOBApiUtils.getProjectDetails(projectId, false);
    if (response != null) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(jsonString);

      showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          content: SingleChildScrollView(
            child: JsonViewerWidget(map, notRoot: false),
          ),
        ),
      );
    }
  }

  // AQUI
  List<SelectableProject> filterListProjects() {
    List<SelectableProject> newList = [];

    for (var item in List.of(urbans)) {
      if (item.projectId == 508 ||
          item.projectId == 507 ||
          item.projectId == 512) {
        newList.add(item);
      }
    }
    return newList;
  }

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);

    // AQUI
    List<SelectableProject> projects = filterListProjects(); // List.of(urbans);
    bool isDev = Utils.isMobilibus;

    String text = searchController.text.toLowerCase();

    if (text.isNotEmpty) {
      projects.removeWhere((project) {
        String name = project.name.toLowerCase();
        String city = project.city.toLowerCase();
        String country = project.country.toLowerCase();

        bool removeId =
            isDev ? project.projectId.toString().contains(text) : false;
        return !name.contains(text) &&
            !city.contains(text) &&
            !country.contains(text) &&
            !removeId;
      });
    }

    projects.sort((a, b) {
      String first = removeDiacritics(a.city);
      String second = removeDiacritics(b.city);
      return first.compareTo(second);
    });

    ListView listView = ListView.builder(
      padding: EdgeInsets.only(bottom: 60),
      itemCount: projects.length,
      itemBuilder: (context, index) {
        SelectableProject urban = projects[index];
        int projectId = urban.projectId;
        String name = urban.name;
        String city = urban.city;
        String country = urban.country;
        return Container(
          padding: EdgeInsets.all(5),
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: InkWell(
              onTap: () => selectedProject(urban),
              onLongPress: isDev ? () => projectDetails(projectId) : null,
              child: Container(
                padding: EdgeInsets.all(5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(isDev ? '[$projectId] $name' : city),
                    Text(isDev ? '$city, $country' : country),
                    if (Utils.isMobilibus)
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          'Hold for details',
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            fontSize: 12,
                            fontStyle: FontStyle.italic,
                            color: Colors.grey[400],
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );

    Widget nourbanProjectContainer = Center(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Text(
            'Nenhuma cidade obtida, reconecte a internet e tente novamente mais tarde'),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Utils.globalPrimaryColor,
        centerTitle: true,
        title: appBarTitle,
        leading: Container(),
        actions: <Widget>[
          IconButton(
            color: themeColor,
            icon: iconSearch,
            onPressed: () {
              if (this.iconSearch.icon == Icons.search) {
                this.iconSearch = Icon(Icons.close, color: themeColor);
                this.appBarTitle = tfFilter;
              } else {
                searchController.text = '';
                this.iconSearch = Icon(Icons.search, color: themeColor);
                this.appBarTitle = txTimetable;
              }
              setState(() {});
            },
          ),
        ],
      ),
      body: loading
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('Carregando...'),
                  CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Utils.isLightTheme(context)
                              ? Utils.getColorFromPrimary(context)
                              : Colors.black)),
                ],
              ),
            )
          : projects.length > 0
              ? listView
              : nourbanProjectContainer,
    );
  }
}
