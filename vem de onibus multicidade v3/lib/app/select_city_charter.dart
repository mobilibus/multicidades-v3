import 'dart:convert' as convert;

import 'package:diacritic/diacritic.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/app/charter/charter_login.dart';
import 'package:mobilibus/base/MOBProject.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SelectCityCharter extends StatefulWidget {
  State<StatefulWidget> createState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    debugPaintSizeEnabled = false;

    return _SelectCityCharter();
  }
}

class _SelectCityCharter extends State<SelectCityCharter> {
  TextEditingController searchController = TextEditingController();

  List<SelectableProject> charters = [];

  Text txTimetable;
  Widget appBarTitle;
  Icon iconSearch = Icon(Icons.search);
  TextField tfFilter;
  bool loading = true;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      http.Response response = await MOBApiUtils.getCharterProjects();
      if (response != null) {
        String jsonString = convert.utf8.decode(response.bodyBytes);
        Iterable iterable = convert.json.decode(jsonString);
        charters = iterable.map((i) => SelectableProject.fromJson(i)).toList();
      }
      loading = false;
      setState(() {});
    });
    SharedPreferences.getInstance().then((prefs) async {
      Color themeColor = Utils.getColorByLuminanceTheme(context);

      iconSearch = Icon(Icons.search, color: themeColor);

      txTimetable = Text('', style: TextStyle(color: themeColor));

      appBarTitle = txTimetable;

      tfFilter = TextField(
        controller: searchController,
        maxLines: 1,
        autofocus: true,
        style: TextStyle(color: themeColor),
        cursorColor: themeColor,
        onChanged: (text) => setState(() {}),
        decoration: InputDecoration(
          fillColor: themeColor,
          focusColor: themeColor,
          labelStyle: TextStyle(color: themeColor),
          labelText: '',
        ),
      );
      setState(() {});
    });
  }

  void selectedCharter(SelectableProject charter) async {
    loading = true;
    setState(() {});
    int projectId = charter.projectId;
    Utils.project = await Utils.selectedProject(projectId);
    if (Utils.project != null){
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => CharterLogin()));
    }
    loading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Color themeColor = Utils.getColorByLuminanceTheme(context);

    List<SelectableProject> projects = List.of(charters);
    bool isDev = Utils.isMobilibus;

    String text = searchController.text.toLowerCase();
    projects.removeWhere((project) {
      String name = project.name.toLowerCase();
      String city = project.city.toLowerCase();
      String country = project.country.toLowerCase();

      return !name.contains(text) &&
          !city.contains(text) &&
          !country.contains(text);
    });

    projects.sort((a, b) {
      String first = removeDiacritics(a.name);
      String second = removeDiacritics(b.name);
      return first.compareTo(second);
    });

    ListView listView = ListView.builder(
      shrinkWrap: true,
      padding: EdgeInsets.only(bottom: 60),
      itemCount: projects.length,
      itemBuilder: (context, index) {
        SelectableProject charter = projects[index];
        int projectId = charter.projectId;
        String name = charter.name;
        String city = charter.city;
        String country = charter.country;
        return Container(
          padding: EdgeInsets.all(5),
          child: Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: InkWell(
              onTap: () => selectedCharter(charter),
              child: Container(
                width: MediaQuery.of(context).size.width - 90,
                padding: EdgeInsets.all(5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: [
                        if (Utils.isMobilibus)
                          Text('$projectId',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20)),
                        Text(isDev ? name : city),
                      ],
                    ),
                    Text(isDev ? '$city, $country' : country),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );

    Widget noCharterProjectContainer = Center(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Text(
            'Nenhuma cidade obtida, reconecte a internet e tente novamente mais tarde'),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: appBarTitle,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: themeColor),
          onPressed: () => Navigator.pop(context, null),
        ),
        actions: <Widget>[
          IconButton(
            color: themeColor,
            icon: iconSearch,
            onPressed: () {
              if (this.iconSearch.icon == Icons.search) {
                this.iconSearch = Icon(Icons.close, color: themeColor);
                this.appBarTitle = tfFilter;
              } else {
                searchController.text = '';
                this.iconSearch = Icon(Icons.search, color: themeColor);
                this.appBarTitle = txTimetable;
              }
              setState(() {});
            },
          ),
        ],
      ),
      body: loading
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('Carregando cidades...'),
                  CircularProgressIndicator(
                      strokeWidth: 3,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Utils.isLightTheme(context)
                              ? Utils.getColorFromPrimary(context)
                              : Colors.black)),
                ],
              ),
            )
          : projects.length > 0
              ? listView
              : noCharterProjectContainer,
    );
  }
}
