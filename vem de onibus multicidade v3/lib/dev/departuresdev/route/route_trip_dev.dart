import 'dart:async';
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/base/MOBTripDetails.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class RouteTripDev extends StatefulWidget {
  RouteTripDev(this.stop, this.routeId, this.tripId) : super();
  final MOBStop stop;
  final int routeId;
  final int tripId;
  _RouteTripDev createState() => _RouteTripDev(stop, routeId, tripId);
}

class _RouteTripDev extends State<RouteTripDev> {
  _RouteTripDev(this.stop, this.routeId, this.tripId) : super();
  final MOBStop stop;
  final int routeId;
  final int tripId;

  GoogleMapController googleMapController;
  Timer timer;
  LatLngBounds bounds;
  Map<String, Marker> vehiclesMarkers = {};
  List<LatLng> latLngs;

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    timer = Timer.periodic(Duration(seconds: Utils.updateDelay), (timer) {});
    super.initState();
    Future.delayed(Duration(), () async {
      await getTripDetails();
      fitBounds();
    });
  }

  Future<void> getTripDetails() async {
    http.Response response = await MOBApiUtils.getTripDetailsFuture(tripId);
    if (response != null) {
      String body = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> object = convert.json.decode(body);
      MOBTripDetails tripDetails = MOBTripDetails.fromJson(object);
      String shape = tripDetails.shape;
      List<PointLatLng> shapeDecoded = PolylinePoints().decodePolyline(shape);
      latLngs = [];
      for (final shapeObject in shapeDecoded) {
        double lat = shapeObject.latitude;
        double lng = shapeObject.longitude;
        LatLng latLng = LatLng(lat, lng);
        latLngs.add(latLng);
      }
      double x0, x1, y0, y1;
      for (final LatLng latLng in latLngs) {
        if (x0 == null) {
          x0 = x1 = latLng.latitude;
          y0 = y1 = latLng.longitude;
        } else {
          if (latLng.latitude > x1) x1 = latLng.latitude;
          if (latLng.latitude < x0) x0 = latLng.latitude;
          if (latLng.longitude > y1) y1 = latLng.longitude;
          if (latLng.longitude < y0) y0 = latLng.longitude;
        }
      }

      LatLng ne = LatLng(x1, y1);
      LatLng sw = LatLng(x0, y0);

      bounds = LatLngBounds(northeast: ne, southwest: sw);
      for (final newVehicle in tripDetails.vehicles)
        createVehicleMarkerAndContainer(newVehicle);
      setState(() {});
    }
  }

  Future<void> fitBounds() async => googleMapController
      .animateCamera(CameraUpdate.newLatLngBounds(bounds, 20));

  void createVehicleMarkerAndContainer(MOBTripDetailsVehicle v) {
    MarkerId id = MarkerId(v.hashCode.toString());
    String vehicleId = v.vehicleId;

    double lat = v.latitude;
    double lng = v.longitude;
    LatLng position = LatLng(lat, lng);

    Marker m = Marker(
      markerId: id,
      position: position,
      infoWindow: InfoWindow(title: vehicleId),
    );
    vehiclesMarkers[v.vehicleId] = m;
  }

  void onMapCreated(onMapCreatead) async {
    googleMapController = onMapCreatead;
    Completer<GoogleMapController>().complete(googleMapController);
    await rootBundle
        .loadString('assets/maps_style_default.json')
        .then((style) => googleMapController.setMapStyle(style));
  }

  @override
  Widget build(BuildContext context) {
    String name = stop.name;
    int stopId = stop.stopId;
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Center(
          child: Container(
            padding: EdgeInsets.only(left: 60, top: 5, bottom: 5, right: 60),
            child: Text('$name\nstopId: $stopId'),
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.center_focus_weak),
            onPressed: fitBounds,
          ),
        ],
      ),
      body: GoogleMap(
        onMapCreated: onMapCreated,
        zoomControlsEnabled: false,
        myLocationButtonEnabled: false,
        buildingsEnabled: false,
        mapToolbarEnabled: false,
        myLocationEnabled: false,
        compassEnabled: false,
        rotateGesturesEnabled: false,
        trafficEnabled: false,
        markers: vehiclesMarkers.values.toSet(),
        polylines: latLngs == null
            ? null
            : Set<Polyline>.of([
                Polyline(
                  polylineId: PolylineId(latLngs.hashCode.toString()),
                  points: latLngs,
                  width: 5,
                  color: Colors.amber,
                )
              ]),
        initialCameraPosition: CameraPosition(target: LatLng(0.0, 0.0)),
      ),
    );
  }
}
