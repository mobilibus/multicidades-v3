import 'dart:async';
import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/base/MOBStop.dart';
import 'package:mobilibus/base/MOBTripDetails.dart';
import 'package:mobilibus/base/dev/DepartureResultDev.dart';
import 'package:mobilibus/dev/departuresdev/route/route_trip_dev.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';
import 'package:mobilibus/utils/Utils.dart';

class DepartureWidgetDev extends StatefulWidget {
  DepartureWidgetDev(this.stop);
  final MOBStop stop;
  _DepartureWidgetDev createState() => _DepartureWidgetDev(stop);
}

class _DepartureWidgetDev extends State<DepartureWidgetDev> {
  _DepartureWidgetDev(this.stop) : super();
  final MOBStop stop;

  DepartureResultDev departureResultDev;
  bool isNextDay = false;
  TextEditingController tecFilter = TextEditingController();
  Timer timer;
  int seconds = 10;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      seconds--;
      if (seconds == -1) request();
      setState(() {});
    });
    request();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  void request() async {
    seconds = 10;
    http.Response response = await MOBApiUtils.getDepartures(stop.stopId);
    if (response != null) {
      String jsonString = convert.utf8.decode(response.bodyBytes);
      Map<String, dynamic> map = convert.json.decode(jsonString);
      departureResultDev = DepartureResultDev.fromJson(map);
      setState(() {});
    }
  }

  void openInfo(int routeId, int tripId) async {
    try {
      http.Response response = await MOBApiUtils.getTripDetailsFuture(tripId);
      if (response != null) {
        String jsonString = convert.utf8.decode(response.bodyBytes);
        Map<String, dynamic> map = convert.json.decode(jsonString);
        MOBTripDetails tripDetails = MOBTripDetails.fromJson(map);
        List<MOBTripDetailsVehicle> vehicles = tripDetails.vehicles;
        int seq = tripDetails.stops
            .indexWhere((element) => element.stopId == stop.stopId);
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(
              'Detalhes da Viagem\nrouteId: $routeId | tripId: $tripId | seq: $seq',
              style: TextStyle(fontSize: 12),
            ),
            content: Container(
              width: MediaQuery.of(context).size.width,
              child: vehicles.isEmpty
                  ? Text('Sem carros')
                  : ListView.builder(
                      shrinkWrap: true,
                      itemCount: vehicles.length,
                      itemBuilder: (context, index) {
                        MOBTripDetailsVehicle vehicle = vehicles[index];
                        String vehicleId = vehicle.vehicleId;
                        String positionTime = vehicle.positionTime;
                        int travelled = vehicle.percTravelled;
                        return Container(
                          padding: EdgeInsets.all(5),
                          child: Card(
                            elevation: 10,
                            child: ListTile(
                              title: Text(vehicleId),
                              subtitle: Text('$positionTime - $travelled%'),
                            ),
                          ),
                        );
                      },
                    ),
            ),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text('Fechar'),
              ),
            ],
          ),
        );
      }
    } catch (e) {}
  }

  Widget listView(List<DepartureTripDev> departures) => ListView.builder(
        padding: EdgeInsets.only(top: 80),
        itemCount: departures.length,
        itemBuilder: (context, index) {
          DepartureTripDev dtv = departures[index];

          int routeId = dtv.routeId;
          int tripId = dtv.tripId;
          String headsign = dtv.headsign;
          String shortName = dtv.shortName;
          String longName = dtv.longName;

          return Container(
            padding: EdgeInsets.all(5),
            child: Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.info),
                        onPressed: () => openInfo(routeId, tripId),
                      ),
                      IconButton(
                        icon: Icon(Icons.map),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) =>
                                RouteTripDev(stop, routeId, tripId),
                          );
                        },
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.remove_red_eye,
                          color: tecFilter.text == tripId.toString()
                              ? Colors.blue
                              : Colors.black,
                        ),
                        onPressed: () {
                          if (tecFilter.text == tripId.toString())
                            tecFilter.clear();
                          else
                            tecFilter.text = tripId.toString();
                          setState(() {});
                        },
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: (MediaQuery.of(context).size.width / 5) - 20,
                            child: Card(
                              color: Utils.getColorFromHex(dtv.color),
                              child: Container(
                                child: Text(
                                  shortName,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Utils.getColorFromHex(dtv.color)
                                                .computeLuminance() >
                                            0.5
                                        ? Colors.black
                                        : Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Text('routeId: $routeId | tripId: $tripId'),
                        ],
                      ),
                      Text(
                        longName,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 10),
                      ),
                      Text(
                        headsign,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 8,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  if (dtv.vehicles.isEmpty)
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        'Nenhum veículo em tempo real',
                        style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.bold,
                          fontSize: 12,
                        ),
                      ),
                    )
                  else
                    Container(
                      height: 110,
                      child: Scrollbar(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.directions_bus),
                              ListView.builder(
                                scrollDirection: Axis.horizontal,
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: dtv.vehicles.length,
                                itemBuilder: (context, index) {
                                  VehicleDev vehicleDev = dtv.vehicles[index];

                                  String vehicleId = vehicleDev.vehicleId;
                                  String positionTime = vehicleDev.positionTime;
                                  String tripFeedId = vehicleDev.tripFeedId;
                                  int delay = vehicleDev.delay;

                                  return Container(
                                    padding: EdgeInsets.all(10),
                                    child: Card(
                                      color: Colors.amber,
                                      elevation: 5,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10.0))),
                                      child: Container(
                                        padding: EdgeInsets.all(10),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              'carro: $vehicleId',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                                'delay: $delay | positionTime: $positionTime'),
                                            Text(
                                              'tripFeedId: $tripFeedId',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  Container(
                    height: 110,
                    child: Scrollbar(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: <Widget>[
                            Icon(Icons.access_time),
                            ListView.builder(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: dtv.departures.length,
                              itemBuilder: (context, index) {
                                DepartureDev dd = dtv.departures[index];

                                bool online = dd.online;
                                String time = dd.time;
                                int minutes = dd.minutes;
                                int seconds = dd.seconds;
                                int secondsDelay = dd.secondsDelay;
                                String tripFeedId = dd.tripFeedId;

                                return Container(
                                  padding: EdgeInsets.all(10),
                                  child: Card(
                                    color: online ? Colors.green : Colors.white,
                                    elevation: 5,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0))),
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                              'time: $time | minutes: $minutes'),
                                          Text(
                                              'seconds: $seconds | secondsDelay: $secondsDelay'),
                                          Text(
                                            'tripFeedId: $tripFeedId',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );

  Widget card(bool nextDay) => Container(
        padding: EdgeInsets.all(10),
        child: Card(
          color: Colors.blue,
          elevation: 5,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          child: Container(
            padding: EdgeInsets.all(10),
            child: Text(
              'NextDay = $nextDay',
              textAlign: TextAlign.center,
            ),
          ),
        ),
      );

  List<DepartureTripDev> filter(List<DepartureTripDev> list) {
    String text = tecFilter.text.toLowerCase();
    return list.where((element) {
      String tripId = element.tripId.toString().toLowerCase();
      String routeId = element.routeId.toString().toLowerCase();
      List<VehicleDev> vehicles = element.vehicles;
      bool containsVehicleId = vehicles.firstWhere((element) {
            String vehicleId = element.vehicleId;
            if (vehicleId == null)
              return false;
            else {
              vehicleId = vehicleId.toLowerCase();
              return vehicleId.contains(text);
            }
          }, orElse: () => null) !=
          null;
      return tripId.contains(text) ||
          containsVehicleId ||
          routeId.contains(text);
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    List<DepartureTripDev> today = departureResultDev?.departures ?? [];
    List<DepartureTripDev> nextDay =
        departureResultDev?.departuresNextDay ?? [];
    List<DepartureTripDev> list = isNextDay ? nextDay : today;
    if (departureResultDev != null && tecFilter.text.isNotEmpty)
      list = filter(list);

    return Scaffold(
      appBar: departureResultDev == null && list.isEmpty
          ? null
          : AppBar(
              title: TextField(
                controller: tecFilter,
                onChanged: (text) => setState(() {}),
                cursorColor: Colors.white,
                decoration: InputDecoration(
                  hintText: 'IDs',
                ),
              ),
              actions: [
                Row(
                  children: [
                    Text('NextDay'),
                    Checkbox(
                      value: isNextDay,
                      onChanged: (value) => setState(() => isNextDay = value),
                    ),
                  ],
                ),
              ],
            ),
      body: departureResultDev == null && list.isEmpty
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                ],
              ),
            )
          : Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                listView(list),
                Container(
                  padding: EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        width: (MediaQuery.of(context).size.width / 1.5) - 10,
                        child: Card(
                          color: Colors.indigo,
                          child: Container(
                            padding: EdgeInsets.all(5),
                            child: Text(
                              '${stop.name}\nstopId: ${stop.stopId}',
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: (MediaQuery.of(context).size.width / 3) - 8,
                        child: Card(
                          child: Container(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  '${seconds < 10 ? '0' : ''}$seconds',
                                  style: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                IconButton(
                                  onPressed: request,
                                  icon: Icon(
                                    Icons.refresh,
                                    size: 30,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
    );
  }
}
