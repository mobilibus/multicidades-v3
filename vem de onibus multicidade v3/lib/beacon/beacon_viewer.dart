import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_beacon/flutter_beacon.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:share/share.dart';

class BeaconViewer extends StatefulWidget {
  _BeaconViewer createState() => _BeaconViewer();
}

class _BeaconViewer extends State<BeaconViewer> {
  Timer timer;

  @override
  void initState() {
    timer = Timer.periodic(Duration(seconds: 5), (timer) => setState(() {}));
    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('Beacons'),
        ),
        body: ListView.builder(
          itemCount: Utils.beaconService?.mBeacons?.length ?? 0,
          itemBuilder: (context, index) {
            Beacon beacon = Utils.beaconService.mBeacons[index];
            String macAddress = beacon.macAddress ?? '?';
            int rssi = beacon.rssi ?? -1;
            int txPower = beacon.txPower ?? 0;
            int major = beacon.major ?? -1;
            int minor = beacon.minor ?? -1;
            String uuid = beacon.proximityUUID ?? '?';

            DateTime dateTime = Utils.beaconService.dateTimeByBeacon[uuid];

            int day = dateTime.day;
            int month = dateTime.month;
            int year = dateTime.year;

            int hour = dateTime.hour;
            int minute = dateTime.minute;
            int second = dateTime.second;

            String lastUpdate = '$day/$month/$year - $hour:$minute:$second';

            double accuracy = beacon.accuracy ?? -1;
            String proximity = accuracy == 0
                ? 'unknown or by side'
                : accuracy < 0.5
                    ? 'immediate'
                    : accuracy < 3.0
                        ? 'near'
                        : 'far';
            String id = uuid + '_' + macAddress;
            return Container(
              padding: EdgeInsets.all(10),
              child: Card(
                child: InkWell(
                  onTap: () => Share.share(id),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          macAddress,
                          style: TextStyle(
                              fontSize: 40, fontWeight: FontWeight.bold),
                        ),
                        Row(
                          children: [
                            Container(
                              width: ((MediaQuery.of(context).size.width / 4) *
                                      3) -
                                  40,
                              child: Text(
                                'uuid: $uuid\n'
                                'major: $major / minor: $minor\n'
                                'rssi: $rssi\ntxPower: $txPower\naccuracy: $accuracy\nproximity: $proximity\n'
                                'Última comunicação: $lastUpdate',
                                style: TextStyle(fontSize: 15),
                              ),
                            ),
                            Container(
                              width: (MediaQuery.of(context).size.width / 4),
                              child: Icon(
                                Icons.bluetooth,
                                size: 100,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Divider(
                            height: 1,
                            thickness: 1,
                          ),
                        ),
                        Text('ID: $id'),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      );
}
