class FormattingUtils {
  static String cpfMask = "###.###.###-##";
  static String dateMask = "##/##/####";
  static String paymentCardMask = "#### #### #### ####";
  static String cardExpirationMask = "##/####";
  static String cvvMask = "####";
}

class Constants {
  static const String APP_NAME = 'mobilibus';
  static const String APP_VERSION = '2.3.0';
  static const String BUYER_ID = 'Mobilibus';
  static const int PROJECT_ID = 608;
}
