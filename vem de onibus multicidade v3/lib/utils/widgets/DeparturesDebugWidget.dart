import 'package:flutter/material.dart';
import 'package:mobilibus/base/MOBDeparture.dart';

class DeparturesDebugWidget extends StatefulWidget {
  DeparturesDebugWidget(this.departureLine) : super();
  final MOBDepartureTrip departureLine;
  _DeparturesDebugWidget createState() => _DeparturesDebugWidget(departureLine);
}

class _DeparturesDebugWidget extends State<DeparturesDebugWidget> {
  _DeparturesDebugWidget(this.departureLine) : super();
  final MOBDepartureTrip departureLine;
  @override
  Widget build(BuildContext context) {
    int routeId = departureLine.routeId;
    int tripId = departureLine.tripId;
    String shortName = departureLine.shortName;
    String longName = departureLine.longName;
    String color = departureLine.color;
    String headSign = departureLine.headsign;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Container(
          child: Text(
            'shortName: $shortName\nlongName: $longName',
            style: TextStyle(fontSize: 12),
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('routeId: $routeId'),
                    Text('tripId: $tripId'),
                    Text('color: $color'),
                    Text('headSign: $headSign'),
                  ],
                ),
              ),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: departureLine.departures.length,
                itemBuilder: (context, index) {
                  MOBDeparture ld = departureLine.departures[index];

                  bool online = ld.online;

                  String time = ld.time;
                  bool wa = ld.wheelchairAccessible;
                  bool nextDay = ld.nextDay;
                  String vehicleId = ld.vehicleId;
                  String gpsTime = ld.gpsTime;
                  int bearing = ld.bearing;
                  int delay = ld.delay;

                  int minutes = ld.minutes;

                  return ListTile(
                    title: Text('time: $time\nminutes: $minutes'),
                    subtitle: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('wheelchairAccessible: $wa'),
                        Text('nextDay: $nextDay'),
                        if (online) Text('vehicleId: $vehicleId'),
                        if (online) Text('gpsTime: $gpsTime'),
                        if (online) Text('bearing: $bearing'),
                        if (online) Text('delay: $delay'),
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
