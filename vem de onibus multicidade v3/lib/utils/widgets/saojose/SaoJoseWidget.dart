import 'package:flutter/material.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

class SaoJoseWidget extends StatefulWidget {
  _SaoJoseWidget createState() => _SaoJoseWidget();
}

class _SaoJoseWidget extends State<SaoJoseWidget>
    with TickerProviderStateMixin {
  Map<String, String> urlMap = {
    'http://blog.mobilibus.com/wp-content/uploads/saojose-rodov.html':
        'INTERESTADUAL',
    'https://saojose.toppassagens.com.br/Principal': 'JARDIM DO ÉDEN / POA',
    'https://www.veppocel.com.br/mobile/php/login.php': 'POA / JARDIM DO ÉDEN',
  };

  List<String> subs = [
    'Tramandaí, Imbé, Capão da Canoa, Arroio do Sal e Torres (RS), Balneário Camboriú, Itapema, Florianópolis e Tubarão (SC)',
    'Oásis Sul e Nova Tramandaí',
    '',
  ];

  List<Widget> contents = [];
  List<Widget> tabs = [];
  TabController tabController;

  @override
  void initState() {
    super.initState();

    List<String> keys = urlMap.keys.toList();
    contents.add(onlineServicesMainPage(keys));
    tabs.add(Text('INÍCIO'));

    for (final key in keys) {
      WebView webView = WebView(
        initialUrl: key,
        javascriptMode: JavascriptMode.unrestricted,
      );
      contents.add(webView);
      tabs.add(Text(urlMap[key]));
    }
    tabController = TabController(
      vsync: this,
      length: tabs.length,
      initialIndex: 0,
    );
  }

  Widget onlineServicesMainPage(List<String> keys) => Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text('Requer acesso a internet.'),
          ListView.separated(
            padding: EdgeInsets.all(20),
            shrinkWrap: true,
            separatorBuilder: (context, index) => Divider(thickness: 3),
            itemCount: urlMap.keys.length,
            itemBuilder: (context, index) {
              String key = keys[index];

              return FlatButton(
                onPressed: () => setState(() => tabController.animateTo(
                      index + 1,
                      duration: Duration(milliseconds: 350),
                    )),
                child: Container(
                  padding: EdgeInsets.only(top: 20, bottom: 20),
                  child: Wrap(
                    children: <Widget>[
                      Container(
                        color: Utils.getColorFromPrimary(context),
                        child: ListTile(
                          title: Text(
                            urlMap[key],
                            style: TextStyle(
                                color:
                                    Utils.getColorFromPrimaryReverse(context)),
                          ),
                        ),
                      ),
                      Text(subs[index]),
                    ],
                  ),
                ),
              );
            },
          )
        ],
      );

  Duration duration = Duration(milliseconds: 350);
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (tabController.index > 0) {
          setState(() => tabController.animateTo(0, duration: duration));
          return false;
        }
        return true;
      },
      child: DefaultTabController(
        length: contents.length,
        initialIndex: 0,
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          //resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            title: Text('VENDA DE PASSAGENS'),
            actions: <Widget>[
              if (tabController.index != 0)
                IconButton(
                  icon: Icon(Icons.open_in_browser),
                  onPressed: () =>
                      launch(urlMap.keys.toList()[tabController.index - 1]),
                )
            ],
            bottom: TabBar(
              controller: tabController,
              isScrollable: true,
              onTap: (index) => setState(() {}),
              tabs: tabs,
            ),
          ),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: contents,
            controller: tabController,
          ),
        ),
      ),
    );
  }
}
