import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:mobilibus/transdata/data/model/transdata_balance.dart';
import 'package:mobilibus/transdata/data/model/transdata_extract.dart';
import 'package:mobilibus/transdata/data/model/transdata_history.dart';
import 'package:mobilibus/transdata/data/model/transdata_model.dart';
import 'package:mobilibus/transdata/data/model/transdata_products.dart';
import 'package:mobilibus/transdata/data/model/transdata_purchase.dart';
import 'package:mobilibus/transdata/data/model/transdata_receipt.dart';
import 'package:mobilibus/transdata/data/model/transdata_taxes.dart';
import 'package:mobilibus/transdata/data/model/transdata_user.dart';
import 'package:rxdart/rxdart.dart';

abstract class TransdataApi {
  Future<void> _handshake(TransdataHandshake transdataModel);
  Future<TransdataUser> readUserDefault(TransdataUserDefaultRequest user);
  Future<TransdataUser> readUserByCpf(TransdataUserCpfRequest user);
  Future<TransdataUser> readUserById(TransdataUserByIdRequest user);
  Future<TransdataExtract> cardExtract(TransdataExtractRequest data);
  Future<TransdataBalance> getBalances(TransdataBalancesRequest data);
  Future<TransdataHistoryData> getHistory(TransdataHistoryRequest data);
  Future<TransdataHistoryDetail> getHistoryDetail(TransdataHistoryDetailRequest data);
  Future<TransdataCardBuyResponse> getBuyRequestCard(TransdataCardBuyRequest data);
  Future<TransdataTaxes> getTaxes(TransdataTaxesRequest data);
  Future<TransdataReceipt> getReceipt(TransdataReceiptRequest data);
  Future<TransdataNewPurchase> newPurchase(TransdataNewPurchaseRequest data);
  Future<TransdataProductResponse> getCpfProducts(TransdataCpfProductsRequest data);
  Future<TransdataProductResponse> getRegisteredProducts(TransdataRegisteredProductRequest data);
  Future<TransdataPaymentResponse> doPayment(TransdataPurchaseRequest data);
  Future<TransdataPaymentResponse> checkPurchase(TransdataCheckPurchaseRequest data);

  Future<void> dispose();

  // Transdata - DEV
  // static const _handShakeData =
  //     TransdataHandshake(uid: 'Mobilibus', cid: 9999, pid: 1, app: 'Mob-001');

  // Transdata - PROD
  static const _handShakeData =
  TransdataHandshake(uid: 'Mobilibus', cid: 427, pid: 88, app: 'Mob-001');
}

class TransdataApiImp implements TransdataApi {
  String _url = 'http://painel.mobilibus.com/mobile/';
  //String _handShake = 'http://rdv.teste.itstransdata.com:9090';
  final _headers = {
    'Content-Type': 'application/json',
  };
  Client _client = Client();

  String tokenGB = '';
  String seqGB = '';

  TransdataHandshakeResponse _handshakeResponse;
  StreamSubscription<bool> _timeoutSubscription;
  BehaviorSubject<bool> _handshakeDone = BehaviorSubject.seeded(false);

  _setTimeout([Duration duration = const Duration(minutes: 19, seconds: 57)]) async {
    if (_timeoutSubscription == null) {
      _timeoutSubscription = TimerStream(false, duration).listen((event) {
        _handshakeDone.add(event);
      });
    } else {
      await _timeoutSubscription?.cancel();
      _timeoutSubscription = null;
      _setTimeout();
    }
  }

  Stream<bool> _verifyHandshake() async* {
    yield* _handshakeDone.stream.asyncMap((ready) async {
      if (!ready) {
        await _handshake(TransdataApi._handShakeData);
      }
      return ready;
    });
  }

  @override
  Future<void> _handshake(TransdataHandshake transdataModel) async {
    String body = transdataModel.toJson();

    print(body);
    Response response = await _client
        .post(_url + 'handshake', body: body, headers: _headers)
        .catchError((error) {});
    if (response.data() != null) {
      _handshakeResponse = TransdataHandshakeResponse.fromJson(response.data());
      _setTimeout();
      _handshakeDone.add(true);
      return;
    }
    _handshakeDone.add(false);
  }

  @override
  Future<TransdataExtract> cardExtract(TransdataExtractRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}user-card-extract');
      Response response =
      await _client.post(_url + 'user-card-extract', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataExtract.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataBalance> getBalances(TransdataBalancesRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);

      tokenGB = data.token;
      seqGB = data.seq;

      print('url: ${_url}get-balances');
      Response response =
      await _client.post(_url + 'get-balances', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataBalance.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataCardBuyResponse> getBuyRequestCard(TransdataCardBuyRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}get-buy-request-card');
      Response response =
      await _client.post(_url + 'get-buy-request-card', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataCardBuyResponse.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataProductResponse> getCpfProducts(TransdataCpfProductsRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);

      data.token = tokenGB;
      data.seq = seqGB;

      print('url: ${_url}get-cpf-products');
      Response response =
      await _client.post(_url + 'get-cpf-products', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataProductResponse.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataHistoryData> getHistory(TransdataHistoryRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}get-history');
      Response response =
      await _client.post(_url + 'get-history', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataHistoryData.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataHistoryDetail> getHistoryDetail(TransdataHistoryDetailRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}get-history-detail');
      Response response =
      await _client.post(_url + 'get-history-detail', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataHistoryDetail.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataReceipt> getReceipt(TransdataReceiptRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}get-receipt');
      Response response =
      await _client.post(_url + 'get-receipt', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        return TransdataReceipt.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataProductResponse> getRegisteredProducts(
      TransdataRegisteredProductRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}get-registered-products');
      Response response = await _client.post(_url + 'get-registered-products',
          body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataProductResponse.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataTaxes> getTaxes(TransdataTaxesRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}get-taxes');
      Response response =
      await _client.post(_url + 'get-taxes', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataTaxes.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataNewPurchase> newPurchase(TransdataNewPurchaseRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}new-purchase');
      Response response =
      await _client.post(_url + 'new-purchase', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataNewPurchase.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataUser> readUserByCpf(TransdataUserCpfRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}read-user-by-cpf');
      Response response =
      await _client.post(_url + 'read-user-by-cpf', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataUser.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataUser> readUserById(TransdataUserByIdRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      Response response =
      await _client.post(_url + 'read-user-by-id', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataUser.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataUser> readUserDefault(TransdataUserDefaultRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
        data = setSeqAndToken(data);
        print('url: ${_url}read-user-default');
        Response response =
        await _client.post(_url + 'read-user-default', body: data.toJson(), headers: _headers);
        if (response.data() != null) {
          return TransdataUser.fromJson(response.data());
        }
        return null;
      }).first;
    }).first;
  }

  @override
  Future<TransdataPaymentResponse> checkPurchase(TransdataCheckPurchaseRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}check-purchase');
      Response response =
      await _client.post(_url + 'check-purchase', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataPaymentResponse.fromJson(response.data());
      }
      return null;
    }).first;
  }

  @override
  Future<TransdataPaymentResponse> doPayment(TransdataPurchaseRequest data) async {
    return _verifyHandshake().where((ready) => ready).asyncMap((_) async {
      data = setSeqAndToken(data);
      print('url: ${_url}do-payment');
      Response response =
      await _client.post(_url + 'do-payment', body: data.toJson(), headers: _headers);
      if (response.data() != null) {
        _setTimeout();
        return TransdataPaymentResponse.fromJson(response.data());
      }
      return null;
    }).first;
  }

  Object setSeqAndToken(TransdataRequest data) {
    data.seq = _handshakeResponse.getSequence();
    data.token = _handshakeResponse.sid;
    if (kDebugMode) {
      print('sending: ${data.toJson()}');
    }
    return data;
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TransdataApiImp && o._url == _url && o._handshakeResponse == _handshakeResponse;
  }

  @override
  int get hashCode => _url.hashCode ^ _handshakeResponse.hashCode;

  @override
  Future<void> dispose() {
    _handshakeDone.close();
    _timeoutSubscription?.cancel();
    _timeoutSubscription = null;
  }
}

extension on Response {
  String data() {
    if (kDebugMode && this != null && this.body != null) {
      print('receive: ${this.body}');
    }
    try {
      if (this != null && this.statusCode == 200 && this.body != null) {
        return this.body;
      }
      return null;
    } catch (e) {
      print('receive error $e  ${this.body}');
      return null;
    }
  }
}
