import 'dart:convert';

class TransportCardLocalModel {
  String userCPF;
  int transportCardNumber;

  TransportCardLocalModel({
    this.userCPF,
    this.transportCardNumber,
  });

  Map<String, dynamic> toMap() {
    return {
      'userCPF': this.userCPF,
      'transportCardNumber': this.transportCardNumber,
    };
  }

  factory TransportCardLocalModel.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransportCardLocalModel(
      userCPF: map['userCPF'],
      transportCardNumber: map['transportCardNumber'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransportCardLocalModel.fromJson(String source) =>
      TransportCardLocalModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TransportCardLocalModel(userCPF: $userCPF, transportCardNumber: $transportCardNumber)';
  }
}
