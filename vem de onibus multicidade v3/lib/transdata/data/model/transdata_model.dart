import 'dart:convert' as convert;
import 'dart:convert';
import 'dart:typed_data';

import 'package:dart_des/dart_des.dart';
import 'package:encrypt/encrypt.dart';

class TransdataRequest {
  String token;
  String seq;
  TransdataRequest({
    this.token,
    this.seq,
  });

  TransdataRequest copyWith({
    String token,
    String seq,
  }) {
    return TransdataRequest(
      token: token ?? this.token,
      seq: seq ?? this.seq,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'token': token,
      'seq': seq,
    };
  }

  factory TransdataRequest.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataRequest(
      token: map['token'],
      seq: map['seq'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataRequest.fromJson(String source) => TransdataRequest.fromMap(json.decode(source));

  @override
  String toString() => 'TransdataRequest(token: $token, seq: $seq)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TransdataRequest && o.token == token && o.seq == seq;
  }

  @override
  int get hashCode => token.hashCode ^ seq.hashCode;
}

class TransdataModel {
  final String key;
  int _seq;
  final String sid;
  final String version;

  TransdataModel(this.key, this._seq, this.sid, this.version);

  factory TransdataModel.fromJson(Map<String, dynamic> json, Encrypter encrypter, String version) {
    String data = json['data'] ?? null;

    String decrypted = encrypter.decrypt64(data);
    Map<String, dynamic> mapDecrypted = convert.json.decode(decrypted);
    String key = mapDecrypted['Key'];
    int seq = mapDecrypted['Seq'];
    String sid = mapDecrypted['Sid'];

    return TransdataModel(key, seq, sid, version);
  }

  int get currentSeq => _seq;

  int nextSeq() => _seq += 1;

  Map<String, dynamic> toMap() {
    return {
      'key': key,
      'seq': _seq,
      'sid': sid,
      'version': version,
    };
  }

  @override
  TransdataModel fromMap(Map<String, Object> map) {
    if (map == null) return null;

    return TransdataModel(
      map['key'],
      map['seq'],
      map['sid'],
      map['version'],
    );
  }

  String toJson() => json.encode(toMap());
}

class TransdataHandshake {
  final int cid;
  final int pid;
  final String uid;
  final String app;
  const TransdataHandshake({
    this.cid,
    this.pid,
    this.uid,
    this.app,
  });

  Map<String, dynamic> toMap() {
    return {
      'Cid': cid,
      'Pid': pid,
      'Uid': uid,
      'App': app,
    };
  }

  factory TransdataHandshake.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return TransdataHandshake(
      cid: map['Cid'],
      pid: map['Pid'],
      uid: map['Uid'],
      app: map['App'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataHandshake.fromJson(String source) =>
      TransdataHandshake.fromMap(json.decode(source));
}

class TransdataHandshakeResponse {
  String key;
  int seq;
  String sid;

  TransdataHandshakeResponse({this.key, this.sid, this.seq});

  TransdataHandshakeResponse copyWith({
    String key,
    int seq,
    String sid,
  }) {
    return TransdataHandshakeResponse(
      key: key ?? this.key,
      seq: seq ?? this.seq,
      sid: sid ?? this.sid,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'Key': key,
      'Seq': seq,
      'Sid': sid,
    };
  }

  factory TransdataHandshakeResponse.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    return TransdataHandshakeResponse(
      key: map['Key'],
      seq: map['Seq'],
      sid: map['Sid'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransdataHandshakeResponse.fromJson(String source) =>
      TransdataHandshakeResponse.fromMap(json.decode(source));

  @override
  String toString() => 'HandshakeResponse(key: $key, seq: $seq, sid: $sid)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is TransdataHandshakeResponse && o.key == key && o.seq == seq && o.sid == sid;
  }

  @override
  int get hashCode => key.hashCode ^ seq.hashCode ^ sid.hashCode;

  String getSequence() {
    seq++;
    print('seq utilizado $seq');
    List<int> bytes = ['$seq'.codeUnitAt(0), 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0];

    Uint8List data = convert.base64Decode(key);

    DES3 desECB =
        DES3(key: data, mode: DESMode.ECB, iv: DES.IV_ZEROS, paddingType: DESPaddingType.None);
    List<int> encryptedSeq = desECB.encrypt(bytes);

    String value = convert.base64Encode(encryptedSeq);
    return value;
  }
}
