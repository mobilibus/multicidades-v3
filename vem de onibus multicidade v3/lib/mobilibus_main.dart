import 'dart:async';
import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mobilibus/base/MOBCharterCompany.dart';
import 'package:mobilibus/base/MOBProject.dart';
import 'package:mobilibus/dev/dev_tools.dart';
import 'package:mobilibus/intro/CharterIntro.dart';
import 'package:mobilibus/intro/UrbanIntro.dart';
import 'package:mobilibus/main.dart';
import 'package:mobilibus/minigames/bicudo_game.dart';
import 'package:mobilibus/minigames/clayton_game.dart';
import 'package:mobilibus/minigames/cristiano_game.dart';
import 'package:mobilibus/minigames/marco_game.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SelectProjectBehaviour extends StatefulWidget {
  _SelectProjectBehaviour createState() => _SelectProjectBehaviour();
}

class _SelectProjectBehaviour extends State<SelectProjectBehaviour> {
  String dynamicAsset;
  Timer timer;
  int index = 0;
  List<String> assets = [
    'man.png',
    'adrianavatar.png',
    'woman.png',
    'workman.png',
    'access.png',
  ];
  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(milliseconds: 200), (timer) {
      String assetFolder = 'assets/mobilibus';
      String asset = assets[index];
      dynamicAsset = '$assetFolder/$asset';
      setState(() {});
      index++;
      if (index > assets.length - 1) index = 0;
    });
    Utils.firebaseUser = FirebaseAuth.instance.currentUser;
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  void go(bool isCharter) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    await sp.setBool('isCharter', isCharter);

    Utils.isCharter = isCharter;

    if (sp.containsKey('selectedProject')) {
      String projectSaved = sp.getString('selectedProject');
      Map<String, dynamic> map = json.decode(projectSaved);
      Utils.project = MOBProject.fromJson(map);
      bool isSameModule = Utils.project.isCharter == isCharter;
      if (!isSameModule) {
        sp.remove('selectedProject');
        sp.remove('selectedProjectTime');
        Utils.project = null;
      }
    }

    Widget widget = Utils.isCharter ? CharterIntro() : UrbanIntro();
    MaterialPageRoute materialPageRoute =
        MaterialPageRoute(builder: (context) => widget);

    Utils.isMobilibus
        ? Navigator.of(context).push(materialPageRoute)
        : Navigator.of(context).pushReplacement(materialPageRoute);
  }

  Widget devWidgetToggle() => Container(
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          color: Theme.of(context).primaryColor,
          child: InkWell(
            onTap: () => setState(() => Utils.isMobilibus = !Utils.isMobilibus),
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.developer_board,
                        size: 40,
                      ),
                      Icon(
                        Utils.isMobilibus
                            ? Icons.check_box
                            : Icons.check_box_outline_blank,
                        size: 30,
                      ),
                    ],
                  ),
                  Text('MOB IDS'),
                ],
              ),
            ),
          ),
        ),
      );
  Widget adWidgetToggle(Function(void Function()) setState) => Container(
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          color: Theme.of(context).primaryColor,
          child: InkWell(
            onTap: () => setState(() => Utils.showAds = !Utils.showAds),
            child: Container(
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('ADMOB', textAlign: TextAlign.center),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.wb_auto,
                        size: 50,
                      ),
                      Icon(
                        Utils.showAds
                            ? Icons.check_box
                            : Icons.check_box_outline_blank,
                        size: 40,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      );

  Widget devButton() => Container(
        padding: EdgeInsets.all(5),
        child: Card(
          color: Theme.of(context).primaryColor,
          elevation: 5,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          child: InkWell(
            onTap: openDevTools,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.developer_mode, size: 30),
                  Text('Testes',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20)),
                ],
              ),
            ),
          ),
        ),
      );

  void openDevTools() async {
    await showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) => Container(
        height: MediaQuery.of(context).size.height - kToolbarHeight,
        child: DevTools(),
      ),
    );
    setState(() {});
  }

  void charterModeToggle() {
    List<CHARTER_TYPE> types = CHARTER_TYPE.values;

    showModalBottomSheet(
      context: context,
      builder: (context) => Scaffold(
        appBar: AppBar(
          title: Text('Modo de Fretamento'),
        ),
        body: ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: types.length,
          itemBuilder: (context, index) {
            CHARTER_TYPE charterType = types[index];
            String text = '';
            switch (charterType) {
              case CHARTER_TYPE.LOGIN:
                text = 'Empresa + Matrícula';
                break;
              case CHARTER_TYPE.NO_LOGIN:
                text = 'Sem Login';
                break;
              case CHARTER_TYPE.COMPANY_SELECT:
                text = 'Empresa somente';
                break;
              case CHARTER_TYPE.NO_LOGIN_MAP:
                text = 'Sem Login c/ Mapa';
                break;
            }
            Color bgColor =
                Utils.charterType == charterType ? Colors.indigo : Colors.white;
            Color textColor =
                Utils.charterType == charterType ? Colors.white : Colors.black;
            return Container(
              child: Card(
                elevation: 5,
                color: bgColor,
                child: Container(
                  child: ListTile(
                    title: Text(
                      text,
                      style: TextStyle(color: textColor),
                    ),
                    subtitle: Text(
                      charterType.toString().replaceAll('CHARTER_TYPE.', ''),
                      style: TextStyle(color: textColor),
                    ),
                    onTap: () {
                      Utils.charterType = charterType;
                      Navigator.of(context).pop();
                      go(true);
                    },
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  void openSettings() async {
    await showModalBottomSheet(
      context: context,
      builder: (context) => StatefulBuilder(
        builder: (context, setState) => Container(
          height: MediaQuery.of(context).size.height - kToolbarHeight / 2,
          child: Scaffold(
            appBar: AppBar(title: Text('Configurações')),
            body: ListView(
              shrinkWrap: true,
              children: <Widget>[
                adWidgetToggle(setState),
              ],
            ),
          ),
        ),
      ),
    );
    setState(() {});
  }

  Widget settingsButton() => Container(
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          child: InkWell(
            onTap: openSettings,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  Icon(
                    Icons.settings,
                    size: 30,
                  ),
                  Text('Configurações'),
                ],
              ),
            ),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    User user = Utils.firebaseUser;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
                Colors.black.withOpacity(0.5), BlendMode.dstATop),
            image: AssetImage(strSplash),
          ),
        ),
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    List<String> titles = [
                      'Marco Game',
                      'Clayton Game',
                      'Cristiano Game',
                      'Bicudo Game',
                    ];
                    List<Widget> games = [
                      MarcoGame(),
                      ClaytonGame(),
                      CristianoGame(),
                      BicudoGame(),
                    ];
                    showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      builder: (context) => ListView.builder(
                        shrinkWrap: true,
                        itemCount: games.length,
                        itemBuilder: (context, index) => ListTile(
                          title: Row(
                            children: <Widget>[
                              Icon(Icons.games),
                              Text(titles[index]),
                            ],
                          ),
                          onTap: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (context) => games[index])),
                        ),
                      ),
                    );
                  },
                  child: Image.asset('assets/header/metrocard_header.webp'),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    devButton(),
                    devWidgetToggle(),
                    settingsButton(),
                  ],
                ),
                if (user != null)
                  Column(
                    children: [
                      Text(user.displayName),
                      Text(user.email),
                      Text(user.uid),
                    ],
                  ),
                if (dynamicAsset != null)
                  Container(
                    padding: EdgeInsets.only(bottom: 80),
                    child: Image.asset(
                      dynamicAsset,
                      height: MediaQuery.of(context).size.height / 3,
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton.extended(
            heroTag: 'fabExtendedCharter',
            onPressed: () => charterModeToggle(),
            label: Text('Fretamento'),
            icon: Icon(
              Icons.business_center,
              size: 30,
            ),
          ),
          FloatingActionButton.extended(
            heroTag: 'fabExtendedUrban',
            onPressed: () => go(false),
            label: Text('Urbano'),
            icon: Icon(
              Icons.location_city,
              size: 30,
            ),
          ),
        ],
      ),
    );
  }
}
