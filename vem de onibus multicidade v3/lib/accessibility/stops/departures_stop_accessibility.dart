import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobilibus/base/MOBDeparture.dart';
import 'package:mobilibus/utils/MOBApiUtils.dart';

class DeparturesAccessibility extends StatefulWidget {
  DeparturesAccessibility(this.projectId, this.stopId, this.name);
  final int projectId;
  final int stopId;
  final String name;

  _DeparturesAccessibility createState() =>
      _DeparturesAccessibility(projectId, stopId, name);
}

class _DeparturesAccessibility extends State<DeparturesAccessibility> {
  _DeparturesAccessibility(this.projectId, this.stopId, this.name) : super();
  final int projectId;
  final int stopId;
  final String name;

  List<MOBDepartureTrip> departuresOn = [];
  List<MOBDepartureTrip> departuresOff = [];

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(), () async {
      http.Response response = await MOBApiUtils.getDepartures(stopId);
      if (response != null) {
        String body = convert.utf8.decode(response.bodyBytes);
        Map<String, dynamic> object = convert.json.decode(body);

        MOBDepartures departureResultOn = MOBDepartures.fromJson(object);
        MOBDepartures departureResultOff = MOBDepartures.fromJson(object);

        setupDepartures(departureResultOn.departuresToday,
            departureResultOff.departuresToday);
      }
    });
  }

  void setupDepartures(
      List<MOBDepartureTrip> depsOn, List<MOBDepartureTrip> depsOff) {
    depsOn.forEach((dep) {
      dep.departures.removeWhere((line) => !line.online);
      if (dep.departures.length > 1)
        dep.departures.removeRange(1, dep.departures.length);
    });
    depsOn.removeWhere((dep) => dep.departures.isEmpty);

    depsOff.forEach((dep) {
      dep.departures.removeWhere((line) => line.online);
      if (dep.departures.length > 1)
        dep.departures.removeRange(1, dep.departures.length);
    });
    depsOff.removeWhere((dep) => dep.departures.isEmpty);

    //order reach first
    depsOn.sort((a, b) {
      int first = a.departures[0].minutes;
      int second = b.departures[0].minutes;
      return first.compareTo(second);
    });

    //order reach first
    depsOff.sort((a, b) {
      int first = a.departures[0].minutes;
      int second = b.departures[0].minutes;
      return first.compareTo(second);
    });

    departuresOn = depsOn;
    departuresOff = depsOff;

    setState(() {});
  }

  Widget list(List<MOBDepartureTrip> departures) => ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: departures.length,
        itemBuilder: (context, index) {
          MOBDepartureTrip departureLine = departures[index];

          String shortName = departureLine.shortName;
          String longName = departureLine.longName;

          String headsign = departureLine.headsign;

          MOBDeparture lineDeparture = departureLine.departures[0];

          int minutes = lineDeparture.minutes;
          bool online = lineDeparture.online;
          String prefix = lineDeparture.vehicleId;

          String vehicle = online ? 'Ônibus $prefix, ' : '';
          String minuteStr;
          if (minutes < 2)
            minuteStr = 'Chegando ao ponto';
          else
            minuteStr = 'em $minutes minutos';

          String desc = '$vehicle$minuteStr, com destino a $headsign';

          return ListTile(
            title: Text('Linha: $shortName - $longName'),
            subtitle: Text(desc),
            isThreeLine: true,
            dense: true,
            enabled: true,
          );
        },
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Pontos de Ônibus próximos\n$name',
          style: TextStyle(fontSize: 12),
        ),
      ),
      body: departuresOn.isEmpty && departuresOff.isEmpty
          ? Text('Partidas não disponíveis, volte e tente novamente mais tarde')
          : ListView(
              children: <Widget>[list(departuresOn), list(departuresOff)],
            ),
    );
  }
}
