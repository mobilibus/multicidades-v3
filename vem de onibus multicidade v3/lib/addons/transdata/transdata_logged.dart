import 'package:flutter/material.dart';
import 'package:mobilibus/addons/transdata/pages/transdata_history.dart';
import 'package:mobilibus/addons/transdata/pages/transdata_home.dart';
import 'package:mobilibus/addons/transdata/pages/transdata_products.dart';
import 'package:mobilibus/base/transdata/TransdataAPIs.dart';
import 'package:mobilibus/base/transdata/TransdataUser.dart';

class TransdataLogged extends StatefulWidget {
  TransdataLogged(this.api, this.user);
  final TransdataAPIs api;
  final TransdataUser user;
  _TransdataLogged createState() => _TransdataLogged(api, user);
}

class _TransdataLogged extends State<TransdataLogged> with TickerProviderStateMixin {
  _TransdataLogged(this.api, this.user);
  final TransdataAPIs api;
  final TransdataUser user;

  TabController tabController;
  int pageIndex = 0;

  @override
  void initState() {
    super.initState();
    tabController = TabController(initialIndex: 0, length: 3, vsync: this);
  }

  void tabBottomBar(int index) {
    pageIndex = index;
    tabController.animateTo(index);

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    List<BottomNavigationBarItem> bottomBars = [
      BottomNavigationBarItem(
        icon: Icon(Icons.home),
        label: 'Início',
        backgroundColor: Theme.of(context).primaryColor,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.shopping_bag),
        label: 'Produtos',
        backgroundColor: Theme.of(context).primaryColor,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.horizontal_split),
        label: 'Histórico',
        backgroundColor: Theme.of(context).primaryColor,
      ),
    ];
    return Scaffold(
      appBar: AppBar(title: Text(user.name)),
      body: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: tabController,
        children: [
          TransdataHomePage(api, user),
          TransdataProducts(api, user),
          TransdataHistoryPage(api, user),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        showUnselectedLabels: true,
        unselectedItemColor: Colors.grey,
        currentIndex: pageIndex,
        onTap: tabBottomBar,
        backgroundColor: Theme.of(context).primaryColor,
        selectedItemColor: Theme.of(context).accentColor,
        items: bottomBars,
      ),
    );
  }
}
