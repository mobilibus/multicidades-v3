import 'package:flutter/material.dart';
import 'package:mobilibus/base/transdata/TransdataAPIs.dart';
import 'package:mobilibus/base/transdata/TransdataProductDetail.dart';
import 'package:mobilibus/base/transdata/TransdataUser.dart';

class ProductDetailService extends StatefulWidget {
  ProductDetailService(this.id, this.user, this.api);
  final String id;
  final TransdataUser user;
  final TransdataAPIs api;

  _ProductDetailService createState() => _ProductDetailService(id, user, api);
}

class _ProductDetailService extends State<ProductDetailService> {
  _ProductDetailService(this.id, this.user, this.api);
  final String id;
  final TransdataUser user;
  final TransdataAPIs api;

  TransdataProductDetail productDetail;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 1), getProductDetails);
  }

  void getProductDetails() async {
    productDetail = await api.getHistoryDetail(id);
    setState(() {});
  }

  Widget tile(String title, String content) => ListTile(
        title: Text(title),
        subtitle: Text(content),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Detalhes')),
      body: productDetail == null
          ? Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              child: Column(
                children: [
                  tile('Local de Compra', productDetail.externalBuyerId),
                  tile('Descrição', productDetail.description),
                  tile(
                      'Taxa',
                      'R\$ ' +
                          (productDetail.taxInCents / 100)
                              .toStringAsFixed(2)
                              .replaceAll('.', ',')),
                  tile('Versão API', productDetail.versionAppName),
                ],
              ),
            ),
    );
  }
}
