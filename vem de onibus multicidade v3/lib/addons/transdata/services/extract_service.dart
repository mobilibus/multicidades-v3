import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobilibus/base/transdata/TransdataAPIs.dart';
import 'package:mobilibus/base/transdata/TransdataExtract.dart';
import 'package:mobilibus/base/transdata/TransdataUser.dart';

class ExtractService extends StatefulWidget {
  ExtractService(this.api, this.user, this.transdataExtract);
  final TransdataAPIs api;
  final TransdataUser user;
  final TransdataExtract transdataExtract;

  _ExtractService createState() => _ExtractService(api, user, transdataExtract);
}

class _ExtractService extends State<ExtractService> {
  _ExtractService(this.api, this.user, this.transdataExtract);
  final TransdataAPIs api;
  final TransdataUser user;
  final TransdataExtract transdataExtract;

  @override
  Widget build(BuildContext context) {
    int cents = transdataExtract.currentBalanceInCents;
    String real = (cents / 100).toStringAsFixed(2);
    DateFormat df = DateFormat('dd/MM/yyyy - HH:mm:ss');
    return Scaffold(
      appBar: AppBar(
        title: Text('Extrato'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Text('Saldo atual: R\$: $real'),
            transdataExtract.items.isEmpty
                ? Center(
                    child:
                        Text('Nenhum item encontrado na faixa especificada.'))
                : ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: transdataExtract.items.length,
                    itemBuilder: (context, index) {
                      TransdataExtractItem item = transdataExtract.items[index];

                      String date = df.format(item.date);
                      String description = item.description;
                      String real = (item.valueInCents / 100)
                          .toStringAsFixed(2)
                          .replaceAll('.', ',');

                      return ListTile(
                        title: Text('R\$ $real - $description'),
                        subtitle: Text(date),
                      );
                    },
                  ),
          ],
        ),
      ),
    );
  }
}
