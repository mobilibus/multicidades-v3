import 'package:flutter/material.dart';
import 'introduction_screen/indicator_intro/dots_decorator.dart';
import 'introduction_screen/intro_screen/introduction_screen.dart';
import 'introduction_screen/intro_screen/model/page_view_model.dart';
import 'package:mobilibus/main.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CharterIntro extends StatefulWidget {
  _CharterIntro createState() => _CharterIntro();
}

class _CharterIntro extends State<CharterIntro> {
  bool intro = true;
  String appName = '';
  String introKey = 'charter_intro';
  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((prefs) async {
      intro = prefs.containsKey(introKey) ? prefs.getBool(introKey) : false;
      if (intro)
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => Splash()));

      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      appName = packageInfo.appName;
      setState(() {});
    });
  }

  void done() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(introKey, true);
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => Splash()));
  }

  @override
  Widget build(BuildContext context) {
    TextStyle style = TextStyle(
      color: Utils.getColorFromPrimaryReverse(context),
      fontSize: 18,
    );
    List<PageViewModel> pages = [
      PageViewModel(
        title: '',
        bodyWidget: Text(
          'No $appName você confere as informações que precisa para utilizar seu '
          'transporte fretado com segurança e pontualidade.',
          style: style,
          textAlign: TextAlign.center,
        ),
        image: Image.asset('assets/intro/charter_intro1.png'),
      ),
      PageViewModel(
        title: '',
        bodyWidget: Text(
          'Quando seu veículo estiver a caminho, você vê no mapa onde ele está, '
          'em quanto tempo vai chegar, e onde você deve embarcar nele.',
          style: style,
          textAlign: TextAlign.center,
        ),
        image: Image.asset('assets/intro/charter_intro2.png'),
      ),
      PageViewModel(
        title: '',
        bodyWidget: Column(
          children: <Widget>[
            Text(
              'Na Central do Cliente você fica por dentro das novidades de seu transporte, e entra em contato para sugestões e reclamações.',
              style: style,
              textAlign: TextAlign.center,
            ),
            Container(height: 30),
            Text(
              'Desejamos uma boa viagem!',
              style: TextStyle(
                  fontSize: 30,
                  color: Utils.getColorFromPrimaryReverse(context)),
              textAlign: TextAlign.center,
            ),
          ],
        ),
        image: Image.asset('assets/intro/charter_intro3.png'),
      ),
    ];

    TextStyle textStyle =
        TextStyle(color: Utils.getColorFromPrimaryReverse(context));

    IntroductionScreen introScreen = IntroductionScreen(
      pages: pages,
      initialPage: 0,
      showNextButton: true,
      showSkipButton: true,
      isProgress: true,
      dotsDecorator: DotsDecorator(
        activeSize: Size.square(4.0),
        size: Size.square(4.0),
      ),
      globalBackgroundColor: Utils.getColorFromPrimary(context),
      next: Text('Próximo', style: textStyle),
      done: Text('Finalizar', style: textStyle),
      skip: Text('Pular', style: textStyle),
      onSkip: done,
      onDone: done,
    );

    return intro
        ? Scaffold(
            resizeToAvoidBottomInset: false,
            //resizeToAvoidBottomPadding: false,
            backgroundColor: Colors.white,
            body: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                alignment: Alignment.bottomRight,
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Image.asset(
                      'assets/header/header.png',
                      width: MediaQuery.of(context).size.width / 1.5,
                    ),
                  ),
                ],
              ),
              decoration: BoxDecoration(
                  image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(''),
              )),
            ),
          )
        : Container(
            color: Utils.getColorFromPrimary(context),
            child: introScreen,
          );
  }
}
