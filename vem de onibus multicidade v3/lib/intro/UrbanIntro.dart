import 'dart:io';

import 'package:flutter/material.dart';
import 'introduction_screen/indicator_intro/dots_decorator.dart';
import 'introduction_screen/intro_screen/introduction_screen.dart';
import 'introduction_screen/intro_screen/model/page_view_model.dart';
import 'package:flutter_native_config/flutter_native_config.dart';
import 'package:mobilibus/main.dart';
import 'package:mobilibus/utils/Utils.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:location/location.dart';

class PermissionLocationIntro extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Utils.globalNameProject,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: [
          GoogleFonts.roboto().fontFamily,
          GoogleFonts.openSans().fontFamily,
          GoogleFonts.catamaran().fontFamily,
        ][1],
        primaryColor: Utils.globalPrimaryColor,
        brightness: Brightness.light, 
        colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Utils.globalAccentColor),
      ),
      home: MyHomePermissionLt(),
    );
  }
}

class MyHomePermissionLt extends StatefulWidget {
  @override
  _MyHomePermissionLt createState() => _MyHomePermissionLt();
}

class _MyHomePermissionLt extends State<MyHomePermissionLt> {
  bool intro = true;
  String appName = '';
  String introKey = "urban_intro"; //'urban_intro';

  void navigatorPermissionGo() {
    setState(() {
      Location()
          .onLocationChanged
          .listen((locationData) => Utils.locationData = locationData);

      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => Splash()));
    });
  }

  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((prefs) async {
      if (Platform.isIOS) {
        appName = await FlutterNativeConfig.getConfig(
          ios: 'CFBundleName',
          android: 'version_name',
        );
      } else {
        PackageInfo packageInfo = await PackageInfo.fromPlatform();
        appName = packageInfo.appName;
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    List<PageViewModel> pages = [
      PageViewModel(
        title: '',
        bodyWidget: Column(
          children: <Widget>[
            Text(
              'Uso de sua localização',
              style: TextStyle(
                  fontFamily: GoogleFonts.openSans().fontFamily,
                  fontSize: 30,
                  color: Utils.getColorFromPrimaryReverse(context)),
              textAlign: TextAlign.center,
            ),
            Container(height: 30),
            Text(
              'O $appName utiliza dados de localização, inclusive em segundo plano, para informar em ' +
                  'tempo real o cálculo de chegada de seu transporte até o ponto de parada em que você se encontra.',
              style: TextStyle(
                  fontFamily: GoogleFonts.openSans().fontFamily,
                  fontSize: 18,
                  color: Utils.getColorFromPrimaryReverse(context)),
              textAlign: TextAlign.center,
            ),
          ],
        ),
        image: Image.asset('assets/intro/consent.webp'),
      ),
    ];

    TextStyle textStyle = TextStyle(color: Colors.white, fontSize: 12);

    IntroductionScreen introScreen = IntroductionScreen(
      pages: pages,
      initialPage: 0,
      showNextButton: true,
      showSkipButton: true,
      isProgress: false,
      dotsDecorator: DotsDecorator(size: Size.square(4.0)),
      globalBackgroundColor: Utils.globalPrimaryColor,
      next: Text('-', style: textStyle),
      done: Text('Ok', style: textStyle),
      skip: Text('-', style: textStyle),
      onSkip: navigatorPermissionGo,
      onDone: navigatorPermissionGo,
    );

    return Container(
      color: Utils.getColorFromPrimary(context),
      child: introScreen,
    );
  }
}

class UrbanIntro extends StatefulWidget {
  _UrbanIntro createState() => _UrbanIntro();
}

class _UrbanIntro extends State<UrbanIntro> {
  bool intro = true;
  String appName = '';
  String introKey = 'urban_intro';
  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((prefs) async {
      intro = prefs.containsKey(introKey) ? prefs.getBool(introKey) : false;
      if (intro) {
        Location()
            .onLocationChanged
            .listen((locationData) => Utils.locationData = locationData);

        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => Splash()));
      }
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      appName = packageInfo.appName;
      setState(() {});
    });
  }

  void done() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(introKey, true);

    runApp(PermissionLocationIntro());
  }

  @override
  Widget build(BuildContext context) {
    TextStyle style = TextStyle(
      color: Colors.white,
      fontSize: 18,
    );
    List<PageViewModel> pages = [
      PageViewModel(
        title: '',
        bodyWidget: Text(
          'Na página inicial do $appName você confere os pontos de parada mais próximos, '
          'pode planejar seu deslocamento e ainda localizar pontos de vendas de créditos.',
          style: style,
          textAlign: TextAlign.center,
        ),
        image: Image.asset('assets/intro/splash1.png'),
      ),
      PageViewModel(
        title: '',
        bodyWidget: Text(
          'Em "Horários" você confere os locais de saída, visualiza onde estão os ônibus de sua '
          'linha e pode compartilhar com outras pessoas onde está seu ônibus.',
          style: style,
          textAlign: TextAlign.center,
        ),
        image: Image.asset('assets/intro/splash2.png'),
      ),
      PageViewModel(
        title: '',
        bodyWidget: Text(
          'Em "Favoritos" você encontra as linhas e pontos de parada que mais utiliza e também os '
          'deslocamentos que você planejou anteriormente.',
          style: style,
          textAlign: TextAlign.center,
        ),
        image: Image.asset('assets/intro/splash3.png'),
      ),
      PageViewModel(
        title: '',
        bodyWidget: Text(
          'No menu "Mais", confira alertas e notícias sobre o sistema de transporte, '
          'a relação dos pontos de venda de créditos e também aprender a utilizar os recursos de acessibilidade.',
          style: style,
          textAlign: TextAlign.center,
        ),
        image: Image.asset('assets/intro/splash4.png'),
      ),
      PageViewModel(
        title: '',
        bodyWidget: Column(
          children: <Widget>[
            Text(
              'Compartilhe o $appName com amigos familiares e colegas.',
              style: style,
              textAlign: TextAlign.center,
            ),
            Container(height: 30),
            Text(
              'Boa Viagem!',
              style: TextStyle(
                  fontSize: 30,
                  color: Utils.getColorFromPrimaryReverse(context)),
              textAlign: TextAlign.center,
            ),
          ],
        ),
        image: Image.asset('assets/intro/splash5.png'),
      ),
    ];

    TextStyle textStyle = TextStyle(color: Colors.white, fontSize: 12);

    IntroductionScreen introScreen = IntroductionScreen(
      pages: pages,
      initialPage: 0,
      showNextButton: true,
      showSkipButton: true,
      isProgress: true,
      dotsDecorator: DotsDecorator(
        activeColor: Utils.globalAccentColor,
        activeSize: Size.square(4.0),
        size: Size.square(3.0),
      ),
      globalBackgroundColor: Utils.globalPrimaryColor,
      next: Text('Próximo', style: textStyle),
      done: Text('Finalizar', style: textStyle),
      skip: Text('Pular', style: textStyle),
      onSkip: done,
      onDone: done,
    );

    return intro ? 
    Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      //body: Image.asset(strSplash),
    )
  : Container(
      color: Utils.getColorFromPrimary(context),
      child: introScreen,
    );
  }
}
