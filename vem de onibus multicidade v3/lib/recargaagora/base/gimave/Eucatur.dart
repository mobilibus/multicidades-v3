class Eucatur {
  final String uuid;
  final String creditCardToken;
  final String maskedCardNumber;

  Eucatur(this.uuid, this.creditCardToken, this.maskedCardNumber);

  factory Eucatur.fromJson(Map<String, dynamic> json) {
    String uuid = json['uuid'] ?? '';
    String creditCardToken = json['credit_card_token'] ?? '';
    String maskedCardNumber = json['credit_card']['number'] ?? '';

    return Eucatur(uuid, creditCardToken, maskedCardNumber);
  }
}
