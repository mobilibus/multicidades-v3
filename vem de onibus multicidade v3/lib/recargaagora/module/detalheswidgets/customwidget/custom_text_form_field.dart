import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextFormField extends StatelessWidget {
  CustomTextFormField(this.formatter, this.hint, this.inputType,
      this.controller, this.validate, this.validateErrorMessage, this.formKey,
      {this.width, this.onChanged, this.textCapitalization, Key key})
      : super(key: key);
  final TextInputFormatter formatter;
  final String hint;
  final TextInputType inputType;
  final TextEditingController controller;
  final bool validate;
  final String validateErrorMessage;
  final GlobalKey<FormState> formKey;
  final double width;
  final Function(String) onChanged;
  final TextCapitalization textCapitalization;

  @override
  Widget build(BuildContext context) => Container(
        width: width,
        child: Container(
          padding: EdgeInsets.all(10),
          child: TextFormField(
            textCapitalization: textCapitalization != null
                ? textCapitalization
                : TextCapitalization.words,
            inputFormatters: [
              formatter,
              if (inputType == TextInputType.number)
                WhitelistingTextInputFormatter.digitsOnly
            ],
            controller: controller,
            textInputAction: TextInputAction.done,
            keyboardType: inputType,
            decoration: InputDecoration(
                hintText: hint, labelText: hint, border: OutlineInputBorder()),
            onChanged: onChanged,
            validator: (text) => validate == null
                ? null
                : text.isEmpty
                    ? 'Campo obrigatório'
                    : validate ? null : validateErrorMessage,
          ),
        ),
      );
}
